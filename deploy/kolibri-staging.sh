#!/bin/sh
cp env.ts env.ts.backup
cp deploy/env-kolibri-staging.ts env.ts
npm run build
scp -pr node_modules/@xbs/webix-pro/webix.min.* kolibri-prod:/var/www/WebixPortalStaging/
scp -pr node_modules/@xbs/webix-pro/fonts kolibri-prod:/var/www/WebixPortalStaging/
scp deploy/index-kolibri-staging.html kolibri-prod:/var/www/WebixPortalStaging/index.html
scp -pr codebase kolibri-prod:/var/www/WebixPortalStaging/
scp -pr assets kolibri-prod:/var/www/WebixPortalStaging/
cp env.ts.backup env.ts