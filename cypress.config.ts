import { defineConfig } from 'cypress'

export default defineConfig({
  viewportHeight: 900,
  viewportWidth: 1400,
  projectId: 'kbq3kc',
  e2e: {
    // We've imported your old cypress plugins here.
    // You may want to clean this up later by importing these.
    setupNodeEvents(on, config) {
      return require('./cypress/plugins/index.js')(on, config)
    },
    baseUrl: 'http://localhost:8093/#!/',
    excludeSpecPattern: '**/examples/*',
    specPattern: 'cypress/e2e/**/*.{js,jsx,ts,tsx}',
  },
})
