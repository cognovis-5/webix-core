let wallabyWebpack = require("wallaby-webpack");
let options = require("./webpack.config");
let wallabyPostprocessor = wallabyWebpack(options);

module.exports = wallaby => {
    return {
        files: [

            { pattern: "sources/**/**/*.spec.ts", load:false, ignore:true},
            { pattern: "node_modules/*.ts", load:false},
            { pattern: "sources/*.ts", load:false},
            { pattern: "sources/**/*.ts", load:false},
            { pattern: "sources/**/**/*.ts", load:false},
            "node_modules/@xbs/webix-pro/webix.js",
            "node_modules/reflect-metadata/Reflect.js",
             "sources/helpers/cognovis-ajax/cognovis-ajax.ts",
             "sources/env.ts",

        ],
        tests: [
            { pattern: "sources/**/**/*.spec.ts", load:false},
            { pattern: "sources/*.spec.ts", load:false},
        ],

        postprocessor: wallabyPostprocessor,
        setup(setup) {
            this.__moduleBundler.loadTests();
        },
        debug: true
    };
  };
