/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICategoryObject } from './ICategoryObject';
import type { INamedId } from './INamedId';

export type IObjectSkill = {
    /**
     * Object we attach the skill to
     */
    object?: INamedId;
    /**
     * Skill which is required
     */
    skill?: INamedId;
    /**
     * Skill type of the required skill
     */
    skill_type?: ICategoryObject;
    /**
     * Level of experience claimed
     */
    claimed_experience?: ICategoryObject;
    /**
     * Level of experience confirmed
     */
    confirmed_experience?: ICategoryObject;
    /**
     * Level of experience required
     */
    required_experience?: ICategoryObject;
    /**
     * How important is this skill for this object in relation to others
     */
    skill_weight?: number;
    /**
     * Is the skill required for this object
     */
    skill_required_p?: boolean;
};

