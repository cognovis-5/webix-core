/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { BooleanT } from './BooleanT';

export type IInvoiceTimesheetTasksPut = {
    /**
     * Hours we want to add to the invoice
     */
    hour_log_ids?: Array<number>;
    /**
     * Only add hours which are not yet assigned to an invoice
     */
    only_unassigned?: BooleanT;
};

