/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetTranslationFileType } from './IntranetTranslationFileType';

export type IntranetTranslationFileTypeId = IntranetTranslationFileType;
