/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum IntranetCostCenterStatus {
    ACTIVE = 3101,
    INACTIVE = 3102,
}
