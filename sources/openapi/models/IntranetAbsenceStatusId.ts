/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetAbsenceStatus } from './IntranetAbsenceStatus';

export type IntranetAbsenceStatusId = IntranetAbsenceStatus;
