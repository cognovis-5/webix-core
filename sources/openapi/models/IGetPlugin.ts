/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IGetPlugin = {
    /**
     * ID of the plugin
     */
    plugin_id?: number;
    /**
     * Title shown for the plugin
     */
    plugin_title?: string;
    /**
     * Name of the plugin. Same as Title if the title does not contain a title_tcl
     */
    plugin_name?: string;
    /**
     * HTML of the plugin with URL replacements being done
     */
    plugin_html?: string;
    /**
     * Number of the plugin on the location (Y-Position)
     */
    sort_order?: number;
    /**
     * Location, typically one of left/right, but might also be top/buttom
     */
    location?: string;
};

