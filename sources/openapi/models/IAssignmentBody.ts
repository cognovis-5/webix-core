/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetFreelanceAssignmentStatus } from './IntranetFreelanceAssignmentStatus';
import type { IntranetUom } from './IntranetUom';

export type IAssignmentBody = {
    /**
     * Rate which we are using for the assignment(s). If empty, get the best rate of the freelancer(s)
     */
    rate?: number;
    /**
     * Number of units for this package (assignment). If empty, use the units from the tasks in the package
     */
    assignment_units?: number;
    /**
     * Unit of measure for the assignment. If empty, use the latest uom_id from the tasks (we should not have mixed uom_ids in packages)
     */
    uom_id?: IntranetUom;
    /**
     * When is the assignment due.
     */
    assignment_deadline?: string;
    /**
     * Time when the assignment will be ready to start working on.
     */
    start_date?: string;
    /**
     * Status of the assignment
     */
    assignment_status_id?: IntranetFreelanceAssignmentStatus;
};

