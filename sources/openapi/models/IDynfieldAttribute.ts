/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IDynfieldAttribute = {
    /**
     * ID of the attribute
     */
    attribute_name?: string;
    /**
     * Display name
     */
    pretty_name?: string;
    /**
     * Can you filter by this attribute
     */
    filter?: boolean;
    /**
     * Are you allowed to edit this attribute
     */
    edit?: boolean;
    /**
     * number / text / ....
     */
    type?: string;
    /**
     * object type of the referencing object (e.g. companies for company_id in projects)
     */
    ref?: string;
};

