/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICategoryObject } from './ICategoryObject';
import type { INamedId } from './INamedId';
import type { IUser } from './IUser';

export type IBizObjectMember = {
    /**
     * Relation information
     */
    rel?: INamedId;
    /**
     * Object that the user is a member of
     */
    object?: INamedId;
    /**
     * User who is a member of the object
     */
    member?: IUser;
    /**
     * Role the user has in relation to the object
     */
    role?: ICategoryObject;
    /**
     * How much of their time are they in this role
     */
    percentage?: number;
};

