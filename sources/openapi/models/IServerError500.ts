/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IServerError500 = {
    success?: boolean;
    /**
     * Error message from the server
     */
    message?: string;
};

