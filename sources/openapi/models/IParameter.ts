/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IParameter = {
    /**
     * package_id of returned package
     */
    package_id?: number;
    /**
     * package_key of returned package
     */
    package_key?: string;
    /**
     * parameter_id of returned package parameter
     */
    parameter_id?: number;
    /**
     * parameter_name of returned package parameter
     */
    parameter_name?: string;
    /**
     * value of package parameter
     */
    value?: string;
    /**
     * default_value of package parameter - Returns a json string even if the default value might be something else...
     */
    default_value?: string;
};

