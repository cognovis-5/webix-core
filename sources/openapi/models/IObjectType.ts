/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IObjectType = {
    /**
     * object type. Should be snake cased.
     */
    object_type?: string;
    /**
     * Pretty name for the object type. Might contain spaces
     */
    pretty_name?: string;
    /**
     * Primary table for the object type. This is where the data relating to the object is stored
     */
    table_name?: string;
    /**
     * What is the primary attribute_name (or id_column) which can be joined against acs_objects.object_id (corresponds to the object_id)
     */
    id_column?: string;
    /**
     * Which column in the table contains the status(_id) of the object
     */
    status_column?: string;
    /**
     * Which column (attribute_name) contains the subtype for this object type
     */
    type_column?: string;
    /**
     * What is the category type to use for the type_column values (e.g. 'Intranet Project Type' for 'im_project')
     */
    type_category_type?: string;
    /**
     * What is the category type to use for the status values.
     */
    status_category_type?: string;
};

