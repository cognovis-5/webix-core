/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { INamedId } from './INamedId';

export type IDynfieldLayoutAttribute = {
    /**
     * Attribute name and id
     */
    attribute?: INamedId;
    /**
     * id from the acs_attributes table
     */
    attribute_id?: number;
    /**
     * Pretty name of the attribute
     */
    pretty_name?: string;
    /**
     * X position of the layout
     */
    pos_x?: number;
    /**
     * Y position of the layout
     */
    pos_y?: number;
    /**
     * Width of the layout element
     */
    size_x?: number;
    /**
     * Height of the layout element
     */
    size_y?: number;
    /**
     * Style of the label
     */
    label_style?: string;
    /**
     * Class for the div element
     */
    div_class?: string;
};

