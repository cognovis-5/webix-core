/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICategoryArray } from './ICategoryArray';
import type { ICategoryObject } from './ICategoryObject';

export type ITransProjectFilter = {
    /**
     * List of source_language_ids which are valid
     */
    source_language?: ICategoryArray;
    /**
     * List of target languages of this project (default filtered)
     */
    target_language?: ICategoryArray;
    /**
     * List of valid target languages to choose from
     */
    valid_target_language?: ICategoryArray;
    /**
     * Subject Areas for this project
     */
    subject_area?: ICategoryObject;
    /**
     * List of Business Sectors (in addition to subject_area) the freelancer has experience.
     */
    skill_business_sector?: ICategoryArray;
    /**
     * Kolibri specific List of Roles the freelancer has experience in.
     */
    skill_role?: ICategoryArray;
};

