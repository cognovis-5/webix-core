/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ICategory = {
    /**
     * ID of the category
     */
    category_id?: number;
    /**
     * Name of the category
     */
    category?: string;
    /**
     * translated string in the locale of the current rest_user_id
     */
    category_translated?: string;
    /**
     * Description for the category
     */
    category_description?: string;
    /**
     * Type of the category, especially important for so called object_type category types
     */
    category_type?: string;
    /**
     * Is the category enabled and ready to use
     */
    enabled_p?: boolean;
    /**
     * aux_int1
     */
    aux_int1?: number;
    /**
     * aux_int2
     */
    aux_int2?: number;
    /**
     * parent_only_p
     */
    parent_only_p?: boolean;
    /**
     * aux_string1
     */
    aux_string1?: string;
    /**
     * aux_string2
     */
    aux_string2?: string;
    /**
     * aux_html1
     */
    aux_html1?: string;
    /**
     * aux_html2
     */
    aux_html2?: string;
    /**
     * Order in which to display the category
     */
    sort_order?: number;
};

