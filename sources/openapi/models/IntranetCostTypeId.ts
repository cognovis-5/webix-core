/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetCostType } from './IntranetCostType';

export type IntranetCostTypeId = IntranetCostType;
