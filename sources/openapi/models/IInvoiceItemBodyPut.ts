/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntegerInt32 } from './IntegerInt32';
import type { IntranetTranslationLanguage } from './IntranetTranslationLanguage';

export type IInvoiceItemBodyPut = {
    /**
     * line item name displayed on invoice
     */
    item_name?: string;
    /**
     * Intranet UoM line item unit of measure id
     */
    item_uom_id?: string;
    /**
     * line item number of units
     */
    item_units?: number;
    /**
     * line item price per unit
     */
    price_per_unit?: number;
    /**
     * position displayed in invoice
     */
    sort_order?: IntegerInt32;
    /**
     * description of line item
     */
    description?: string;
    /**
     * Source Language of the Material
     */
    source_language_id?: IntranetTranslationLanguage;
    /**
     * Target Language of the Material
     */
    target_language_id?: IntranetTranslationLanguage;
    /**
     * Material Nr. of the material we are trying to create the invoice_item for
     */
    material_nr?: string;
    /**
     * Material ID of the material we are trying to create the invoice_item for
     */
    item_material_id?: IntegerInt32;
    /**
     * Assignment or other context for this item
     */
    context_id?: IntegerInt32;
};

