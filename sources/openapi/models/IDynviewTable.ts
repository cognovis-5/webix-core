/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IDynviewColumn } from './IDynviewColumn';

export type IDynviewTable = {
    /**
     * id of the im_view
     */
    view_id?: number;
    /**
     * name of the im_view
     */
    view_name?: string;
    /**
     * label for the dynview
     */
    view_label?: string;
    /**
     * json config for the whole im_view
     */
    json_config?: string;
    /**
     * Array of dynview rows together with fields
     */
    dynview_columns?: Array<IDynviewColumn>;
    /**
     * order of the im_view
     */
    sort_order?: number;
};

