/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IReportModule = {
    /**
     * ID of the report
     */
    report_module_id?: number;
    /**
     * Configuration of the report
     */
    config?: string;
    /**
     * Name of the Report
     */
    report_name?: string;
    /**
     * When was it last updated
     */
    updated?: string;
};

