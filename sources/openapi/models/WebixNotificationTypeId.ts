/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { WebixNotificationType } from './WebixNotificationType';

export type WebixNotificationTypeId = WebixNotificationType;
