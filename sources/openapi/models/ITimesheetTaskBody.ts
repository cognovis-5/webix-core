/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntegerInt32 } from './IntegerInt32';
import type { IntranetGanttTaskStatus } from './IntranetGanttTaskStatus';
import type { IntranetGanttTaskType } from './IntranetGanttTaskType';
import type { IntranetUom } from './IntranetUom';
import type { UserId } from './UserId';

export type ITimesheetTaskBody = {
    /**
     * Name of the task
     */
    task_name: string;
    /**
     * Information about the person who added the note
     */
    task_assignee_id?: UserId;
    /**
     * Status_id of the task
     */
    task_status_id?: IntranetGanttTaskStatus;
    /**
     * Task_id of the task
     */
    task_type_id?: IntranetGanttTaskType;
    /**
     * How much of the task is already finished. Does not have to relate to hours
     */
    percent_completed?: number;
    /**
     * How many units are planned for this task
     */
    planned_units?: number;
    /**
     * How many units can you charge the customer for
     */
    billable_units?: number;
    /**
     * When is the task starting
     */
    start_date?: string;
    /**
     * Date for the deadline
     */
    end_date?: string;
    /**
     * Material we want to create the task for / with
     */
    material_id?: IntegerInt32;
    /**
     * What unit of measure do we have for the timesheet task.
     */
    uom_id?: IntranetUom;
};

