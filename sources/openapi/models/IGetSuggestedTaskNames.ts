/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { INamedId } from './INamedId';

export type IGetSuggestedTaskNames = {
    /**
     * task name
     */
    task?: INamedId;
};

