/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetPriceComplexity } from './IntranetPriceComplexity';

export type IntranetPriceComplexityId = IntranetPriceComplexity;
