/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICategoryObject } from './ICategoryObject';

export type IFreelancerLanguage = {
    /**
     * Target language for which this freelancer can work on
     */
    language?: ICategoryObject;
    /**
     * Type of language (source or target)
     */
    type?: string;
};

