/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Boolean0 } from './Boolean0';
import type { ImCostId } from './ImCostId';
import type { ImTransTaskIds } from './ImTransTaskIds';

export type ITransQuote = {
    /**
     * Task Id for which we want to create the tasks.
     */
    trans_tasks: ImTransTaskIds;
    /**
     * Should we create the quote per language? defaults to no.
     */
    quote_per_language_p?: Boolean0;
    /**
     * Quote into which we want to add the tasks
     */
    quote_id?: ImCostId;
};

