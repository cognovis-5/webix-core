/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICategoryObject } from './ICategoryObject';
import type { INamedId } from './INamedId';

export type IOffice = {
    /**
     * Office we are looking at
     */
    office?: INamedId;
    /**
     * Type of the office, defaults to main office
     */
    office_type?: ICategoryObject;
    /**
     * Status of the office, defaults to active
     */
    office_status?: ICategoryObject;
    /**
     * First address line
     */
    address_line1?: string;
    /**
     * Second address line (e.g. PO box or appartment)
     */
    address_line2?: string;
    /**
     * Postal / ZIP code
     */
    address_postal_code?: string;
    /**
     * City the company resides in
     */
    address_city?: string;
    /**
     * ISO for the country
     */
    address_country_code?: string;
    /**
     * Who is the contact person for the office
     */
    contact_person?: INamedId;
    /**
     * Note about the office
     */
    note?: string;
    /**
     * Phone number under which we can reach this office
     */
    phone?: string;
};

