/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICategoryObject } from './ICategoryObject';
import type { INamedId } from './INamedId';

export type ICompany = {
    /**
     * Company information (name and id)
     */
    company?: INamedId;
    /**
     * Internal company_path for identifiying the company
     */
    company_path?: string;
    /**
     * First address line
     */
    address_line1?: string;
    /**
     * Second address line (e.g. PO box or appartment)
     */
    address_line2?: string;
    /**
     * Postal / ZIP code
     */
    address_postal_code?: string;
    /**
     * City the company resides in
     */
    address_city?: string;
    /**
     * Internationalized name of the country
     */
    address_country_full?: string;
    /**
     * Primary contact for the company
     */
    primary_contact?: INamedId;
    /**
     * Accounting contact for the company
     */
    accounting_contact?: INamedId;
    /**
     * Status of company (Intranet Company Status)
     */
    company_status?: ICategoryObject;
    /**
     * Type of company (Intranet Company Type)
     */
    company_type?: ICategoryObject;
    /**
     * VAT Classification of the company
     */
    vat_type?: ICategoryObject;
    /**
     * company vat number
     */
    vat_number?: string;
    /**
     * phone number of the main office
     */
    phone?: string;
    /**
     * Website for the company
     */
    url?: string;
    /**
     * Where did this company come from
     */
    default_referral_source?: ICategoryObject;
    /**
     * Terms for the payment (aka how fast has the payment to be made)
     */
    payment_term?: ICategoryObject;
    /**
     * we use for new invoices for this customer
     */
    default_invoice_template?: ICategoryObject;
    /**
     * Template we use for new quotes for this customer
     */
    default_quote_template?: ICategoryObject;
    /**
     * When did we last contact the company
     */
    last_contact?: string;
    /**
     * URL to the portrait of the company
     */
    logo_url?: string;
    /**
     * Main office information
     */
    main_office?: INamedId;
    /**
     * Collmex account nr. for the company
     */
    collmex_id?: number;
};

