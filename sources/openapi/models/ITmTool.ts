/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IError } from './IError';

export type ITmTool = {
    /**
     * URL in which to open the tm_tool
     */
    url?: string;
    /**
     * Can we only download a file under this url (instead of going to different server)
     */
    download_only?: boolean;
    /**
     * Array of errors found (if any)
     */
    errors?: Array<IError>;
};

