/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IAssignmentRating } from './IAssignmentRating';
import type { ICategoryObject } from './ICategoryObject';
import type { ICognovisObject } from './ICognovisObject';
import type { ICrFile } from './ICrFile';
import type { INamedId } from './INamedId';

export type IAssignment = {
    /**
     * name and id for the assignment
     */
    assignment?: INamedId;
    /**
     * Name and ID of the assignee
     */
    assignee?: INamedId;
    /**
     * Package/Batch for which the assignment is used for.
     */
    freelance_package?: INamedId;
    /**
     * Task Type (like edit / proof / ..) for the assignment
     */
    assignment_type?: ICategoryObject;
    /**
     * Given the translation workflow, at which step does this assignment_type come in. Typically trans = 1, proof = 2
     */
    assignment_type_sort_order?: number;
    /**
     * Status of the assignment
     */
    assignment_status?: ICategoryObject;
    /**
     * Number of units for this assignment
     */
    assignment_units?: number;
    /**
     * Price per unit for the assignment
     */
    rate?: number;
    /**
     * Unit of measure
     */
    uom?: ICategoryObject;
    /**
     * Purchase Order for this assignment
     */
    purchase_order?: ICognovisObject;
    /**
     * Time when the assignment will be ready to start working on.
     */
    start_date?: string;
    /**
     * Deadline until which the assignment needs to be delivered
     */
    assignment_deadline?: string;
    /**
     * Comment provided with the rating
     */
    rating_comment?: string;
    /**
     * Average Rating for the assignment
     */
    rating_value?: number;
    /**
     * Detailed Rating information
     */
    rating?: Array<IAssignmentRating>;
    /**
     * Files provided by the PM for this assignment
     */
    package_files?: Array<ICrFile>;
    /**
     * Files provided by the Freelancer for this assignment
     */
    assignment_files?: Array<ICrFile>;
    /**
     * Project in which the notification occured
     */
    project?: ICognovisObject;
};

