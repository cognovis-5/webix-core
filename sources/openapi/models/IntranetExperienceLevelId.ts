/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetExperienceLevel } from './IntranetExperienceLevel';

export type IntranetExperienceLevelId = IntranetExperienceLevel;
