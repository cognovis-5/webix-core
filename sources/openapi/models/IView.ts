/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IView = {
    /**
     * id of the dynview (im_views table)
     */
    view_id?: number;
    /**
     * name of the dynview #@return view_type_id category Intranet DynView Type type of the dynview
     */
    view_name?: string;
    /**
     * placeholder for future view_status (line commmented above)
     */
    view_status_id?: string;
    /**
     * sql query which is used to later get data with that view
     */
    view_sql?: string;
    /**
     * tcl proc which checks against permissions
     */
    visible_for?: string;
    /**
     * dynview title (visible to end user)
     */
    view_label?: string;
    /**
     * json config of table (usefeul especially for webix js framework)
     */
    json_configuration?: string;
    /**
     * order of dynview (e.g. on a subpage)
     */
    sort_order?: number;
};

