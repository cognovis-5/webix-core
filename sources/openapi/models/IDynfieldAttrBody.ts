/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Boolean0 } from './Boolean0';
import type { IntegerInt32 } from './IntegerInt32';
import type { StringPlain } from './StringPlain';

export type IDynfieldAttrBody = {
    /**
     * Name of the attribute.
     */
    attribute_name: string;
    /**
     * Type of object the attribute is associated with.
     */
    object_type: string;
    /**
     * Name of the widget associated with the attribute.
     */
    widget_name: string;
    /**
     * Human-readable name of the attribute.
     */
    pretty_name: string;
    /**
     * Database table where the attribute is stored.
     */
    table_name: string;
    /**
     * Indicates if the attribute is hard-coded or dynamic.
     */
    also_hard_coded?: Boolean0;
    /**
     * Indicates if the attribute is required or optional.
     */
    required?: Boolean0;
    /**
     * Plural form of the attribute''s name.
     */
    pretty_plural?: string;
    /**
     * Specifies if the attribute is included in search operations.
     */
    include_in_search?: Boolean0;
    /**
     * Style or class for the attribute''s label.
     */
    label_style?: StringPlain;
    /**
     * Specifies the vertical position of the attribute in UI.
     */
    pos_y?: IntegerInt32;
    /**
     * Help text associated with the attribute for UI hints.
     */
    help_text?: string;
    /**
     * URL to provide additional help or documentation for the attribute.
     */
    help_url?: string;
    /**
     * Default value of the attribute if not provided.
     */
    default_value?: string;
    /**
     * Heading or section where the attribute is grouped in UI.
     */
    section_heading?: string;
};

