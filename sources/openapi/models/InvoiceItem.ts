/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IInvoiceItem } from './IInvoiceItem';

export type InvoiceItem = Array<IInvoiceItem>;
