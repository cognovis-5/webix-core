/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Boolean0 } from './Boolean0';
import type { Boolean1 } from './Boolean1';
import type { ImCompanyId } from './ImCompanyId';
import type { IntegerInt32 } from './IntegerInt32';

export type ITransCloneProjectBody = {
    /**
     * New name for the project
     */
    project_name?: string;
    /**
     * Should we include timesheet_tasks
     */
    timesheet_tasks?: Boolean0;
    /**
     * Should we include trans tasks
     */
    trans_tasks?: Boolean1;
    /**
     * Should we include quotes
     */
    quotes?: Boolean0;
    /**
     * Should we recreate the packages / batches. If yes to both tasks and packages, we put the tasks into the packages
     */
    packages?: Boolean1;
    /**
     * Should we import the project members?
     */
    project_members?: Boolean0;
    /**
     * Should we copy the the notes?
     */
    project_notes?: Boolean0;
    /**
     * Should we copy the files in original, project info and freelancer specific ones
     */
    files?: Boolean0;
    /**
     * Company for which we want to clone the project
     */
    company_id?: ImCompanyId;
    /**
     * Contact of the company for whom we want to clone the project
     */
    company_contact_id?: IntegerInt32;
};

