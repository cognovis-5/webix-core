/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetTransTaskType } from './IntranetTransTaskType';

export type IntranetTransTaskTypeId = IntranetTransTaskType;
