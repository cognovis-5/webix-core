/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IWidgetBody = {
    /**
     * ame of the widget.
     */
    widget_name: string;
    /**
     * name of the widget.
     */
    pretty_name: string;
    /**
     * plural form of the widget''s name #@param storage_type_id integer Identifier for the storage type associated with the widget.
     */
    pretty_plural: string;
    /**
     * datatype as recognized by the ACS system.
     */
    acs_datatype: string;
    /**
     * details and configuration of the widget itself.
     */
    widget: string;
    /**
     * sql datatype associated with the widget.
     */
    sql_datatype: string;
    /**
     * any parameters or configurations associated with the widget.
     */
    parameters?: string;
    /**
     * function used for dereferencing
     */
    deref_plpgsql_function?: string;
};

