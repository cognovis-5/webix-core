/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IDynviewColumn = {
    /**
     * id of column
     */
    column_id?: number;
    /**
     * name of the column (usually translation key)
     */
    column_name?: string;
    /**
     * TCL executed to add some fancy render
     */
    column_render_tcl?: string;
    /**
     * name of the variable
     */
    variable_name?: string;
    /**
     * for whom is that column visible
     */
    visible_for?: string;
    /**
     * ajax config (usually related to display)
     */
    ajax_configuration?: string;
    /**
     * order of the column
     */
    sort_order?: number;
};

