/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetProjectType } from './IntranetProjectType';

export type IntranetProjectTypeIds = Array<IntranetProjectType>;
