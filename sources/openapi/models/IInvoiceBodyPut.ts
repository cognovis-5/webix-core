/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntegerInt32 } from './IntegerInt32';
import type { IntranetCostStatus } from './IntranetCostStatus';
import type { IntranetCostTemplate } from './IntranetCostTemplate';
import type { IntranetCostType } from './IntranetCostType';
import type { IntranetInvoicePaymentMethod } from './IntranetInvoicePaymentMethod';
import type { IntranetPaymentTerm } from './IntranetPaymentTerm';
import type { StringDate } from './StringDate';

export type IInvoiceBodyPut = {
    /**
     * im_company Company which is the recipient / provider for this invoice (aka the ''other'' party)
     */
    company_id?: IntegerInt32;
    /**
     * user Contact for the invoice
     */
    company_contact_id?: IntegerInt32;
    /**
     * im_cost_center Cost center for the invoice
     */
    cost_center_id?: IntegerInt32;
    /**
     * Status of the invoice
     */
    cost_status_id?: IntranetCostStatus;
    /**
     * Type of the invoice
     */
    cost_type_id?: IntranetCostType;
    /**
     * Template used for printing the invoice
     */
    template_id?: IntranetCostTemplate;
    /**
     * Method of payment
     */
    payment_method_id?: IntranetInvoicePaymentMethod;
    /**
     * Terms for the payment (aka how fast has the payment to be made)
     */
    payment_term_id?: IntranetPaymentTerm;
    /**
     * ISO code of the currency
     */
    currency_iso?: string;
    /**
     * When were the services provided (especially if the invoice is issued later)
     */
    delivery_date?: StringDate;
    /**
     * The actual invoice date (to be printed on the invoice pdf)
     */
    effective_date?: StringDate;
};

