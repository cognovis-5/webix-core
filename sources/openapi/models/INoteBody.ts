/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetNotesStatus } from './IntranetNotesStatus';
import type { IntranetNotesType } from './IntranetNotesType';

export type INoteBody = {
    /**
     * HTML of the note
     */
    note: string;
    /**
     * What kind of note are we creating. Will default to default
     */
    note_type_id?: IntranetNotesType;
    /**
     * What status is the note going to be in. Typically active. Set to deleted instead of delete endpoint (unless you want to purge)
     */
    note_status_id?: IntranetNotesStatus;
};

