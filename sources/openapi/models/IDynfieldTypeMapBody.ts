/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntegerInt32 } from './IntegerInt32';

export type IDynfieldTypeMapBody = {
    /**
     * id of the dynamic field type we want to map
     */
    attribute_id: IntegerInt32;
    /**
     * type of the action. One of the following: ''none'', ''display'', ''edit''.
     */
    action: string;
    /**
     * object type of attribute / display mode
     */
    object_type_id: IntegerInt32;
};

