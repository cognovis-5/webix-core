/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IViewColumn = {
    /**
     * The identifier for the column.
     */
    column_id?: number;
    /**
     * The identifier for the view.
     */
    view_id?: number;
    /**
     * The identifier for the group.
     */
    group_id?: number;
    /**
     * Name of the column.
     */
    column_name?: string;
    /**
     * TCL rendering for the column.
     */
    column_render_tcl?: string;
    /**
     * Extra select information for the column.
     */
    extra_select?: string;
    /**
     * Extra from information for the column.
     */
    extra_from?: string;
    /**
     * Extra where condition for the column.
     */
    extra_where?: string;
    /**
     * Sort order for the column.
     */
    sort_order?: number;
    /**
     * Order by clause for the column.
     */
    order_by_clause?: string;
    /**
     * Visibility settings for the column.
     */
    visible_for?: string;
    /**
     * JSON configuration for the column.
     */
    json_configuration?: string;
    /**
     * Variable name associated with the column.
     */
    variable_name?: string;
    /**
     * Data type of the column.
     */
    datatype?: string;
};

