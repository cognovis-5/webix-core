/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { INamedId } from './INamedId';

export type IPrivilege = {
    /**
     * User we has the permission
     */
    permission_user?: INamedId;
    /**
     * Object that user is looking at
     */
    object?: INamedId;
    /**
     * Can the user view the object (e.g. in lists)
     */
    view?: boolean;
    /**
     * Can the user read the full object information
     */
    read?: boolean;
    /**
     * Can the user create and / or update the object
     */
    write?: boolean;
    /**
     * Can the user admin / delete the object
     */
    admin?: boolean;
};

