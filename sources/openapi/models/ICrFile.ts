/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { INamedId } from './INamedId';

export type ICrFile = {
    /**
     * item_id of the cr_file
     */
    file_id?: number;
    /**
     * RevisionId of the live revision we could find
     */
    id?: number;
    /**
     * RevisionId of the live revision we could find
     */
    revision_id?: number;
    /**
     * Filesize of the file in bytes.
     */
    size?: number;
    /**
     * Size as a text.
     */
    sizetext?: string;
    /**
     * name of the file
     */
    name?: string;
    /**
     * type of the content revision
     */
    mime_type?: string;
    /**
     * Extension of the mime_type
     */
    type?: string;
    /**
     * Parent object for the file
     */
    parent?: INamedId;
    /**
     * Date when the revision was uploaded
     */
    publish_date?: string;
    /**
     * Description of file
     */
    description?: string;
    /**
     * RevisionID for webix
     */
    sname?: string;
    /**
     * Always server
     */
    status?: string;
};

