/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICategoryObject } from './ICategoryObject';

export type IWebixPortlet = {
    /**
     * ID for the portlet
     */
    portlet_id?: number;
    /**
     * For which object type is this portlet
     */
    object_type?: string;
    /**
     * Type of the portlet (usually the category is the class name)
     */
    portlet_type?: ICategoryObject;
    /**
     * Status for this portlet - usually enabled / disabled
     */
    portlet_status?: ICategoryObject;
    /**
     * On which URL should we show this portlet
     */
    page_url?: string;
    /**
     * For sorting the portlets by default.
     */
    sort_order?: number;
    /**
     * Default name from I18N
     */
    portlet_name?: string;
    /**
     * where to put the portlet
     */
    location?: string;
};

