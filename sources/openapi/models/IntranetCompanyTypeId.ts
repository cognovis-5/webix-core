/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetCompanyType } from './IntranetCompanyType';

export type IntranetCompanyTypeId = IntranetCompanyType;
