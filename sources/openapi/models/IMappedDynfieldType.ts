/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IMappedDynfieldType = {
    /**
     * display mode. 'edit' / 'display'
     */
    display_mode?: string;
    /**
     * id of the object_type
     */
    mapped_object_type_id?: number;
    /**
     * name of the obeject type
     */
    mapped_object_type_name?: string;
};

