/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IDynviewField } from './IDynviewField';

export type IDynviewRow = {
    /**
     * id of the object
     */
    object_id?: number;
    /**
     * Array of id+values for the object. ID is the attribute_name and value is the corresponding value.
     */
    object_fields?: Array<IDynviewField>;
};

