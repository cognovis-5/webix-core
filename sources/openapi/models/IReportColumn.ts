/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICategoryObject } from './ICategoryObject';

export type IReportColumn = {
    /**
     * Column Mame used to display in the header
     */
    column_name?: string;
    /**
     * Name of the variable used for the column value in the SQL statement. Identifier to match the rows.
     */
    variable_name?: string;
    /**
     * Sort order of the column
     */
    sort_order?: number;
    /**
     * Description of the column
     */
    column_description?: string;
    /**
     * Status of the column
     */
    column_status?: ICategoryObject;
    /**
     * Type of data to be displayed in the column. Defines what to expect in the value of the cells of the row
     */
    column_type?: ICategoryObject;
    /**
     * Position of the column. Useful for Pivot Grids
     */
    column_position?: string;
    /**
     * filters turned on or turned off ? (Pivot Grid)
     */
    filters_p?: boolean;
};

