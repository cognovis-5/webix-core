/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { INamedId } from './INamedId';

export type IUserProject = {
    /**
     * Project to look at
     */
    project?: INamedId;
    /**
     * ProjectNR for the project
     */
    project_nr?: string;
};

