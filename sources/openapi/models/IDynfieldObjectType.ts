/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IDynfieldAttribute } from './IDynfieldAttribute';
import type { IDynfieldRef } from './IDynfieldRef';

export type IDynfieldObjectType = {
    /**
     * Pretty name of the object type
     */
    pretty_name?: string;
    /**
     * Actual object type name / id
     */
    object_type?: string;
    /**
     * Array of dynfield_attributes
     */
    dynfield_attributes?: Array<IDynfieldAttribute>;
    /**
     * Array of dynfield_refs with all the refs where the object_type is either source or target.
     */
    dynfield_refs?: Array<IDynfieldRef>;
};

