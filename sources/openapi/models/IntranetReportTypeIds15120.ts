/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetReportType } from './IntranetReportType';

export type IntranetReportTypeIds15120 = Array<IntranetReportType>;
