/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetTranslationSubjectArea } from './IntranetTranslationSubjectArea';

export type IntranetTranslationSubjectAreaIds = Array<IntranetTranslationSubjectArea>;
