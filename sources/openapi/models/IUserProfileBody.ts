/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IUserProfileBody = {
    /**
     * id of user
     */
    user_id: number;
    /**
     * id of profile
     */
    profile_id: number;
};

