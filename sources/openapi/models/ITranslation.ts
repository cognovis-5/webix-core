/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ITranslation = {
    /**
     * Key of the message used in PO
     */
    message_key?: string;
    /**
     * Translated string of the message key
     */
    message?: string;
    /**
     * Package key of fetched message
     */
    key_of_package?: string;
    /**
     * Locale of the translation
     */
    locale?: string;
};

