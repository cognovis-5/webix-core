/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IGetCompanyNewProjectDefaults = {
    /**
     * default project type for that customer
     */
    project_type_id?: number;
    /**
     * default source language for that customer
     */
    source_language_id?: number;
    /**
     * default target language for that customer
     */
    target_language_id?: number;
    /**
     * default subject area for that customer
     */
    subject_area_id?: number;
    /**
     * default final company for that customer
     */
    final_company_id?: number;
    /**
     * default unit of measure for that customer
     */
    uom_id?: number;
    /**
     * default complexity_type_id for that customer
     */
    complexity_type_id?: number;
};

