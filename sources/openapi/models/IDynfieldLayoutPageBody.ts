/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IDynfieldLayoutPageBody = {
    /**
     * URL of the page for the layout.
     */
    page_url: string;
    /**
     * Type of object the layout is associated with.
     */
    object_type: string;
    /**
     * Type of the layout.
     */
    layout_type: string;
};

