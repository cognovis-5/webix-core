/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IIdValue = {
    /**
     * Identifier for the id/value pair
     */
    id?: string;
    /**
     * Actual name which belongs to the id
     */
    value?: string;
    /**
     * Internationalized value to display
     */
    display_value?: string;
};

