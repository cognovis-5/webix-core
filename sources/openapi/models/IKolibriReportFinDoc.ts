/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IKolibriReportFinDoc = {
    /**
     * Financial document number
     */
    document_nr?: string;
    /**
     * Cost Center for the financial document
     */
    cost_center?: string;
    /**
     * Type of financial document
     */
    cost_type?: string;
    /**
     * Status of the financial document
     */
    cost_status?: string;
    /**
     * Customer/Provider for the financial document
     */
    company?: string;
    /**
     * Date when the invoice is due
     */
    due_date?: string;
    /**
     * Daten when the invoice was issued
     */
    effective_date?: string;
    /**
     * Total amount of the invoice
     */
    total_amount?: number;
    /**
     * ID of the company in collmex
     */
    company_collmex_id?: number;
    /**
     * Net Amount of the invoice
     */
    net_amount?: number;
    /**
     * VAT Percentage of the financial document
     */
    vat?: number;
    /**
     * Type of tax applied to the financial document
     */
    tax_classification?: string;
    /**
     * International VAT number of the customer/provider
     */
    vat_number?: string;
};

