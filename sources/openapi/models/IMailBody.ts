/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntegerInt32 } from './IntegerInt32';
import type { PartyId } from './PartyId';
import type { PartyIds } from './PartyIds';

export type IMailBody = {
    /**
     * Sender for the E-mail
     */
    from_party_id: PartyId;
    /**
     * Recipients, might be empty in case we use to_addr instead
     */
    to_party_ids?: PartyIds;
    /**
     * E-Mail address to whom we send the mail to (instead of using the party_id)
     */
    to_addr?: string;
    /**
     * CC E-Mail addresses
     */
    cc_addr?: string;
    /**
     * Subject for the E-Mail
     */
    subject?: string;
    /**
     * Body for the E-Mail
     */
    body?: string;
    /**
     * object_id which is related to this E-Mail
     */
    context_id?: IntegerInt32;
    /**
     * List of files
     */
    files?: string;
};

