/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { INamedId } from './INamedId';

export type ICollmexAccdoc = {
    /**
     * Invoice to which this booking relates
     */
    invoice?: INamedId;
    /**
     * Year of the booking
     */
    booking_year?: number;
    /**
     * ID of the booking in the given year
     */
    booking_id?: number;
    /**
     * Position of the line item in the booking
     */
    booking_pos?: number;
    /**
     * Amount of the booking
     */
    booking_amount?: number;
    /**
     * Date when the booking was created
     */
    creation_date?: string;
    /**
     * Date when the booking was effective
     */
    booking_date?: string;
    /**
     * Account which was used for this booking line
     */
    account?: INamedId;
    /**
     * Note with the booking
     */
    internal_note?: string;
    /**
     * ID for the company associated with the booking
     */
    company_collmex_id?: number;
    /**
     * Company associated with the booking if found
     */
    company?: INamedId;
};

