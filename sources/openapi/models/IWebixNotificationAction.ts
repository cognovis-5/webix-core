/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICategoryObject } from './ICategoryObject';
import type { ICognovisObject } from './ICognovisObject';

export type IWebixNotificationAction = {
    /**
     * Type of action we can perform
     */
    action_type?: ICategoryObject;
    /**
     * Text we want to pass through to the action (e.g. the default text for an email)
     */
    action_text?: string;
    /**
     * Tooltip text to describe the action based of the action_type
     */
    action_help?: string;
    /**
     * Class to use for the icon
     */
    action_icon?: string;
    /**
     * Object which the action shall be performed on
     */
    action_object?: ICognovisObject;
};

