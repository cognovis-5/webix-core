/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ImTransTaskIds } from './ImTransTaskIds';

export type IFreelancePackageBodyPut = {
    /**
     * List of trans task_ids which we want to generate a package
     */
    trans_task_ids?: ImTransTaskIds;
    /**
     * Name of the package, will auto generate if not provided
     */
    package_name?: string;
    /**
     * Comment for the package
     */
    package_comment?: string;
};

