/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { INamedId } from './INamedId';
import type { IProfilePrivilege } from './IProfilePrivilege';

export type IUserWithPrivilege = {
    /**
     * User who is a member of the object
     */
    user?: INamedId;
    /**
     * array of profile privliges
     */
    user_privileges?: Array<IProfilePrivilege>;
};

