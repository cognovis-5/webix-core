/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IGroup = {
    /**
     * ID of the group the user is a member of
     */
    group_id?: number;
    /**
     * Name of the group the user is a member of
     */
    group_name?: string;
};

