/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Boolean0 } from './Boolean0';
import type { IntegerInt32 } from './IntegerInt32';

export type IDynfieldLayoutPageAttributesBody = {
    /**
     * URL of the page for the layout.
     */
    page_url: string;
    /**
     * id of the attribute. It''s mandatory for the PUT request.
     */
    attribute_id?: IntegerInt32;
    /**
     * Type of object the layout attribute is associated with.
     */
    object_type?: string;
    /**
     * X position of the attribute on the layout.
     */
    pos_x?: IntegerInt32;
    /**
     * Y position of the attribute on the layout.
     */
    pos_y?: IntegerInt32;
    /**
     * Width of the attribute on the layout.
     */
    size_x?: IntegerInt32;
    /**
     * Height of the attribute on the layout.
     */
    size_y?: IntegerInt32;
    /**
     * Style for the label of the attribute.
     */
    label_style?: string;
    /**
     * CSS class for the attribute div.
     */
    div_class?: string;
    /**
     * flag which allows to decide if we want to add all attributes.
     */
    add_all_attributes_p?: Boolean0;
};

