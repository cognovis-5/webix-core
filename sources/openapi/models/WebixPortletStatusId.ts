/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { WebixPortletStatus } from './WebixPortletStatus';

export type WebixPortletStatusId = WebixPortletStatus;
