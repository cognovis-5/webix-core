/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetSalutation } from './IntranetSalutation';

export type ICompanyContactBody = {
    /**
     * First Name(s) of the contact. Just in case you manually want to address the person
     */
    first_names: string;
    /**
     * Last name of the contact
     */
    last_name: string;
    /**
     * E-Mail address of the contact
     */
    email: string;
    /**
     * Locale for the contact to be used in communication
     */
    locale?: string;
    /**
     * Salutation we are using for the user
     */
    salutation_id?: IntranetSalutation;
    /**
     * Home Phone of the contact
     */
    home_phone?: string;
    /**
     * Cell Phone of the contact
     */
    cell_phone?: string;
    /**
     * Work phone for the contact
     */
    work_phone?: string;
    /**
     * Position of the contact in the company
     */
    position?: string;
};

