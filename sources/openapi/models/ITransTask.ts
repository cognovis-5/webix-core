/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICategoryObject } from './ICategoryObject';
import type { INamedId } from './INamedId';

export type ITransTask = {
    /**
     * Task ID and name - typically the name of the file to be translated
     */
    task?: INamedId;
    /**
     * Units for the task
     */
    task_units?: number;
    /**
     * Billable Units for the task
     */
    billable_units?: number;
    /**
     * Unit of measure for the task
     */
    task_uom?: ICategoryObject;
    /**
     * Project in which this task resides
     */
    project?: INamedId;
    /**
     * Language into which this task is translated to
     */
    target_language?: ICategoryObject;
    /**
     * Type of task (by default the one from the project). You can read the steps involved based of the aux_int1 of the category
     */
    task_type?: ICategoryObject;
    /**
     * Deadline until when the CUSTOMER wants to have the task delivered back
     */
    task_deadline?: string;
};

