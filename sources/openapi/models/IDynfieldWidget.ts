/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICategoryObject } from './ICategoryObject';

export type IDynfieldWidget = {
    /**
     * unique identifier for the widget
     */
    widget_id?: number;
    /**
     * unique Name of the widget
     */
    widget_name?: string;
    /**
     * readable name of the widget
     */
    pretty_name?: string;
    /**
     * readable name of the widget (plural)
     */
    pretty_plural?: string;
    /**
     * type of the storage for widget
     */
    storage_type?: ICategoryObject;
    /**
     * acs_datatype for the widget
     */
    acs_datatype?: string;
    /**
     * description or content of the widget
     */
    widget?: string;
    /**
     * SQL datatype used by the widget
     */
    sql_datatype?: string;
    /**
     * parameters for the widget
     */
    parameters?: string;
    /**
     * function for the widget ('im_name_from_id' as used as default one )
     */
    deref_plpgsql_function?: string;
};

