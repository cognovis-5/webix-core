/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IUserProfileBodyPut = {
    /**
     * name of the role (e.g. ''Project Managers'' or ''Employee'')
     */
    name?: string;
    /**
     * icon used to decorate user profile (probably font awesome icon)
     */
    icon?: string;
    /**
     * color used to decorate user profile (e.g. ''#CDCDCD'' or ''grey'')
     */
    color?: string;
};

