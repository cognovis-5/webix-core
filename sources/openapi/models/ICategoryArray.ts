/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICategoryObject } from './ICategoryObject';

export type ICategoryArray = Array<ICategoryObject>;
