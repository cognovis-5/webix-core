/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IDynfieldAttr = {
    /**
     * id of the dynfield (im_dynfield_attributes)
     */
    dynfield_id?: number;
    /**
     * name of the dynfield (im_dynfield_attributes)
     */
    dynfield_name?: string;
    /**
     * id from acs_attributes
     */
    acs_attribute_id?: number;
    /**
     * type of the object that field is related to
     */
    object_type?: string;
    /**
     * name of the widget used for the dynamic field
     */
    widget_name?: string;
    /**
     * flag indicating if the attribute is also hard-coded (true or false)
     */
    also_hard_coded_p?: boolean;
    /**
     * flag indicating if attribute is mandatory (true or false)
     */
    required_p?: boolean;
    /**
     * name of the database table associated with the attribute
     */
    table_name?: string;
    /**
     * user-friendly name of the attribute
     */
    pretty_name?: string;
    /**
     * plural form of the pretty name
     */
    pretty_plural?: string;
    /**
     * position on the x-axis
     */
    pos_x?: number;
    /**
     * position on the y-axis
     */
    pos_y?: number;
    /**
     * help text in English
     */
    help_text?: string;
    /**
     * help url
     */
    help_url?: string;
    /**
     * default value of the dynfield attribute
     */
    default_value?: string;
    /**
     * section heading of the dynfield attribute
     */
    secion_heading?: string;
};

