/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICategoryObject } from './ICategoryObject';
import type { INamedId } from './INamedId';

export type IFreelancerTaskUnits = {
    /**
     * Freelancer for whom to get the units
     */
    freelancer?: INamedId;
    /**
     * task for which we fetch units
     */
    task?: INamedId;
    /**
     * calculated number of units
     */
    task_units?: number;
    /**
     * Unit of measure. Hopefully a package was not created with different UOM ids...
     */
    uom?: ICategoryObject;
};

