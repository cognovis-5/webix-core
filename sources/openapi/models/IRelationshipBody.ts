/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntegerInt32 } from './IntegerInt32';

export type IRelationshipBody = {
    /**
     * object_id of the primary object in the relationship
     */
    object_id_one: IntegerInt32;
    /**
     * object_id of the secondary object in the relationship
     */
    object_id_two: IntegerInt32;
    /**
     * type of the relationship
     */
    rel_type: string;
};

