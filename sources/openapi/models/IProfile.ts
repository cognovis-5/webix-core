/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { INamedId } from './INamedId';
import type { IProfilePrivilege } from './IProfilePrivilege';

export type IProfile = {
    /**
     * Object im_profile name and id of profile
     */
    profile?: INamedId;
    /**
     * array of profile privliges
     */
    profile_privileges?: Array<IProfilePrivilege>;
};

