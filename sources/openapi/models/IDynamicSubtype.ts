/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { INamedId } from './INamedId';

export type IDynamicSubtype = {
    /**
     * Help Text we want to display for the attribute in case someone does not know how to fill the filled
     */
    help_text?: string;
    /**
     * Section we want to display the attribute - Useful if we want to group the fields in the frontend.
     */
    section_heading?: string;
    /**
     * Value to use as default - might actually be an integer but we get it as string from the DB
     */
    default_value?: string;
    /**
     * Subtype for which this attribute
     */
    subtype?: INamedId;
    /**
     * Any of edit/display/none
     */
    display_mode?: string;
    /**
     * Is this a required attribute (for this subtype)
     */
    required_p?: boolean;
    /**
     * URL to link to if someone clicks on the helptext icon
     */
    help_url?: string;
};

