/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetTranslationSubjectArea } from './IntranetTranslationSubjectArea';

export type IntranetTranslationSubjectAreaId = IntranetTranslationSubjectArea;
