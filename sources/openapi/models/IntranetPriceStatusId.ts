/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetPriceStatus } from './IntranetPriceStatus';

export type IntranetPriceStatusId = IntranetPriceStatus;
