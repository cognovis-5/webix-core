/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetNotesType } from './IntranetNotesType';

export type IntranetNotesTypeId = IntranetNotesType;
