/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICategoryObject } from './ICategoryObject';

export type IFreelancerWorkedWith = {
    /**
     * Task type for which we are looking up the # of worked with. Corresponds to the workflow steps.
     */
    task_type?: ICategoryObject;
    /**
     * How often did the freelancer execute the workflow step in general
     */
    total?: number;
    /**
     * How often did the freelancer work with the (end-)customer in this workflow step
     */
    customer?: number;
};

