/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum WebixNotificationStatus {
    INFORMATION = 4240,
    WARNING = 4241,
    IMPORTANT = 4242,
}
