/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetTranslationLanguage } from './IntranetTranslationLanguage';

export type IntranetTranslationLanguageId = IntranetTranslationLanguage;
