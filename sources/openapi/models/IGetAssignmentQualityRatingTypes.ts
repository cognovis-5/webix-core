/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICategoryArray } from './ICategoryArray';

export type IGetAssignmentQualityRatingTypes = {
    /**
     * Type of rating
     */
    quality_type?: ICategoryArray;
    /**
     * Quality Level
     */
    quality_level?: ICategoryArray;
};

