/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IUserPrivilege = {
    /**
     * id of privilege (fake id)
     */
    privilege_id?: number;
    /**
     * name of the privilege
     */
    privilege_name?: string;
    /**
     * is operation permitted
     */
    permitted_p?: boolean;
};

