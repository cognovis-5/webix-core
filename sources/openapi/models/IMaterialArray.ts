/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IMaterialObject } from './IMaterialObject';

export type IMaterialArray = Array<IMaterialObject>;
