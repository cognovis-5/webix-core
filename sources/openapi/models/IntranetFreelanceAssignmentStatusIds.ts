/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetFreelanceAssignmentStatus } from './IntranetFreelanceAssignmentStatus';

export type IntranetFreelanceAssignmentStatusIds = Array<IntranetFreelanceAssignmentStatus>;
