/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICategoryObject } from './ICategoryObject';
import type { IMaterial } from './IMaterial';
import type { INamedId } from './INamedId';

export type IPrice = {
    /**
     * Price identification
     */
    price_id?: number;
    /**
     * Type of price
     */
    price_type?: ICategoryObject;
    /**
     * Status of the price
     */
    price_status?: ICategoryObject;
    /**
     * Material for which this price is valid
     */
    material?: IMaterial;
    /**
     * Company for which this price is valid
     */
    company?: INamedId;
    /**
     * Complexity of the work
     */
    complexity_type?: ICategoryObject;
    /**
     * Since when is this price valid
     */
    valid_from?: string;
    /**
     * How long is it valid (defaults to nothing = unlimited)
     */
    valid_through?: string;
    /**
     * ISO for the currency
     */
    currency?: string;
    /**
     * Actual price per Unit
     */
    price?: number;
    /**
     * Minimum price charged in case units * price is lower
     */
    min_price?: number;
    /**
     * Note associated with this price
     */
    note?: string;
};

