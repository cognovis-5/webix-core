/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetSkillType } from './IntranetSkillType';

export type IntranetSkillTypeId = IntranetSkillType;
