/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type INamedId = {
    /**
     * object_id of the object we look for
     */
    id?: number;
    /**
     * Name for the object_id
     */
    name?: string;
};

