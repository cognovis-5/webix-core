/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { INamedId } from './INamedId';

export type ICompanyContact = {
    /**
     * Company the contact works for
     */
    company?: INamedId;
    /**
     * Contact we requested (with ID and name - first_names + last_name typically)
     */
    contact?: INamedId;
    /**
     * Key Account for this contact
     */
    key_account?: INamedId;
    /**
     * Locale for the contact to be used in communication
     */
    locale?: string;
    /**
     * E-Mail address of the contact
     */
    email?: string;
    /**
     * First Name(s) of the contact. Just in case you manually want to address the person
     */
    first_names?: string;
    /**
     * Last name of the contact
     */
    last_name?: string;
    /**
     * Salutation (how to address the contact in communication), eg. Dear Malte or Dear Mr. Sussdorff
     */
    salutation?: string;
    /**
     * Home Phone of the contact
     */
    home_phone?: string;
    /**
     * Cell Phone of the contact
     */
    cell_phone?: string;
    /**
     * Work phone for the contact
     */
    work_phone?: string;
    /**
     * Position in the company of the contact
     */
    position?: string;
    /**
     * URL to the portrait of the contact
     */
    portrait_url?: string;
};

