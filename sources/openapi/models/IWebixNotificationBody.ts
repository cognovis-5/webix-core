/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ImProjectId } from './ImProjectId';
import type { IntegerInt32 } from './IntegerInt32';
import type { WebixNotificationStatus } from './WebixNotificationStatus';
import type { WebixNotificationType } from './WebixNotificationType';

export type IWebixNotificationBody = {
    /**
     * Type of notification we are looking at
     */
    notification_type_id?: WebixNotificationType;
    /**
     * Status of the notification we are looking at
     */
    notification_status_id?: WebixNotificationStatus;
    /**
     * Text we are displaying for the notification. Default comes form the notification_type
     */
    message?: string;
    /**
     * Object which caused the notification (context)
     */
    context_id: IntegerInt32;
    /**
     * Project in which the notification is created
     */
    project_id?: ImProjectId;
    /**
     * Recipient of the notification
     */
    recipient_id: IntegerInt32;
};

