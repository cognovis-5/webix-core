/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetTmTool } from './IntranetTmTool';

export type IntranetTmToolId = IntranetTmTool;
