/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetReportStatus } from './IntranetReportStatus';

export type IntranetReportStatusId15000 = IntranetReportStatus;
