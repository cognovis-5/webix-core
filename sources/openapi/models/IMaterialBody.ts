/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { BooleanT } from './BooleanT';
import type { IntegerInt32 } from './IntegerInt32';
import type { IntranetMaterialStatus } from './IntranetMaterialStatus';
import type { IntranetMaterialType } from './IntranetMaterialType';
import type { IntranetTranslationFileType } from './IntranetTranslationFileType';
import type { IntranetTranslationLanguage } from './IntranetTranslationLanguage';
import type { IntranetTranslationSubjectArea } from './IntranetTranslationSubjectArea';
import type { IntranetUom } from './IntranetUom';

export type IMaterialBody = {
    /**
     * Name of the material
     */
    material_name?: string;
    /**
     * Material Nr.
     */
    material_nr?: string;
    /**
     * Type of material
     */
    material_type_id?: IntranetMaterialType;
    /**
     * Status of the material (e.g. can we use it)
     */
    material_status_id?: IntranetMaterialStatus;
    /**
     * Description of the material
     */
    description?: string;
    /**
     * Unit of Measure for the material
     */
    material_uom_id: IntranetUom;
    /**
     * Is the material billable?
     */
    material_billable_p?: BooleanT;
    /**
     * which is the source language for this material
     */
    source_language_id?: IntranetTranslationLanguage;
    /**
     * target language (if any)
     */
    target_language_id?: IntranetTranslationLanguage;
    /**
     * Subject area for the translation
     */
    subject_area_id?: IntranetTranslationSubjectArea;
    /**
     * what type of action do we make for this material
     */
    task_type_id?: IntegerInt32;
    /**
     * File type we work on
     */
    file_type_id?: IntranetTranslationFileType;
};

