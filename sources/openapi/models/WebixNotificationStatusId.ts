/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { WebixNotificationStatus } from './WebixNotificationStatus';

export type WebixNotificationStatusId = WebixNotificationStatus;
