/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IDynfieldRef = {
    id?: number;
    /**
     * Name of the dynfield_object_type offering to be the target of a reference
     */
    target?: string;
    /**
     * Name of the source object_type (where we reference from)
     */
    source?: string;
    /**
     * Name of dynfield attribute in the SOURCE object_type
     */
    name?: string;
};

