/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetTranslationQualityType } from './IntranetTranslationQualityType';

export type IntranetTranslationQualityTypeId = IntranetTranslationQualityType;
