/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICompany } from './ICompany';

export type IInternalCompany = {
    /**
     * Object with the internal company data
     */
    company?: ICompany;
    /**
     * url of the company logo_url
     */
    logo_url?: string;
    /**
     * url of favicon which is gonna be used across all apps
     */
    favicon_url?: string;
};

