/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IPostAssignmentRemindFreelancer = {
    /**
     * was sending the reminder Successful
     */
    success?: boolean;
    /**
     * message to display back to the user
     */
    message?: string;
    /**
     * Body of the mail send to the freelancer
     */
    body?: string;
};

