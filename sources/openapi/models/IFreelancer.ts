/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IFreelancerFee } from './IFreelancerFee';
import type { IFreelancerWorkedWith } from './IFreelancerWorkedWith';
import type { INamedId } from './INamedId';

export type IFreelancer = {
    /**
     * Freelancer whom we can select
     */
    freelancer?: INamedId;
    /**
     * Color coding for the status icon
     */
    freelancer_status_color?: string;
    /**
     * Tooltip to use for this freelancer's status (to explain why the color)
     */
    freelancer_status_description?: string;
    /**
     * Order as configured based on status for the freelancer. Only a suggestion for the frontend, and you have to decide how to sort within the same sort_order
     */
    sort_order?: number;
    /**
     * Quality rating for the freelancer
     */
    rating?: number;
    /**
     * Fee Information
     */
    fee?: Array<IFreelancerFee>;
    /**
     * Detailed information about the freelancer engagement
     */
    worked_with?: Array<IFreelancerWorkedWith>;
};

