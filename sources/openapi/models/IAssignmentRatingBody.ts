/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ImAssignmentQualityReportId } from './ImAssignmentQualityReportId';
import type { IntranetQuality } from './IntranetQuality';
import type { IntranetTranslationQualityType } from './IntranetTranslationQualityType';

export type IAssignmentRatingBody = {
    /**
     * Report where we want to store the rating for
     */
    report_id: ImAssignmentQualityReportId;
    /**
     * Type of rating
     */
    quality_type_id: IntranetTranslationQualityType;
    /**
     * Quality Level
     */
    quality_level_id: IntranetQuality;
};

