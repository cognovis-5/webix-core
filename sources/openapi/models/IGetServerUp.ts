/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IGetServerUp = {
    /**
     * Returns true if we could successfully access the database
     */
    success?: boolean;
};

