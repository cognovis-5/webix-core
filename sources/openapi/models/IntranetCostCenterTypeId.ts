/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetCostCenterType } from './IntranetCostCenterType';

export type IntranetCostCenterTypeId = IntranetCostCenterType;
