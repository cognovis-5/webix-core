/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICategoryObject } from './ICategoryObject';
import type { INamedId } from './INamedId';

export type IMainTranslator = {
    /**
     * id of mapping
     */
    mapping_id?: number;
    /**
     * information of main translator
     */
    freelancer?: INamedId;
    /**
     * company for which user is main translator
     */
    customer?: INamedId;
    /**
     * source language (skill)
     */
    source_language?: ICategoryObject;
    /**
     * target language (skill)
     */
    target_language?: ICategoryObject;
};

