/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IKolibriUrl = {
    /**
     * URL to access the T: drive
     */
    url?: string;
    /**
     * Type of URL we provide
     */
    type?: string;
};

