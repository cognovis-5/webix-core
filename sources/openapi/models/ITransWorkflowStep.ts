/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICategoryObject } from './ICategoryObject';

export type ITransWorkflowStep = {
    /**
     * The task type describing the workflow
     */
    task_type?: ICategoryObject;
    /**
     * At which step does this workflow occur.
     */
    sort_order?: number;
};

