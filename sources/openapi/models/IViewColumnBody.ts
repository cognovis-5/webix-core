/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntegerInt32 } from './IntegerInt32';

export type IViewColumnBody = {
    /**
     * The identifier for the view.
     */
    view_id: IntegerInt32;
    /**
     * Name of the column in the dynview
     */
    column_name: string;
    /**
     * Name of the variable that corresponds to the column
     */
    variable_name: string;
    /**
     * TCL proc which checks against permissions
     */
    visible_for?: string;
    /**
     * JSON configuration for the column (useful especially for Webix JS framework)
     */
    json_configuration?: string;
    /**
     * Order of columns in the dynview (e.g., when displayed in a table)
     */
    sort_order?: IntegerInt32;
};

