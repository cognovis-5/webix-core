/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum IntranetExperienceLevel {
    UNCONFIRMED = 2200,
    CERTIFIED = 2204,
}
