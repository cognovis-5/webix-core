/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IWebixFile = {
    /**
     * name of the file/folder
     */
    value?: string;
    /**
     * item_id of the file/folder
     */
    id?: string;
    /**
     * UNIX Time When was the file last updated
     */
    date?: number;
    /**
     * Filesize of the file in bytes.
     */
    size?: number;
    /**
     * type of file
     */
    type?: string;
};

