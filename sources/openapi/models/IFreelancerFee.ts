/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICategoryObject } from './ICategoryObject';
import type { INamedId } from './INamedId';

export type IFreelancerFee = {
    /**
     * Freelancer for whom to get the fees
     */
    freelancer?: INamedId;
    /**
     * Rate of the freelancer
     */
    rate?: number;
    /**
     * Currency queried
     */
    currency?: string;
    /**
     * Minimum Price
     */
    min_price?: number;
    /**
     * MaterialID which matches the fee - to be used for line items
     */
    material_id?: number;
    /**
     * PriceID we found
     */
    price_id?: number;
    /**
     * Which source langauge was matched
     */
    source_language?: ICategoryObject;
    /**
     * Which target language was matched
     */
    target_language?: ICategoryObject;
};

