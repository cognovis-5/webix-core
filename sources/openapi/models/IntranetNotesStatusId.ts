/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetNotesStatus } from './IntranetNotesStatus';

export type IntranetNotesStatusId = IntranetNotesStatus;
