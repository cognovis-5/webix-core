/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IdValue } from './IdValue';
import type { IntegerInt32 } from './IntegerInt32';

export type IDynamicObjectBody = {
    /**
     * Object we want to retrieve. Will check read permission
     */
    object_id: IntegerInt32;
    /**
     * Array of attribute_name (id) and value (of the attribute) objects.
     */
    id_values: IdValue;
};

