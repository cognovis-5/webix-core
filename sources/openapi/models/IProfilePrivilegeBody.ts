/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntegerInt32 } from './IntegerInt32';

export type IProfilePrivilegeBody = {
    /**
     * Object that the user is a member of
     */
    object_id: IntegerInt32;
    /**
     * name of privilege
     */
    privilege_name: string;
};

