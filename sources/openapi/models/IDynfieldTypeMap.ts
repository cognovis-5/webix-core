/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IMappedDynfieldType } from './IMappedDynfieldType';

export type IDynfieldTypeMap = {
    /**
     * id of the dynfield (im_dynfield_attributes)
     */
    dynfield_id?: number;
    /**
     * name of the dynfield (im_dynfield_attributes)
     */
    dynfield_name?: string;
    /**
     * type of object the attribute is associated with
     */
    object_type?: string;
    /**
     * mapped types for that dynfield
     */
    mapped_dynfield_types?: Array<IMappedDynfieldType>;
};

