/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICategoryObject } from './ICategoryObject';

export type IAssignmentRating = {
    /**
     * Type of rating
     */
    quality_type?: ICategoryObject;
    /**
     * Quality Level
     */
    quality_level?: ICategoryObject;
    /**
     * Value for the quality level
     */
    quality_value?: number;
};

