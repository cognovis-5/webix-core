/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { INamedId } from './INamedId';

export type IUserProfile = {
    /**
     * Object im_profile name and id of profile
     */
    profile?: INamedId;
    /**
     * icon of profile
     */
    icon?: string;
    /**
     * color of profile
     */
    color?: string;
};

