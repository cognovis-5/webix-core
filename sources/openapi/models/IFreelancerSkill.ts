/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICategoryObject } from './ICategoryObject';
import type { INamedId } from './INamedId';

export type IFreelancerSkill = {
    /**
     * Freelancer posessing the skill
     */
    user?: INamedId;
    /**
     * which is required
     */
    skill?: ICategoryObject;
    /**
     * Skill type of the required skill
     */
    skill_type?: ICategoryObject;
    /**
     * Level of experience claimed
     */
    claimed_experience?: ICategoryObject;
    /**
     * Level of experience confirmed
     */
    confirmed_experience?: ICategoryObject;
    /**
     * Person who confirmed the experience
     */
    confirmation_user?: INamedId;
};

