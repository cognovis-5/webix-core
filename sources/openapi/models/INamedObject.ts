/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type INamedObject = {
    /**
     * ID of the object we return
     */
    id?: number;
    /**
     * Actual name of the object usually from im_name_from_id
     */
    name?: string;
};

