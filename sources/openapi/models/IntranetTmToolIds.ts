/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetTmTool } from './IntranetTmTool';

export type IntranetTmToolIds = Array<IntranetTmTool>;
