/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICognovisObject } from './ICognovisObject';
import type { INamedId } from './INamedId';

export type IRelationship = {
    /**
     * primary object in the relationship
     */
    object_one?: ICognovisObject;
    /**
     * secondary object in the relationship
     */
    object_two?: ICognovisObject;
    /**
     * type of the relationship
     */
    rel_type?: string;
    /**
     * relationship_id used as an identifier
     */
    rel?: INamedId;
};

