/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ImProjectId } from './ImProjectId';
import type { ImTransTaskIds } from './ImTransTaskIds';
import type { IntranetTranslationLanguage } from './IntranetTranslationLanguage';
import type { IntranetTransTaskType } from './IntranetTransTaskType';

export type IFreelancePackageBody = {
    /**
     * Project in which we want to add the package
     */
    project_id?: ImProjectId;
    /**
     * List of trans task_ids which we want to generate a package
     */
    trans_task_ids?: ImTransTaskIds;
    /**
     * Task Type for which we want to generate the package. If empty, generate packages for all types of the project
     */
    package_type_id: IntranetTransTaskType;
    /**
     * Name of the package, will auto generate if not provided
     */
    package_name?: string;
    /**
     * Comment for the package
     */
    package_comment?: string;
    /**
     * Target language for which this package is used.
     */
    target_language_id: IntranetTranslationLanguage;
};

