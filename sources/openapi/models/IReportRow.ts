/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IIdValue } from './IIdValue';

export type IReportRow = {
    /**
     * Identifier for the row - Primary key if we know it
     */
    row_id?: number;
    /**
     * Array of id+values for the object. ID is the variable_name and value is the corresponding value.
     */
    cells?: Array<IIdValue>;
};

