/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetQuality } from './IntranetQuality';

export type IntranetQualityId = IntranetQuality;
