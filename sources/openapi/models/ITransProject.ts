/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICategoryArray } from './ICategoryArray';
import type { ICategoryObject } from './ICategoryObject';
import type { INamedId } from './INamedId';
import type { IUser } from './IUser';

export type ITransProject = {
    /**
     * Project to look at
     */
    project?: INamedId;
    /**
     * ProjectNR for the project
     */
    project_nr?: string;
    /**
     * Status_id of the project
     */
    project_status?: ICategoryObject;
    /**
     * Project which shall be the parent for this one.
     */
    parent?: INamedId;
    /**
     * Type_id of the project
     */
    project_type?: ICategoryObject;
    /**
     * Customer Reference Number
     */
    company_project_nr?: string;
    /**
     * Date when project starts
     */
    start_date?: string;
    /**
     * Date for the deadline
     */
    end_date?: string;
    /**
     * When should we contact again ?
     */
    contact_again_date?: string;
    /**
     * source language ID of the project
     */
    source_language?: ICategoryObject;
    /**
     * List of target languagages
     */
    target_language?: ICategoryArray;
    /**
     * Subject Area ID of the project
     */
    subject_area?: ICategoryObject;
    /**
     * PM running the project
     */
    project_lead?: IUser;
    /**
     * Company contact for this project
     */
    company_contact?: INamedId;
    /**
     * E-Mail address for the company contact
     */
    company_contact_email?: string;
    /**
     * Time the project usually takes to finish in days
     */
    processing_time?: number;
    /**
     * Best price we could find for this project based on filters et.al.
     */
    price?: number;
    /**
     * Invoice Amount for the project
     */
    invoice_amount?: number;
    /**
     * Quote Amonut for the project
     */
    quote_amount?: number;
    /**
     * Purchase Order Amonut for the project
     */
    po_amount?: number;
    /**
     * Bill Amonut for the project
     */
    bill_amount?: number;
    /**
     * Project folder we hopefully have created.
     */
    project_folder?: INamedId;
    /**
     * Company for which we run this project. This is the company being billed
     */
    company?: INamedId;
    /**
     * Final customer / company of the project. Usually won't be billed directly.
     */
    final_company?: INamedId;
    /**
     * Complexity level of the project. Important for prices.
     */
    complexity_type?: ICategoryObject;
    /**
     * description of the project. Usually Customer facing
     */
    description?: string;
    /**
     * MemoQ GUID in case we have memoq enabled for this project
     */
    memoq_guid?: string;
    /**
     * Is Trados enabled for this project
     */
    trados?: boolean;
};

