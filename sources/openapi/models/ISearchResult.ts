/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICognovisObject } from './ICognovisObject';
import type { IWebixNotificationAction } from './IWebixNotificationAction';

export type ISearchResult = {
    /**
     * The object of the search result
     */
    object?: ICognovisObject;
    /**
     * Text we display for the search result
     */
    search_text?: string;
    /**
     * Actions for the notification
     */
    actions?: Array<IWebixNotificationAction>;
    /**
     * order by which to order the search result
     */
    sort_order?: number;
};

