/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum IntranetMaterialStatus {
    ACTIVE = 9100,
    INACTIVE = 9102,
}
