/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetSkillBusinessSector } from './IntranetSkillBusinessSector';

export type IntranetSkillBusinessSectorIds = Array<IntranetSkillBusinessSector>;
