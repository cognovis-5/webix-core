/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IGetBasicProjectInfo = {
    /**
     * Name of the variable
     */
    name?: string;
    /**
     * Value of the info field
     */
    value?: string;
    /**
     * Translated label of the variable
     */
    label?: string;
};

