/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetProjectSource } from './IntranetProjectSource';

export type IntranetProjectSourceId = IntranetProjectSource;
