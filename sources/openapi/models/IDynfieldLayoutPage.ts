/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IDynfieldLayoutPage = {
    /**
     * URL of the page
     */
    page_url?: string;
    /**
     * type of object the layout is associated with
     */
    object_type?: string;
    /**
     * type of the layout
     */
    layout_type?: string;
    /**
     * is page default?
     */
    default_p?: boolean;
};

