/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IDynviewField = {
    /**
     * Identifier for that field. Usually same as 'variable_name'
     */
    field_name?: string;
    /**
     * Actual name which belongs to the id
     */
    field_id?: string;
    /**
     * Actual name which belongs to the id
     */
    field_value?: string;
    /**
     * Internationalized value to display
     */
    field_display_value?: string;
};

