/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { InvoiceItem } from './InvoiceItem';

export type IInvoiceItemsSortOrderPut = {
    /**
     * array of integers appearing in new order
     */
    invoice_items_sort_order: InvoiceItem;
};

