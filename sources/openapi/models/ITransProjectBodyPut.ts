/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Id } from './Id';
import type { ImCompanyId } from './ImCompanyId';
import type { ImProjectId } from './ImProjectId';
import type { IntranetExperienceLevel } from './IntranetExperienceLevel';
import type { IntranetProjectSource } from './IntranetProjectSource';
import type { IntranetProjectStatus } from './IntranetProjectStatus';
import type { IntranetProjectType } from './IntranetProjectType';
import type { IntranetTranslationLanguage } from './IntranetTranslationLanguage';
import type { IntranetTranslationLanguageIds } from './IntranetTranslationLanguageIds';
import type { IntranetTranslationSubjectArea } from './IntranetTranslationSubjectArea';
import type { PersonId } from './PersonId';

export type ITransProjectBodyPut = {
    /**
     * Name of the project. Defaults to project_nr if not provided.
     */
    project_name?: string;
    /**
     * Number of the project. Mostly automatically determined now though
     */
    project_nr?: string;
    /**
     * Status_id of the project
     */
    project_status_id?: IntranetProjectStatus;
    /**
     * Project which shall be the parent for this one.
     */
    parent_id?: ImProjectId;
    /**
     * Company in which to create the project
     */
    company_id?: ImCompanyId;
    /**
     * Project Type of the project to create. Defaults to trans+edit (87)
     */
    project_type_id?: IntranetProjectType;
    /**
     * Source Language for the project. Typically the language the source material is in
     */
    source_language_id?: IntranetTranslationLanguage;
    /**
     * Language into which this task is translated to. Leaving it empty will use the projects target languages
     */
    target_language_ids?: IntranetTranslationLanguageIds;
    /**
     * Subject Area of the project (determines pricing among other things)
     */
    subject_area_id?: IntranetTranslationSubjectArea;
    /**
     * Final company for this translation (determines the reference material and Termbase / Translation Memory to use)
     */
    final_company_id?: ImCompanyId;
    /**
     * Project Manager of this project
     */
    project_lead_id?: PersonId;
    /**
     * Contact in the company who requested this project
     */
    customer_contact_id?: PersonId;
    /**
     * Required experience level for this project. Used (previously) to differentiate certified translations
     */
    language_experience_level_id?: IntranetExperienceLevel;
    /**
     * How many days will this project take
     */
    processing_time?: number;
    /**
     * Reference number of the client of this project
     */
    company_project_nr?: string;
    /**
     * Source of the project.
     */
    project_source_id?: IntranetProjectSource;
    /**
     * Date when project starts
     */
    start_date?: string;
    /**
     * Date for the deadline
     */
    end_date?: string;
    /**
     * Date to contact again
     */
    contact_again_date?: string;
    /**
     * Complexity level of the project. Important for prices.
     */
    complexity_type_id?: Id;
    /**
     * description of the project. Usually Customer facing
     */
    description?: string;
};

