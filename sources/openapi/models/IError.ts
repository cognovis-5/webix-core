/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IError = {
    /**
     * Parameter which caused the error
     */
    parameter?: string;
    /**
     * ID of the object which caused the error
     */
    object_id?: number;
    /**
     * Message of the error
     */
    err_msg?: string;
};

