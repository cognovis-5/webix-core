/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ReferralSource } from './ReferralSource';

export type ReferralSourceId = ReferralSource;
