/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IGetUpdateSystem = {
    /**
     * Key of the package which can be updated
     */
    package_key?: string;
    /**
     * Name of the package (more extensive description)
     */
    package_name?: string;
    /**
     * Version number of the new package
     */
    new_version?: string;
    /**
     * Version number of the currently installed package
     */
    installed_version?: string;
};

