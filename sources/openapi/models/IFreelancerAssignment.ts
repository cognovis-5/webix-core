/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICategoryArray } from './ICategoryArray';
import type { ICategoryObject } from './ICategoryObject';
import type { ICrFile } from './ICrFile';
import type { INamedId } from './INamedId';

export type IFreelancerAssignment = {
    /**
     * name and id for the assignment
     */
    assignment?: INamedId;
    /**
     * Name and ID of the assignee
     */
    assignee?: INamedId;
    /**
     * Package/Batch for which the assignment is used for.
     */
    freelance_package?: INamedId;
    /**
     * Task Type (like edit / proof / ..) for the assignment
     */
    assignment_type?: ICategoryObject;
    /**
     * Status of the assignment
     */
    assignment_status?: ICategoryObject;
    /**
     * Number of units for this assignment
     */
    assignment_units?: number;
    /**
     * Price per unit for the assignment
     */
    rate?: number;
    /**
     * Unit of measure
     */
    uom?: ICategoryObject;
    /**
     * Purchase Order for this assignment
     */
    purchase_order?: INamedId;
    /**
     * Provider Bill (credit note) for this assignment (usually present once the assignment is finished)
     */
    provider_bill?: INamedId;
    /**
     * Time when the assignment will be ready to start working on. Defaults to now
     */
    start_date?: string;
    /**
     * Deadline until which the assignment needs to be delivered
     */
    assignment_deadline?: string;
    /**
     * Comment on the package
     */
    package_comment?: string;
    /**
     * Project information which we return
     */
    project?: INamedId;
    /**
     * Project Nr of the project
     */
    project_nr?: string;
    /**
     * Project lead for this project
     */
    project_lead?: INamedId;
    /**
     * Project folder we hopefully have created.
     */
    project_folder?: INamedId;
    /**
     * Final Customer for this project - defaults to company if no final customer is defined.
     */
    final_company?: INamedId;
    /**
     * Files provided by the PM for this assignment
     */
    package_files?: Array<ICrFile>;
    /**
     * Files provided by the Freelancer for this assignment
     */
    assignment_files?: Array<ICrFile>;
    /**
     * Source language for this assignment
     */
    source_language?: ICategoryObject;
    /**
     * Target language of this assignment
     */
    target_language?: ICategoryObject;
    /**
     * Subject Area for this project
     */
    subject_area?: ICategoryObject;
    /**
     * List of Business Sectors (in addition to subject_area) the freelancer has experience.
     */
    skill_business_sector?: ICategoryArray;
};

