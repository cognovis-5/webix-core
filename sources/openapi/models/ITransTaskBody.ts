/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetProjectType } from './IntranetProjectType';
import type { IntranetTranslationLanguageIds } from './IntranetTranslationLanguageIds';
import type { IntranetUom } from './IntranetUom';

export type ITransTaskBody = {
    /**
     * Name of the task - typically the name of the file to be translated
     */
    task_name: string;
    /**
     * Units for the task (used for freelancer assignments)
     */
    task_units?: number;
    /**
     * Billable Units for the task (to be used in quotes & invoices) - defaults to task_units
     */
    billable_units?: number;
    /**
     * Unit of measure for the task
     */
    task_uom_id: IntranetUom;
    /**
     * Language into which this task is translated to. Leaving it empty will use the projects target languages
     */
    target_language_ids?: IntranetTranslationLanguageIds;
    /**
     * Type of task (by default the one from the project). You can read the steps involved based of the aux_int1 of the category
     */
    task_type_id?: IntranetProjectType;
    /**
     * Deadline until when the CUSTOMER wants to have the task delivered back
     */
    task_deadline?: string;
};

