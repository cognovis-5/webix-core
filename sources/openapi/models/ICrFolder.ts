/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { INamedId } from './INamedId';

export type ICrFolder = {
    /**
     * Folder name of the folder
     */
    folder?: INamedId;
    /**
     * UNIX Time When was the file last updated
     */
    date?: number;
    /**
     * Filesize of the file in bytes.
     */
    size?: number;
    /**
     * Folder name of the parent # @return sub_folders json_object cr_folder Folders contained in this folder
     */
    parent?: INamedId;
};

