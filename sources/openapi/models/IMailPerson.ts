/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { INamedId } from './INamedId';

export type IMailPerson = {
    /**
     * Person details if this is a user in the system
     */
    person?: INamedId;
    /**
     * EMail of the mailer (recipient/sender)
     */
    email?: string;
    /**
     * Type of email with regards to this mail (to,from,cc,bcc)
     */
    type?: string;
};

