/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetSkillRole } from './IntranetSkillRole';

export type IntranetSkillRoleIds = Array<IntranetSkillRole>;
