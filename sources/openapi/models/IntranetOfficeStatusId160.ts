/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetOfficeStatus } from './IntranetOfficeStatus';

export type IntranetOfficeStatusId160 = IntranetOfficeStatus;
