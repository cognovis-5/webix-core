/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ImCompanyId } from './ImCompanyId';
import type { IntranetTranslationLanguage } from './IntranetTranslationLanguage';
import type { PersonId } from './PersonId';

export type IMainTranslatorBody = {
    /**
     * User (freelancer) who will become main translator for give company
     */
    freelancer_id: PersonId;
    /**
     * Company for which user will become main translator
     */
    customer_id: ImCompanyId;
    /**
     * Source Language for which use will become main translator
     */
    source_language_id: IntranetTranslationLanguage;
    /**
     * Target Language for which use will become main translator
     */
    target_language_id: IntranetTranslationLanguage;
};

