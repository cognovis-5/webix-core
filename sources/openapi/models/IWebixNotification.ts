/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICategoryObject } from './ICategoryObject';
import type { ICognovisObject } from './ICognovisObject';
import type { INamedId } from './INamedId';
import type { IWebixNotificationAction } from './IWebixNotificationAction';

export type IWebixNotification = {
    /**
     * The notification object, especially the ID is of note here
     */
    notification?: INamedId;
    /**
     * Who is receiving the notification
     */
    recipient?: INamedId;
    /**
     * Type of notification we are looking at
     */
    notification_type?: ICategoryObject;
    /**
     * Status of the notification we are looking at
     */
    notification_status?: ICategoryObject;
    /**
     * Color of the notification. By default derived from the status
     */
    notification_color?: string;
    /**
     * When was the notification viewed (if at all)
     */
    viewed_date?: string;
    /**
     * Text we are displaying for the notification. Default comes form the notification_type
     */
    message?: string;
    /**
     * Object which caused the notification (context)
     */
    context?: ICognovisObject;
    /**
     * Project in which the notification occured
     */
    project?: ICognovisObject;
    /**
     * ProjectNr of the project (as the name usually is too long)
     */
    project_nr?: string;
    /**
     * Actions for the notification
     */
    actions?: Array<IWebixNotificationAction>;
};

