/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IGroup } from './IGroup';
import type { IUser } from './IUser';

export type IUserWithRoles = {
    /**
     * User who is a member of the object
     */
    user?: IUser;
    /**
     * array of user roles
     */
    user_roles?: Array<IGroup>;
};

