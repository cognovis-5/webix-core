/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IReportModuleBody = {
    /**
     * Configuration of the report
     */
    config: string;
    /**
     * Name of the Report
     */
    report_name: string;
};

