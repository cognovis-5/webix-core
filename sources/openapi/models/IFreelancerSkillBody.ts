/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntegerInt32 } from './IntegerInt32';
import type { IntranetExperienceLevel } from './IntranetExperienceLevel';
import type { IntranetSkillType } from './IntranetSkillType';
import type { PersonId } from './PersonId';

export type IFreelancerSkillBody = {
    /**
     * Freelancer posessing the skill
     */
    user_id: PersonId;
    /**
     * Skill which is required
     */
    skill_id: IntegerInt32;
    /**
     * Skill type of the required skill
     */
    skill_type_id: IntranetSkillType;
    /**
     * Level of experience claimed
     */
    claimed_experience_id?: IntranetExperienceLevel;
    /**
     * Level of experience confirmed
     */
    confirmed_experience_id?: IntranetExperienceLevel;
    /**
     * Person who confirmed the experience
     */
    confirmation_user_id?: PersonId;
};

