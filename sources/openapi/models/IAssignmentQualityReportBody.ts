/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetTranslationSubjectArea } from './IntranetTranslationSubjectArea';

export type IAssignmentQualityReportBody = {
    /**
     * Comment provided with the quality report
     */
    rating_comment?: string;
    /**
     * Subject Areas for this project
     */
    subject_area_id?: IntranetTranslationSubjectArea;
};

