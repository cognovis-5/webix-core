/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntegerInt32 } from './IntegerInt32';
import type { IntranetAbsenceStatus } from './IntranetAbsenceStatus';
import type { IntranetAbsenceType } from './IntranetAbsenceType';
import type { PersonId } from './PersonId';
import type { StringDate } from './StringDate';

export type IUserAbsenceBody = {
    /**
     * Name of the absence
     */
    absence_name: string;
    /**
     * Information about the person who was / will be absent
     */
    owner_id?: PersonId;
    /**
     * Group for which we want to add the absence
     */
    group_id?: IntegerInt32;
    /**
     * When is the absence starting
     */
    start_date: StringDate;
    /**
     * when is absence ending
     */
    end_date: StringDate;
    /**
     * what kind of absence do we have
     */
    absence_type_id?: IntranetAbsenceType;
    /**
     * what is current status of absence
     */
    absence_status_id?: IntranetAbsenceStatus;
    /**
     * description (reason) for the absence
     */
    description?: string;
    /**
     * how can the user be reached
     */
    contact_info?: string;
};

