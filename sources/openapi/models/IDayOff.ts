/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICategoryObject } from './ICategoryObject';

export type IDayOff = {
    /**
     * Date which is off
     */
    day?: string;
    /**
     * Hex Code for the color of the absence
     */
    color?: string;
    /**
     * what kind of absence do we have
     */
    absence_type?: ICategoryObject;
};

