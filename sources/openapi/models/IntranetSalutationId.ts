/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetSalutation } from './IntranetSalutation';

export type IntranetSalutationId = IntranetSalutation;
