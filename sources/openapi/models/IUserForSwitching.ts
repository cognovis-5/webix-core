/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IGroup } from './IGroup';
import type { INamedId } from './INamedId';

export type IUserForSwitching = {
    /**
     * User which we want to switch into
     */
    user?: INamedId;
    /**
     * locale of the user
     */
    locale?: string;
    /**
     * array of groups the user is a member of.
     */
    profiles?: Array<IGroup>;
};

