/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IAssignmentRating } from './IAssignmentRating';
import type { ICategoryObject } from './ICategoryObject';
import type { INamedId } from './INamedId';

export type IAssignmentQualityReport = {
    /**
     * Report we return
     */
    report?: INamedId;
    /**
     * name and id for the assignment
     */
    assignment?: INamedId;
    /**
     * Name and ID of the assignee being rated
     */
    assignee?: INamedId;
    /**
     * Project information which we return
     */
    project?: INamedId;
    /**
     * Project Nr of the project
     */
    project_nr?: string;
    /**
     * Task type for which we are looking up the # of worked with. Corresponds to the workflow steps.
     */
    task_type?: ICategoryObject;
    /**
     * Comment provided with the quality report
     */
    rating_comment?: string;
    /**
     * Subject Areas for this project
     */
    subject_area?: ICategoryObject;
    /**
     * Average Rating for the assignment of the assignee
     */
    rating_value?: number;
    /**
     * Detailed Rating information
     */
    rating?: Array<IAssignmentRating>;
};

