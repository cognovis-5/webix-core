/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IDynfieldPermission = {
    /**
     * id of the dynfield (im_dynfield_attributes)
     */
    dynfield_id?: number;
    /**
     * name of the dynfield (im_dynfield_attributes)
     */
    dynfield_name?: string;
    /**
     * type of object the attribute is associated with
     */
    object_type?: string;
    /**
     * id of the group
     */
    group_id?: number;
    /**
     * name of the privilege
     */
    privilege?: string;
};

