/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ICategoryObject = {
    /**
     * category_id of the category
     */
    id?: number;
    /**
     * Translated category name
     */
    name?: string;
    /**
     * HTML for color or icon (like fas fa-icon)
     */
    icon_or_color?: string;
};

