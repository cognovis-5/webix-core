/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICategoryObject } from './ICategoryObject';
import type { ICognovisObject } from './ICognovisObject';
import type { INamedId } from './INamedId';
import type { ITransTask } from './ITransTask';

export type IFreelancePackage = {
    /**
     * Package/Batch for which the assignment is used for.
     */
    freelance_package?: INamedId;
    /**
     * Trans Task Type (workflow step) of this package
     */
    package_type?: ICategoryObject;
    /**
     * Does this package have assignments (so you might want to look them up)
     */
    has_assignments?: boolean;
    /**
     * Total number of units in this package
     */
    units?: number;
    /**
     * Unit of measure. Hopefully a package was not created with different UOM ids...
     */
    uom?: ICategoryObject;
    /**
     * Target language for which this package is used.
     */
    target_language?: ICategoryObject;
    /**
     * End Date when the task needs to be delivered to the customer
     */
    package_deadline?: string;
    /**
     * Comment for the package. Different from the assignment comments
     */
    package_comment?: string;
    /**
     * Array of tasks included in this package
     */
    tasks?: Array<ITransTask>;
    /**
     * Project in which the notification occured
     */
    project?: ICognovisObject;
};

