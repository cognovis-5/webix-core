/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetProjectStatus } from './IntranetProjectStatus';

export type IntranetProjectStatusId = IntranetProjectStatus;
