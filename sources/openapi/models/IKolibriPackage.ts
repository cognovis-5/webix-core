/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICategoryObject } from './ICategoryObject';
import type { INamedId } from './INamedId';

export type IKolibriPackage = {
    /**
     * Package into which we imported the file
     */
    freelance_package?: INamedId;
    /**
     * name of the file which we imported into the package
     */
    trados_filename?: string;
    /**
     * Was the import successful
     */
    success?: boolean;
    /**
     * Task Type to return
     */
    task_type?: ICategoryObject;
    /**
     * Error message if the import was not successful. Otherwise might contain additional useful information
     */
    message?: string;
};

