/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IUserError404 = {
    /**
     * Property (element) which caused the error
     */
    property?: string;
    /**
     * Error message from the server
     */
    message?: string;
};

