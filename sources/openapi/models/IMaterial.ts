/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICategoryObject } from './ICategoryObject';
import type { INamedId } from './INamedId';

export type IMaterial = {
    /**
     * Material to return
     */
    material?: INamedId;
    /**
     * Material Nr.
     */
    material_nr?: string;
    /**
     * Type of material
     */
    material_type?: ICategoryObject;
    /**
     * Status of the material (e.g. can we use it)
     */
    material_status?: ICategoryObject;
    /**
     * Description of the material
     */
    description?: string;
    /**
     * Unit of Measure for the material
     */
    material_uom?: ICategoryObject;
    /**
     * Is the material billable?
     */
    material_billable_p?: boolean;
    /**
     * which is the source language for this material
     */
    source_language?: ICategoryObject;
    /**
     * target language (if any)
     */
    target_language?: ICategoryObject;
    /**
     * Subject area for the translation
     */
    subject_area?: ICategoryObject;
    /**
     * what type of action do we make for this material
     */
    task_type?: ICategoryObject;
    /**
     * File type we work on
     */
    file_type?: ICategoryObject;
};

