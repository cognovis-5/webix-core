/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum IntranetReportStatus {
    ACTIVE = 15000,
    DELETED = 15002,
}
