import { CognovisRestService, ITranslation } from "../openapi/index";
import { injectable } from "tsyringe";

@injectable()
export default class CognovisI18n {
    getTranslations(): Promise<ITranslation[]> {
        return CognovisRestService.getI18N({ packageKey: "webix-portal,cognovis-rest,cognovis-core", locale:"all"});
    }
}
