import { CognovisPleaseWaitWindow } from "../../services/cognovis-please-wait-window";
import CognovisBasicModal from "../../modules/cognovis-basic-modal/cognovis-basic-modal";
import { container } from "tsyringe";
import { i18nHelper } from "../../modules/i18n-helper/i18n-helper";
import {CognovisRestDynamicService, IViewBody} from "../../../sources/openapi";

export default class DynviewsEditModal extends CognovisBasicModal {

    viewId:number;
    idPrefix = "evm";  // short for EditViewModal
    cognovisPleaseWaitWindow: CognovisPleaseWaitWindow;
    modalTitle: string;
    afterSubmitAction: () => void;
    

    config(): webix.ui.windowConfig {
        const mainLayout = super.getMainLayout(" ", 960, 720);  // adjust size if needed
        this.cognovisPleaseWaitWindow = container.resolve(CognovisPleaseWaitWindow);
        return mainLayout;
    }

    openModal(viewId?:number, afterSubmitAction?: () => void): void {
        this.modalTitle = i18nHelper.getTranslation("Create new view");
        const modalContent = this.getContent();
        const actionButtons = this.getActionButtons();
        this.setContent(modalContent, actionButtons, this.modalTitle);
        this.cognovisPleaseWaitWindow.hide();          
        const modal = (this.getRoot() as webix.ui.window);
        modal.show(); 
        if(viewId) {
            this.viewId = viewId;
            this.setFormValues();
        }
        if(afterSubmitAction) {
            this.afterSubmitAction = afterSubmitAction;
        }
    }

    getContent(): webix.ui.layoutConfig {
        // Define your form's layout
        const layout = {
            view: "scrollview",
            body: {
                padding: 13,
                rows: [
                    {
                        view: "form",
                        id: `${this.idPrefix}DynviewEditViewForm`,
                        elements: [
                            { 
                                view: "text", 
                                label: i18nHelper.getTranslation("View name"), 
                                labelWidth: 160,
                                name: "view_name" 
                            },
                            { 
                                view: "text", 
                                label: i18nHelper.getTranslation("View label"), 
                                labelWidth: 160,
                                name: "view_label" 
                            },
                            { 
                                view: "combo", 
                                label: i18nHelper.getTranslation("View type"), 
                                labelWidth: 160,
                                name: "view_type_id" ,
                                options:[
                                    { id: "1415", value: "Ajax" },
                                    { id: "1400", value: "Object List" },
                                    { id: "1405", value: "Object View" },
                                    { id: "1410", value: "Backup" },
                                    { id: "1420", value: "Webix Dynviews" },
                                    { id: "1422", value: "Reports" },
                                    { id: "1430", value: "Freelancer projects" },
                                    { id: "1450", value: "List - Invoice" },
                                    { id: "1451", value: "List - Project" }
                                ]
                            },
                            { 
                                view: "text", 
                                label: i18nHelper.getTranslation("Visible for"),
                                labelWidth: 160,
                                name: "visible_for" 
                            },
                            { 
                                view: "text", 
                                label: i18nHelper.getTranslation("Sort order"),
                                labelWidth: 160,
                                name: "sort_order", 

                            },
                            { 
                                view: "textarea", 
                                label: i18nHelper.getTranslation("View SQL"),
                                labelWidth: 160,
                                height:160,
                                name: "view_sql" 
                            },
                            { 
                                view: "textarea", 
                                label: i18nHelper.getTranslation("JSON configuration"),
                                labelWidth: 160,
                                height:160,
                                name: "json_configuration" 
                            }
                        ]
                    }
                ]
            }
        };
        return layout;
    }

    setFormValues():void {
        const form = webix.$$(`${this.idPrefix}DynviewEditViewForm`) as webix.ui.form;
        CognovisRestDynamicService.getViews({viewIds:`${this.viewId}`})
        .then(views => {
            if(views[0]) {
                const view = views[0];
                this.setTitle(view.view_name);
                Object.entries(view).forEach(([key, value]) => {
                    const element = form.queryView({name:key});
                    if(element) {
                        element.setValue(value);
                    }
                });
            }
        });
    }

    getActionButtons(): webix.ui.layoutConfig {
        const buttons = {
            view: "layout",
            padding: 0,
            cols: [
                {
                    view: "button",
                    value: `${i18nHelper.getTranslation('Close')}`,
                    click: () => {
                        this.hide();
                    }
                },
                {
                    view: "button",
                    localId: `${this.idPrefix}SaveButton`,
                    value: `${i18nHelper.getTranslation('Save')}`,
                    click: () => {
                        this.submit();
                    }
                }
            ]
        };
        return buttons;
    }

    submit(): void {
        this.cognovisPleaseWaitWindow.show({ message: i18nHelper.getTranslation("Please_wait") });
        const form = webix.$$(`${this.idPrefix}DynviewEditViewForm`) as webix.ui.form;
        const values = form.getValues();
        // Here we need PUT / POST login for dynviews
        if(this.viewId) {
            CognovisRestDynamicService.putView({
                viewId: this.viewId,
                requestBody: values as IViewBody
            })
                .then(result => {
                    this.afterSubmitAction();
                    this.cognovisPleaseWaitWindow.hide();
                    this.hide();
                });
        } else {
            CognovisRestDynamicService.postView({requestBody: values as IViewBody})
                .then(result => {
                    this.afterSubmitAction();
                    this.cognovisPleaseWaitWindow.hide()
                    this.hide();
                });
        }
        this.cognovisPleaseWaitWindow.hide();
    }
}

