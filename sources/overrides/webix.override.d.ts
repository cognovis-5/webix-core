/* eslint-disable @typescript-eslint/no-unused-vars */

declare namespace webix {
  function ajax(): webix.Ajax;
  function ajax(url: string, params?: { body: string }): Promise<unknown>;
}