import HtmlTemplateHelper from "./html-template-helper";

describe("html-template-helper", function() {

    const htmlHelperInstance = new HtmlTemplateHelper();

    it("should return template containing Albert and Einstein", function() {

        const templateToParse = "<div class='some-class'>{{firstName}} {{lastName}}</div>";
        const replacementParams = {
            firstName:"Albert",
            lastName:"Einstein"
        };
        const parsedTemplate = htmlHelperInstance.replaceParams(templateToParse,replacementParams);
        expect(parsedTemplate).toContain(replacementParams.firstName);
        expect(parsedTemplate).toContain(replacementParams.lastName);
        expect(parsedTemplate).not.toContain("{{firstName}}");
        expect(parsedTemplate).not.toContain("{{lastName}}");

    });

});