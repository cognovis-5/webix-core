import { CognovisPleaseWaitWindow } from "../../services/cognovis-please-wait-window";
import { container } from "tsyringe";
import { i18nHelper } from "../i18n-helper/i18n-helper";
import { CognovisRestDynamicService, IDynamicAttribute } from "../../openapi";
import { CognovisCategory } from "../cognovis-category/cognovis-category";
import { IJetApp, JetView } from "webix-jet";


export default class DynfieldBasicForm extends JetView {

    idPrefix = "dbf";
    firstFocusFieldName:string;
    dynfields:IDynamicAttribute[] = [];
    cognovisPleaseWaitWindow: CognovisPleaseWaitWindow;
    dynfieldsConfig:IDynfieldSetup;
    predefinedFields:webix.ui.baseviewConfig[];
    customRules:{field:string, rule:Function}[] = [];
    customEvents:{[key: string]: string | number | Function}[] = [];
    customPlusSignActions:{[key: string]: string | number | Function}[] = [];
    submitAction:(data:unknown, callback?:() => void) => void;
    customAfterSubmitAction:(data:unknown, callback?:() => void) => void;

    constructor(app:IJetApp, dynfieldsConfig:IDynfieldSetup, predefinedFields:webix.ui.baseviewConfig[]) {
        super(app, {});
        this.dynfieldsConfig = dynfieldsConfig;
        this.predefinedFields = predefinedFields;
    }

    config():webix.ui.scrollviewConfig {
        //this.cognovisPleaseWaitWindow = container.resolve(CognovisPleaseWaitWindow);
        const mainLayout = this.getContent();
        return mainLayout;
    }

    init():void {
        const totalNumberOfDynfieldSetups = this.dynfieldsConfig.pages.length;
        this.dynfieldsConfig.pages.map((dynfieldSetup, index) => {
            this.getDynfields(dynfieldSetup.objectType, dynfieldSetup.pageUrl, dynfieldSetup.objectSubtypeId)
            .then(dynfields => {
                const temp = [];
                dynfields.map(df => {
                    temp.push(df);
                });
                this.dynfields = this.dynfields.concat(temp);
                if((index + 1) >= totalNumberOfDynfieldSetups) {
                    this.rebuildForms(this.dynfieldsConfig);
                };
            });
        });
    }

    bindCustomEvents():void {
        const root = this.getRoot() as webix.ui.layout;
        const form = root.queryView({view:"form"}) as webix.ui.form;
        this.customEvents.map(event => {
            const field = form.queryView({name:event["field"]});
            if(field) {
                field.attachEvent(event["type"], event["action"]);
            }
        });   
    }

    updateButtonLabel(buttonId:string, newLabel:string) {
        const button = this.$$(`${this.idPrefix}${buttonId}`) as webix.ui.button;
        if(button) {
            button.define("value", newLabel);
            button.refresh();
        }
    }

    extractValuesFromObject<T>(object:T):{[key: string]: string} {
        const newObj = {};
        Object.keys(object).forEach(key => {
            if(object[key]) {
                if(object[key]["id"]) {
                    newObj[key] = object[key].id.toString();
                    newObj[`${key}_id`] = object[key].id.toString();
                    newObj[`${key}_name`] = object[key].name.toString();   
                } else {
                    newObj[key] = object[key];
                }
            }
        });
        return newObj
    }

    afterShow():void {
        // Nothing here
        // Just a blueprint
    }

    submit():void {
        // Nothing here
        // Just a blueprint
    }

    afterSubmit<T>(obj:T):void {
        // Nothing here
        // Just a blueprint
    }

    rebuildForms(dynfieldsConfig:IDynfieldSetup):void {
        const totalNumberOfDynfieldSetups = dynfieldsConfig.pages.length;
        dynfieldsConfig.pages.map((dynfieldSetup, index) => {
            this.getDynfields(dynfieldSetup.objectType, dynfieldSetup.pageUrl, dynfieldSetup.objectSubtypeId)
            .then(dynfields => {
                const temp = [];
                dynfields.map(df => {
                    temp.push(df);
                });
                this.dynfields = this.dynfields.concat(temp);
                if((index + 1) >= totalNumberOfDynfieldSetups) {
                    const container = this.$$(`${this.idPrefix}DynfielCreationModalForm`) as webix.ui.layout;
                    const elements = [
                        {
                            cols:[
                                {
                                    view:"layout",
                                    rows:this.buildFields(0),
                                },
                                {
                                    view:"spacer",
                                    width:120
                                },
                                {
                                    view:"layout",
                                    rows:this.buildFields(1)
                                }
                            ]
                        }
                    ];
                    container.define("elements", elements);
                    container.reconstruct();
                    this.afterShow();
                    this.dynfields = [];
                    this.bindCustomEvents();
                };
            });
        });
    }

    getContent():webix.ui.layoutConfig {
       //this.cognovisPleaseWaitWindow.show({ message: i18nHelper.getTranslation("Please_wait")});
        const layout = {
            view:"scrollview",
            body:{
                padding:13,
                rows:[
                    {
                        view:"layout",
                        gravity:1,
                        localId:`${this.idPrefix}DynfieldCreationModalContainer`,
                        rows:[
                            {
                                view:"form",
                                borderless:true,
                                localId:`${this.idPrefix}DynfielCreationModalForm`,
                                elements:[],
                                rules:this.getFormRules()
                            }
                        ]
                    }                   
                ]
            }
        };
        return layout
    }

    getDynfields(objectType:string, pageUrl:string, objectSubtypeId?:number):Promise<IDynamicAttribute[]> {
        let params = {
            objectType:objectType,
            pageUrl:pageUrl
        }
        if(objectSubtypeId) {
            params["objectSubtypeId"] = objectSubtypeId;
        }
        return CognovisRestDynamicService.getDynamicAttributes(params)    
        .then(dynfields => {
            const sortedBySortOrder = dynfields.sort((a,b) => (a["sort_order"] > b["sort_order"]) ? 1 : ((b["sort_order"] > a["sort_order"]) ? -1 : 0))
            // Useful
            const fakeSectionHeadings = ["Personal Data", "Address", "Other"];
            dynfields = dynfields.map(field => {
                const random = Math.floor(Math.random() * 3);
                field.dynamic_subtypes.map(subtpye => {
                    subtpye.section_heading = fakeSectionHeadings[random];
                });
                return field;
            });
            return dynfields
        });
    }

    getFormRules():{[key:string]:() => boolean} {
        const rules = {};
        this.dynfields.map(dynfield => {
            let fieldIsRequired = false;
            dynfield.dynamic_subtypes.map(subtype => {
                if(subtype.required_p) {
                    fieldIsRequired = true;
                }
            });
            if(fieldIsRequired) {
                rules[dynfield.attribute.name] = webix.rules.isNotEmpty;
            }
        });
        this.customRules.map(rule => {
            rules[rule.field] = rule.rule;     
        });
        return rules;
    }

    buildFields(whichSet:number):webix.ui.baseviewConfig[] {
        const fieldsArr = [];
        this.dynfields = this.executeSubtypesFiltering(this.dynfields, "display");
        const countDynfields = this.dynfields.length;
        this.dynfields.map((field, index) => {
            const newField = this.buildSpecificType(field);
            if(newField) {
                fieldsArr.push(newField);
            }
        });
        this.predefinedFields.map(predefinedField => {
            const newPredefinedField = this.getSingleFieldContainer(predefinedField);
            if(newPredefinedField) {
                fieldsArr.push(newPredefinedField);
            }
        });
        // Now we still need to make checks against subtypes
        const half = Math.ceil(fieldsArr.length / 2); 
        const firstHalf = fieldsArr.slice(0, half);
        const secondHalf = fieldsArr.slice(-half);
        const both = [firstHalf, secondHalf];
        return both[whichSet]
    }

    executeSubtypesFiltering(dynfields:IDynamicAttribute[], neededDisplayMode:string):IDynamicAttribute[] {
        // We need to loop thru all subtypes of all dynfields to decide wheter such type of object should have such field.
        let filteredDynfields:IDynamicAttribute[] = [];
        const alreadyUsed = [];
        this.dynfieldsConfig.pages.map(dynfieldSetup => {
            switch(dynfieldSetup.objectType) {
                default:
                    dynfields.map(dynfield => {
                        let addFlag = false;
                        dynfield.dynamic_subtypes.map(subtype => {
                            if(this.hasProperDisplayMode(neededDisplayMode,subtype.display_mode)) {
                                addFlag = true;
                            }
                        });
                        if(addFlag && alreadyUsed.indexOf(dynfield.attribute.id) === -1) {
                            filteredDynfields.push(dynfield);
                            alreadyUsed.push(dynfield.attribute.id);
                        }
                    });
                    break;
            }
        });
        return filteredDynfields
    }

    hasProperDisplayMode(neededDisplayMode:string, subtypeDisplayMode:string):boolean {
        let final = false;
        switch(neededDisplayMode) {
            case "none":
                break;
            case "display":
                if(subtypeDisplayMode === "display" || subtypeDisplayMode === "edit") {
                    final = true;
                }
                break;
            case "edit":
                if(subtypeDisplayMode === "edit") {
                    final = true;
                }
                break;
            default:
                break;
        }
        return final
    }

    makeReadonly():void {
        const root = this.getRoot() as webix.ui.layout;
        const form = root.queryView({view:"form"}) as webix.ui.form;
        for (var key in form.elements) {
            const element = form.elements[key];
            element.define("disabled", true);
            element.refresh();
        };
    }

    getActionButtons():webix.ui.layoutConfig {
        const buttons = 
                {
                    view:"layout",
                    padding:0,
                    cols:[
                        {
                            view:"button",
                            localId:`${this.idPrefix}SaveButton`,
                            value:`${i18nHelper.getTranslation(`Save`)}`,
                            click:() => {
                                this.submit();
                            }
                        }
                    ] 
                };
        return buttons;
    }

    buildSpecificType(field:IDynamicAttribute):webix.ui.layoutConfig {
        // Figure out value
        let value:number | string = "";      
        const component = this.getWebixComponent(field, value as string);
        if(component) {
            const container = this.getSingleFieldContainer(component);
            return container
        }
    }
    
    getSingleFieldContainer(field:webix.ui.layoutConfig):webix.ui.layoutConfig {
        return {
            view:"layout",
            rows:[
                {
                    view:"spacer",
                    height:4
                },
                {
                    view:"layout",
                    gravity:1,
                    cols:[
                        field,
                        this.getPlusSignAction(field)
                    ]
                },
                {
                    view:"spacer",
                    height:4
                }
            ]
        }
    }

    getPlusSignAction(field:webix.ui.layoutConfig):webix.ui.layoutConfig {
        const customPlusSignActionDefined = this.customPlusSignActions.find(caField => caField.field === field["name"]);
        if(customPlusSignActionDefined) {
            return {
                view:"button",
                width:20,
                id:`${this.idPrefix}${field["name"]}`,
                type:"icon",
                css:"cog-icon",
                icon:"wxi-plus",
                click:customPlusSignActionDefined.action
            } as unknown as webix.ui.layoutConfig
        } else {
            return {
                view:"spacer",
                width:20
            }
        }
    }

    getWebixComponent(field:IDynamicAttribute, value:string):webix.ui.baseviewConfig {
        let webixComponent = {
            view:field.widget_type,
            name:field.attribute.name,
            label: i18nHelper.getTranslationWithKey(field.display_name, field.i18n_key),
            labelWidth:180,
            value:value,
            placeholder:field.attribute.name
        };
        if(field.widget_type === "datepicker") {
            webixComponent["format"] = "%d.%m.%Y";
        }
        if(field.widget_type === "datetimepicker") {
            webixComponent["view"] = "datepicker";
            webixComponent["format"] = "%d.%m.%Y %H:%i";
            webixComponent["timepicker"] = true;
        }
        if(field.widget_type === "richtext") {
            webixComponent["height"] = 200;
        }
        if(field.widget_data && field.widget_data.length > 0) {
            webixComponent["options"] = field.widget_data;
        }
        if(field.category_type) {
            const suggestBox = {
                body: {
                    url: () => CognovisCategory.getCategory(field.category_type),
                }
            };
            webixComponent["suggest"] = suggestBox;
        }
        if(this.firstFocusFieldName !== "" && webixComponent["name"] === this.firstFocusFieldName) {
            webixComponent["on"] = {
                onAfterRender:function() {
                    setTimeout(() => {
                        this.focus();
                    },100);
                }
            }
        }
        return webixComponent
    }

    validateIfNeeded(form:webix.ui.form):boolean {
        if(!this.dynfieldsConfig[0].checkForRequiredFields) {
            return true;
        } else {
            return form.validate();
        }
    }

    focusOnInput(fieldName:string) {
        const root = this.getRoot() as webix.ui.layout;
        const form = this.$$(`${this.idPrefix}DynfielCreationModalForm`) as webix.ui.form;
        const field = form.elements.find(value => value === fieldName) as webix.ui.text;  
        if(field) {
            field.focus();
        }
    }

}