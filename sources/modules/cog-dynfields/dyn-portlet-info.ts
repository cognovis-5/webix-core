import { IJetApp, JetView } from "webix-jet";
import { CognovisRestDynamicService, IDynamicAttribute, INamedId } from "../../../sources/openapi";
//import { i18nHelper } from "../../sources/modules/i18n-helper/i18n-helper";

export default class DynPortletInfo extends JetView {

    idPrefix = "dpi";
    dynfieldsConfig:{objectType:string, pageUrl:string};
    title:string;
    dynFields:IDynamicAttribute[];

    constructor(app:IJetApp, portletPrefix:string, dynfieldsConfig:{objectType:string, pageUrl:string}, title:string) {
        super(app, {});
        this.dynfieldsConfig = dynfieldsConfig;
        this.idPrefix = `${this.idPrefix}${portletPrefix}`;
        this.title = title;
    }

    config():Promise<webix.ui.layoutConfig> {
        return CognovisRestDynamicService.getDynamicAttributes({
            objectType:this.dynfieldsConfig.objectType,
            pageUrl:this.dynfieldsConfig.pageUrl
        })
        .then(dynFields => {
            this.dynFields = dynFields;
            const layout = this.getLayout();
            return layout;
        });
    }

    getLayout():webix.ui.layoutConfig {
        return {
            view:"layout",
            rows:[
                this.getHeader(), 
                {
                    view:"layout",
                    css:"cog-box-content",
                    padding:13,
                    rows:[
                        ...this.buildFields()
                    ]
                }
            ]
        }
    }

    getHeader():webix.ui.layoutConfig {
        return {
            view:"layout",
            height:44,
            css: "cog-box-header", 
            cols: [
                { 
                    view: "template", 
                    type: "header",
                    css: "cog-box-header",  
                    template: this.title, 
                    borderless: true 
                },
            ]
        };
    }

    buildFields():webix.ui.layoutConfig[] {
        const fieldsArr = [];
        this.dynFields.map(field => {
            const newField = this.buildSpecificType(field.attribute.id, field.display_name, field.widget_type);
            if(newField) {
                fieldsArr.push(newField);
            }
            
        });
        return fieldsArr
    }

    buildSpecificType(attributeId:number, label:string, widgetName:string):webix.ui.layoutConfig {
        const componentTypeName = this.getProperWebixComponentType(widgetName);
        const field = {
            view:componentTypeName,
            name:attributeId,
            label:label,
            labelWidth:250
        };
        const container = this.getSingleFieldContainer(field, 4);
        return container
    }
    
    getSingleFieldContainer(field:webix.ui.layoutConfig, space:number):webix.ui.layoutConfig {
        return {
            view:"layout",
            rows:[
                {
                    view:"spacer",
                    height:space
                },
                field,
                {
                    view:"spacer",
                    height:space
                }
            ]
        }
    }

    getProperWebixComponentType(widgetType:string) {
        const webixComponentTypesMap = this.getWebixComponentTypeMap();
        return webixComponentTypesMap[widgetType];
    }

    getWebixComponentTypeMap():{[key: string]: string} {
        const map = {
            "text":"text",
            "combo":"combo"
        };
        return map;
    }

}