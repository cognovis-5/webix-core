import DateHelper from "./date-helper";

describe("html-template-helper", function() {

    const dateHelper = new DateHelper();

    it("should return 1 as days difference", function() {

        // today
        const dateToday = new Date();
        const dateYesterday = new Date(new Date().setDate(new Date().getDate()-1));
        const daysDifference = dateHelper.getDaysDifference(dateToday,dateYesterday);
        expect(daysDifference).toBe(1);

    });

    // Same as above, the only difference is sequence of passing parameters
    it("should return -1 as days difference", function() {

        // today
        const dateToday = new Date();
        const dateYesterday = new Date(new Date().setDate(new Date().getDate()-1));
        const daysDifference = dateHelper.getDaysDifference(dateYesterday,dateToday);
        expect(daysDifference).toBe(-1);

    });

    it("should return 0 as days difference", function() {

        // today
        const todayDate = new Date();
        const todayDateTwo = new Date();
        const daysDifference = dateHelper.getDaysDifference(todayDateTwo,todayDate);
        expect(daysDifference).toBe(0);

    });

    it("should return 364 as days difference", function() {

        // today
        const someDate = new Date("2020-01-01");
        const someDateTwo = new Date("2019-01-02");
        const daysDifference = dateHelper.getDaysDifference(someDate,someDateTwo);
        expect(daysDifference).toBe(364);

    });

});