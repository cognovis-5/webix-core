
// KOL-1339 Testcase
import { CognovisRestInvoiceService, CognovisRestService, OpenAPI } from "../../../sources/openapi";


describe("testing pdf download", () => {
    before(() => {
        cy.fixture('cypress.env.json')
        .then((testEnv) => {
            // First we authorize to our app
            cy.loginToApp(testEnv.enviromentTestVariables.DEV.admin.email, testEnv.enviromentTestVariables.DEV.admin.password);
            OpenAPI.TOKEN = testEnv.enviromentTestVariables.DEV.admin.api_key;
        });
    });

    // This needs fixing to find the URL and then use download to actually download the PDF
    
    it("should download a PDF for the provider bill", () => {
        cy.fixture('project.env.json')
        .then((projectEnv) => {
            const projectId = projectEnv.projectVariables.DEV.projectId;
            cy.navigateToProjectByProjectId(`open-quotes-table`, projectId);

            cy.navigateFromSidebar("financial-overview.financial-overview", "piSaveContactButton");

            // Get a project with an invoice
            let costName: string
            CognovisRestInvoiceService.getProjectCosts({projectId:projectId,costTypeId: 3704})
            .then(_invoice => {
                expect(_invoice.success).true;
                costName = _invoice.project_quotes[0].cost_name;


                cy.get(`[view_id=freelancer-costs-table]`).find(`[cog_column_name='purchase_order']`).contains(costName)
                .then(po_name => {
                    const href = po_name.prop('href');
                });
            });
        });
    });
});

