///<reference types="cypress"/>

describe("project info test", () => {
    beforeEach(() => {
        cy.loginToEnv("DEV", "customer");
        cy.visit("/projects");
    });
    it("should click a project and check its details in project-info", () => {
        cy.get(".project__name").then((projectName) => {
            cy.wrap(projectName).eq(2).find(".webix_cell").eq(0).dblclick({ force: true });
        });
    });
});
