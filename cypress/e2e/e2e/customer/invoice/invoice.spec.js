import invoiceData from "../../../../fixtures/invoice.data.json";
///<reference types="cypress"/>

describe("invoices test", () => {
    beforeEach(() => {
        cy.loginToEnv("DEV", "customer");
    });
    it("should filter through invoice table", () => {
        cy.get('[webix_tm_id="invoices.invoices"]').click();

        cy.get('input[type="text"]').then((inputField) => {
            cy.wrap(inputField).eq(0).type(invoiceData.invoiceName, { force: true });
            cy.wait(1000);
            cy.wrap(inputField).eq(0).clear();
            cy.wrap(inputField).eq(1).type(invoiceData.invoiceTypeOne, { force: true });
            cy.wait(1000);
            cy.wrap(inputField).eq(1).clear();
            cy.wrap(inputField).eq(5).type(invoiceData.invoiceStatusSend, { force: true });
            cy.wait(1000);
            cy.wrap(inputField).eq(5).clear();
        });
    });
});
