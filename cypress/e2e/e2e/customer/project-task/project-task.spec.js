///<reference types="cypress"/>

describe("Project task view test", () => {
    beforeEach(() => {
        cy.loginToEnv("DEV", "customer");
    });

    it("should test add single task button", () => {
        cy.visit("/project-task");
        cy.get(".form").then((form) => {
            //Click the combo boxes and select the dropdown lists
            cy.wrap(form)
                .find("input")
                .then((combo) => {
                    cy.wrap(combo).eq(0).click();
                    cy.get("[webix_l_id='Upload']").click({ force: true });

                    cy.wrap(combo).eq(1).click();
                    cy.get("[webix_l_id='Quote']").click({ force: true });
                });

            //Add single task
            cy.wrap(form)
                .find(".form-add__single__task")
                .then((addSingleBtn) => {
                    cy.wrap(addSingleBtn).click();
                    cy.wrap(addSingleBtn).click();
                    cy.wrap(addSingleBtn).click();
                    cy.wrap(addSingleBtn).click();
                });
        });

        //Edit the the Task names
        cy.get("[type='text']").then((textInput) => {
            console.log(textInput);
            cy.wrap(textInput).eq(0).type("task_39291");
        });

        //Click the dot button for task confirmation
        cy.get('[class="webix_icon wxi-dots"]').then((confirm) => {
            cy.wrap(confirm).eq(0).click();
        });
    });
});
