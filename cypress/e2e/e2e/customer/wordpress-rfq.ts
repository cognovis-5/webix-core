describe("Wordpress RFQ Tests", () => {

    it("should create an RFQ with multiple target languages", () => {
        cy.visit('https://www.kolibri.online/');
        // Use the "." to identify the class in the DOM model. This is how I could click
        cy.get('.et_pb_button_2').click();
        cy.wait(1000);
        cy.get('.dienstleistung > .wpcf7-form-control').select('Fachübersetzung');
        cy.get('.ausgangssprache > .wpcf7-select').select('Deutsch');
        // Select only works on single select, have to use click on the option instead.
        cy.get('.zielsprachen option:nth-child(10)').click();
        cy.get('.zielsprachen option:nth-child(12)').click();
        // upload the file
        cy.get('.cd-upload-btn').click();
        cy.get('.wpcf7-drag-n-drop-file').attachFile('Dummy.pdf');
        cy.get('.wpcf7-date').type('2021-02-13');
        cy.get('.land > .wpcf7-form-control').select('Deutschland');
        cy.get('.firma > .wpcf7-form-control').type('Test Firma');
        cy.get('.vorname > .wpcf7-form-control').type('Test Vorname');
        cy.get('.nachname > .wpcf7-form-control').type('Test Nachname');
        cy.get('.wpcf7-email').type('test899079@malte.sussdorff.de');
        cy.get('.telefon > .wpcf7-form-control').type('09129');
        cy.get('.anmerkungen').type('This is an automated test case, please ignore');
        // The button is hidden behind cookies, so use force:true here
        cy.get('.wpcf7-list-item-label').click({force:true});
        
        cy.get('.wpcf7-form').submit();
    });


});
