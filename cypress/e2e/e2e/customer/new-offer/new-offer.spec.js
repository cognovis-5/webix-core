import newOfferData from "../../../../fixtures/new-offer-data.json";
///<reference types="cypress"/>

describe("new offer test", () => {
    beforeEach(() => {
        cy.loginToEnv("DEV", "customer");
        cy.contains("New Offer").click();
    });
    it("should check the fields meant for customers", () => {
        //check for Project Name
        cy.contains("Project Name");
        cy.get(".project__name").then((projectName) => {
            expect(projectName).to.not.be.undefined;
            cy.wrap(projectName).type(newOfferData.projectName);
        });

        //check for Project Type
        cy.contains("Project Type");
        cy.get(".project__type").then((projectType) => {
            expect(projectType).to.not.be.undefined;

            cy.wrap(projectType).click();
            cy.get('[webix_l_id="95"]').click();
        });

        //check for Customer Contact
        cy.contains("Customer Contact");
        cy.get(".customer__contact").then((customerContact) => {
            expect(customerContact).to.not.be.undefined;
        });

        //check for Source Language
        cy.contains("Source Language");
        cy.get(".source__language").then((sourceLanguage) => {
            expect(sourceLanguage).to.not.be.undefined;

            cy.wrap(sourceLanguage).click();
            cy.get('[webix_l_id="253"]').click();
        });

        //check for Target Languages
        cy.contains("Target Languages");
        cy.get(".target__language").then((targetLanguage) => {
            expect(targetLanguage).to.not.be.undefined;

            cy.wrap(targetLanguage).click();
            cy.get('[webix_l_id="251"]').click({ multiple: true, force: true });
            cy.get('[webix_l_id="252"]').click({ multiple: true, force: true });
            cy.get('[webix_l_id="253"]').click({ multiple: true, force: true });

            cy.contains("New Offer").click();
        });

        //check for Comment
        cy.contains("Comment");
        cy.get(".textarea").then((commentArea) => {
            expect(commentArea).to.not.be.undefined;
            cy.wrap(commentArea).type(newOfferData.comment);
        });

        //check for Subject Area
        cy.contains("Subject Area");
        cy.get(".subject__area").then((subjectArea) => {
            expect(subjectArea).to.not.be.undefined;

            cy.wrap(subjectArea).click();
            cy.get('[webix_l_id="10000542"]').click();
        });

        //check for Start Date
        cy.contains("Start Date");
        cy.get(".start__date").then((startDate) => {
            expect(startDate).to.not.be.undefined;
        });

        //check for End Date
        cy.contains("End Date");
        cy.get(".end__date").then((endDate) => {
            expect(endDate).to.not.be.undefined;
        });

        //Add files
        const pdf = "Dummy.pdf";
        const png = "Logo.png";
        cy.get('[type="file"]').eq(0).attachFile(pdf);
        cy.get('[type="file"]').eq(1).attachFile(png);
        cy.contains("remove").eq(0).click({ force: true, timeout: 8000 });
        cy.get('[type="file"]').eq(0).attachFile(pdf);

        //Click Submit button
        cy.get(".add__project").click();

        //Click Project sidebar item to check if project is present
        cy.get('[webix_tm_id="projects.projects"]').click();
        cy.get(".project__name")
            .find(".webix_cell")
            .each((projectName) => {
                let newlyCreatedProject = "";
                console.log(projectName);

                if (projectName.text() === newOfferData.projectName) {
                    newlyCreatedProject = projectName.text();
                    expect(newlyCreatedProject).to.not.be.undefined;
                }
            });
    });
});
