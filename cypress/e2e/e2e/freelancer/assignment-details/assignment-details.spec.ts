/// <reference path="../../../../support/index.d.ts" />
import { settings } from "cluster";
import { OpenAPI } from "../../../../../sources/openapi";


const acceptProjectButton = "sadActionsAcceptProject";
const rejectProjectButton = "sadActionsRejectProject";
const downloadOriginalFilesButton = "sadActionsDownloadOriginalFiles";
const downloadBatchFilesButton = "sadActionsDownloadBatchFiles";
const uploadReturnFilesButton = "sadActionsUploadReturnFiles";
const downloadPurchaseOrderButton = "sadActionsDownloadPurchaseOrder";
const setToInProgressButton = "sadActionsSetToInProgress";
const setToWorkDeliveredButton = "sadActionsSetToWorkDelivered";
const downloadCreditNoteButton = "sadDownloadCreditNote";

const correctActionButtonsForStatuses = [
    {
        // 4220 (CREATED)
        assignmentStatusId:4220,
        correctActionButtons:[
            downloadBatchFilesButton
        ]
    },
    {
        // 4221 (REQUESTED)
        assignmentStatusId:4221,
        correctActionButtons:[
            acceptProjectButton,
            rejectProjectButton,
            downloadOriginalFilesButton,
            downloadBatchFilesButton
        ]
    },
    {
        // 4222 (ACCEPTED)
        assignmentStatusId:4222,
        correctActionButtons:[
            downloadOriginalFilesButton,
            downloadBatchFilesButton,
            uploadReturnFilesButton,
            downloadPurchaseOrderButton,
            setToInProgressButton
        ]
    },
    {
        // 4224 (IN PROGRESS)
        assignmentStatusId:4224,
        correctActionButtons:[
            acceptProjectButton,
            rejectProjectButton,
            downloadOriginalFilesButton,
            downloadBatchFilesButton,
            uploadReturnFilesButton,
            downloadPurchaseOrderButton,
            setToWorkDeliveredButton
        ]
    },
    {
        // 4225 (WORKD DELIVERED)
        assignmentStatusId:4225,
        correctActionButtons:[
            acceptProjectButton,
            rejectProjectButton,
            downloadOriginalFilesButton,
            downloadBatchFilesButton,
            uploadReturnFilesButton,
            downloadPurchaseOrderButton
        ]
    },
    {
        // 4226 (DELIVERY_ACCEPTED)
        assignmentStatusId:4226,
        correctActionButtons:[
            downloadPurchaseOrderButton,
            downloadCreditNoteButton
        ]
    },
    {
        // 4231 (ASSIGNMENT_CLOSED)
        assignmentStatusId:4231,
        correctActionButtons:[
            downloadPurchaseOrderButton,
            downloadCreditNoteButton
        ]
    },
];



describe("testing assignment creation", () => {
    before(() => {
        cy.fixture('cypress.env.json')
        .then((testEnv) => {
            // First we authorize to our app
            cy.loginToApp(testEnv.enviromentTestVariables.DEV.freelancer.email, testEnv.enviromentTestVariables.DEV.freelancer.password);
            OpenAPI.TOKEN = testEnv.enviromentTestVariables.DEV.freelancer.api_key;
        });
    });
    it("should check if we display correct icons for 'ACCEPTED (4222)' assignment when logged in as a freelancer", () => {
        const acceptedAssignmentId = 1131729;
        cy.visitAssignmentByAssignmentId(acceptedAssignmentId.toString());
        // Find correct buttons settings
        const actionsForAcceptedAssignment = correctActionButtonsForStatuses.find(settings => settings.assignmentStatusId === 4222).correctActionButtons;
        actionsForAcceptedAssignment.map(potentialActionButton => {
            cy.get(`[view_id="${potentialActionButton}"]`).should("be.visible");
        });
        
    })

})