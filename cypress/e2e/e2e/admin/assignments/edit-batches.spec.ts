/// <reference path="../../../../support/index.d.ts" />
import '@4tw/cypress-drag-drop'

const projectId = 1128445;
const packageId = 1128924;

describe("testing edition of translation batches", () => {
    before(() => {
        cy.fixture('cypress.env.json')
        .then((testEnv) => {
            // First we authorize to our app
            cy.loginToApp(testEnv.enviromentTestVariables.DEV.admin.email, testEnv.enviromentTestVariables.DEV.admin.password);
            cy.navigateToProjectByProjectId(`open-quotes-table`, projectId);
            cy.navigateFromSidebar("assignments.overview", "piSaveContactButton");
        });
    });
    it("should create drag packages", () => {
        cy.clickVisibleButton("aoEditBatchesButton");
        cy.selectComboValueWithId("ebmSelectTranslationStepCombo", 4210, 0);
        cy.selectComboValueWithId("ebmSelectPackageCombo", packageId, 0);
        cy.get("[view_id='ebmUnsassignedTasksList']").find(`[role="option"]`).eq(0).should("be.visible").click();
        cy.clickVisibleButton("ebmMoveItemRightButton");
        cy.clickVisibleButton("ebmSaveAndCloseButton");
        // Open it again 
        cy.clickVisibleButton("aoEditBatchesButton");
        cy.selectComboValueWithId("ebmSelectTranslationStepCombo", 4210, 0);
        cy.selectComboValueWithId("ebmSelectPackageCombo", packageId, 0);
        cy.get("[view_id='ebmPackagesList']").should("contain", "task10");
    });
});
