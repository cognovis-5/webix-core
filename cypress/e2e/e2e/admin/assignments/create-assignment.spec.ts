/// <reference path="../../../../support/index.d.ts" />
import { OpenAPI } from "../../../../../sources/openapi";
import { WebixPortalTranslationService, WebixPortalAssignmentService } from "../../../../../sources/openapi/index";

// Vars used in test
const projectId = 1131797;
const assignmentsToBeCreated = [
];

describe("testing assignment creation", () => {
    before(() => {
        cy.fixture('cypress.env.json')
        .then((testEnv) => {
            // First we authorize to our app
            cy.loginToApp(testEnv.enviromentTestVariables.DEV.admin.email, testEnv.enviromentTestVariables.DEV.admin.password);
            OpenAPI.TOKEN = testEnv.enviromentTestVariables.DEV.admin.api_key;
            // We go to specific project by projectNr
            //cy.navigateToProjectByProjectNr(`open-quotes-table`, projectNr);
            cy.navigateToProjectByProjectId(`open-quotes-table`, projectId);
            //cy.visitProjectByProjectId(projectId)
        });
    });
    it("should assign two freelancers", () => {
        // Getting packages ids
        WebixPortalAssignmentService.getPackages({projectId:projectId})
        .then(res => {
            res.packages.forEach((_package, index) => {
                // Consider only assignments without a freelancer
                if(!_package.has_assignments) {
                    assignmentsToBeCreated.push({
                        freelancePackageId: _package.freelance_package.id,
                        packageTypeId: _package.package_type.id
                    })
                }
            });
        });
            
        //cy.visitProjectByProjectId(projectId);
        //cy.navigateTo("assignments.overview", "assignments");
        cy.navigateFromSidebar("assignments.overview", "piSaveContactButton");
        // Click 'New Assignment' button 
        cy.clickVisibleButton("newAssignmentButton");
        // Make sure that cog-cell is loaded
        cy.get(`[data-cell-name='cog-cell']`).should("be.visible")
        .then(() => {
            //cy.get(`[view_id='fsClearFiltersButton']`).click();
            cy.deselectAllValuesInCombo("sfRoleCombo");
            cy.selectComboValueWithId("sfRoleCombo", 10000433, 0);
            cy.clickVisibleButton("fsFilterButton");
        });
        cy.get(`[data-cell-name='cog-cell']`).should("be.visible");
        cy.selectDtRowByIndex("selectFreelancersTable", 1);
        cy.selectDtRowByIndex("selectFreelancersTable", 2);
        cy.clickVisibleButton("fsAssignTasks");
        
        // Generation assignment
        cy.get(`[view_id="gaDataRows"]`).should("be.visible")
        .then(() => {
            const generatedIds = [`${assignmentsToBeCreated[0].freelancePackageId}-${assignmentsToBeCreated[0].packageTypeId}-0`, `${assignmentsToBeCreated[1].freelancePackageId}-${assignmentsToBeCreated[1].packageTypeId}-1`];
            // First Slot
            const randomRateOne = (Math.random() * 0.9 + 0.1).toFixed(2);
            cy.get(`[view_id="pFee-${generatedIds[0]}"]`).should("be.visible").type(`${randomRateOne}`);
            //cy.get(`[view_id="pbUnits-${generatedIds[0]}"]`).should("be.visible").type(`${assignmentsToBeCreated[0].units}`);
            //cy.selectComboValueWithId(`pbfreelancerSelector-${generatedIds[0]}`, assignmentsToBeCreated[0].freelancerId, 0);
            cy.selectComboValueWithIndex(`pbfreelancerSelector-${generatedIds[0]}`,0);
            // cy.selectComboValueWithId(`pbUom-${generatedIds[0]}`, 324, 0);
            // cy.get(`[view_id="pbDeadline-${generatedIds[0]}"]`).should("be.visible").click().wait(100)
            // .then(() => {
            //     cy.get('[aria-label="Calendar"]').eq(0).should("be.visible").find('[day="6"]').eq(6).click();
            // });
        
            // Second Slot
            const randomRateTwo = (Math.random() * 0.9 + 0.1).toFixed(2);
            cy.get(`[view_id="pFee-${generatedIds[1]}"]`).should("be.visible").type(`${randomRateTwo}`);
           // cy.get(`[view_id="pbUnits-${generatedIds[1]}"]`).should("be.visible").type(`${assignmentsToBeCreated[1].units}`);
            //cy.selectComboValueWithId(`pbfreelancerSelector-${generatedIds[1]}`, assignmentsToBeCreated[1].freelancerId,1);
            cy.selectComboValueWithIndex(`pbfreelancerSelector-${generatedIds[1]}`,1);
            // cy.selectComboValueWithId(`pbUom-${generatedIds[1]}`, 324, 1);
            // cy.get(`[view_id="pbDeadline-${generatedIds[1]}"]`).should("be.visible").click().wait(100)
            // .then(() => {
            //     cy.get('[aria-label="Calendar"]').eq(1).should("be.visible").find('[day="6"]').eq(6).click();
            // });
            
            cy.clickVisibleButton("gaRequestForParticpationButton");
        });
  
    });
});