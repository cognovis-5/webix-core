/// <reference path="../../../../support/index.d.ts" />

import { IntranetTranslationLanguage, OpenAPI, WebixPortalAssignmentService } from "../../../../../sources/openapi";

const projectId = 1128860;

let currentDeadline = null;

const ddMMYYtoJSDateObj = (dateString:string) => {
    const splitted = dateString.split(" ");
    const date = splitted[0];
    const time = splitted[1];
    const timeSplitted = time.split(":");
    const dateParts = date.split(".");
    const dateObject = new Date(+dateParts[2], parseInt(dateParts[1]) - 1, +dateParts[0], +timeSplitted[0], +timeSplitted[1]); 
    return dateObject
}

describe("testing if hovering on clock icon shows assignment deadline", () => {
    before(() => {
        cy.fixture('cypress.env.json')
        .then((testEnv) => {
            // First we authorize to our app
            OpenAPI.TOKEN = testEnv.enviromentTestVariables.DEV.admin.api_key;
            cy.loginToApp(testEnv.enviromentTestVariables.DEV.admin.email, testEnv.enviromentTestVariables.DEV.admin.password);
            cy.navigateToProjectByProjectId(`open-quotes-table`, projectId);
            cy.navigateFromSidebar("assignments.overview", "piSaveContactButton");
        });
    });
    it("should test assignment deadline update ", () => {
        cy.wrap(
            WebixPortalAssignmentService.getPackages({
            projectId: projectId,
            }), { timeout: 10000} 
        )
        .its('packages').should('have.length.gt',0)
        .as('freelancePackages');
        cy.get('@freelancePackages').then((freelancePackages) => {
            cy.wrap(freelancePackages.filter(flPackage => flPackage["has_assignments"]))
            .then((flPackages) => {
                // Randomizing selection of package
                const randomNumber = Math.floor(Math.random() * (flPackages.length -1 ));
                // const randomizedPackage = flPackages[randomNumber];
                const randomizedPackage = flPackages[9];
                // First let's check if correct panel was opened
                const languagePanelId = `ao-lp-${randomizedPackage["target_language"]["id"]}`;
                cy.get(`[view_id="${languagePanelId}"`)
                .then((panel) => {
                    cy.wrap(panel).find("[role='tab']").first().invoke("attr", "aria-expanded")
                    .then(isExpanded => {
                        if(isExpanded !== "true") {
                            // Let's close opened one first
                            cy.get("[role='tab']").first().click();
                            cy.wrap(panel).click();
                        }
                    });
                });

                // Let's sav current deadline into browser memory
                cy.get(`[id="ao-di-${randomizedPackage["freelance_package"]["id"]}-${randomizedPackage["package_type"]["id"]}"]`)
                .then(deadlineIcon => {
                    cy.wrap(deadlineIcon)
                    .invoke('attr', 'webix_tooltip')
                    .then(deadline => {
                        currentDeadline = deadline;
                        const currentDeadlineAsObj = ddMMYYtoJSDateObj(currentDeadline);
                    });
                    cy.wrap(deadlineIcon).click();
                });
                
                // cy.get(`[id="ao-di-${flPackage["freelance_package"]["id"]}-${flPackage["package_type"]["id"]}"]`)
                // .invoke("attr", "cog-update-deadline-possible")
                // .should("eq", "true")
                // .click()
                

                // // First we make sure that view loaded, so we grab first element
                // cy.get("[cog-update-deadline-possible='true']").eq(0).should("be.visible");
                // // Now we grab all of them
                // cy.get("[cog-update-deadline-possible='true']").each((icon) => {
                //     cy.wrap(icon).invoke('attr', 'webix_tooltip').should("equal", "01.07.2020 18:00")
                // });
            });

        });
    })
});
