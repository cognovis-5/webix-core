/// <reference path="../../../../support/index.d.ts" />
import { OpenAPI, WebixPortalAssignmentService } from "../../../../../sources/openapi";

const projectId = 1131797;


interface ICypressExampleBatch {
    freelance_package:{
        id:number,
        name:string
    },
    assignment_type: {
        id:number,
        name:string
    }
}

const correctSubmenusForStatuses = [
    {
        // CRETED
        assignmentStatusId:4220,
        correctSubmenuIcons:[
            "aoActionUpdateDeadline",
            "aoActionUpdateFee",
            "aoActionReassign",
            "aoActionDeleteAssignment",
            "aoActionShowAssignment"
        ]
    },
    {
        // REQUESTED
        assignmentStatusId:4221,
        correctSubmenuIcons:[
            "aoActionUpdateDeadline",
            "aoActionUpdateFee",
            "aoActionRemindFreelancer",
            "aoActionReassign",
            "aoActionDeleteAssignment",
            "aoActionShowAssignment"
        ]
    },
    {
        // ACCEPTED
        assignmentStatusId:4222,
        correctSubmenuIcons:[
            "aoActionUpdateDeadline",
            "aoActionShowAssignment"
        ]
    },
    {
        // DENIED
        assignmentStatusId:4223,
        correctSubmenuIcons:[
            "aoActionUpdateDeadline",
            "aoActionReassign",
            "aoActionDeleteAssignment",
            "aoActionShowAssignment"    
        ]
    },
    {
        // IN_PROGRESS
        assignmentStatusId:4224,
        correctSubmenuIcons:[
            "aoActionUpdateDeadline",
            "aoActionShowAssignment"   
        ]
    },
    {
        // WORK_DELIVERED
        assignmentStatusId:4225,
        correctSubmenuIcons:[
            "aoActionShowAssignment"   
        ]
    },
    {
        // DELIVERY_ACCEPTED
        assignmentStatusId:4226,
        correctSubmenuIcons:[
            "aoActionShowAssignment"   
        ]
    },
    {
        // DELIVERY_REJECTED
        assignmentStatusId:4227,
        correctSubmenuIcons:[
            "aoActionUpdateDeadline",
            "aoActionReassign",
            "aoActionShowAssignment",
        ]
    },
    {
        // ASSIGNED_OTHER
        assignmentStatusId:4228,
        correctSubmenuIcons:[
            "aoActionUpdateDeadline",
            "aoActionShowAssignment",
        ]
    },

    {
        // PACKAGE_AVAILABLE
        assignmentStatusId:4229,
        correctSubmenuIcons:[
            "aoActionUpdateDeadline",
            "aoActionShowAssignment",
        ]
    },
    {
        // ASSIGNMENT_DELETED
        assignmentStatusId:4230,
        correctSubmenuIcons:[
            "aoActionReassign",
            "aoActionShowAssignment",
        ]
    },
    {
        // ASSIGNMENT_CLOSED
        assignmentStatusId:4231,
        correctSubmenuIcons:[
            "aoActionShowAssignment",
        ]
    },


];


describe("testing if correct submenu action icons are displayed", () => {
    before(() => {
        cy.fixture('cypress.env.json')
        .then((testEnv) => {
            // First we authorize to our app
            cy.loginToApp(testEnv.enviromentTestVariables.DEV.admin.email, testEnv.enviromentTestVariables.DEV.admin.password);
            OpenAPI.TOKEN = testEnv.enviromentTestVariables.DEV.admin.api_key;
            WebixPortalAssignmentService.getTranslationAssignments({projectId:projectId})
            .then(res => {
                res.assignments.forEach(assignment => {
                    correctSubmenusForStatuses.forEach(freelancePackage => {
                        if(freelancePackage.assignmentStatusId === assignment.assignment_status.id) {
                            freelancePackage["packageToTest"] = assignment as ICypressExampleBatch;
                        }
                    });
                });
            })
            // We go to specific project by project_id
            cy.navigateToProjectByProjectId(`open-quotes-table`, projectId);
            cy.navigateFromSidebar("assignments.overview", "piSaveContactButton");
        });
    });

    correctSubmenusForStatuses.forEach(freelancePackage => {
        it(`should check submenu action icons for assignment status of ${freelancePackage.assignmentStatusId}`, () => {
            // First we always make sure that table was loaded
            cy.get(`[data-cell-name="cog-cell"]`).should("be.visible");
            // Then we check if packages of concrete status exists in project
            if(freelancePackage["packageToTest"]) {
                const buttonId = `aoSubmenuButton-${freelancePackage["packageToTest"]?.assignment.id}`;
                cy.verifySubmenuMenuOptions(buttonId, "fpSubmenu", freelancePackage.correctSubmenuIcons);
            } else {
                cy.log(`Freelance package with status:${freelancePackage.assignmentStatusId} wasn't found, skipping test`)
            }
        })
    });


})