/// <reference path="../../../../support/index.d.ts" />

import { WebixPortalTranslationService } from "../../../../../sources/openapi";


describe("testing creation of tasks", () => {
    before(() => {
        cy.fixture('cypress.env.json')
        .then((testEnv) => {
            // First we authorize to our app
            cy.loginToApp(testEnv.enviromentTestVariables.DEV.admin.email, testEnv.enviromentTestVariables.DEV.admin.password);
            cy.get(`[cog_column_name="project_type"]`).should("be.visible").its("length")
            .then(totalSelectableProjects => {
                const random = Math.floor(Math.random() * (totalSelectableProjects -1));
                cy.get(`[cog_column_name="project_type"]`).should("be.visible").eq(random).click();
            });
        });
    });

    it("should check for correct values in Basic Project Information section", () => {
        cy.wrap(WebixPortalTranslationService.getTransProjects({
            projectId: this.projectId
        })).its(0).its('target_language').its(0).its('id').as('targetLanguageId')
        cy.get('@targetLanguageId').then(targetLanguageId => {
            const targetSkills = skills.filter(el => el.skill.id === targetLanguageId as unknown as number)
            cy.log("Target.. " + targetLanguageId + "   " + skills.length)
            expect(targetSkills).length.be.gt(0)
        })
    });
});