///<reference types="cypress"/>

describe("project info test ", () => {
    beforeEach(() => {
        cy.loginToEnv("DEV", "admin");
        cy.get('[webix_tm_id="projects.projects"]').click();
    });
    it("should check the clicked project", () => {
        cy.get(".project__name").then((projectName) => {
            cy.wrap(projectName).eq(0).find(".webix_cell").eq(0).click();
            cy.wrap(projectName)
                .eq(0)
                .find(".webix_cell")
                .eq(0)
                .invoke("text")
                .then((projectNameText) => {
                    const projectName = projectNameText;
                    console.log(projectName);
                    cy.get('[webix_tm_id="project-info.project-info"]').click();
                });
        });
    });
    it.only("should test the deep linking", () => {
        cy.visit("/projects?project_id=1133619");
        cy.get(".project__name").then((projectName) => {
            expect(projectName).to.not.be.undefined;
        });
    });
});
