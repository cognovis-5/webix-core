///<reference types="cypress"/>

describe("projects test", () => {
    beforeEach(() => {
        cy.loginToEnv("DEV", "admin");
        cy.contains("Projects").click();
    });
    it("should check project-name data in request offer", () => {
        cy.contains("1759_6036 - Alru Tipkin_Dykyleq_PO_AN");
    });
    it("should check active project-names at index 0", () => {
        cy.get(".project__name")
            .eq(0)
            .find(".webix_cell")
            .eq(0)
            .should("contain", "6886_3746 - Firnal Virbh_Vaqjeke Üporsupzemt_PE_IX_");
    });
    it("should check the length of project-type in active project table", () => {
        cy.get(".active__projects__project__type")
            .find(".webix_cell")
            .then(($projectType) => {
                expect($projectType).to.not.be.undefined;
            });
    });
});
