///<reference types="cypress"/>

describe("account info test", () => {
    beforeEach(() => {
        cy.loginToEnv("DEV", "admin");
        cy.contains("Account Info").click();
    });
    it("should check if logged in user exist in the switcher", () => {
        cy.get('[class="webix_input_icon wxi-menu-down"]').click();
        cy.contains("B Muchil").click();

        cy.get('[class="webix_inp_static"],[role="combobox"]')
            .should("contain", "B Muchil")
            .then((user) => {
                expect(user.text()).to.equal("B Muchil");
            });
    });
    it("should add new member", () => {
        cy.get(".add__member button").click({ force: true });
        cy.get(".first__names").type("Jackson");
        cy.get(".last__name").type("Osmond");
        cy.get(".member__email").type("jaosmond@fake.com");
        cy.get(".member__phone").type("+576494474");
        cy.contains("Save").click({ force: true });
    });
});
