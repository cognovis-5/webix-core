/// <reference path="../../../../support/index.d.ts" />

import { getRandomNumberBetween} from "../../../../support/helpers";

describe("testing project creation", () => {
    beforeEach(() => {
        cy.fixture('cypress.env.json')
        .then((testEnv) => {
            // First we authorize to our app
            cy.loginToApp(testEnv.enviromentTestVariables.DEV.admin.email, testEnv.enviromentTestVariables.DEV.admin.password);
            cy.navigateTo("new-offer.new-offer", "new-offer");
        });
    });

    it("should create a new project", () => {
        
        const randomNumber = Math.floor((9999 - 1) * Math.random());
        const randomProjectNameWithNumber = `test_project_${randomNumber}`;
        // Project name 
        cy.typeValue("npProjectName", randomProjectNameWithNumber, false);
        // Project type
        cy.selectRandomValueFromCombo("npProjectType");
        // Customer
        cy.selectRandomValueFromCombo("npCustomer");
        // Final Company
        cy.selectRandomValueFromCombo("npFinalCompany");
        // Processing time
        cy.typeValue("npProcessingTime", "2", false);
        // Skill
        cy.selectRandomValueFromCombo("npSkill");
        // Reference nr
        cy.typeValue("npRefNr", "cypress ref", false)
        // Comment
        // Now after switching to wysiwyg editor, we need to comment that 
        //cy.get(`[view_id=npComment]`).should("be.visible").type("This is cypress comment !")
        // Source Language
        cy.selectRandomValueFromCombo("npSourceLanguage");
        // Target Language
        cy.selectRandomValuesFromMultiselect("npTargetLanguages", 1);
        // Subject Area
        cy.selectRandomValueFromCombo("npSubjectArea");
        // Finally submit
        cy.clickVisibleButton("npAddProject");
        // Make sure we were properly redirected
        cy.url().should("contain", "project-info");
        cy.get("body").should("contain", randomProjectNameWithNumber);
    });

    xit("should create a new project for a completely new customer with new contact person", () => {
        const randomNumber = Math.floor((9999 - 1) * Math.random());
        const randomProjectNameWithNumber = `test_project_${randomNumber}`;
        // Project name 
        cy.typeValue("npProjectName", randomProjectNameWithNumber, false);
        // Project type
        cy.selectRandomValueFromCombo("npProjectType");
        // Now we create a new user
        cy.clickVisibleButton(`npAddNewCustomerButton`);
        // Let's wait for modal to show
        cy.get(`[role="dialog"]`).should("be.visible");
        // Filling the new customer modal 
        cy.typeRandomName(`ccmCompanyName`, "", 12, false);
        cy.typeRandomName(`ccmShortCompanyName`, "", 6, false);

        cy.typeRandomName(`ccmStreet`, "",4, false);
        cy.typeRandomName(`ccmZip`, "",4, false);
        cy.typeRandomName(`ccmCity`, "",8, false);
        cy.typeValue(`ccmCountry`, "de", false);
        cy.typeRandomValue(`ccmPhone`, "", false);
        cy.typeRandomName(`ccmWebsite`, "",11, false);
        cy.typeRandomName(`ccmSource`, "", 10, false);
        cy.typeRandomName(`ccmVatId`, "", 10, false);
        cy.selectRandomValueFromCombo(`ccmTaxClassification`);
        cy.selectRandomValueFromCombo(`ccmPaymentTerm`);
        cy.selectRandomValueFromCombo(`ccmDefaultQuoteTemplate`);
        cy.selectRandomValueFromCombo(`ccmDefaultInvoiceTemplate`);
        cy.clickVisibleButton(`ccmSubmitNewCompanyButton`);
        cy.typeRandomValue(`ccmPhone`, "", false);

        cy.typeValue("npProcessingTime", "2", false);
        // Skill
        cy.selectRandomValueFromCombo("npSkill");
        // Reference nr
        cy.typeValue("npRefNr", "cypress ref", false)
        // Comment
        cy.get(`[view_id=npComment]`).should("be.visible").type("This is cypress comment !")
        // Source Language
        cy.selectRandomValueFromCombo("npSourceLanguage");
        // Subject Area
        cy.selectRandomValueFromCombo("npSubjectArea");
    
        // Open new company contact modal
        cy.clickVisibleButton(`npAddNewUserButton`);
        cy.get(`[role="dialog"]`).should("be.visible");

        cy.typeRandomValue(`ucmFirstNames`, `first_name_`, false);
        cy.typeRandomValue(`ucmLastName`, `last_name`,false);
        const randomNumberTwo = getRandomNumberBetween(0,9999);
        const randomNumberThree = getRandomNumberBetween(0,100);
        const randomEmail = `someone_${randomNumberTwo}@${randomNumberThree}.fakedomaincom`
        cy.typeValue(`ucmEmail`, randomEmail, true);
        cy.typeRandomValue(`ucmSystemName`, `sn_`, false);
        const randomPassword = getRandomNumberBetween(1,300);
        cy.typeValue(`ucmPassword`, randomPassword.toString(), false);
        cy.typeValue(`ucmPasswordConfirm`, randomPassword.toString(), false);
        cy.selectRandomValueFromCombo(`ucmSalutation`);
        cy.typeRandomValue(`ucmWorkPhone`, "", false);
        cy.typeRandomValue(`ucmWorkMobile`, "", false);
        
        cy.clickVisibleButton(`ucmSubmitNewCompanyContactButton`);

        // Submit project
        cy.clickVisibleButton("npAddProject");

        // Check if it exsits in Pontential Projects table in /projects
        cy.url().should("contain", "projects");
        cy.get(`[view_id='poPotentialProjects']`).should("contain", randomProjectNameWithNumber);
    });

});