///<reference types="cypress"/>

describe("edit task test", () => {
    const projectIndex = 4;
    beforeEach(() => cy.goToProjectTask(projectIndex));
    it("should test the task_name edition", () => {
        const randomNr = Math.floor(Math.random() * 101);
        const randomTaskName = `editted_${randomNr}`;
        cy.editTaskName(1,randomTaskName);
        cy.wait(1000);
        cy.get('[view_id="tasks-table"]')
        .then(table => {
            cy.wrap(table).contains(randomTaskName);
        });

    });
    /*
    it("should edit two task to have same same name and language and its expected to fail ", () => {
        cy.editTwoTask();
    });
    */
});
