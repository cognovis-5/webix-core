/// <reference path="../../../../support/index.d.ts" />

// Vars used in test
const projectId = 1131797;
const projectNr = `6563_4987 - P-lialemt_Q7lizs_QA_WL`;

const testTaskname = "cypress_test";
const testTaskTypeId = 87;
const testTaskUnits = 100;
const testTaskUomId = 324;
const testTargetLanguages = "all";

describe("testing creation of tasks", () => {
    before(() => {
        cy.fixture('cypress.env.json')
        .then((testEnv) => {
            // First we authorize to our app
            cy.loginToApp(testEnv.enviromentTestVariables.DEV.admin.email, testEnv.enviromentTestVariables.DEV.admin.password);
            // We go to specific project by projectNr
            cy.navigateToProjectByProjectNr(`open-quotes-table`, projectNr);
            // Navigate to project tasks
            cy.navigateTo("project-tasks.project-tasks", "project-task");
        });
    });

    it("should create a new task", () => {
        const randomNumber = Math.floor((9999 - 1) * Math.random());
        const randomizedTaskName = `${testTaskname}_${randomNumber}`;
        cy.addSingleTask(randomizedTaskName, testTaskTypeId, testTaskUnits, testTargetLanguages, testTaskUomId);
    });
});