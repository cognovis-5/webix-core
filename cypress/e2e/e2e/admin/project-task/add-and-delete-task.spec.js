///<reference types="cypress"/>

describe("testing delete functionality", () => {
    before(() => cy.goToProjectTask());
    it("should delete newly created task", () => {
        cy.addSingleTask("cypress_task");
        cy.wait(500);
        cy.deleteTask();
    });
});
