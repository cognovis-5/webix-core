import { IFreelancePackage, ITransProject, OpenAPI, WebixPortalAssignmentService, ITransTask, ICompany, IntranetTranslationLanguage, CognovisRestCompanyService, IntranetCompanyType, WebixPortalTranslationService, IFreelancer, IntranetTransTaskType, IntranetProjectType } from "../../../../sources/openapi";

const projectId = 1131797;
const testTaskname = "cypress_test";
const randomNumber = Math.floor((9999 - 1) * Math.random());
const randomizedTaskName = `${testTaskname}_${randomNumber}`;

before(() => {
    cy.fixture('cypress.env.json')
    .then((testEnv) => {
        // First we authorize to our app
        OpenAPI.TOKEN = testEnv.enviromentTestVariables.DEV.admin.api_key;
    });
});

xdescribe("Check for issues in packages endpoint", () => {
    it("Should run through random projects and check the packages endpoint", () => {
        // Get a project with multiple target languages        
        cy.wrap(WebixPortalTranslationService.getTransProjects({
            projectStatusId: [76,71]
        })).as('projects')    
        
        cy.get('@projects').then(_projects => {
            const projects = _projects as unknown as Array<ITransProject>
            const num = 10
            for(let i = num;i>=1;i--) {
                const projectId = projects[Math.floor(Math.random() * (projects.length -1))].project.id
                cy.wrap(WebixPortalAssignmentService.getPackages({
                    projectId: projectId
                }),{timeout:10000})
                }
        })
    })

    
})
xdescribe("testing assignment getting with project creation", () => {

    before(() => {
        // get a random test company
        cy.wrap(CognovisRestCompanyService.getCompanies({
            autocompleteQuery: 'test_',
            companyTypeIds: [IntranetCompanyType.CUSTOMER]
        }).then(res => {
            return res[Math.floor(Math.random() * (res.length -1))]
        })).as('test_company')

        cy.get('@test_company').then(_company => {
            const company = _company as unknown as ICompany 
            cy.wrap(WebixPortalTranslationService.postTransProject({
                requestBody: {
                    company_id: company.company.id,
                    source_language_id: IntranetTranslationLanguage.DEDE,
                    target_language_ids: [IntranetTranslationLanguage.ENGB, IntranetTranslationLanguage.ESES]
                }
            }), {timeout: 10000}).then(_project => {
                const project = _project as unknown as ITransProject
                return project.project.id
            })
        }).as('projectId')        
    })

    it("should create a task", function () {
        cy.wrap(WebixPortalTranslationService.postTransTasks({
            projectId: this.projectId, 
            requestBody: {task_name: randomizedTaskName, task_uom_id: 324, task_units: randomNumber, task_type_id:87}
        })).as('tasks')
        cy.get('@tasks').then(_tasks => {
            const tasks = _tasks as unknown as ITransTask[]
            expect(tasks).to.have.members
            tasks.forEach(task => {
                cy.log("Found task " + task.task.name)
            })
        })
    });

    it("should have a package", function () {
        cy.wrap(WebixPortalAssignmentService.getPackages({
            projectId: this.projectId
        })).as('packages')
    });

    it("Should check that we can update a package", function () {
        // Get a project with multiple target languages        
        cy.wrap(WebixPortalAssignmentService.getPackages({
            projectId: this.projectId
        }))
        .then(_packages => {
            const packages = _packages as unknown as IFreelancePackage[]
            const package1 = packages[0]
            const oldName = package1.freelance_package.name
            let oldComment = package1.package_comment
            if (oldComment == '') {
                oldComment = 'DELETED'
            }
            cy.wrap(WebixPortalAssignmentService.putPackages({
                freelancePackageId: package1.freelance_package.id,
                requestBody: {
                    package_name: 'New package Name',
                    package_comment: "New package Comment"
                }
            }))
            .then(_package => {
                const updatedPackage = _package as unknown as IFreelancePackage
                expect(updatedPackage.package_comment).eq('New package Comment')
                expect(updatedPackage.freelance_package.name).eq('New package Name')
                cy.wrap(WebixPortalAssignmentService.putPackages({
                    freelancePackageId: package1.freelance_package.id,
                    requestBody: {
                        package_name: oldName,
                        package_comment: oldComment
                    }
                }))
                .then(_package => {
                    const updated2Package = _package as unknown as IFreelancePackage
                    expect(updated2Package.package_comment).eq(oldComment)
                    expect(updated2Package.freelance_package.name).eq(oldName)
                })
            })
        })
    });

    it("should delete the project again", function () {
        cy.wrap(WebixPortalAssignmentService.getPackages({
            projectId: this.projectId
        })).as('packages')

        cy.get('@packages').then(_packages => {
            const packages = _packages as unknown as IFreelancePackage[]
            const packageIds = [...new Set(packages.map(item => item.freelance_package.id))]
            cy.log("Deleting packages")
            cy.wrap(WebixPortalAssignmentService.deletePackages({
                packageIds: packageIds
            })).should('be.empty')
        }).then(() => {
            cy.wrap(WebixPortalTranslationService.deleteTransProject({
                projectId: this.projectId
            })).should('be.empty')
        })
    })
    
}); 

describe("Creating project packages without tasks", () => {

    before(() => {
        // get a random test company
        cy.wrap(CognovisRestCompanyService.getCompanies({
            autocompleteQuery: 'test_',
            companyTypeIds: [IntranetCompanyType.CUSTOMER]
        }).then(res => {
            return res[Math.floor(Math.random() * (res.length -1))]
        })).as('test_company')

        cy.get('@test_company').then(_company => {
            const company = _company as unknown as ICompany 
            cy.wrap(WebixPortalTranslationService.postTransProject({
                requestBody: {
                    company_id: company.company.id,
                    source_language_id: IntranetTranslationLanguage.DEDE,
                    target_language_ids: [IntranetTranslationLanguage.ENGB, IntranetTranslationLanguage.ESES]
                }
            }), {timeout: 10000}).then(_project => {
                const project = _project as unknown as ITransProject
                return project.project.id
            })
        }).as('projectId')        
    })

    it("should create a package", function () {
        cy.wrap(WebixPortalAssignmentService.postPackages({
            projectId: this.projectId,
            requestBody: {
                package_type_id: IntranetTransTaskType.TRANS
            }
        })).as('packages').should('have.length',2)

        cy.get('@packages').then(() => {
            cy.wrap(WebixPortalAssignmentService.postPackages({
                projectId: this.projectId,
                requestBody: {

                }
            })).as('packages2').should('have.length',4)
        })
    
    });

    it("should have a package", function () {
        cy.wrap(WebixPortalAssignmentService.getPackages({
            projectId: this.projectId
        })).as('packages')
    })

    xit("should delete the project again", function () {
        cy.wrap(WebixPortalAssignmentService.getPackages({
            projectId: this.projectId
        })).as('packages')

        cy.get('@packages').then(_packages => {
            const packages = _packages as unknown as IFreelancePackage[]
            const packageIds = [...new Set(packages.map(item => item.freelance_package.id))]
            cy.log("Deleting packages")
            cy.wrap(WebixPortalAssignmentService.deletePackages({
                packageIds: packageIds
            })).should('be.empty')
        }).then(() => {
            cy.wrap(WebixPortalTranslationService.deleteTransProject({
                projectId: this.projectId
            })).should('be.empty')
        })
    })
    
}); 



xdescribe("KOL-1408 - testing freelancer finding with fees", () => {

    before(() => {

        // Get a project with multiple target languages        
        cy.wrap(WebixPortalTranslationService.getTransProjects({
            projectTypeId: [87],
            projectStatusId: [76,71]
        })).as('projects')    
        
        cy.get('@projects').then(_projects => {
            const projects = _projects as unknown as Array<ITransProject>
            const multiTargetProject = projects.filter(el => el.target_language.length >1)
            return multiTargetProject[Math.floor(Math.random() * (multiTargetProject.length -1))]
        }).as('project')

        cy.get('@project').then(_project => {
            const project = _project as unknown as ITransProject
            cy.log("Found project " + project.project.name + " with number " + project.project.id)

            // Select a single target language
            const targetLanguageId = project.target_language[Math.floor(Math.random() * project.target_language.length)].id

            cy.log("Using language " + targetLanguageId)


            cy.wrap(WebixPortalAssignmentService.getPackages({projectId:projectId})
            .then(_packages =>{
                const packages = _packages as unknown as IFreelancePackage[]
                const single_package = packages[Math.floor(Math.random() * (packages.length -1))]
                return single_package
            })).as('package')

            cy.get('@package').then(_package => {
                const single_package = _package as unknown as IFreelancePackage
       
                cy.wrap(WebixPortalAssignmentService.getFreelancersForPackage({
                    freelancePackageId: single_package.freelance_package.id
                }).then(_freelancers => {
                    const freelancers = _freelancers as unknown as IFreelancer[]
                    console.log("Found "+ freelancers.length + " freelancer")
                    const freelancer = freelancers[Math.floor(Math.random() * (freelancers.length -1))]
                    console.log(freelancer.freelancer.name);              
                    return freelancer
                })).as('freelancer')
            })
        })    
    })
    it("should return freelancers for with fees", function () {
        const freelancer = this.freelancer as IFreelancer
        const fee = freelancer.fee[0]
        expect(fee.rate).greaterThan(0)
    });

    it("should return freelancers for package", () => {
        WebixPortalAssignmentService.getPackages({projectId:projectId})
            .then(_package =>{
                WebixPortalAssignmentService.getFreelancersForPackage({freelancePackageId: _package[0].freelance_package.id})
                    .then(_freelancer => {
                        expect(_freelancer.length).greaterThan(0);
                    })
            })
    })

});
