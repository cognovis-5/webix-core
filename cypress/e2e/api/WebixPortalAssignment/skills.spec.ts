import { ITransProject,  WebixPortalTranslationService, OpenAPI, WebixPortalFreelancerService, ISkill, IntranetSkillBusinessSector, IntranetSkillType } from "../../../../sources/openapi";


before(() => {
    cy.fixture('cypress.env.json')
    .then((testEnv) => {
        // First we authorize to our app
        OpenAPI.TOKEN = testEnv.enviromentTestVariables.DEV.admin.api_key;
    });
});

describe("Get list of skills for a project / object", () => {
    before(() => {

        // Get a project with multiple target languages        
        cy.wrap(WebixPortalTranslationService.getTransProjects({
            projectTypeId: [87],
            projectStatusId: [76,71]
        })).as('projects')    
        

        cy.get('@projects').then(_projects => {
            const projects = _projects as unknown as Array<ITransProject>
            const multiTargetProject = projects.filter(el => el.target_language.length >1)
            return multiTargetProject[Math.floor(Math.random() * (multiTargetProject.length -1))].project.id
        }).as('projectId')
    })

    it("Should return skills for the target languages", function () {
        cy.wrap(WebixPortalFreelancerService.getObjectSkill({
            objectId: this.projectId
        }))
        .then(_skills => {
            const skills = _skills as ISkill[]
            cy.wrap(WebixPortalTranslationService.getTransProjects({
                projectId: this.projectId
            })).its(0).its('target_language').its(0).its('id').as('targetLanguageId')
            cy.get('@targetLanguageId').then(targetLanguageId => {
                const targetSkills = skills.filter(el => el.skill.id === targetLanguageId as unknown as number)
                cy.log("Target.. " + targetLanguageId + "   " + skills.length)
                expect(targetSkills).length.be.gt(0)
            })
        })
    })

    it("should add a new skill", function () {
        cy.wrap(WebixPortalFreelancerService.postObjectSkill({
            objectId: this.projectId,
            skillIds: [IntranetSkillBusinessSector.AUGENOPTIK]
        }))
        .then(_skills => {
            const skills = _skills as ISkill[]
            skills.forEach(skill => {
                expect(skill.skill_type.id).eq(IntranetSkillType.BUSINESS_SECTOR)
            })
        })
    })

    it("should delete the skill again", function () {
        cy.wrap(WebixPortalFreelancerService.deleteObjectSkill({
            objectId: this.projectId,
            skillIds: [IntranetSkillBusinessSector.AUGENOPTIK]
        }))
    })
})