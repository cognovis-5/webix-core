import { IFreelancePackage, ITransTask, CognovisRestCompanyService, IntranetTranslationLanguage, ICompany, IntranetCompanyType, IntranetProjectType, ITransProject, IFreelancer, IAssignment, OpenAPI, WebixPortalAssignmentService, WebixPortalFreelancerService, WebixPortalTranslationService, IFreelancerLanguage, IntranetUom } from "../../../../sources/openapi";
import { randomizedName , getRandomNumberBetween} from "../../../support/helpers"

before(() => {
    cy.fixture('cypress.env.json')
    .then((testEnv) => {
        // First we authorize to our app
        OpenAPI.TOKEN = testEnv.enviromentTestVariables.DEV.admin.api_key;
    });
});

beforeEach(() => {
    // get a random test company
    cy.wrap(CognovisRestCompanyService.getCompanies({
        autocompleteQuery: 'test_',
        companyTypeIds: [IntranetCompanyType.CUSTOMER]
    }).then(res => {
        const company = res[Math.floor(Math.random() * (res.length -1))] as ICompany 
        return WebixPortalTranslationService.postTransProject(
            {
                requestBody: {
                    company_id: company.company.id,
                    project_type_id: IntranetProjectType.TRANS_PROOF_EDIT,
                    source_language_id: IntranetTranslationLanguage.DEDE,
                    target_language_ids: [IntranetTranslationLanguage.ENGB, IntranetTranslationLanguage.ESES]
                }
            }
        )
        
    }), {timeout: 10000}).as('project')
})

describe("testing assignment getting", () => {


    it("should create a two tasks and they should be in the same package", () => {
        cy.get('@project').then(_project => {
            const project = _project as unknown as ITransProject 

            cy.wrap(WebixPortalTranslationService.postTransTasks(
                {
                    projectId:project.project.id, 
                    requestBody: {
                        task_name: randomizedName('TestTask+'), 
                        task_uom_id: IntranetUom.SWORD, 
                        task_units: getRandomNumberBetween(10,100), 
                        task_type_id: IntranetProjectType.TRANS_PROOF
                    }
                })
            ).as('trans_tasks').should('have.length',2);
            
            cy.get('@trans_tasks').then(_trans_tasks => {
                const trans_task = _trans_tasks[0] as unknown as ITransTask;
                const trans_task_id = trans_task.task.id;
                cy.log('Getting packages for created task ' + trans_task_id);

                cy.wrap(WebixPortalAssignmentService.getPackages({
                    projectId: trans_task.project.id
                }), { timeout: 10000} ).should('have.length',4)
                .as('trans_packages')
            })

            cy.get('@trans_packages').then(_trans_packages => {
                const trans_packages = _trans_packages as unknown as [IFreelancePackage];
                trans_packages.forEach(element => {
                    expect(element.tasks.length).eq(1)
                });
                cy.wrap(WebixPortalTranslationService.postTransTasks(
                    {
                        projectId:project.project.id, 
                        requestBody: {
                            task_name: randomizedName('TestTask+'), 
                            task_uom_id: IntranetUom.SWORD, 
                            task_units: getRandomNumberBetween(10,100), 
                            task_type_id: IntranetProjectType.TRANS_PROOF
                        }
                    })
                ).as('trans_tasks2').should('have.length',2); 

                cy.get('@trans_tasks2').then(_trans_tasks => {
                    const trans_task = _trans_tasks[0] as unknown as ITransTask;
                    const trans_task_id = trans_task.task.id;
                    cy.log('Getting packages for created task ' + trans_task_id);
    
                    cy.wrap(WebixPortalAssignmentService.getPackages({
                        projectId: trans_task.project.id
                    }), { timeout: 10000} ).should('have.length',4)
                    .as('trans_packages2')
                })
                cy.get('@trans_packages2').then(_trans_packages => {
                    const trans_packages = _trans_packages as unknown as [IFreelancePackage];
                    trans_packages.forEach(element => {
                        expect(element.tasks.length).eq(2)
                    });
                })
            })
        })
    });


    it("should create a task and remove it from the package", () => {
        cy.get('@project').then(_project => {
            const project = _project as unknown as ITransProject
            cy.wrap(WebixPortalTranslationService.postTransTasks(
                {
                    projectId:project.project.id, 
                    requestBody: {
                        task_name: randomizedName('TestTask+'), 
                        task_uom_id: IntranetUom.SWORD, 
                        task_units: getRandomNumberBetween(10,100), 
                        task_type_id: IntranetProjectType.TRANS_PROOF_EDIT
                    }
                })
            ).as('trans_tasks').should('have.length.gt',0);
        })
                
        cy.get('@trans_tasks').then(_trans_tasks => {
            const trans_tasks = _trans_tasks as unknown as [ITransTask];
            const trans_task_id = trans_tasks[0].task.id;
            cy.log('Getting packages for created task ' + trans_task_id);

            cy.wrap(WebixPortalAssignmentService.getPackages({
                projectId: projectId,
                transTaskId: trans_task_id
            }), { timeout: 10000} ).should('have.length.gt',0)
            .its(0).as('package');

            cy.get('@package').then(_package => {
                const trans_package = _package as unknown as IFreelancePackage;
                // cy.removeTaskFromPackage(trans_task_id, trans_package.freelance_package.id)
                cy.log('Removing task ' + trans_task_id + ' from ' + trans_package.freelance_package.name);
                const tasks = trans_package.tasks
                const taskIds = [...new Set(tasks.map(item => item.task.id))]
                taskIds.forEach((element,index)=>{
                    if(element==trans_task_id) taskIds.splice(index,1);
                 });
                cy.wrap(WebixPortalAssignmentService.putPackages({
                    freelancePackageId: trans_package.freelance_package.id,
                    requestBody: {
                        trans_task_ids: taskIds
                    }
                })).as('return_package')
            })
        
            cy.get('@return_package').then(_package => {
                const trans_package = _package as unknown as IFreelancePackage;
                const tasks = trans_package.tasks
                const taskIds = tasks.map(item => item.task.id)
                expect(taskIds.indexOf(trans_task_id)).lessThan(0)
                
            })
        })
    });

    it("should add the task to the package", () => {
        cy.wrap(WebixPortalTranslationService.getTransTasks({
            projectId: projectId,
            taskName: randomizedName('TestTask+')
        })).should('have.length.gt',0)
        .its(0).as('trans_task');

        cy.get('@trans_task').then(_trans_task => {
            const trans_task = _trans_task as unknown as ITransTask
            const trans_task_id = trans_task.task.id;
            cy.log('Getting packages for created task ' + trans_task_id);

            cy.wrap(WebixPortalAssignmentService.getPackages({
                projectId: projectId,
                transTaskId: trans_task_id
            }), { timeout: 10000} ).should('have.length.gt',0)
            .its(0).as('package');

            
            cy.get('@package').then(_package => {
                const trans_package = _package as unknown as IFreelancePackage;
                // cy.removeTaskFromPackage(trans_task_id, trans_package.freelance_package.id)
                cy.log('Removing task ' + trans_task_id + ' from ' + trans_package.freelance_package.name);
                const tasks = trans_package.tasks
                const taskIds = [...new Set(tasks.map(item => item.task.id))]
                taskIds.forEach((element,index)=>{
                    if(element==trans_task_id) taskIds.splice(index,1);
                 });
                cy.wrap(WebixPortalAssignmentService.putPackages({
                    freelancePackageId: trans_package.freelance_package.id,
                    requestBody: {
                        trans_task_ids: taskIds
                    }
                })).as('return_package')
            })
        
            cy.get('@return_package').then(_package => {
                const trans_package = _package as unknown as IFreelancePackage;
                const tasks = trans_package.tasks
                const taskIds = tasks.map(item => item.task.id)
                expect(taskIds.indexOf(trans_task_id)).lessThan(0)
                
            })
        });
    });

    it("should remove the task", () => {
        cy.log("Getting task for deletion from name " + randomizedName('TestTask+'));
        cy.wrap(WebixPortalTranslationService.getTransTasks({
            projectId: projectId,
            taskName: randomizedName('TestTask+')
        })).should('have.length.gt',0)
        .its(0).as('trans_task');
  
        cy.get('@trans_task').then(_trans_task => {
            const trans_task = _trans_task as unknown as ITransTask
            cy.log('Deleting task ' + trans_task.task.name);
            cy.wrap(WebixPortalTranslationService.deleteTransTasks({taskIds: [trans_task.task.id]}));
        });
    })
}); 


describe("KOL-1365 - Get ratings for an assignment", () => {

    const assignmentId = 1132995;
    it("Get an assignment which has a rating", function () {
        cy.wrap(WebixPortalAssignmentService.getTranslationAssignments({
            assignmentId: assignmentId
        })).its(0).as("assignment").its("rating").should('not.be.empty');

        cy.get('@assignment').then(_assignment => {
            const assignment = _assignment as unknown as IAssignment;
            expect (assignment.rating_comment).not.null;
            expect(assignment.rating[0].quality_type.id).be.within(7000,7020);
        })
    })
}) 

/*
describe("KOL-1360 - Freelance Reminder endpoint", () => {

    before(() => {
        // get an assignment which could still have a reminder
        cy.wrap(WebixPortalAssignmentService.getTranslationAssignments({
            assignmentStatusId: 4221
        }).then(_assignments => {
            return _assignments[Math.floor(Math.random() * _assignments.length)].assignment.id;
        })).as('assignmentId')

        cy.wrap(WebixPortalAssignmentService.getTranslationAssignments({
            assignmentStatusId: 4222
        }).then(_assignments => {
            return _assignments[Math.floor(Math.random() * _assignments.length)].assignment.id;
        })).as('invalid_assignmentId')
    })
    it("Get an okay for an assignment for which we can send a reminder", function () {
        const assignmentId = this.assignmentId
        cy.log("Testing valid assignment " + assignmentId)
        cy.wrap(WebixPortalAssignmentService.postAssignmentRemindFreelancer({
            assignmentId: assignmentId
        }),{timeout: 10000}).its('success').should('be.true');
    });

    it("Return an error if we could not send the reminder", function () {
        const assignmentId = this.invalid_assignmentId
        cy.log("Testing invalid assignment " + assignmentId)
        cy.wrap(WebixPortalAssignmentService.postAssignmentRemindFreelancer({
            assignmentId: assignmentId
        }),{timeout: 10000}).its('success').should('be.false')        
    }); 
});



describe("KOL-1421 - Get unassigned tasks for a single language", () => {
    before(() => {

        // Get a project with multiple target languages        
        cy.wrap(WebixPortalTranslationService.getTransProjects({
            projectTypeId: [87],
            projectStatusId: [76,71]
        })).as('projects')    
        

        cy.get('@projects').then(_projects => {
            const projects = _projects as unknown as Array<ITransProject>
            const multiTargetProject = projects.filter(el => el.target_language.length >1)
            return multiTargetProject[Math.floor(Math.random() * (multiTargetProject.length -1))]
        }).as('project')
    })

    it("Should return unassigned tasks for a project", function () {
        cy.wrap(WebixPortalAssignmentService.getTasksWithoutPackage({
            projectId: this.project.project.id,
            targetLanguageId: this.project.target_language[0].id
        })).should('not.be.empty')
        .then(_tasks => {
            const tasks = _tasks as ITransTask[]
            tasks.forEach(task => {
                expect(task.target_language.id).eq(this.project.target_language[0].id)
            });
        })
    })
})
*/ 

describe("KOL-1428 - Limit packages to only those of the freelancers language", () => {

    before(() => {

        // Get a project with multiple target languages        
        cy.wrap(WebixPortalTranslationService.getTransProjects({
            projectTypeId: [87],
            projectStatusId: [76,71]
        })).as('projects')    
        

        cy.get('@projects').then(_projects => {
            const projects = _projects as unknown as Array<ITransProject>
            const multiTargetProject = projects.filter(el => el.target_language.length >1)
            return multiTargetProject[Math.floor(Math.random() * (multiTargetProject.length -1))]
        }).as('project')

        cy.get('@project').then(_project => {
            const project = _project as unknown as ITransProject
            cy.log("Found project " + project.project.name)

            // Select a single target language
            const targetLanguageId = project.target_language[Math.floor(Math.random() * project.target_language.length)].id

            cy.log("Using language " + targetLanguageId)

            // select a freelancer for this language
            cy.wrap(WebixPortalAssignmentService.getFreelancersForProject({
                projectId: project.project.id,
                targetLanguageIds: [targetLanguageId]
            }).then(_freelancers => {
                const freelancers = _freelancers as unknown as IFreelancer[]
                console.log("Found "+ freelancers.length + " freelancer")
                const freelancer = freelancers[Math.floor(Math.random() * (freelancers.length -1))]
                console.log(freelancer.freelancer.name);              
                return freelancer
            })).as('freelancer')

            cy.get('@freelancer').then(_freelancer => {
                const freelancer = _freelancer as unknown as IFreelancer
                cy.log(freelancer.freelancer.name)
                cy.wrap(WebixPortalFreelancerService.getFreelancerProjectLanguages({
                    freelancerIds: [freelancer.freelancer.id],
                    projectId: project.project.id
                })).as('freelancer_languages')
            })
        })    
    })

    it("Ensure that the only the freelancers languages are returned in the endpoint", function () {
        const project = this.project
        const freelancer = this.freelancer as IFreelancer
        const freelancer_languages = new Array<number>();
        this.freelancer_languages.forEach(_language => {
            const language = _language as IFreelancerLanguage
            freelancer_languages.push(language.language.id)
        })
         
        cy.wrap(WebixPortalAssignmentService.getPackages({
            projectId: project.project.id,
            freelancerIds: [freelancer.freelancer.id]
        })).as('packages').should('not.be.null')

        cy.log("Target languages " + freelancer_languages)
        

        cy.get('@packages').then(_packages => {
            const packages = _packages as unknown as IFreelancePackage[]
            cy.log("Found " + packages.length + " packages")
        })

        cy.get('@packages').then(_packages => {
            const packages = _packages as unknown as IFreelancePackage[]
            if(packages.length>0) {
                packages.forEach(_package => {
                    console.log(_package.freelance_package.name)
                    cy.log("Looking at package " + _package.freelance_package.name + " with " + _package.target_language.name)
                })
            } 
        })
    })
});

describe("KOL-1306 - Return package comment with the packages endpoints", () => {

    beforeEach(() => {

        // get an assignment which could still have a reminder
        cy.wrap(WebixPortalAssignmentService.getTranslationAssignments({
            assignmentStatusId: 4221
        }).then(_assignments => {
            const freelance_package_id = _assignments[Math.floor(Math.random() * _assignments.length)].freelance_package.id;
            return freelance_package_id
        })).as('freelancePackageId')

        cy.get('@freelancePackageId').then(_packageId => {
            const packageId = _packageId as unknown as number
            cy.wrap(WebixPortalAssignmentService.getPackages({
                freelancePackageIds: [packageId]
            })).its(0).as('package')
        })
    })
    it('Should contain the package_comment field', function () {
        cy.get('@package').then(_package => {
            const trans_package = _package as unknown as IFreelancePackage
            cy.log("Found package " + trans_package.freelance_package.name)
            expect(trans_package.freelance_package.name).not.null
            expect(trans_package.package_comment).not.undefined
        })
    })

    it('should update the package_comment field', function() {
        cy.get('@package').then(_package => {
            const trans_package = _package as unknown as IFreelancePackage
            cy.log("Found package " + trans_package.freelance_package.name)

            if(trans_package.package_comment === null) {
                cy.wrap(WebixPortalAssignmentService.putPackages({
                    freelancePackageId: trans_package.freelance_package.id,
                    requestBody: {
                        package_comment: 'This is a default comment from testing'   
                    }
                })).its('package_comment').should('equal', 'This is a default comment from testing')
            } else {
                cy.log('Package already had a comment, not testing an update: ' + trans_package.package_comment)
            }
        })
    })
})