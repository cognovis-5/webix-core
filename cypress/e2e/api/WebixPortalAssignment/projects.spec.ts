import { ITransProject, OpenAPI,  WebixPortalTranslationService,  CognovisRestCompanyService, IntranetCompanyType, IntranetTranslationLanguage, ICompany, IntranetProjectType, IntranetTranslationSubjectArea, IntranetSkillType, WebixPortalFreelancerService, IntranetSkillBusinessSector, WebixPortalAssignmentService, WebixPortalProjectService, CognovisRestNotesService, IntranetProjectStatus, IntranetUom, ITransTask, IInvoice, CognovisRestInvoiceService, IInvoiceItem, CognovisRestRelationshipService, CognovisRestObjectService, CognovisRestService, IObjectSkill } from "../../../../sources/openapi/"
import { randomizedName } from "../../../support/helpers"

before(() => {
    cy.fixture('cypress.env.json')
    .then((testEnv) => {
        // First we authorize to our app
        OpenAPI.TOKEN = testEnv.enviromentTestVariables.DEV.pm.api_key;
    });
});

beforeEach(() => {
    // get a random test company
    cy.wrap(CognovisRestCompanyService.getCompanies({
        autocompleteQuery: 'cypress',
        companyTypeIds: [IntranetCompanyType.CUSTOMER]
    }).then(res => {
        const customers = res.filter(el => el.primary_contact !== null)
        return customers[Math.floor(Math.random() * (customers.length -1))]
    })).as('test_company')

    cy.wrap(CognovisRestCompanyService.getCompanies({
        companyTypeIds: [IntranetCompanyType.FINAL_COMPANY]
    }).then(res => {
        return res[Math.floor(Math.random() * (res.length -1))]
    })).as('final_company')

    cy.fixture('cypress.env.json')
    .then((testEnv) => {
        // First we authorize to our app
        return testEnv.enviromentTestVariables.DEV.accounting.user_id;
    }).as('accountingId')

    cy.fixture('cypress.env.json')
    .then((testEnv) => {
        // First we authorize to our app
        return testEnv.enviromentTestVariables.DEV.pm.user_id;
    }).as('pmId')
})

describe("Should create a new translation project with languages", () => {

    it("should create a new open project", function () {
        const company = this.test_company as ICompany 
        cy.wrap(WebixPortalTranslationService.postTransProject(
            {
                requestBody: {
                    company_id: company.company.id,
                    customer_contact_id: company.primary_contact.id,
                    source_language_id: IntranetTranslationLanguage.DEDE,
                    target_language_ids: [IntranetTranslationLanguage.ENGB, IntranetTranslationLanguage.ESES],
                    description: 'This is a new description which should become a note',
                    project_status_id: IntranetProjectStatus.OPEN
                }
            }
        ), {timeout: 10000}).as('project')
        
        cy.get('@project').then(_project => {

            const project = _project as unknown as ITransProject

            cy.wrap(WebixPortalTranslationService.getTransProjects({
                projectNr: project.project_nr
            })).should('not.be.empty')

            cy.wrap(CognovisRestNotesService.getNote({
                objectId: project.project.id
            })).should('not.be.empty')
            
            cy.wrap(WebixPortalAssignmentService.getProjectDefaultFilters({
                projectId: project.project.id
            })
            .then(res => {
                expect(res.source_language).have.members
                const targetLanguageIds = [...new Set(res.target_language.map(item => item.id))]
                expect(targetLanguageIds).to.have.members([IntranetTranslationLanguage.ENGB,IntranetTranslationLanguage.ESES])
            }))

            cy.log("Getting basic project info")
            cy.wrap(WebixPortalProjectService.getBasicProjectInfo({
                projectId: project.project.id
            })).should('not.be.empty')

            cy.log("created project " + project.project_nr + " with folder " + project.project_folder.name)

            expect(project.start_date).not.null
        })
    })
    
    it.only("should create a new open project with default language", function () {
        const company = this.test_company as ICompany 
        cy.wrap(WebixPortalTranslationService.postTransProject(
            {
                requestBody: {
                    company_id: company.company.id,
                    customer_contact_id: company.primary_contact.id,
                    description: 'This is a new description which should become a note',
                    project_status_id: IntranetProjectStatus.OPEN
                }
            }
        ), {timeout: 10000}).as('project')
        
        cy.get('@project').then(_project => {

            const project = _project as unknown as ITransProject

            cy.wrap(WebixPortalTranslationService.getTransProjects({
                projectNr: project.project_nr
            })).should('not.be.empty')

            cy.wrap(CognovisRestNotesService.getNote({
                objectId: project.project.id
            })).should('not.be.empty')
            
            cy.wrap(WebixPortalAssignmentService.getProjectDefaultFilters({
                projectId: project.project.id
            })
            .then(res => {
                expect(res.source_language).have.members
                const targetLanguageIds = [...new Set(res.target_language.map(item => item.id))]
                expect(targetLanguageIds).to.have.members([IntranetTranslationLanguage.DEDE])
            }))

            cy.log("Getting basic project info")
            cy.wrap(WebixPortalProjectService.getBasicProjectInfo({
                projectId: project.project.id
            })).should('not.be.empty')

            cy.log("created project " + project.project_nr + " with folder " + project.project_folder.name)

            expect(project.start_date).not.null
        })
    })

    it("Should create a new translation + edit project with multiple languages and edit it in meantime", function () {
        const company = this.test_company as ICompany 
        const company_contact_id = this.company_contact_id
        const final_company = this.final_company as ICompany 
        const company_project_nr = randomizedName('ProjectNr+')

        cy.wrap(WebixPortalTranslationService.postTransProject(
            {
                requestBody: {
                    company_id: company.company.id,
                    project_type_id: IntranetProjectType.TRANS_PROOF_EDIT,
                    source_language_id: IntranetTranslationLanguage.DEDE,
                    target_language_ids: [IntranetTranslationLanguage.ENGB, IntranetTranslationLanguage.ESES],
                    subject_area_id: IntranetTranslationSubjectArea.FINANZTEXTE,
                    final_company_id: final_company.company.id,
                    customer_contact_id: company_contact_id,
                    company_project_nr:  company_project_nr,
                    project_name: '2021_0001'
                }
            }
        ), {timeout: 10000}).as('project')

        cy.get('@project').then(_project => {

            const project = _project as unknown as ITransProject

            expect(project.project_folder.id).gt(0)
            expect(project.final_company.id).gt(0)
            expect(project.project.name).eq('2021_0001')
            expect(project.project_nr).not.eq('2021_0001')

            cy.wrap(WebixPortalFreelancerService.postObjectSkill({
                objectId: project.project.id,
                skillIds: [IntranetSkillBusinessSector.ARCHITEKTUR]
            }))

            cy.wrap(WebixPortalTranslationService.getTransProjects({
                projectNr: project.project_nr
            })).should('not.be.empty')

            cy.wrap(WebixPortalFreelancerService.getObjectSkill({
                objectId: project.project.id,
                skillTypeId: IntranetSkillType.BUSINESS_SECTOR,
            }))
            .then(_skills => {
                const skills = _skills as IObjectSkill[]
                skills.forEach(skill => {
                    expect(skill.skill_type.id).eq(IntranetSkillType.BUSINESS_SECTOR)
                    cy.log("Found skill " + skill.skill.name)
                })
            })
            
            cy.wrap(WebixPortalTranslationService.putTransProject({
                projectId: project.project.id,
                requestBody: {
                    company_id: company.company.id,
                    project_type_id: IntranetProjectType.TRANS_PROOF_EDIT,
                    source_language_id: IntranetTranslationLanguage.DEDE,
                    target_language_ids: [IntranetTranslationLanguage.FRFR],
                    subject_area_id: IntranetTranslationSubjectArea.MARKETINGUNTERLAGEN,
                    final_company_id: final_company.company.id,
                    company_project_nr:  "New project number",
                    processing_time: 10
                }
            }))
            .then(_update_project => {
                const update_project = _update_project as unknown as ITransProject
                expect(update_project.company_project_nr).eq('New project number')
                expect(update_project.processing_time).eq(10)
                expect(update_project.target_language.length).eq(1)
                expect(update_project.target_language[0].id).eq(IntranetTranslationLanguage.FRFR)
                cy.log("Updated project name is: " + update_project.company_project_nr)
            })
            
            cy.log("created project " + project.project_nr)
        })
    })

    afterEach(() => {
        cy.get('@project').then(_project => {
            const project = _project as unknown as ITransProject
            cy.wrap(WebixPortalTranslationService.deleteTransProject({
                projectId: project.project.id
            })).should('be.empty')
            .then(() => {
                cy.wrap(WebixPortalTranslationService.getTransProjects({
                    projectNr: project.project_nr
                })).should('be.empty')    
            })
        })
    }) 

/*    it("Should create a new translation project with skills and file upload", function () {
//
    })*/
})

describe("Scenarios for copying projects", () => {
    beforeEach(() => {
        // get a random test company
        cy.wrap(CognovisRestCompanyService.getCompanies({
            autocompleteQuery: 'test_',
            companyTypeIds: [IntranetCompanyType.CUSTOMER]
        }).then(res => {
            return res[Math.floor(Math.random() * (res.length -1))]
        })).as('company')

        cy.get('@company').then(_company => {
            const company = _company as unknown as ICompany 
            cy.wrap(WebixPortalTranslationService.getTransProjects({
                projectStatusId: [76,71],
                companyId: company.company.id
            }).then(res => {
                return res[Math.floor(Math.random() * (res.length -1))]
            })).as('original_project')
        })
    })
    
    afterEach(() => {
        cy.get('@new_project').then(_project => {
            const project = _project as unknown as ITransProject
            cy.wrap(WebixPortalTranslationService.deleteTransProject({
                projectId: project.project.id
            })).should('be.empty')
            .then(() => {
                cy.wrap(WebixPortalTranslationService.getTransProjects({
                    projectNr: project.project_nr
                })).should('be.empty')    
            })
        })
    }) 

    it("Should copy a translation project", function () {
        const originalProject = this.original_project as unknown as ITransProject
        cy.wrap(WebixPortalTranslationService.postCloneTransProject({
            projectId: originalProject.project.id,
            requestBody: {}
        })).as('new_project')

        cy.get('@new_project').then(_new_project => {
            const new_project = _new_project as unknown as ITransProject
            expect(originalProject.processing_time).eq(new_project.processing_time)
            expect(originalProject.target_language.length).eq(new_project.target_language.length)
            expect(originalProject.source_language.id).eq(new_project.source_language.id)
            expect(originalProject.subject_area?.id).eq(new_project.subject_area?.id)
            expect(originalProject.project.name).eq(new_project.project.name)
            expect(originalProject.project_nr).not.eq(new_project.project_nr)
            expect(originalProject.complexity_type?.id).eq(new_project.complexity_type?.id)
            expect(originalProject.final_company?.id).eq(new_project.final_company?.id)
        })
    })

    it("Should clone a project with batches for a new customer", function () {
        cy.createTestTransProjectWithAssignments().as('project')
        .then(_project => {
            cy.getCompany(_project).then(_company => {
                return _company[0]
            }).as('company')
        })

        cy.get('@company').then(_company => {
            const company = _company as unknown as ICompany 
            cy.wrap(CognovisRestCompanyService.getCompanies({
                autocompleteQuery: 'test_',
                companyTypeIds: [IntranetCompanyType.CUSTOMER]
            }).then(res => {
                res = res.filter(obj => obj !== company);
                return res[Math.floor(Math.random() * (res.length -1))]
            })).as('new_company')

        })

        cy.get('@project').then(_project => {
            const originalProject = _project as ITransProject
            cy.wrap(CognovisRestService.postBizObjectMember({
                requestBody: {
                    object_id: originalProject.project.id,
                    member_id: this.accountingId,
                    role_id: 1300 
                }
            }))
            cy.get('@new_company').then(_new_company => {
                const new_company = _new_company as unknown as ICompany 
                cy.wrap(WebixPortalTranslationService.postCloneTransProject({
                    projectId: originalProject.project.id,
                    requestBody: {
                        company_id: new_company.company.id,
                        packages: true,
                        project_members: true
                    }
                })).as('new_project')
            })

            cy.get('@new_project').then(_new_project => {
                const new_project = _new_project as unknown as ITransProject
                expect(originalProject.processing_time).eq(new_project.processing_time)
                expect(originalProject.target_language.length).eq(new_project.target_language.length)
                expect(originalProject.source_language.id).eq(new_project.source_language.id)
                expect(originalProject.subject_area?.id).eq(new_project.subject_area?.id)
                expect(originalProject.project.name).eq(new_project.project.name)
                expect(originalProject.project_nr).not.eq(new_project.project_nr)
                expect(originalProject.complexity_type?.id).eq(new_project.complexity_type?.id)
                expect(originalProject.final_company?.id).eq(new_project.final_company?.id)
                cy.get('@new_company').then(_new_company => {
                    const new_company = _new_company as unknown as ICompany 
                    expect(new_project.company.id).eq(new_company.company.id)
                })
                cy.get('@company').then(_company => {
                    const company = _company as unknown as ICompany 
                    expect(new_project.company.id).not.eq(company.company.id)
                })
            })
        })

        cy.get('@project').then(_project => {
            const project = _project as unknown as ITransProject
            cy.wrap(WebixPortalTranslationService.deleteTransProject({
                projectId: project.project.id
            })).should('be.empty')
            .then(() => {
                cy.wrap(WebixPortalTranslationService.getTransProjects({
                    projectNr: project.project_nr
                })).should('be.empty')    
            })
        })
    })
})

describe("Quote handling from trans tasks", () => {

    afterEach(() => {
        cy.get('@project').then(_project => {
            const project = _project as unknown as ITransProject
            cy.wrap(WebixPortalTranslationService.deleteTransProject({
                projectId: project.project.id
            }), {timeout: 10000}).should('be.empty')
            .then(() => {
                cy.wrap(WebixPortalTranslationService.getTransProjects({
                    projectNr: project.project_nr
                })).should('be.empty')    
            })
        })
    }) 

    it ("Should create a project with a task, quote, then add a new task and add it to the existing quote", function () {   
        const company = this.test_company as ICompany 
        cy.wrap(WebixPortalTranslationService.postTransProject(
            {
                requestBody: {
                    company_id: company.company.id,
                    customer_contact_id: company.primary_contact.id,
                    source_language_id: IntranetTranslationLanguage.DEDE,
                    target_language_ids: [IntranetTranslationLanguage.ENGB, IntranetTranslationLanguage.ESES],
                    description: 'This is a new description which should become a note',
                    project_status_id: IntranetProjectStatus.OPEN
                }
            }
        ), {timeout: 10000}).as('project')

        cy.get('@project').then(_project => {
            const project = _project as unknown as ITransProject
            cy.wrap(WebixPortalTranslationService.postTransTasks({
                projectId:project.project.id, 
                requestBody: {
                    task_name: randomizedName('TestTask+'), 
                    task_uom_id: IntranetUom.SWORD,
                    task_units: 300, 
                    task_type_id:87
                }
            })).as('originalTasks')

            cy.wrap(WebixPortalTranslationService.postTransTasks({
                projectId:project.project.id, 
                requestBody: {
                    task_name: randomizedName('NewTask+'), 
                    task_uom_id: IntranetUom.SWORD,
                    task_units: 100, 
                    task_type_id:87
                }
            })).as('newTasks')
        })

        cy.get('@originalTasks').then(_tasks => {
            const tasks = _tasks as unknown as ITransTask[]
            const taskIds = [...new Set(tasks.map(item => item.task.id))]
            cy.log("Creating quote for the task")
            cy.wrap(WebixPortalTranslationService.postTransQuote({
                requestBody: {
                    trans_tasks: taskIds,
                    quote_per_language_p: true
                }
            }), {timeout: 15000}).as('quotes')
        })

        cy.get('@quotes').then(_quotes => {
            const quotes = _quotes as unknown as IInvoice[]
            expect(quotes.length).eq(2)
            cy.get('@newTasks').then(_tasks => {
                const tasks = _tasks as unknown as ITransTask[]
                const taskIds = [...new Set(tasks.map(item => item.task.id))] 
                cy.wrap(WebixPortalTranslationService.postTransQuote({
                    requestBody: {
                        trans_tasks: taskIds,
                        quote_id: quotes[0].invoice.id
                    }
                }), {timeout: 15000}).its(0).as('enlargedQuote')
            })
        })

        cy.get('@enlargedQuote').then(_quote => {
            const quote = _quote as unknown as IInvoice
            cy.wrap(CognovisRestInvoiceService.getInvoiceItem({
                invoiceId: quote.invoice.id
            })).as('invoiceItems')
        })

        cy.get('@invoiceItems').then(_items => {
            const items = _items as unknown as IInvoiceItem[]
            expect(items.length).eq(3)
        })
    })
})
