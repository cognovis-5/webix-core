import {IntranetUom, OpenAPI, WebixPortalAssignmentService, CognovisRestCompanyService, IntranetTranslationLanguage, ICompany, IntranetCompanyType, WebixPortalTranslationService,IntranetTransTaskType, IntranetFreelanceAssignmentStatus, IntranetTranslationSubjectArea, ITransProject, IAssignment, IntranetProjectType, IntranetProjectStatus, IntegerInt32, IntranetCostStatus, IFreelancerAssignment } from "../../../../sources/openapi";
import { IAssignmentQualityReport } from "../../../../sources/openapi/models/IAssignmentQualityReport";
import type { INamedId } from '../../../../sources/openapi/models/INamedId';
import { IntranetQuality } from "../../../../sources/openapi/models/IntranetQuality";
import { IntranetTranslationQualityType } from "../../../../sources/openapi/models/IntranetTranslationQualityType";
import { randomizedName,getRandomNumberBetween } from "../../../support/helpers"

before(() => {
    cy.fixture('cypress.env.json')
    .then((testEnv) => {
        // First we authorize to our app
        OpenAPI.TOKEN = testEnv.enviromentTestVariables.DEV.admin.api_key;
    });
});

describe("Get Assignment", () => {
    
    before(() => {

        // Get the original assignment into the b variable, so we can use it
        // in the next IT. Don't use beforeEach, as this would have overwritten
        // the deadline, then called again the getTranslationAssignments
        // which at that point would have already had the new deadline.

        cy.wrap(WebixPortalAssignmentService.getTranslationAssignments({
            assignmentStatusId: IntranetFreelanceAssignmentStatus.ACCEPTED
        })
        .then(_assignments => {
            return _assignments[0]
        })).as('b')
        .should('have.property','assignment_deadline')
    });

    it('Should get Update Assignment and reset', function () {

        cy.log("Change Deadline");
        const assignment_deadline = '2017-08-16T22:00:00.000000Z'
        const assignment = this.b
        cy.wrap(WebixPortalAssignmentService.putTranslationAssignments({
            assignmentId: assignment.assignment.id,
            requestBody: {
                uom_id: assignment.uom.id,
                assignment_deadline: assignment_deadline,
                start_date: assignment.start_date,
                assignment_status_id: assignment.assignment_status.id    
            }
        }))
        .its('assignment_deadline').should('eq',assignment_deadline)

        .then(() => {        
            // Using .then here is the key, as we can inherit the assignment from before.
            // Probably need to do this everytime now. First the "action" and .then what we need to do afterwards.
            cy.log("Reset Deadline to " + assignment.assignment_deadline);
            cy.wrap(WebixPortalAssignmentService.putTranslationAssignments({
                assignmentId: assignment.assignment.id,
                requestBody: {
                    uom_id: assignment.uom.id,
                    assignment_deadline: assignment.assignment_deadline,
                    start_date: assignment.start_date,
                    assignment_status_id: assignment.assignment_status.id    
                }
            }))
            .its('assignment_deadline').should('eq',this.b.assignment_deadline)
        });
    });
});

describe("testing assignment creation from task", () => {
    
    before(() => {
    
        // get a random test company
        cy.wrap(CognovisRestCompanyService.getCompanies({
            autocompleteQuery: 'test_',
            companyTypeIds: [IntranetCompanyType.CUSTOMER]
        }).then(res => {
            return res[Math.floor(Math.random() * (res.length -1))]
        })).as('test_company')

        cy.wrap(CognovisRestCompanyService.getCompanies({
            companyTypeIds: [IntranetCompanyType.FINAL_COMPANY]
        }).then(res => {
            return res[Math.floor(Math.random() * (res.length -1))]
        })).as('final_company')
    });
    

    it("should create and delete a task", function () {
        const company = this.test_company as ICompany 
        const randomNumber = Math.floor((9999 - 1) * Math.random());
        cy.wrap(WebixPortalTranslationService.postTransProject(
            {
                requestBody: {
                    company_id: company.company.id,
                    customer_contact_id: company.primary_contact.id,
                    project_type_id: IntranetProjectType.TRANS_PROOF,
                    project_status_id: IntranetProjectStatus.QUOTING,
                    source_language_id: IntranetTranslationLanguage.DEDE,
                    target_language_ids: [IntranetTranslationLanguage.ENGB, IntranetTranslationLanguage.ESES],
                    description: 'This is a new description which should become a note'
                }
            }
        ), {timeout: 10000}).as('project')

        cy.get('@project').then(_project => {
            const project = _project as any as ITransProject
            expect(project.project_status.id).eq(IntranetProjectStatus.QUOTING)
            cy.wrap(WebixPortalTranslationService.postTransTasks({
                projectId:project.project.id, 
                requestBody: {
                    task_name: randomizedName('TaskNr+'), 
                    task_uom_id: IntranetUom.SWORD,
                    task_units: randomNumber, 
                    task_type_id: IntranetProjectType.TRANS_PROOF
                }
            }))
            .its(0).its('task').its('id').as('taskId')

            cy.get('@taskId').then(taskId => {
                cy.log('Createing a package for task ' + taskId);
                const task_id = taskId as unknown as number;

                cy.wrap(WebixPortalAssignmentService.getPackages({
                    transTaskId: task_id,
                    packageTypeId: IntranetTransTaskType.TRANS
                }))
                .should('have.length',1)
                .its(0).its('freelance_package.id').as('packageId')
            });

            cy.get('@packageId').then(packageId => {
                const package_id = packageId as unknown as number;
                cy.log("Get freelancers to assign to " + package_id);
                cy.wrap(WebixPortalAssignmentService.getFreelancersForPackage({
                    freelancePackageId: package_id
                }))
                .should('have.length.greaterThan',0)
                .its(0).its('freelancer').as('freelancer')

                cy.get('@freelancer').then(freelancer_obj => {
                    const freelancer = freelancer_obj as unknown as INamedId
                    cy.log("Creating assignment for package " + packageId + " and freelancer " + freelancer.name)

                    cy.wrap(WebixPortalAssignmentService.postTranslationAssignments({
                        freelancePackageId: package_id,
                        freelancerIds: [freelancer.id],
                        requestBody: {}
                    }),{timeout: 10000})
                    .should('have.length.greaterThan',0)
                    .its(0).as('assignment')

                    cy.get('@assignment').then(assignment_obj => {
                        const assignment = assignment_obj as unknown as IAssignment
                        cy.log("Created assignment " + assignment.assignment.name + " with id " + assignment.assignment.id + " in status " + assignment.assignment_status.name)
                        expect(assignment.assignment_status.id).eq(IntranetFreelanceAssignmentStatus.CREATED);

                        cy.log('Updateing the project setting it to open');
                        cy.wrap(WebixPortalTranslationService.putTransProject({
                            projectId: project.project.id,
                            requestBody: {
                                project_status_id: IntranetProjectStatus.OPEN
                            }
                        }),{timeout: 10000}).as('openProject')
                        
                        cy.get('@openProject').then(() => {
                            cy.wrap(WebixPortalAssignmentService.getTranslationAssignments({
                                assignmentId: assignment.assignment.id
                            })).its(0).as('assignmentUpdated')

                            cy.get('@assignmentUpdated').then(_up_assignment => {
                                const upAssignment = _up_assignment as unknown as IAssignment 
                                expect(upAssignment.assignment_status.id).eq(IntranetFreelanceAssignmentStatus.REQUESTED)

                                cy.log("Delete assignment again")
                                cy.wrap(WebixPortalAssignmentService.deleteTranslationAssignments({assignmentIds: [assignment.assignment.id],purgeP:true}))
                                .should('be.empty');    
                            }).then(() => {
                                cy.log('New assignment should immediately be requested')
                                cy.wrap(WebixPortalAssignmentService.postTranslationAssignments({
                                    freelancePackageId: package_id,
                                    freelancerIds: [freelancer.id],
                                    requestBody: {}
                                }),{timeout: 10000})
                                .should('have.length.greaterThan',0)
                                .its(0).as('assignmentRequested')
    
                                cy.get('@assignmentRequested').then(_req_assignment => {
                                    const reqAssignment = _req_assignment as unknown as IAssignment 
                                    expect(reqAssignment.assignment_status.id).eq(IntranetFreelanceAssignmentStatus.REQUESTED)
                                })
                            })
                        })
                    })
                })
            });            
        });
    });

    after(() => {
        cy.get('@project').then(_project => {
            const project = _project as any as ITransProject
    
            cy.wrap(WebixPortalTranslationService.deleteTransProject({
                projectId: project.project.id
            })).should('be.empty')
            .then(() => {
                cy.wrap(WebixPortalTranslationService.getTransProjects({
                    projectNr: project.project_nr
                })).should('be.empty')    
            })
        });
    });

});


describe("Get Freelancer Assignments", () => {
    const propertiesForEachAssignments = [
        "assignment", "freelance_package", "assignment_type", "assignment_units",
        "purchase_order", "start_date", "assignment_deadline", "package_comment",
        "project", "project_lead", "final_company", "source_language", "target_language",
        "subject_area", "skill_business_sector"
    ];

    const freelancerId = 605302;

    it('Should check if getFreelancerAssignments returns all required properties', function () {
        cy.wrap(WebixPortalAssignmentService.getTranslationAssignments({
            assignmentStatusId: IntranetFreelanceAssignmentStatus.ACCEPTED,
        }).then(_assignments => {
            return _assignments[Math.floor(Math.random() * _assignments.length)].assignee.id;
        })).as('assigneeId')

        cy.get('@assigneeId').then(_assigeeId => {
            cy.wrap(WebixPortalAssignmentService.getFreelancerAssignments({
                freelancerId:_assigeeId as unknown as number
            }).then(_freelancerAssignments => {
                return _freelancerAssignments
            }))
            .then(_freelancerAssignments => {
                cy.wrap(_freelancerAssignments).each(_singleFreelancerAssignment => {
                    propertiesForEachAssignments.forEach(property => {
                        cy.wrap(_singleFreelancerAssignment).should("have.a.property", property);
                    });
                });
            });
        })
    });

    it('Should get a proof assignment and return the previous assignments', function () {
        cy.wrap(WebixPortalAssignmentService.getTranslationAssignments({
            assignmentTypeId: IntranetTransTaskType.PROOF
        }).then(_assignments => {
            return _assignments[Math.floor(Math.random() * _assignments.length)].assignment.id;
        })).as('assignmentId')

        cy.get('@assignmentId').then(_assignmentId => {
            cy.wrap(WebixPortalAssignmentService.getPreviousAssignments({
                assignmentId: _assignmentId as unknown as number,
                //assignmentStatusIds: [IntranetFreelanceAssignmentStatus.WORK_DELIVERED, IntranetFreelanceAssignmentStatus.DELIVERY_ACCEPTED, IntranetFreelanceAssignmentStatus.ASSIGNMENT_CLOSED]
            })).should('not.be.empty')
        })
    })
});

describe("Quality ratings for assignments", () => {
    it.only('Should create a quality report, change the comment, change a value in the quality report, delete a value in the report and then delete the whole report', function () {
        cy.log("getting the rating types for the assignments in question")
        cy.wrap(WebixPortalAssignmentService.getTranslationAssignments({
            assignmentStatusId: IntranetFreelanceAssignmentStatus.ACCEPTED,
            assignmentTypeId: IntranetTransTaskType.PROOF
        }).then(_assignments => {
            const assignmentId =  _assignments[Math.floor(Math.random() * _assignments.length)].assignment.id;
            return WebixPortalAssignmentService.getPreviousAssignments({
                assignmentId: assignmentId
            }).then(_prev_assignments => {
                return _prev_assignments[Math.floor(Math.random() * _prev_assignments.length)]
            })
        })).as('assignment')
        cy.get('@assignment').then(_assignment => {
            console.log(_assignment)
            const assignment = _assignment as unknown as IFreelancerAssignment
            cy.wrap(WebixPortalAssignmentService.getAssignmentQualityRatingTypes({
                assignmentIds: [assignment.assignment.id]
            })).as('rating_types')

            cy.log("Creating a single report with multiple ratings per type")

            cy.wrap(WebixPortalAssignmentService.postAssignmentQualityReport({
                assignmentId: assignment.assignment.id,
                requestBody: {
                    rating_comment: "This is a comment"
                }
            }))
            .then(_report => {
                const report = _report as unknown as IAssignmentQualityReport
                expect(report.report.id).not.null

                cy.log("Change the comment of the report")
                cy.wrap(WebixPortalAssignmentService.putAssignmentQualityReport({
                    reportId: report.report.id,
                    requestBody: {
                        rating_comment: "This is a new comment"
                    }
                })).then(_report2 => {
                    const report2 = _report2 as unknown as IAssignmentQualityReport
                    expect(report2.rating_comment).eq('This is a new comment')
                })

                cy.log("Posting an individual rating")
                cy.wrap(WebixPortalAssignmentService.postAssignmentQualityRating({
                    requestBody: {
                        report_id: report.report.id,
                        quality_level_id: IntranetQuality.HIGH_QUALITY,
                        quality_type_id: IntranetTranslationQualityType.GRAMMAR
                    }
                })).then(_report => {
                    const report = _report as unknown as IAssignmentQualityReport
                    expect(report.rating.length).eq(1);
                    expect(report.rating[0].quality_type.id).eq(IntranetTranslationQualityType.GRAMMAR)
                    expect(report.rating[0].quality_level.id).eq(IntranetQuality.HIGH_QUALITY)

                    cy.log("Change an individual rating level")
                    cy.wrap(WebixPortalAssignmentService.putAssignmentQualityRating({
                        requestBody: {
                            report_id: report.report.id,
                            quality_type_id: IntranetTranslationQualityType.GRAMMAR,
                            quality_level_id: IntranetQuality.AVERAGE_QUALITY
                        }
                    })).then(_report => {
                        const report = _report as unknown as IAssignmentQualityReport
                        expect(report.rating.length).eq(1);
                        expect(report.rating[0].quality_level.id).eq(IntranetQuality.AVERAGE_QUALITY)
                        expect(report.rating_value).eq(2)

                        cy.log("Delete an individual rating level")
                        cy.wrap(WebixPortalAssignmentService.deleteAssignmentQualityRating({
                            reportId: report.report.id,
                            qualityTypeId: IntranetTranslationQualityType.GRAMMAR
                        })).then(_report => {
                            const report = _report as unknown as IAssignmentQualityReport
                            expect(report.rating).null
                            expect(report.rating_value).null    
                            cy.log("Delete the complete report")

                            cy.wrap(WebixPortalAssignmentService.deleteAssignmentQualityReport({
                                reportId: report.report.id
                            })).should('be.null')
                        })
                    })
                })
            })
        })
    })        

    it("should return ratings for a freelancer", function() {
        const freelancerIds = [430792, 776335, 211964, 433082]
        const freelancerId = freelancerIds[Math.floor(Math.random() * freelancerIds.length)]
        cy.log("Found freelancer" + freelancerId)

        cy.wrap(WebixPortalAssignmentService.getAssignmentQualityReport({
            assigneeId: freelancerId
        })).should('not.be.empty')
        .then(_quality_reports => {
            const quality_reports = _quality_reports as unknown as IAssignmentQualityReport[]
            quality_reports.forEach(report => {
                expect(report.assignee.id).eq(freelancerId)
            })
        })
    })        

})

describe("Deletion checks", () => {
    beforeEach(() => {
        cy.fixture('cypress.env.json')
        .then((testEnv) => {
            // First we authorize to our app
            return testEnv.enviromentTestVariables.DEV.accounting.user_id;
        }).as('accountingId')

        cy.fixture('cypress.env.json')
        .then((testEnv) => {
            // First we authorize to our app
            return testEnv.enviromentTestVariables.DEV.pm.user_id;
        }).as('pmId')
        cy.createTestTransProjectWithAcceptedAssignments().as('project')
        .then(_project => {
            cy.getCompany(_project).then(_company => {
                return _company[0]
            }).as('company')
        })
    })

    afterEach(() => {
        cy.fixture('cypress.env.json')
        .then((testEnv) => {
            OpenAPI.TOKEN = testEnv.enviromentTestVariables.DEV.admin.api_key;

            cy.get('@project').then(_project => {
                const project = _project as unknown as ITransProject
        
                cy.wrap(WebixPortalTranslationService.deleteTransProject({
                    projectId: project.project.id
                }))
                .then(() => {
                    cy.wrap(WebixPortalTranslationService.getTransProjects({
                        projectNr: project.project_nr
                    })).should('be.empty')    
                })
            })  
        })
    })
    
    it("Should try to delete accepted assignments", function () {
        cy.log("running to delete")
        const projectLeadId = this.pmId as IntegerInt32 
        cy.get('@project').then(_project => {
            const project = _project as unknown as ITransProject
            cy.wrap(WebixPortalAssignmentService.getTranslationAssignments({
                projectId: project.project.id
            })).as('assignments')
        })
        cy.get('@assignments').then(_assignments => {
            const assignments = _assignments as unknown as IAssignment[]
            cy.log('Trying to delete the first assignment without purging the PO');
            cy.wrap(WebixPortalAssignmentService.deleteTranslationAssignments({
                assignmentIds: [assignments[0].assignment.id]
            }));
            cy.log('Trying to purge the assignment with PO');
            cy.wrap(WebixPortalAssignmentService.deleteTranslationAssignments({
                assignmentIds: [assignments[0].assignment.id],
                purgeP: true
            }))
            cy.log('Trying to purge the assignment with PO while deleting the po - should return nothing');
            cy.wrap(WebixPortalAssignmentService.deleteTranslationAssignments({
                assignmentIds: [assignments[0].assignment.id],
                purgeP: true,
                deletePoP: true
            }))
            cy.log('Trying to delete the assignment with PO with a reason');
            cy.wrap(WebixPortalAssignmentService.deleteTranslationAssignments({
                assignmentIds: [assignments[1].assignment.id],
                deletePoP: true,
                reason: "Testing the deletion with po"
            })).its(0).as('deletedAssignment')
        })
        cy.get('@deletedAssignment').then(_assignment => {
            const assignment = _assignment as unknown as IAssignment
            expect(assignment.assignment_status.id).eq(IntranetFreelanceAssignmentStatus.ASSIGNMENT_DELETED)
            expect(assignment.purchase_order.status.id).eq(IntranetCostStatus.DELETED)
        })
    })
})