import { CognovisRestCompanyService, CognovisRestInvoiceService, IAssignment, ICompany, ICrFile, IFreelancePackage, IFreelancer, IFreelancerAssignment, IGetAssignmentQualityRatingTypes, IInvoice, IntranetCompanyType, IntranetCostStatus, IntranetCostType, IntranetCustKolibriRestService, IntranetFreelanceAssignmentStatus, IntranetProjectStatus, IntranetProjectType, IntranetTranslationLanguage, IntranetTransTaskType, IntranetUom, ITransProject, ITransTask, IWebixNotification, OpenAPI, WebixPortalAssignmentService, WebixPortalNotificationService, WebixPortalTranslationService } from "../../../../sources/openapi/";
import { randomizedName } from "../../../support/helpers";



before(() => {
    cy.fixture('cypress.env.json')
    .then((testEnv) => {
        // First we authorize to our app
        OpenAPI.TOKEN = testEnv.enviromentTestVariables.DEV.pm.api_key;
    });
});

describe("Verify the assignments work as per kolibri", () => {
    beforeEach(() => {
        // get a random test company
        cy.wrap(CognovisRestCompanyService.getCompanies({
            autocompleteQuery: 'cypress',
            companyTypeIds: [IntranetCompanyType.CUSTOMER]
        }).then(res => {
            const company = res[Math.floor(Math.random() * (res.length -1))] as ICompany 
            return WebixPortalTranslationService.postTransProject(
                {
                    requestBody: {
                        company_id: company.company.id,
                        project_type_id: IntranetProjectType.TRANS_PROOF_EDIT,
                        source_language_id: IntranetTranslationLanguage.DEDE,
                        target_language_ids: [IntranetTranslationLanguage.ENGB, IntranetTranslationLanguage.ESES]
                    }
                }
            )
            
        }), {timeout: 10000}).as('project')
    })

   afterEach(() => {
        cy.get('@project').then(_project => {
            const project = _project as unknown as ITransProject
    
            cy.wrap(WebixPortalTranslationService.deleteTransProject({
                projectId: project.project.id
            }),{timeout: 10000})
            .then(() => {
                cy.wrap(WebixPortalTranslationService.getTransProjects({
                    projectNr: project.project_nr
                })).should('be.empty')    
            })
        })  
    })


    it("Should verify packages correctly created", function () {
        // Create a project
        const randomNumber = Math.floor((9999 - 1) * Math.random());

        cy.get('@project').then(_project => {
            const project = _project as unknown as ITransProject
            expect(project.target_language.length).eq(2)
            expect(project.invoice_amount).be.null
            cy.log('ProjectNr' + project.project_nr)
            // Create tasks (and its packages)
            cy.wrap(WebixPortalTranslationService.postTransTasks({
                projectId:project.project.id, 
                requestBody: {
                    task_name: randomizedName('TestTask+'), 
                    task_uom_id: IntranetUom.SWORD,
                    task_units: randomNumber
                }
            }))
            .then(_tasks => {
                const tasks = _tasks as unknown as ITransTask[]
                expect(tasks.length).eq(2)
                cy.wrap(WebixPortalAssignmentService.getPackages({
                    projectId: project.project.id
                }))
                .then(_packages => {
                    const packages = _packages as unknown as IFreelancePackage[]
                    expect(packages.length).eq(6)
                })
            })
        })
    })

    it("Should accept an assignment", function () {
        // Create a project
        const randomNumber = Math.floor((9999 - 1) * Math.random());

        
        cy.get('@project').then(_project => {
            const project = _project as unknown as ITransProject
            expect(project.target_language.length).eq(2)
            cy.log('ProjectNr' + project.project_nr)

            // Create tasks (and its packages)
            cy.wrap(WebixPortalTranslationService.postTransTasks({
                projectId:project.project.id, 
                requestBody: {
                    task_name: randomizedName('TestTask+'), 
                    task_uom_id: IntranetUom.SWORD,
                    task_units: randomNumber, 
                    task_type_id: IntranetProjectType.TRANS_PROOF
                }
            })).its(0).its('task').its('id').as('taskId')
        })        

        cy.get('@taskId').then(taskId => {
            const task_id = taskId as unknown as number;

            cy.wrap(WebixPortalAssignmentService.getPackages({
                transTaskId: task_id,
                packageTypeId: IntranetTransTaskType.TRANS
            }))
            .should('have.length',1)
            .its(0).its('freelance_package.id').as('packageId')
        });

        cy.log("Creating multiple assignments")
        cy.get('@packageId').then(packageId => {
            const package_id = packageId as unknown as number;
            cy.wrap(WebixPortalAssignmentService.getFreelancersForPackage({
                freelancePackageId: package_id
            }))
            .should('have.length.greaterThan',0)
            .as('freelancers')
    
            cy.get('@freelancers').then(_freelancers => {
                const freelancers = _freelancers as unknown as IFreelancer[]
                const freelancers_to_assign = []
                for (let i = 3; i > 0; i--) {
                    freelancers_to_assign.push(freelancers[Math.floor(Math.random() * (freelancers.length -1))].freelancer.id)
                }

                cy.wrap(WebixPortalAssignmentService.postTranslationAssignments({
                    freelancePackageId: package_id,
                    freelancerIds: freelancers_to_assign,
                    requestBody: {
                        rate: 0.3
                    }
                }),{timeout: 50000})
                .should('have.length.greaterThan',0)
            })
        })

        cy.log("Setting project to open")
        cy.get('@project').then(_project => {
            const project = _project as unknown as ITransProject
            cy.wrap(WebixPortalTranslationService.putTransProject({
                projectId: project.project.id,
                requestBody: {
                    project_status_id: IntranetProjectStatus.OPEN
                }
            }),{timeout: 10000})
            .then(() => {
                cy.wrap(WebixPortalAssignmentService.getTranslationAssignments({
                    projectId: project.project.id
                })).as('assignments')
            })
        })

        cy.log("Assigning one assignee")

        cy.get("@assignments").then(_assignments => {
            const assignments = _assignments as unknown as IAssignment[]
            const acceptedAssignmentIds = []
            // Find an assignment with a rate so we can generate the purchase order
            assignments.forEach(assignment => {
                if (assignment.rate >0) {
                    acceptedAssignmentIds.push(assignment.assignment.id)
                }
                expect(assignment.assignment_status.id).eq(IntranetFreelanceAssignmentStatus.REQUESTED)
            })
            const acceptOthersAssignmentIds = []
            const acceptedAssignmentId = acceptedAssignmentIds[0]
            assignments.forEach(assignment => {
                if (assignment.assignment.id !== acceptedAssignmentId) {
                    acceptOthersAssignmentIds.push(assignment.assignment.id);
                }
            })

            cy.log("Accepted " + acceptedAssignmentId)

            cy.log("OThers ids " + acceptOthersAssignmentIds)

            // Verify that the assignment is accepted and purchase order is created

            cy.wrap(WebixPortalAssignmentService.putTranslationAssignments({
                assignmentId: acceptedAssignmentId,
                requestBody:{
                    assignment_status_id: IntranetFreelanceAssignmentStatus.ACCEPTED
                }
            }),{timeout:10000}).as('accepted_assignment')
            
            cy.get('@accepted_assignment').then(_assignment => {
                const accepted_assignment = _assignment as unknown as IAssignment
                expect(accepted_assignment.assignment_status.id).eq(IntranetFreelanceAssignmentStatus.ACCEPTED)
    
                cy.log('Checking purchase order and link to assignment');
                expect(accepted_assignment.purchase_order).not.null
                cy.wrap(CognovisRestInvoiceService.getInvoice({
                    invoiceIds: [accepted_assignment.purchase_order.id]
                })).its(0).then(_po => {
                    const po = _po as unknown as IInvoice
                    expect(po.company_contact.id).eq(accepted_assignment.assignee.id);
                    expect(po.linked_objects).not.null
                })
            })
            .then(() => {
                cy.log("Ensure we didn't change the status to something wrong and it is still accepted")
                const fileName = "Dummy.pdf"
                cy.uploadCrFile(fileName, acceptedAssignmentId, "This is my description").then(_file => {
                    const file = _file as unknown as ICrFile
                    expect(file.name).eq(fileName)
                }).then(() => {
                    cy.wrap(WebixPortalAssignmentService.getFreelancerAssignments({
                        assignmentId: acceptedAssignmentId
                    })).its(0).its('assignment_status').its('id').should('eq',IntranetFreelanceAssignmentStatus.ACCEPTED)
                })

                cy.log('Other assignments should be accepted other')
                acceptOthersAssignmentIds.forEach(assignmentId => {
                    // Check other assignments are set to accept others
                    cy.wrap(WebixPortalAssignmentService.getTranslationAssignments({
                        assignmentId: assignmentId
                    })).its(0).its('assignment_status').its('id').should('equal',IntranetFreelanceAssignmentStatus.ASSIGNED_OTHER)
                })
            })
            .then(() => {
                cy.log('Setting assignment to work delivered')
                cy.wrap(WebixPortalAssignmentService.putTranslationAssignments({
                    assignmentId: acceptedAssignmentId,
                    requestBody:{
                        assignment_status_id: IntranetFreelanceAssignmentStatus.WORK_DELIVERED
                    }
                }),{timeout:10000})
                .then(_assignment => {
                    const accepted_assignment = _assignment as unknown as IAssignment
                    expect(accepted_assignment.assignment_status.id).eq(IntranetFreelanceAssignmentStatus.WORK_DELIVERED)
                })
            })
        })
    })

    it("Should deny an assignment", function () {
        const randomNumber = Math.floor((9999 - 1) * Math.random());
        
        cy.get('@project').then(_project => {
            const project = _project as unknown as ITransProject
            // Create tasks (and its packages)
            cy.wrap(WebixPortalTranslationService.postTransTasks({
                projectId:project.project.id, 
                requestBody: {
                    task_name: randomizedName('TestTask+'), 
                    task_uom_id: IntranetUom.SWORD,
                    task_units: randomNumber, 
                    task_type_id:87
                }
            })).its(0).its('task').its('id').as('taskId')
        })        

        cy.get('@taskId').then(taskId => {
            const task_id = taskId as unknown as number;

            cy.wrap(WebixPortalAssignmentService.getPackages({
                transTaskId: task_id,
                packageTypeId: IntranetTransTaskType.TRANS
            }))
            .should('have.length',1)
            .its(0).its('freelance_package.id').as('packageId')
        });
        cy.log("Creating multiple assignments")
        cy.get('@packageId').then(packageId => {
            const package_id = packageId as unknown as number;
            cy.wrap(WebixPortalAssignmentService.getFreelancersForPackage({
                freelancePackageId: package_id
            }))
            .should('have.length.greaterThan',0)
            .as('freelancers')
    
            cy.get('@freelancers').then(_freelancers => {
                const freelancers = _freelancers as unknown as IFreelancer[]
                const freelancerId = (freelancers[Math.floor(Math.random() * (freelancers.length -1))].freelancer.id)

                cy.wrap(WebixPortalAssignmentService.postTranslationAssignments({
                    freelancePackageId: package_id,
                    freelancerIds: [freelancerId],
                    requestBody: {}
                }),{timeout: 10000})
                .should('have.length.greaterThan',0)
                .its(0)
                .then(_assignment => {
                    const assignment = _assignment as unknown as IAssignment
                    cy.log("Denying the assignment");
                    cy.wrap(WebixPortalAssignmentService.putTranslationAssignments({
                        assignmentId: assignment.assignment.id,
                        requestBody:{
                            assignment_status_id: IntranetFreelanceAssignmentStatus.DENIED
                        }
                    }),{timeout: 10000})
                })
                .then(_assignment => {
                    const denied_assignment = _assignment as unknown as IAssignment
                    expect(denied_assignment.assignment_status.id).eq(IntranetFreelanceAssignmentStatus.DENIED)
                    expect(denied_assignment.purchase_order).null
                    cy.log("Checking notification")
                    cy.wrap(WebixPortalNotificationService.getWebixNotification({
                        contextId: denied_assignment.assignment.id
                    })).then(_notifications => {
                        const notifications = _notifications as unknown as IWebixNotification[]
                        notifications.forEach(notification => {
                            cy.log("Found notification: " + notification.message)
                            notification.actions.forEach(action => {
                                cy.log("Found actions: " + action.action_help)
                            })
                        })
                    })
                })
            })
        })
    })
})


describe("Quality ratings for assignments", () => {
    it("Should get the specific rating types for Trans/Proof and Trans ", function () {

        cy.log("getting the rating types for the assignments in question")
        cy.wrap(WebixPortalAssignmentService.getTranslationAssignments({
            assignmentTypeId: IntranetTransTaskType.TRANS,
            projectTypeId: IntranetProjectType.TRANS_PROOF
        })).
        then(_assignments => {
            const assignments = _assignments as unknown as IAssignment[]
            const assignment =  assignments[Math.floor(Math.random() * assignments.length)]
            cy.wrap(WebixPortalAssignmentService.getAssignmentQualityRatingTypes({
                assignmentIds: [assignment.assignment.id]
            }))
            .then(_rating_types => {
                const rating_types = _rating_types as unknown as IGetAssignmentQualityRatingTypes
                const ratingTypeIds = [...new Set(rating_types.quality_type.map(item => item.id))]
                const transTypeIds = [7015, 7016, 7017, 7018, 7019]
                expect(ratingTypeIds).to.eql(transTypeIds)
            })
        })
    })
})

describe.only("Verify the assignments updates work", () => {
    beforeEach(() => {
        // get a random test company
        cy.wrap(CognovisRestCompanyService.getCompanies({
            autocompleteQuery: 'cypress',
            companyTypeIds: [IntranetCompanyType.CUSTOMER]
        }).then(res => {
            const company = res[Math.floor(Math.random() * (res.length -1))] as ICompany 
            return WebixPortalTranslationService.postTransProject(
                {
                    requestBody: {
                        company_id: company.company.id,
                        project_type_id: IntranetProjectType.TRANS_PROOF,
                        source_language_id: IntranetTranslationLanguage.DEDE,
                        project_status_id: IntranetProjectStatus.OPEN,
                        target_language_ids: [IntranetTranslationLanguage.ENGB]
                    }
                }
            )
            
        }), {timeout: 10000}).as('project')
    })

    afterEach(() => {
        cy.get('@project').then(_project => {
            const project = _project as unknown as ITransProject
    
            cy.wrap(WebixPortalTranslationService.deleteTransProject({
                projectId: project.project.id
            }))
            .then(() => {
                cy.wrap(WebixPortalTranslationService.getTransProjects({
                    projectNr: project.project_nr
                })).should('be.empty')    
            })
        })  
    })


    it("Create an accepted assignment and update fee + deadline", function () {
        cy.get('@project').then(_project => {
            const project = _project as unknown as ITransProject
            cy.log('ProjectNr' + project.project_nr)

            // Create tasks (and its packages)
            cy.wrap(WebixPortalTranslationService.postTransTasks({
                projectId:project.project.id, 
                requestBody: {
                    task_name: randomizedName('TestTask+'), 
                    task_uom_id: IntranetUom.SWORD,
                    task_units: 500, 
                    task_type_id: IntranetProjectType.TRANS_PROOF
                }
            })).its(0).its('task').its('id').as('taskId')
        })        

        cy.get('@taskId').then(taskId => {
            const task_id = taskId as unknown as number;

            cy.wrap(WebixPortalAssignmentService.getPackages({
                transTaskId: task_id,
                packageTypeId: IntranetTransTaskType.TRANS
            }))
            .should('have.length',1)
            .its(0).its('freelance_package.id').as('transPackageId')
        });

        cy.get('@taskId').then(taskId => {
            const task_id = taskId as unknown as number;

            cy.wrap(WebixPortalAssignmentService.getPackages({
                transTaskId: task_id,
                packageTypeId: IntranetTransTaskType.PROOF
            }))
            .should('have.length',1)
            .its(0).its('freelance_package.id').as('proofPackageId')
        });

        cy.log("Create Trans assignment")

        cy.get('@transPackageId').then(packageId => {
            const transPackageId = packageId as unknown as number;
            cy.wrap(WebixPortalAssignmentService.getFreelancersForPackage({
                freelancePackageId: transPackageId
            }))
            .should('have.length.greaterThan',0)
            .its(0).its('freelancer.id').as('transFreelancerId')
    
            cy.get('@transFreelancerId').then(freelancerId => {
                cy.wrap(WebixPortalAssignmentService.postTranslationAssignments({
                    freelancePackageId: transPackageId,
                    freelancerIds: [freelancerId as unknown as number],
                    requestBody: {
                        rate: 0.3,
                        assignment_deadline: '2021-10-12 14:00:00'
                    }
                }),{timeout: 50000})
                .should('have.length.greaterThan',0)
                .its(0).as('transAssignment')
            })
        });

        cy.log("Create Proof assignment")

        cy.get('@proofPackageId').then(packageId => {
            const proofPackageId = packageId as unknown as number;
            cy.wrap(WebixPortalAssignmentService.getFreelancersForPackage({
                freelancePackageId: proofPackageId
            }))
            .should('have.length.greaterThan',0)
            .its(1).its('freelancer.id').as('proofFreelancerId')
    
            cy.get('@proofFreelancerId').then(freelancerId => {
                cy.wrap(WebixPortalAssignmentService.postTranslationAssignments({
                    freelancePackageId: proofPackageId,
                    freelancerIds: [freelancerId as unknown as number],
                    requestBody: {
                        rate: 0.3,
                        assignment_deadline: '2021-10-14 08:00:00'
                    }
                }),{timeout: 50000})
                .should('have.length.greaterThan',0)
                .its(0).as('proofAssignment')
            })
        });

        cy.get('@proofAssignment').then( assignment => {
            const proofAssignment = assignment as unknown as IFreelancerAssignment
            // expect(proofAssignment.start_date).eq('2021-10-12T16:00:00.000000Z')
            expect(proofAssignment.assignment_deadline).eq('2021-10-14T08:00:00.000000Z')
            const start_date = new Date(proofAssignment.start_date)
            const end_date = new Date(proofAssignment.assignment_deadline)
            const diff  = end_date.valueOf() - start_date.valueOf()
            return diff
        }).as('proofDifference')

        cy.log('Accept the trans task and change deadline')
        cy.get('@transAssignment').then( assignment => {
            const transAssignment = assignment as unknown as IFreelancerAssignment
            expect(transAssignment.start_date).not.null;
            cy.log('Start date fro trans' + transAssignment.start_date)
            cy.wrap(WebixPortalAssignmentService.putTranslationAssignments({
                assignmentId: transAssignment.assignment.id,
                requestBody:{
                    assignment_status_id: IntranetFreelanceAssignmentStatus.ACCEPTED
                }
            }),{timeout:10000}).as('accepted_assignment')
        })

        cy.get('@transAssignment').then( assignment => {
            const transAssignment = assignment as unknown as IFreelancerAssignment
            cy.wrap(WebixPortalAssignmentService.putTranslationAssignments({
                assignmentId: transAssignment.assignment.id,
                requestBody:{
                    assignment_deadline: '2021-10-12T17:00:00.000000Z'
                }
            }),{timeout:10000}).as('accepted_assignment')
        })

        cy.get('@proofAssignment').then( assignment => {
            const proofAssignment = assignment as unknown as IFreelancerAssignment
            cy.wrap(WebixPortalAssignmentService.getTranslationAssignments({
                assignmentId: proofAssignment.assignment.id
            })).its(0).as('upProofAssignment')
        })

        cy.get('@upProofAssignment').then( assignment => {
            const upProofAssignment = assignment as unknown as IFreelancerAssignment
            // expect(upProofAssignment.start_date).eq('2021-10-12T19:00:00.000000Z')
            cy.get('@proofDifference').then( _diff => {
                const proofDifference = _diff as unknown as number
                const start_date = new Date(upProofAssignment.start_date)
                const end_date = new Date(upProofAssignment.assignment_deadline)
                const diff  = end_date.valueOf() - start_date.valueOf()
                expect(diff).eq(proofDifference)
            })
        })
    })
})

describe("Verify the assignments purchase orders work", () => {
    beforeEach(() => {
        // get a random test company
        cy.wrap(CognovisRestCompanyService.getCompanies({
            autocompleteQuery: 'cypress',
            companyTypeIds: [IntranetCompanyType.CUSTOMER]
        }).then(res => {
            const company = res[Math.floor(Math.random() * (res.length -1))] as ICompany 
            return WebixPortalTranslationService.postTransProject(
                {
                    requestBody: {
                        company_id: company.company.id,
                        project_type_id: IntranetProjectType.TRANS_PROOF,
                        source_language_id: IntranetTranslationLanguage.DEDE,
                        project_status_id: IntranetProjectStatus.OPEN,
                        target_language_ids: [IntranetTranslationLanguage.ENGB]
                    }
                }
            )
        })).as('project')
        .then(res => {
            const project = res as unknown as ITransProject
            cy.log('ProjectNr' + project.project_nr)

            // Create tasks (and its packages)
            return cy.wrap(WebixPortalTranslationService.postTransTasks({
                projectId:project.project.id, 
                requestBody: {
                    task_name: randomizedName('TestTask+'), 
                    task_uom_id: IntranetUom.SWORD,
                    task_units: 500, 
                    task_type_id: IntranetProjectType.TRANS_PROOF
                }
            })).its(0).its('task').its('id')
        })
        .then(taskId => {
            const task_id = taskId as unknown as number;

            return cy.wrap(WebixPortalAssignmentService.getPackages({
                transTaskId: task_id,
                packageTypeId: IntranetTransTaskType.TRANS
            }))
            .should('have.length',1)
            .its(0).its('freelance_package.id')
        })
        .then(packageId => {
            const transPackageId = packageId as unknown as number;
            cy.wrap(WebixPortalAssignmentService.getFreelancersForPackage({
                freelancePackageId: transPackageId
            }))
            .should('have.length.greaterThan',0)
            .its(0).its('freelancer.id').as('transFreelancerId')
    
            return cy.get('@transFreelancerId').then(freelancerId => {
                return cy.wrap(WebixPortalAssignmentService.postTranslationAssignments({
                    freelancePackageId: transPackageId,
                    freelancerIds: [freelancerId as unknown as number],
                    requestBody: {
                        rate: 0.3,
                        assignment_deadline: '2021-10-12 14:00:00'
                    }
                }),{timeout: 50000})
                .should('have.length.greaterThan',0)
                .its(0).as('transAssignment')
            })
        })
        .then( assignment => {
            const transAssignment = assignment as unknown as IFreelancerAssignment
            expect(transAssignment.start_date).not.null;
            cy.log('Start date fro trans' + transAssignment.start_date)
            cy.wrap(WebixPortalAssignmentService.putTranslationAssignments({
                assignmentId: transAssignment.assignment.id,
                requestBody:{
                    assignment_status_id: IntranetFreelanceAssignmentStatus.ACCEPTED
                }
            }),{timeout:10000}).as('accepted_assignment')
        })
    })

    afterEach(() => {
        cy.get('@project').then(_project => {
            const project = _project as unknown as ITransProject
    
            cy.wrap(WebixPortalTranslationService.deleteTransProject({
                projectId: project.project.id
            }))
            .then(() => {
                cy.wrap(WebixPortalTranslationService.getTransProjects({
                    projectNr: project.project_nr
                })).should('be.empty')    
            })
        })  
    })

    it('Should create a two purchase orders, create one bill manually and close the project and then generate bill from PO', function () {
        let assignment :IAssignment
        cy.get('@accepted_assignment').then(_assignment => {
            assignment = _assignment as unknown as IAssignment
            cy.wrap(CognovisRestInvoiceService.postInvoiceCopy({
                sourceInvoiceIds: [assignment.purchase_order.id],
                targetCostTypeId: IntranetCostType.PROVIDER_BILL,
                filedP: true
            }).then(_bill => {
                const bill = _bill as unknown as IInvoice
                expect(bill.cost_status.id).eq(IntranetCostStatus.CREATED)
                CognovisRestInvoiceService.getInvoice({
                    invoiceIds: [assignment.purchase_order.id]
                }).then(_po => {
                    const po = _po[0] as unknown as IInvoice
                    expect(po.cost_status.id).eq(IntranetCostStatus.FILED)
                })    
            }))
            return cy.wrap(WebixPortalTranslationService.postTransTasks({
                projectId:assignment.project.id, 
                requestBody: {
                    task_name: randomizedName('TestTask+'), 
                    task_uom_id: IntranetUom.SWORD,
                    task_units: 800, 
                    task_type_id: IntranetProjectType.TRANS_PROOF
                }
            })).its(0).its('task').its('id')
        })
        .then(taskId => {
            const task_id = taskId as unknown as number;

            return cy.wrap(WebixPortalAssignmentService.getPackages({
                transTaskId: task_id,
                packageTypeId: IntranetTransTaskType.TRANS
            }))
            .should('have.length',1)
            .its(0).its('freelance_package.id')
        })
        .then(packageId => {
            const transPackageId = packageId as unknown as number;
                return cy.wrap(WebixPortalAssignmentService.postTranslationAssignments({
                    freelancePackageId: transPackageId,
                    freelancerIds: [assignment.assignee.id],
                    requestBody: {
                        rate: 0.3,
                        assignment_deadline: '2021-10-12 14:00:00'
                    }
                }),{timeout: 50000})
                .should('have.length.greaterThan',0)
                .its(0)
        })
        .then( assignment => {
            const transAssignment = assignment as unknown as IFreelancerAssignment
            expect(transAssignment.start_date).not.null;
            cy.log('Start date fro trans' + transAssignment.start_date)
            cy.wrap(WebixPortalAssignmentService.putTranslationAssignments({
                assignmentId: transAssignment.assignment.id,
                requestBody:{
                    assignment_status_id: IntranetFreelanceAssignmentStatus.ACCEPTED
                }
            }),{timeout:10000}).as('newAssignment')
        })

        let newAssignment :IAssignment
        cy.get('@newAssignment').then(_newassignment => {
            newAssignment = _newassignment as unknown as IAssignment
            expect(newAssignment.purchase_order).not.null
            cy.wrap(WebixPortalTranslationService.putTransProject({
                projectId: newAssignment.project.id,
                requestBody: {
                    project_status_id: IntranetProjectStatus.CLOSED
                }
            })).as('closedProject')
        })
        cy.get('@closedProject').then(_closedProject => {
            const closedProject = _closedProject as unknown as ITransProject

            cy.wrap(IntranetCustKolibriRestService.putSendProviderBills({
                projectId: closedProject.project.id,
                senderId: closedProject.project_lead.user.id
            }),{timeout: 10000}).its(0).as('bill')
        })
        cy.get('@bill').then(_bill=> {
            const bill = _bill as unknown as IInvoice
            expect(bill.cost_status.id).eq(IntranetCostStatus.OUTSTANDING)
            CognovisRestInvoiceService.getInvoice({
                invoiceIds: [assignment.purchase_order.id]
            }).then(_po => {
                const po = _po[0] as unknown as IInvoice
                expect(po.cost_status.id).eq(IntranetCostStatus.FILED)
            })
            cy.wrap(CognovisRestInvoiceService.getInvoice({
                projectId:newAssignment.project.id,
                costTypeIds: [IntranetCostType.PROVIDER_BILL]
            }))
            .then(_bills => {
                const bills = _bills as unknown as [IInvoice]
                expect(bills.length).eq(2)
            })
        })
    })


    it('Should create a purchase order, close the project and then generate bill from PO', function () {
        let assignment :IAssignment
        cy.get('@accepted_assignment').then(_assignment => {
            assignment = _assignment as unknown as IAssignment
            expect(assignment.purchase_order).not.null
            cy.wrap(WebixPortalTranslationService.putTransProject({
                projectId: assignment.project.id,
                requestBody: {
                    project_status_id: IntranetProjectStatus.CLOSED
                }
            }))
            .then(_closedProject => {
                const closedProject = _closedProject as unknown as ITransProject
                CognovisRestInvoiceService.getInvoice({
                    invoiceIds: [assignment.purchase_order.id]
                }).then(_po => {
                    const po = _po[0] as unknown as IInvoice
                    expect(po.cost_status.id).eq(IntranetCostStatus.ACCEPTED)
                })
                return closedProject
            }).as('closedProject')
        })
        cy.get('@closedProject').then(_closedProject => {
            const closedProject = _closedProject as unknown as ITransProject

            cy.wrap(IntranetCustKolibriRestService.putSendProviderBills({
                projectId: closedProject.project.id,
                senderId: closedProject.project_lead.user.id
            }),{timeout: 10000}).its(0).as('bill')
        })
        cy.get('@bill').then(_bill=> {
            const bill = _bill as unknown as IInvoice
            expect(bill.cost_status.id).eq(IntranetCostStatus.OUTSTANDING)
            CognovisRestInvoiceService.getInvoice({
                invoiceIds: [assignment.purchase_order.id]
            }).then(_po => {
                const po = _po[0] as unknown as IInvoice
                expect(po.cost_status.id).eq(IntranetCostStatus.FILED)
            })
        })
    })

})