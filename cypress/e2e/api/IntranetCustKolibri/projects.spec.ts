import * as exp from "constants";
import { ITransProject, OpenAPI,  WebixPortalTranslationService, IntranetUom, IAssignment, IntranetTransTaskType, ITimesheetTask, CognovisRestTimesheetService, CognovisRestCompanyService, IKolibriUrl, IntranetCompanyType, IntranetTranslationLanguage, ICompany, IntranetProjectType, IntranetTranslationSubjectArea, WebixPortalAssignmentService, WebixPortalProjectService, IntranetTmTool, IntranetCustKolibriRestService, IntranetProjectStatus, IntranetFreelanceAssignmentStatus, CognovisRestInvoiceService, IntranetCostType, IntranetCostStatus, IInvoice, CognovisRestFileService, WebixPortalNotificationService, IWebixNotification, WebixNotificationType, ITransTask, IFreelancerAssignment, ICrFolder, CognovisRestService, IPrivilege, IntegerInt32, ICrFile, CognovisRestSystemService } from "../../../../sources/openapi/"
import { IUserToken } from "../../../../sources/openapi/models/IUserToken";
import { randomizedName } from "../../../support/helpers"

before(() => {
    cy.fixture('cypress.env.json')
    .then((testEnv) => {
        // First we authorize to our app
        OpenAPI.TOKEN = testEnv.enviromentTestVariables.DEV.admin.api_key;
    });
});


describe ("check folders", () => {
    before(() => {
        // Get a project with multiple target languages 
        const projectIds = [1057800,1041742,1035121]

        const projectId = projectIds[Math.floor(Math.random() * (projectIds.length -1))]

        cy.wrap(WebixPortalTranslationService.getTransProjects({
            projectId: projectId
        })).its(0).as('project')    
    })

    it("Should return t: urls", function () {
        const project = this.project as ITransProject
        cy.wrap(IntranetCustKolibriRestService.getKolibriTUrls({
            projectId: project.project.id
        })).should('not.be.empty')
        .then(res => {
            const urls = res as IKolibriUrl[]
            urls.forEach(url => {
                expect(url.url).not.null
            })
        })
    })
})

describe("Should check projects for folders and tasks", () => {


    it("should create a new translation project, check tasks and folders and delete it", function () {
        cy.createTestTransProject(IntranetProjectStatus.POTENTIAL).as('project').then(_project => {
            cy.getCompany(_project).as('company')
        })
        
        cy.get('@project').then(_project => {

            const project = _project as unknown as ITransProject

            cy.wrap(CognovisRestFileService.getCrFolders({
                folderId: project.project_folder.id
            }))
            .then(_folders => {
                const folders = _folders as unknown as ICrFolder[]
                const folderNames = [...new Set(folders.map(item => item.folder.name))]
                expect(folderNames).to.include('Original');
                expect(folderNames).to.include('Projektinfos extern');
                expect(folderNames).to.include('Lieferung');
                folders.forEach(folder => {
                    if (folder.folder.name == 'Original' || folder.folder.name == 'Lieferung') {
                        cy.log('Checking permissions on ' + folder.folder.name)
                        // check permissions
                        cy.wrap(CognovisRestService.getObjectPermission({
                            objectId: folder.folder.id,
                            permissionUserId: project.company_contact.id
                        }))
                        .then(_res => {
                            const res = _res as unknown as IPrivilege
                            expect(res.read).eq(true)
                        })
                    } 
                })
            })
            cy.wrap(CognovisRestTimesheetService.getTimesheetTask({
                projectId: project.project.id
            }))
            .then(res => {
                const tasks = res as unknown as ITimesheetTask[]
                cy.log("Looking if we have the Projektmanagement und Finalization task")
                tasks.forEach(task => {
                    cy.log("Found task " + task.task.name + " " + task.task_type.name)
                })
                const taskNames = [...new Set(tasks.map(item => item.task.name))]

                //const kolibriTasks = ['Projektmanagement', 'Finalization']
                expect(taskNames).to.include('Glossarpflege')
                expect(taskNames).to.include('Finalisierung')
                expect(taskNames).to.include('Angebot')

            })

            cy.wrap(WebixPortalAssignmentService.getProjectDefaultFilters({
                projectId: project.project.id
            })
            .then(res => {
                expect(res.source_language).have.members
                const targetLanguageIds = [...new Set(res.target_language.map(item => item.id))]
                expect(targetLanguageIds).to.have.members([IntranetTranslationLanguage.ENGB,IntranetTranslationLanguage.ESES])
            }))

            cy.wrap(WebixPortalTranslationService.deleteTransProject({
                projectId: project.project.id
            })).should('be.empty')
            .then(() => {
                cy.wrap(WebixPortalTranslationService.getTransProjects({
                    projectNr: project.project_nr
                })).should('be.empty')    
            })
        })
    })

    it("should create a new translation project, check trados and memoq and delete it", function () {
        cy.createTestTransProject(IntranetProjectStatus.POTENTIAL).as('project').then(_project => {

            const project = _project as unknown as ITransProject

            cy.wrap(WebixPortalTranslationService.getTmToolUrl({
                projectId: project.project.id,
                tmToolId: IntranetTmTool.MEMOQ
            })).its('url').should('be.empty')

            cy.wrap(WebixPortalTranslationService.getTmToolUrl({
                projectId: project.project.id,
                tmToolId: IntranetTmTool.TRADOS_STUDIO_2017
            })).its('url').should('not.be.null')

            cy.wrap(WebixPortalTranslationService.deleteTransProject({
                projectId: project.project.id
            })).should('be.empty')
            .then(() => {
                cy.wrap(WebixPortalTranslationService.getTransProjects({
                    projectNr: project.project_nr
                })).should('be.empty')    
            })
        })
    })


    it("Should create a new content project and check the timesheet tasks", function () {
        // get a random test company
        cy.wrap(CognovisRestCompanyService.getCompanies({
            autocompleteQuery: 'test_',
            companyTypeIds: [IntranetCompanyType.CUSTOMER]
        }).then(res => {
            return res[Math.floor(Math.random() * (res.length -1))]
        })).as('company')
        cy.get('@company').then(_company => {
            const company = _company as unknown as ICompany
            const company_project_nr = randomizedName('ProjectNr+')

            cy.wrap(WebixPortalTranslationService.postTransProject(
                {
                    requestBody: {
                        company_id: company.company.id,
                        project_type_id: IntranetProjectType.CONTENT_DEUTSCH,
                        source_language_id: IntranetTranslationLanguage.DEDE,
                        target_language_ids: [IntranetTranslationLanguage.DEDE],
                        subject_area_id: IntranetTranslationSubjectArea.FINANZTEXTE,
                        customer_contact_id: company.primary_contact.id,
                        company_project_nr:  company_project_nr
                    }
                }
            ), {timeout: 15000}).as('project')    
        })

        cy.get('@project').then(_project => {

            const project = _project as unknown as ITransProject

            expect(project.project_folder.id).gt(0)

            cy.wrap(CognovisRestTimesheetService.getTimesheetTask({
                projectId: project.project.id
            }))
            .then(res => {
                const tasks = res as unknown as ITimesheetTask[]
                const taskNames = [...new Set(tasks.map(item => item.task.name))]
                expect(taskNames).to.include('Copywriting')
                expect(taskNames).to.include('Projektmanagement')
            })

            cy.wrap(WebixPortalTranslationService.deleteTransProject({
                projectId: project.project.id
            })).should('be.empty')
            .then(() => {
                cy.wrap(WebixPortalTranslationService.getTransProjects({
                    projectNr: project.project_nr
                })).should('be.empty')    
            })
        })
        
    })

})

describe("Quick project tests on new projects", () => {
    beforeEach(() => {
        cy.createTestTransProjectWithAssignments().as('project')
        .then(_project => {
            cy.getCompany(_project).then(_company => {
                return _company[0]
            }).as('company')
        })

        cy.fixture('cypress.env.json')
        .then((testEnv) => {
            // First we authorize to our app
            return testEnv.enviromentTestVariables.DEV.accounting.user_id;
        }).as('accountingId')

        cy.fixture('cypress.env.json')
        .then((testEnv) => {
            // First we authorize to our app
            return testEnv.enviromentTestVariables.DEV.pm.user_id;
        }).as('pmId')
    })

    afterEach(() => {
        cy.get('@project').then(_project => {
            const project = _project as unknown as ITransProject
    
            cy.wrap(WebixPortalTranslationService.deleteTransProject({
                projectId: project.project.id
            }))
            .then(() => {
                cy.wrap(WebixPortalTranslationService.getTransProjects({
                    projectNr: project.project_nr
                })).should('be.empty')    
            })
        })  
    })

    it("Should create a project and close it. The tasks within it should be closed, quote should be denied", function() {
        cy.log('Setting project to closed');
        cy.get('@project').then(_project => {
            const project = _project as unknown as ITransProject
            cy.wrap(WebixPortalNotificationService.postWebixNotification({
                requestBody: {
                    context_id: project.project.id,
                    recipient_id: project.project_lead.user.id,
                    project_id: project.project.id,
                    notification_type_id: WebixNotificationType.QUOTE_ACCEPTED
                }
            }))
            .then(() => {
                cy.wrap(WebixPortalTranslationService.putTransProject({
                    projectId: project.project.id,
                    requestBody: {
                        project_status_id: IntranetProjectStatus.CLOSED
                    }
                })).as('closedProject')
            })
        })
        cy.get('@closedProject').then(_project => {
            const project = _project as unknown as ITransProject
            cy.log('We should have not notifications');
            cy.wrap(WebixPortalNotificationService.getWebixNotification({
                projectId: project.project.id
            })).then(_notifications => {
                const notifications = _notifications as IWebixNotification[]
                expect(notifications.length).eq(0)
            })
            cy.log('Tasks should be closed');
            cy.wrap(CognovisRestTimesheetService.getTimesheetTask({
                projectId: project.project.id
            })).then(_tasks => {
                const tasks = _tasks as unknown as ITimesheetTask[]
                tasks.forEach((task, index) => {
                    expect(task.task_status.id).eq(IntranetProjectStatus.CLOSED)
                    if((index + 1) === tasks.length) {
                        return tasks
                    }  
                })
            })
        })
    })
})


describe("Quick project tests on existing open projects", () => {
    beforeEach(() => {
        // Get a project with multiple target languages        
        cy.wrap(WebixPortalTranslationService.getTransProjects({
            projectTypeId: [87],
            projectStatusId: [IntranetProjectStatus.OPEN]
        })).as('projects')    
            
        cy.get('@projects').then(_projects => {
            const projects = _projects as unknown as Array<ITransProject>
            const multiTargetProject = projects.filter(el => el.target_language.length >1)
            return multiTargetProject[Math.floor(Math.random() * (multiTargetProject.length -1))]
        }).as('project')

        cy.fixture('cypress.env.json')
        .then((testEnv) => {
            // First we authorize to our app
            return testEnv.enviromentTestVariables.DEV.accounting.user_id;
        }).as('accountingId')

        cy.fixture('cypress.env.json')
        .then((testEnv) => {
            // First we authorize to our app
            return testEnv.enviromentTestVariables.DEV.admin.user_id;
        }).as('adminUserId')
    })

    it('Should create a notification and then set the project to delivered', function () {
        const accountingId = this.accountingId as IntegerInt32
        cy.get('@project').then(_project => {
            const project = _project as unknown as ITransProject
            cy.wrap(WebixPortalNotificationService.postWebixNotification({
                requestBody: {
                    context_id: project.project.id,
                    recipient_id: project.project_lead.user.id,
                    project_id: project.project.id,
                    notification_type_id: WebixNotificationType.QUOTE_ACCEPTED
                }
            }))
            .then(() => {
                cy.wrap(WebixPortalTranslationService.putTransProject({
                    projectId: project.project.id,
                    requestBody: {
                        project_status_id: IntranetProjectStatus.DELIVERED
                    }
                }))
                .then(() => {
                    cy.log("Accounting contact should have a notification")
    
                    cy.wrap(WebixPortalNotificationService.getWebixNotification({
                        projectId: project.project.id
                    })).then(_notifications => {
                        const notifications = _notifications as unknown as IWebixNotification[]
                        expect(notifications.length).eq(1)
                        const notification = notifications[0]
                        expect(notification.recipient.id).eq(accountingId)
                        expect(notification.notification_type.id).eq(WebixNotificationType.PROJECT_DELIVERED)
                    })
                })
            })
        })
    })

    it('Should reassign the project to a different project lead', function() {
        cy.get("@project").then(_project => {
            const project = _project as unknown as ITransProject
            const projectLeadId = project.project_lead.user.id

            cy.wrap(WebixPortalProjectService.postProjectTakeOver({
                projectId: project.project.id
            })).should('be.empty')
            .then(() => {
                const adminId = this.adminUserId
                cy.wrap(WebixPortalTranslationService.getTransProjects({
                    projectId: project.project.id
                })).its(0).its('project_lead').its('user').its('id').should('eq', adminId)
                .then(() => {
                    cy.wrap(WebixPortalProjectService.postProjectTakeOver({
                        projectId: project.project.id,
                        newProjectLeadId: projectLeadId
                    })).should('be.empty')
                    .then(() => {
                        cy.wrap(WebixPortalTranslationService.getTransProjects({
                            projectId: project.project.id
                        })).its(0).its('project_lead').its('user').its('id').should('eq', projectLeadId)        
                    })
                })
            })
        })
    })
})

describe("End2End Test", () => {

    beforeEach(() => {
        cy.createTestTransProjectWithAssignments().as('project')
        .then(_project => {
            cy.getCompany(_project).then(_company => {
                return _company[0]
            }).as('company')
        })

        cy.fixture('cypress.env.json')
        .then((testEnv) => {
            // First we authorize to our app
            return testEnv.enviromentTestVariables.DEV.accounting.user_id;
        }).as('accountingId')

        cy.fixture('cypress.env.json')
        .then((testEnv) => {
            // First we authorize to our app
            return testEnv.enviromentTestVariables.DEV.pm.user_id;
        }).as('pmId')
    })

    afterEach(() => {
        cy.fixture('cypress.env.json')
        .then((testEnv) => {
            OpenAPI.TOKEN = testEnv.enviromentTestVariables.DEV.admin.api_key;

            cy.get('@project').then(_project => {
                const project = _project as unknown as ITransProject
        
                cy.wrap(WebixPortalTranslationService.deleteTransProject({
                    projectId: project.project.id
                }))
                .then(() => {
                    cy.wrap(WebixPortalTranslationService.getTransProjects({
                        projectNr: project.project_nr
                    })).should('be.empty')    
                })
            })  
        })
    })

    it("Normal E2E Process", function () {
        const projectLeadId = this.pmId as IntegerInt32
        const accountingId = this.accountingId as IntegerInt32

        const fileName = "Dummy.pdf"
        const contactAgainDate = new Date();
        contactAgainDate.setDate(contactAgainDate.getDate() - 2);
        const contactAgainString = `${contactAgainDate.getFullYear()}-${(contactAgainDate.getMonth()+1)}-${contactAgainDate.getDate()} ${contactAgainDate.getHours()}:${contactAgainDate.getMinutes()}`;

        let assignmentOneId 
        let assignmentTwoId
        let assignmentThreeId
        let assignmentFourId

        cy.log('should return notifications for overdue date');
        cy.get('@project').then(_project => {
            const project = _project as unknown as ITransProject
            expect(project.project_lead.user.id).eq(projectLeadId)

            cy.wrap(WebixPortalTranslationService.putTransProject({
                projectId:project.project.id,
                requestBody: {
                    contact_again_date: contactAgainString
                }
            })).then(_project => {
                const project = _project as unknown as ITransProject
                // expect(Date.parse(project.contact_again_date)).eq(contactAgainDate)
                cy.wrap(WebixPortalNotificationService.postWebixNotificationSchedule({
                    scheduleProc: 'create_contact_again_projects'
                })).then(_notifications => {
                    const notifications = _notifications as unknown as IWebixNotification[]
                    const notification = notifications.find(e => e.context.id === project.project.id && e.recipient.id === projectLeadId && e.notification_type.id === WebixNotificationType.PROJECT_CONTACT_AGAIN)
                    expect(notification).not.null
                })
            })
        })

        cy.log('Should set the quote to accepted and the project should be open');

        const company = this.company as ICompany

        cy.get('@project').then(_project => {
            const project = _project as unknown as ITransProject
            cy.wrap(CognovisRestInvoiceService.getInvoice({
                projectId: project.project.id,
                costTypeIds: [IntranetCostType.QUOTE]
            })).its(0).as('quote').then(_quote => {
                const quote = _quote as unknown as IInvoice
                expect(quote.cost_type.id).eq(IntranetCostType.QUOTE)
                expect(quote.cost_status.id).eq(IntranetCostStatus.CREATED)
                expect(company.vat_type.id).eq(quote.vat_type.id)
                expect(company.payment_term.id).eq(quote.payment_term.id) // KOL-2257
                cy.wrap(CognovisRestSystemService.getUserToken({
                    userId: quote.company_contact.id
                })).then(_contactUser => {
                    const contactUser = _contactUser as unknown as IUserToken
                    const currentToken = OpenAPI.TOKEN
                    OpenAPI.TOKEN = contactUser.bearer_token
                    console.log("Current token " + currentToken)
                    console.log("OpenAPI token " + OpenAPI.TOKEN)
                    cy.wrap(CognovisRestInvoiceService.putInvoiceReply({
                        invoiceId: quote.invoice.id,
                        costStatusId: IntranetCostStatus.ACCEPTED
                    })).as('acceptedQuote')
                    .then(_quote => {
                        OpenAPI.TOKEN = currentToken
                    })    
                })
            })
            cy.get('@acceptedQuote').then(_quote => {
                const quote = _quote as unknown as IInvoice
                cy.log('Checking Project is open plus notification about acceptance')
                expect(quote.cost_status.id).eq(IntranetCostStatus.ACCEPTED)

                cy.wrap(WebixPortalTranslationService.getTransProjects({
                    projectId: project.project.id
                }))
                .then(_projects => {
                    const projects = _projects as unknown as ITransProject[]
                    expect(projects.length).eq(1)
                    const acceptedProject = projects[0]
                    expect(acceptedProject.project_status.id).eq(IntranetProjectStatus.OPEN)
                    cy.wrap(WebixPortalNotificationService.getWebixNotification({
                        contextId: quote.invoice.id,
                        recipientId: project.project_lead.user.id,
                        notificationTypeId: WebixNotificationType.QUOTE_ACCEPTED
                    })).then(_notifications => {
                        const notifications = _notifications as unknown as IWebixNotification[]
                        expect(notifications.length).eq(1)
                        expect(notifications[0].project.id).eq(project.project.id)
                    })
                })
                cy.wrap(WebixPortalAssignmentService.getTranslationAssignments({
                    projectId: project.project.id
                })).as('assignments')
            })
        })

        cy.get('@assignments').then(_assignments => {
            const assignments = _assignments as unknown as IAssignment[]
            cy.log('Uploading package file for assignment ' + assignments[1].assignment.name)
        
            cy.uploadCrFile(fileName, assignments[1].freelance_package.id, "Assignemnt 2 description of package file").then(_file => {
                const file = _file as unknown as ICrFile
                expect(file.name).eq(fileName)
            })
        })

        cy.log('Checking assignments are requested and have notifications')

        cy.get('@assignments').then(_assignments => {
            const assignments = _assignments as unknown as IAssignment[]
            assignmentOneId = assignments[0].assignment.id
            assignmentTwoId = assignments[1].assignment.id
            assignmentThreeId = assignments[2].assignment.id
            assignmentFourId = assignments[3].assignment.id

            cy.log("Checking assignments are requested")
            assignments.forEach((assignment, index) => {
                expect(assignment.assignment_status.id).eq(IntranetFreelanceAssignmentStatus.REQUESTED)
                cy.log("Checking notification for "+ assignment.assignee.name)
                cy.wrap(WebixPortalNotificationService.getWebixNotification({
                    contextId: assignment.assignment.id,
                    recipientId: assignment.assignee.id,
                    notificationTypeId: WebixNotificationType.ASSIGNMENT_REQUEST_OUTSTANDING
                })).then(_notifications => {
                    const notifications = _notifications as unknown as IWebixNotification[]
                    expect(notifications.length).eq(1)
                    expect(notifications[0].project.id).eq(assignment.project.id)
                })
                .then(() => {
                    if((index + 1) === assignments.length) {
                        return assignments
                    }
                })
            })
        }).then(_assignments => {
            const assignments = _assignments as unknown as IAssignment[]

            cy.log("Accepting first assignment")
            cy.wrap(WebixPortalAssignmentService.putTranslationAssignments({
                assignmentId: assignments[0].assignment.id,
                requestBody: {
                    assignment_status_id: IntranetFreelanceAssignmentStatus.ACCEPTED
                }
            })).as('acceptedAssignmentOne')

            cy.get('@acceptedAssignmentOne').then(_assignment => {
                const assignment = _assignment as unknown as IAssignment
                cy.log('Checking Purchase order is generated for first assignment '+ assignment.assignment.name)
                expect(assignment.purchase_order).not.null;

                cy.log('Checking Notification is generated for Project Lead about acceptance')
                cy.wrap(WebixPortalNotificationService.getWebixNotification({
                    contextId: assignment.assignment.id,
                    recipientId: projectLeadId,
                    notificationTypeId: WebixNotificationType.ASSIGNMENT_ACCEPTED
                })).then(_notifications => {
                    const notifications = _notifications as unknown as IWebixNotification[]
                    expect(notifications.length).eq(1)
                    const notification = notifications[0]
                    expect(notification.project.id).eq(assignment.project.id)
                    const actions = notification.actions
                    
                    cy.log('Checking Notification is generated for Project Lead about missing package file')
                    expect(actions.length).eq(4)
                    const action = actions.find(e => e.action_type.id === 14301 && e.action_object.id === assignment.freelance_package.id)
                    expect(action).not.null
                }).then(() => {
                    cy.log("Uploading files to package")
                    cy.uploadCrFile(fileName, assignment.freelance_package.id, "Assignemnt 2 description of package file").then(_file => {
                        const file = _file as unknown as ICrFile
                        expect(file.name).eq(fileName)
                        const fileID = file.file_id

                        cy.log("check notification for assignee with uploaded files")
                        cy.wrap(WebixPortalNotificationService.getWebixNotification({
                            contextId: assignment.assignment.id,
                            recipientId: assignment.assignee.id,
                            notificationTypeId: WebixNotificationType.FILE_WAS_UPLOADED
                        })).then(_notifications => {
                            const notifications = _notifications as unknown as IWebixNotification[]
                            expect(notifications.length).eq(1)
                            const notification = notifications[0]
                            expect(notification.project.id).eq(assignment.project.id)
                            expect(notification.actions.length).eq(3)
                            cy.wrap(WebixPortalNotificationService.postWebixNotificationRead({
                                notificationId: notification.notification.id
                            }))
                        }).then(() => {
                            cy.log("Re-uploading with different comment")
                            cy.uploadCrFile(fileName, assignment.freelance_package.id, "New Description").then(_file => {
                                const file = _file as unknown as ICrFile
                                expect(file.name).eq(fileName)
                                expect(file.file_id).eq(fileID)

                                cy.wrap(WebixPortalNotificationService.getWebixNotification({
                                    contextId: assignment.assignment.id,
                                    recipientId: assignment.assignee.id,
                                    notificationTypeId: WebixNotificationType.FILE_WAS_UPLOADED
                                })).then(_notifications => {
                                    const notifications = _notifications as unknown as IWebixNotification[]
                                    expect(notifications.length).eq(1)
                                    const notification = notifications[0]
                                    expect(notification.project.id).eq(assignment.project.id)
                                    expect(notification.actions.length).eq(3)
                                    const actions = notification.actions
                            
                                    cy.log('Checking Notification is generated with new comment action')
                                    const action = actions.find(e => e.action_type.id === 14310)
                                    expect(action.action_text).eq('New Description')
                                })        
                            })
                        })
                    })
                })
            }).then(() => {
                cy.log('Creating second assignment')
                cy.wrap(WebixPortalAssignmentService.putTranslationAssignments({
                    assignmentId: assignments[1].assignment.id,
                    requestBody: {
                        assignment_status_id: IntranetFreelanceAssignmentStatus.ACCEPTED
                    }
                })).as('acceptedAssignmentTwo')    
            })


            cy.get('@acceptedAssignmentTwo').then(_assignment => {
                const assignment = _assignment as unknown as IAssignment
                cy.log('Checking Purchase order is generated for second assignment '+ assignment.assignment.name)
                expect(assignment.purchase_order).not.null;
                cy.log('Checking Notification is generated for Project Lead about acceptance')
                cy.wrap(WebixPortalNotificationService.getWebixNotification({
                    contextId: assignment.assignment.id,
                    recipientId: projectLeadId,
                    notificationTypeId: WebixNotificationType.ASSIGNMENT_ACCEPTED
                })).then(_notifications => {
                    const notifications = _notifications as unknown as IWebixNotification[]
                    expect(notifications.length).eq(1)
                    expect(notifications[0].project.id).eq(assignment.project.id)
                })
                cy.log('Checking Notification is NOT generated for Project Lead about missing package file')
                cy.wrap(WebixPortalNotificationService.getWebixNotification({
                    contextId: assignment.assignment.id,
                    recipientId: projectLeadId,
                    notificationTypeId: WebixNotificationType.PACKAGE_FILE_MISSING
                })).then(_notifications => {
                    const notifications = _notifications as unknown as IWebixNotification[]
                    expect(notifications.length).eq(0)
                })


                cy.log("Setting other accepted assignent in the past due with higher rate")
                cy.wrap(WebixPortalAssignmentService.putTranslationAssignments({
                    assignmentId: assignment.assignment.id,
                    requestBody: {
                        assignment_deadline: '2021-10-01 14:00:00',
                        rate: 0.17
                    }
                }),{timeout: 15000}).as('pastDeadline')

                cy.get('@pastDeadline').then(pastDeadline => {
                    const pastDeadlineAssignment = pastDeadline as unknown as IAssignment
                    cy.log('Ensuring deadline and rate have changed')
                    expect(pastDeadlineAssignment.assignment_deadline).eq('2021-10-01T14:00:00.000000Z')
                    expect(pastDeadlineAssignment.rate).eq(0.17)

                    cy.log('Checking notifications for deadline and fee change');
                    expect(pastDeadlineAssignment.assignment_deadline).eq('2021-10-01T14:00:00.000000Z')
                    expect(pastDeadlineAssignment.rate).eq(0.17)

                    cy.log('Running overdue procedure')
                    cy.wrap(WebixPortalNotificationService.postWebixNotificationSchedule({
                        scheduleProc: 'create_overdue_assignments'
                    })).then(_notifications => {
                        cy.log('Checking notifications for project lead')
                        const notifications = _notifications as unknown as IWebixNotification[]
                        const notification = notifications.find(e => e.context.id === assignment.project.id && e.recipient.id === projectLeadId && e.notification_type.id === WebixNotificationType.ASSIGNMENT_OVERDUE)
                        // expect(notification).not.undefined
                        // expect(notification.message).not.null
                        cy.log('Checking notifications for assignee')
                        const notification2 = notifications.find(e => e.context.id === assignment.project.id && e.recipient.id === assignment.assignee.id && e.notification_type.id === WebixNotificationType.ASSIGNMENT_OVERDUE)
                        // expect(notification2).not.undefined
                        // expect(notification.message).not.null
                    })
                })
                .then(() => {
                    cy.log('Uploading return file to the second assignment')
                    cy.uploadCrFile(fileName, assignment.assignment.id, "Return file for the assignment")
                    .then(_file => {
                        const file = _file as unknown as ICrFile
                        expect(file.name).eq(fileName)
                        expect(file.parent.id).eq(assignment.assignment.id)
                        const fileID = file.file_id

                        cy.log("check notification for PM with uploaded file by assignee")
                        cy.wrap(WebixPortalNotificationService.getWebixNotification({
                            contextId: assignment.assignment.id,
                            recipientId: projectLeadId,
                            notificationTypeId: WebixNotificationType.FILE_WAS_UPLOADED
                        })).then(_notifications => {
                            const notifications = _notifications as unknown as IWebixNotification[]
                            expect(notifications.length).eq(1)
                            const notification = notifications[0]
                            expect(notification.project.id).eq(assignment.project.id)
                            expect(notification.actions.length).eq(4)
                            expect(notification.context.id).eq(assignment.assignment.id)
                            cy.wrap(WebixPortalNotificationService.postWebixNotificationRead({
                                notificationId: notification.notification.id
                            }))
                        })
                    })
                })
                .then(() => {
                    cy.log("Setting assignment to work delivered")
                    cy.wrap(WebixPortalAssignmentService.putTranslationAssignments({
                        assignmentId: assignment.assignment.id,
                        requestBody: {
                            assignment_status_id: IntranetFreelanceAssignmentStatus.WORK_DELIVERED
                        }
                    }),{timeout: 15000})
                    .then(_assignment => {
                        const accepted_assignment = _assignment as unknown as IAssignment
                        expect(accepted_assignment.assignment_status.id).eq(IntranetFreelanceAssignmentStatus.WORK_DELIVERED)
    
                        // Check PM Notification
                        cy.log('Checking notification on work delivered')
                        cy.wrap(WebixPortalNotificationService.getWebixNotification({
                            contextId: assignment.assignment.id,
                            recipientId: projectLeadId,
                            notificationTypeId: WebixNotificationType.ASSIGNMENT_DELIVERED
                        })).then(_notifications => {
                            const notifications = _notifications as unknown as IWebixNotification[]
                            expect(notifications.length).eq(1)
                        })
                    })    
                })              
            })

            cy.log('Denying the third assignment')
            cy.wrap(WebixPortalAssignmentService.putTranslationAssignments({
                assignmentId: assignments[2].assignment.id,
                requestBody: {
                    assignment_status_id: IntranetFreelanceAssignmentStatus.DENIED
                }
            })).as('deniedAssignment')

            cy.get('@deniedAssignment').then(_assignment => {
                const assignment = _assignment as unknown as IAssignment
                cy.log('Checking assignment status')
                expect(assignment.assignment_status.id).eq(IntranetFreelanceAssignmentStatus.DENIED);
                expect(assignment.purchase_order).null;
                cy.log('Checking notification for PM')
                cy.wrap(WebixPortalNotificationService.getWebixNotification({
                    contextId: assignment.assignment.id,
                    recipientId: projectLeadId,
                    notificationTypeId: WebixNotificationType.ASSIGNMENT_DENIED
                })).then(_notifications => {
                    const notifications = _notifications as unknown as IWebixNotification[]
                    expect(notifications.length).eq(1)
                    const notification = notifications[0]
                    expect(notification.project.id).eq(assignment.project.id)
                    expect(notification.actions.length).eq(3)
                    const action = notification.actions.find(e => e.action_type == 14306)
                    expect(action).not.null
                })
            })
            
            cy.log("Reminding freelancer about missed request" + assignments[3].assignment.name)
            cy.wrap(WebixPortalAssignmentService.postAssignmentRemindFreelancer({
                assignmentId: assignments[3].assignment.id
            }))

            cy.wrap(WebixPortalAssignmentService.getTranslationAssignments({
                assignmentId:  assignments[3].assignment.id
            })).its(0).as('remindedAssignment')
            
            cy.get('@remindedAssignment').then(_assignment => {
                const assignment = _assignment as unknown as IAssignment
                cy.log('Checking notification for freelancer')
                cy.wrap(WebixPortalNotificationService.getWebixNotification({
                    contextId: assignment.assignment.id,
                    recipientId: assignment.assignee.id,
                    notificationTypeId: WebixNotificationType.ASSIGNMENT_REQUEST_OUTSTANDING
                })).then(_notifications => {
                    const notifications = _notifications as unknown as IWebixNotification[]
                    expect(notifications.length).eq(1)
                    const notification = notifications[0]
                    expect(notification.project.id).eq(assignment.project.id)
                    expect(notification.actions.length).eq(2)
                })
            })
        })
        .then(() => {
            cy.log("Set project to delivered")

            cy.get('@project').then(_project => {
                const project = _project as unknown as ITransProject
                cy.log("Setting project to delivered")
                cy.wrap(WebixPortalTranslationService.putTransProject({
                    projectId: project.project.id,
                    requestBody: {
                        project_status_id: IntranetProjectStatus.DELIVERED,
                        subject_area_id: IntranetTranslationSubjectArea.FINANZTEXTE
                    }
                }),{timeout: 15000}).as('deliveredProject')
            })
            cy.get('@deliveredProject').then(res => {
                const deliveredProject = res as unknown as ITransProject
                expect(deliveredProject.project_status.id).eq(IntranetProjectStatus.DELIVERED)
                expect(deliveredProject.source_language.id).eq(IntranetTranslationLanguage.DEDE)
                expect(deliveredProject.subject_area.id).eq(IntranetTranslationSubjectArea.FINANZTEXTE)
        
                cy.log("Accounting contact should have a notification")

                cy.wrap(WebixPortalNotificationService.getWebixNotification({
                    projectId: deliveredProject.project.id
                })).then(_notifications => {
                    const notifications = _notifications as unknown as IWebixNotification[]
                    expect(notifications.length).eq(1)
                    const notification = notifications[0]
                    expect(notification.recipient.id).eq(accountingId)
                    expect(notification.notification_type.id).eq(WebixNotificationType.PROJECT_DELIVERED)
                })

                cy.log("Sending out provider bills")
                cy.wrap(IntranetCustKolibriRestService.putSendProviderBills({
                    projectId: deliveredProject.project.id,
                    senderId: accountingId
                }),{timeout: 30000}).as('providerBills')
    
                cy.get('@providerBills').then(res => {
                    const providerBills = res as unknown as IInvoice[]
                    expect(providerBills.length).eq(2)
                    providerBills.forEach(bill => {
                        expect(bill.cost_status.id).eq(IntranetCostStatus.OUTSTANDING)
                    })
                }).then(() => {
                    cy.log("Assignments should be closed")
                    cy.log('Checking provider bill exists and status is closed ' + assignmentOneId)
                    cy.wrap(WebixPortalAssignmentService.getFreelancerAssignments({
                        assignmentId: assignmentOneId
                    })
                    .then(_freelance_assignments => {
                        const assignment = _freelance_assignments[0] as unknown as IFreelancerAssignment
                        expect(assignment.purchase_order).not.null
                        expect(assignment.provider_bill).not.null
                        
                        expect(assignment.assignment_status.id).eq(IntranetFreelanceAssignmentStatus.ASSIGNMENT_CLOSED)
                    }))
    
                    cy.log('Checking provider bill exists and status is closed ' + assignmentTwoId)
                    cy.wrap(WebixPortalAssignmentService.getFreelancerAssignments({
                        assignmentId: assignmentTwoId
                    })
                    .then(_freelance_assignments => {
                        const assignment = _freelance_assignments[0] as unknown as IFreelancerAssignment
                        expect(assignment.purchase_order).not.null
                        expect(assignment.provider_bill).not.null
                        expect(assignment.assignment_status.id).eq(IntranetFreelanceAssignmentStatus.ASSIGNMENT_CLOSED)
                    }))

                    cy.log('Checking provider bill does not exist for not accepted assignment ' + assignmentThreeId)

                    cy.wrap(WebixPortalAssignmentService.getFreelancerAssignments({
                        assignmentId: assignmentFourId
                    })
                    .then(_freelance_assignments => {
                        const assignment = _freelance_assignments[0] as unknown as IFreelancerAssignment
                        expect(assignment.purchase_order).null
                        expect(assignment.provider_bill).null
                        expect(assignment.assignment_status.id).eq(IntranetFreelanceAssignmentStatus.ASSIGNMENT_CLOSED)
                    }))

                    cy.log('Checking provider bill does not exist for denied assignment and assignment is still denied ' + assignmentFourId)

                    cy.wrap(WebixPortalAssignmentService.getFreelancerAssignments({
                        assignmentId: assignmentThreeId
                    })
                    .then(_freelance_assignments => {
                        const assignment = _freelance_assignments[0] as unknown as IFreelancerAssignment
                        expect(assignment.purchase_order).null
                        expect(assignment.provider_bill).null
                        expect(assignment.assignment_status.id).eq(IntranetFreelanceAssignmentStatus.DENIED)
                    }))    
                })

                cy.log("Sending our invoice")
                cy.wrap(CognovisRestInvoiceService.getInvoice({
                    projectId: deliveredProject.project.id,
                    costTypeIds: [IntranetCostType.CUSTOMER_INVOICE]
                }))
            })
        })
    })

    it("Accepting project before quote", function () {
        const projectLeadId = this.pmId as IntegerInt32
        cy.get('@project').then(_project => {
            const project = _project as unknown as ITransProject
            cy.wrap(WebixPortalTranslationService.putTransProject({
                projectId: project.project.id,
                requestBody: {
                    project_status_id: IntranetProjectStatus.OPEN,
                    description: 'We are open'
                }
            }),{timeout: 15000})
            .then(_res => {
                const res = _res as unknown as ITransProject
                cy.log("Checking if the quote is accepted")
                expect(res.description).eq('We are open')
                cy.wrap(CognovisRestInvoiceService.getInvoice({
                    projectId:project.project.id,
                    costTypeIds: [IntranetCostType.QUOTE]
                })).its(0).its('cost_status').its('id').should('equal', IntranetCostStatus.ACCEPTED)
                cy.wrap(WebixPortalNotificationService.getWebixNotification({
                    contextId: project.project.id,
                    recipientId: projectLeadId
                })).should('be.empty')

                cy.log('Checking that all assignments are requested and have notifications');
                cy.wrap(WebixPortalAssignmentService.getTranslationAssignments({
                    projectId: project.project.id
                })).then(_assignments => {
                    const assignments = _assignments as unknown as IAssignment[]
                    assignments.forEach(assignment => {
                        expect(assignment.assignment_status.id).eq(IntranetFreelanceAssignmentStatus.REQUESTED)
                        cy.wrap(WebixPortalNotificationService.getWebixNotification({
                            contextId: assignment.assignment.id,
                            recipientId: assignment.assignee.id,
                            notificationTypeId: WebixNotificationType.ASSIGNMENT_REQUEST_OUTSTANDING
                        })).then(_notifications => {
                            const notifications = _notifications as unknown as IWebixNotification[]
                            expect(notifications.length).eq(1)
                            expect(notifications[0].project.id).eq(assignment.project.id)
                        })
                    })
                })
            })
        })
    })

    it("Customer denies a quote", function () {
        const projectLeadId = this.pmId as IntegerInt32

        cy.get('@project').then(_project => {
            const project = _project as unknown as ITransProject
            cy.log("Setting project to Declined")
            cy.wrap(WebixPortalTranslationService.putTransProject({
                projectId: project.project.id,
                requestBody: {
                    project_status_id: IntranetProjectStatus.DECLINED,
                    description: 'We are declined'
                }
            }),{timeout: 15000}).as('declinedProject')
            
            cy.get('@declinedProject').then(_declined_project => {
                const declined_project = _declined_project as unknown as ITransProject
                expect(declined_project.project_status.id).eq(IntranetProjectStatus.DECLINED)
                cy.wrap(CognovisRestInvoiceService.getInvoice({
                    projectId: project.project.id
                })).should('be.empty')
                cy.wrap(CognovisRestInvoiceService.getInvoice({
                    projectId: project.project.id,
                    costStatusIds: [IntranetCostStatus.REJECTED]
                })).should('have.length',2)

                cy.wrap(WebixPortalNotificationService.getWebixNotification({
                    contextId: project.project.id,
                    recipientId: projectLeadId
                }))
                .then(_notifications => {
                    const notification = _notifications[0] as unknown as IWebixNotification
                    expect(notification.recipient.id).eq(projectLeadId)
                    expect(notification.notification_type.id).eq(WebixNotificationType.QUOTE_DENIED)
                })

                cy.wrap(WebixPortalAssignmentService.getTranslationAssignments({
                    projectId: declined_project.project.id
                })).as('assignments')
                cy.get('@assignments').then(_assignments => {
                    const assignments = _assignments as unknown as IAssignment[]
                    assignments.forEach(assignment => {
                        expect(assignment.assignment_status.id).eq(IntranetFreelanceAssignmentStatus.ASSIGNMENT_DELETED)
                    })
                })
            })
        })
    })
    it("Customer denies both quotes", function () {
        const projectLeadId = this.pmId as IntegerInt32

        cy.get('@project').then(_project => {
            const project = _project as unknown as ITransProject
            cy.log("Setting project to Declined")
            cy.wrap(WebixPortalTranslationService.putTransProject({
                projectId: project.project.id,
                requestBody: {
                    project_status_id: IntranetProjectStatus.DECLINED,
                    description: 'We are declined'
                }
            }),{timeout: 15000}).as('declinedProject')
            
            cy.get('@declinedProject').then(_declined_project => {
                const declined_project = _declined_project as unknown as ITransProject
                expect(declined_project.project_status.id).eq(IntranetProjectStatus.DECLINED)
                cy.wrap(CognovisRestInvoiceService.getInvoice({
                    projectId: project.project.id
                })).should('be.empty')
                cy.wrap(CognovisRestInvoiceService.getInvoice({
                    projectId: project.project.id,
                    costStatusIds: [IntranetCostStatus.REJECTED]
                })).should('have.length',2)

                cy.wrap(WebixPortalNotificationService.getWebixNotification({
                    contextId: project.project.id,
                    recipientId: projectLeadId
                }))
                .then(_notifications => {
                    const notification = _notifications[0] as unknown as IWebixNotification
                    expect(notification.recipient.id).eq(projectLeadId)
                    expect(notification.notification_type.id).eq(WebixNotificationType.QUOTE_DENIED)
                })

                cy.wrap(WebixPortalAssignmentService.getTranslationAssignments({
                    projectId: declined_project.project.id
                })).as('assignments')
                cy.get('@assignments').then(_assignments => {
                    const assignments = _assignments as unknown as IAssignment[]
                    assignments.forEach(assignment => {
                        expect(assignment.assignment_status.id).eq(IntranetFreelanceAssignmentStatus.ASSIGNMENT_DELETED)
                    })
                })
            })
        })
    })
})