import { OpenAPI, WebixPortalSearchService, ISearchResult } from "../../../../sources/openapi";

before(() => {
    cy.fixture('cypress.env.json')
    .then((testEnv) => {
        // First we authorize to our app
        OpenAPI.TOKEN = testEnv.enviromentTestVariables.DEV.admin.api_key;
    });
});

describe("Basic search functionality", () => {
    it("Should find projects from 2021", function () {
        cy.wrap(WebixPortalSearchService.getWebixSearch({
            query: '2019_061'
        })).should('not.be.empty')
        .then(res => {
            const results = res as unknown as ISearchResult[]
            results.forEach(result => {
                expect(result.object.object_type).eq('im_project')
                cy.log("Found project " + result.search_text)
            })
        })
    })
})