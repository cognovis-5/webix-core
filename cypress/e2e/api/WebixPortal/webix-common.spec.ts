import { WebixPortalProjectService, OpenAPI,IUserForSwitching} from "../../../../sources/openapi";

before(() => {
    cy.fixture('cypress.env.json')
    .then((testEnv) => {
        // First we authorize to our app
        OpenAPI.TOKEN = testEnv.enviromentTestVariables.DEV.admin.api_key;
    });
});

describe("User switcher", () => {
    it("Should return users with local + role", () => {
        cy.wrap(WebixPortalProjectService.getUserSwitches({
            realUserId: 91246
        })).as('users')
        cy.get('@users').then(_users => {
            const users = _users as unknown as IUserForSwitching[]
            expect(users.length).eq(30)
            users.forEach(user => {
                expect(user.user.name).not.empty
                expect(user.profiles).not.empty
                let role
                if(user.profiles.find(e => e.group_id === 461) != undefined) {
                    role = 'Customer'
                }
                if(user.profiles.find(e => e.group_id === 465) != undefined) {
                    role = 'Freelancer'
                }
                if(user.profiles.find(e => e.group_id === 463) != undefined) {
                    role = 'Employee'
                }
                if(user.profiles.find(e => e.group_id === 467) != undefined) {
                    role = 'PM'
                }
                if(user.profiles.find(e => e.group_id === 471) != undefined) {
                    role = 'Accounting'
                }
                if(user.profiles.find(e => e.group_id === 459) != undefined) {
                    role = ']po[ admin'
                }
                cy.log(user.user.name + " (" + user.locale + ") " + role)
            })
        })
    });
}) 