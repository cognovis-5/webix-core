import * as exp from "constants";
import { OpenAPI, WebixPortalAssignmentService, ITransProject, WebixPortalTranslationService, WebixPortalNotificationService, IntranetFreelanceAssignmentStatus, IntranetTransTaskType, IAssignment, WebixNotificationType, WebixNotificationStatus, IWebixNotification, IWebixNotificationAction } from "../../../../sources/openapi";

before(() => {
    cy.fixture('cypress.env.json')
    .then((testEnv) => {
        // First we authorize to our app
        OpenAPI.TOKEN = testEnv.enviromentTestVariables.DEV.pm.api_key;
    });
});

describe("Basic notification functionality", () => {
    before(() => {
        cy.wrap(WebixPortalAssignmentService.getTranslationAssignments({
            assignmentStatusId: IntranetFreelanceAssignmentStatus.ACCEPTED,
            assignmentTypeId: IntranetTransTaskType.EDIT
        }).then(_assignments => {
            const assignments = _assignments.filter(el => el.project !== null)
            return assignments[Math.floor(Math.random() * (assignments.length -1))]
        })).as('assignment')
    })
    it("Should add a notification and mark it as viewed and delete it afterwards", function () {
        const contextId = this.assignment.assignment.id
        const recipientId = this.assignment.assignee.id
        const projectId = this.assignment.project.id


        cy.wrap(WebixPortalNotificationService.postWebixNotification({
            requestBody: {
                context_id: contextId,
                recipient_id: recipientId,
                notification_type_id: WebixNotificationType.ASSIGNMENT_OVERDUE,
                notification_status_id: WebixNotificationStatus.IMPORTANT,
                message: 'This is my notification',
                project_id: projectId
            }
        })).then(_notification => {
            const notification = _notification as unknown as IWebixNotification
            expect(notification.recipient.id).eq(recipientId)
            expect(notification.project.id).eq(projectId)
            expect(notification.project_nr).not.null

            cy.log("Created notification for an assignment")
            cy.wrap(WebixPortalNotificationService.getWebixNotificationAction({
                notificationId: notification.notification.id
            })).then(_actions => {
                const actions = _actions as unknown as IWebixNotificationAction[]
                actions.forEach(action => {
                    cy.log("Found action " + action.action_type.name)
                    cy.log("Help - " + action.action_help)
                    expect(action.action_type).not.null
                    expect(action.action_icon).not.null
                })
            })
            cy.wrap(WebixPortalNotificationService.putWebixNotification({
                notificationId: notification.notification.id,
                requestBody: {
                    recipient_id: notification.recipient.id,
                    context_id: notification.context.id,
                    message: "This is a new message for my notification"
                }
            })).then(_up_notif => {
                const up_notif = _up_notif as unknown as IWebixNotification
                expect(up_notif.message).eq("This is a new message for my notification")
                expect(up_notif.viewed_date).be.null
                expect(up_notif.project.id).eq(projectId)

                cy.wrap(WebixPortalNotificationService.postWebixNotificationRead({
                    notificationId: up_notif.notification.id
                })).then(_notif2 => {
                    const viewed_notif = _notif2 as unknown as IWebixNotification
                    expect(viewed_notif.viewed_date).be.not.null
                }).then(() => {
                    cy.wrap(WebixPortalNotificationService.deleteWebixNotification({
                        notificationId: notification.notification.id
                    })).should('be.empty')
                })
            })
        })
    })
})

xdescribe("Notifications for assignments", () => {
    it("Should create an overdue assignment and set it to delivered", function () {
        cy.createTestTransProjectWithAssignments().then(_project => {
            console.log(_project)
            const project = _project as unknown as ITransProject
            cy.wait(8000).then(()=> {
                cy.wrap(WebixPortalAssignmentService.getTranslationAssignments({
                    projectId: project.project.id,
                    assignmentTypeId: IntranetTransTaskType.TRANS
                }))
                .then(_assignment => {
                    const assignment = _assignment[0] as unknown as IAssignment
                    cy.wrap(WebixPortalNotificationService.postWebixNotification({
                        requestBody: {
                            context_id: assignment.assignment.id,
                            notification_type_id: WebixNotificationType.ASSIGNMENT_OVERDUE,
                            recipient_id: assignment.assignee.id
                        }
                    })).then(_notification => {
                        const notification = _notification as unknown as IWebixNotification
                        cy.log("Created notification " + notification.message)
                        cy.wrap(WebixPortalAssignmentService.putTranslationAssignments({
                            assignmentId: assignment.assignment.id,
                            requestBody: {
                                assignment_status_id: IntranetFreelanceAssignmentStatus.WORK_DELIVERED
                            }
                        })).then(() => {
                            cy.wrap(WebixPortalNotificationService.getWebixNotification({
                                contextId: assignment.assignment.id
                            })).then(_notifications => {
                                const notifications = _notifications as unknown as IWebixNotification[]
                                notifications.forEach(notification => {
                                    expect(notification.notification_type.id).not.eq(WebixNotificationType.ASSIGNMENT_OVERDUE)
                                })
                                return notifications[0]
                            }) 
                        })
                    })
                })
                .then(() => {
                    cy.wrap(WebixPortalTranslationService.deleteTransProject({
                        projectId: project.project.id
                    }),{timeout:10000})    
                })
            })
        })
    })
})