import { CognovisRestReportService, IReport, OpenAPI} from "../../../../sources/openapi";

before(() => {
    cy.fixture('cypress.env.json')
    .then((testEnv) => {
        // First we authorize to our app
        OpenAPI.TOKEN = testEnv.enviromentTestVariables.DEV.admin.api_key;
    });
});

describe('Basic report testing', () => {
    it('Should return an array of reports', function () {
        cy.wrap(CognovisRestReportService.getReports()).as('reports')
        cy.get('@reports').then(_reports => {
            const reports = _reports as unknown as IReport[]
            expect(reports.length).gt(0)
        })
    })

    it('should return a random report', function () {
        cy.wrap(CognovisRestReportService.getReports().then(res => {
            return res[Math.floor(Math.random() * (res.length -1))]
        })).as('report')
        cy.get('@report').then(_report => {
            const report = _report as unknown as IReport
            cy.wrap(CognovisRestReportService.getReport({
                reportId: report.report.id
            })).as('report_with_data')
        })
        cy.get('@report_with_data').then(_report => {
            const report = _report as unknown as IReport
            expect(report.report_rows.length).gt(0)
        })
    })
})