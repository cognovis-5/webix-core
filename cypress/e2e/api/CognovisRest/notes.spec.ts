import { ITransProject, OpenAPI, INote, CognovisRestNotesService, WebixPortalTranslationService, IntranetNotesStatus, IntranetProjectStatus, IntranetProjectType, IntranetNotesType} from "../../../../sources/openapi";


before(() => {
    cy.fixture('cypress.env.json')
    .then((testEnv) => {
        // First we authorize to our app
        OpenAPI.TOKEN = testEnv.enviromentTestVariables.DEV.admin.api_key;
    });
});

describe("Round Robin notes functionality", () => {
    beforeEach(() => {
        // Get a project with multiple target languages 
        cy.wrap(WebixPortalTranslationService.getTransProjects({
            projectStatusId: [IntranetProjectStatus.OPEN],
            projectTypeId: [IntranetProjectType.TRANS_PROOF]
        }).then(res => {
            const projects = res.filter(el => el.subject_area != null)
            return projects[Math.floor(Math.random()* (projects.length))]
        })).as('project')    
    })

    it("SHould create, update and a note for a project", function () {
        cy.get('@project').then(_project => {
            const project = _project as unknown as ITransProject
            cy.wrap(CognovisRestNotesService.postNote({
                objectId: project.project.id,
                requestBody: {
                    note_type_id: IntranetNotesType.MEETING_NOTE,
                    note: "This is a new note for a meeting"
                }
            }))
            .then(_note => {
                const note = _note as INote
                cy.log("Created note " + note.note.id + " with status " + note.note.status.name)
                cy.wrap(CognovisRestNotesService.putNote({
                    noteId: note.note.id,
                    requestBody: {
                        note: "This is a changed meeting note",
                        note_status_id: IntranetNotesStatus.DELETED
                    }
                }))
                .then(res => {
                    const put_note = res as INote
                    cy.log("Updated note " + put_note.note.id + " with note " + put_note.note + " and status " + put_note.note.status.name)
                    expect(put_note.note.name).eq("This is a changed meeting note")
                    expect(put_note.note.status.id).eq(IntranetNotesStatus.DELETED)

                    cy.wrap(CognovisRestNotesService.deleteNote({
                        noteId:put_note.note.id
                    })).should('be.empty')
                    .then(() => {
                        cy.log("Deleted the note" + put_note.note.id)

                        cy.wrap(CognovisRestNotesService.getNote({
                            objectId: project.project.id
                        }))
                        .then(res => {
                            const notes = res as unknown as INote[]
                            notes.forEach(note => {
                                cy.log("Found note for " + project.project_nr + " - " + note.note.id)
                                expect(note.note).not.null
                                expect(note.note.id).not.eq(put_note.note.id)
                            }) 
                        })
                    })
                })
            })
        })
    })
})