import { ITransProject, OpenAPI, CognovisRestCompanyService, CognovisRestTimesheetService, WebixPortalTranslationService, ITimesheetTask, IntranetCompanyType, IntranetProjectType, IntranetTranslationLanguage, ICompany, IntranetProjectStatus, ITimesheetEntry} from "../../../../sources/openapi";


before(() => {
    cy.fixture('cypress.env.json')
    .then((testEnv) => {
        // First we authorize to our app
        OpenAPI.TOKEN = testEnv.enviromentTestVariables.DEV.admin.api_key;
    });
});

describe("Basic timesheet task functionality", () => {
    beforeEach(() => {
        // get a random test company
        cy.wrap(WebixPortalTranslationService.getTransProjects({
            projectStatusId: [IntranetProjectStatus.POTENTIAL]
        }).then(res => {
            return res[Math.floor(Math.random() * (res.length -1))] as ITransProject 
        }), {timeout: 10000}).as('project')
    })

    it("Should create update and delete a timesheet task for a project", function () {
        const taskName = Math.floor((9999 - 1) * Math.random()) + " Dummy Test Task"
        cy.get('@project').then(_project => {
            const project = _project as unknown as ITransProject
            cy.wrap(CognovisRestTimesheetService.postTimesheetTask({
                projectId: project.project.id,
                requestBody: {
                    task_name: taskName,
                    planned_units: 5,
                    billable_units: 5,
                    end_date: "2023-04-01T22:00:01"
                }
            }))
            .then(res => {
                const task = res as unknown as ITimesheetTask
                expect(task.project.id).eq(project.project.id)
                expect(task.task.name).eq(taskName)
                expect(task.billable_units).eq(5)
                cy.log("Created new task " + task.task.id)
                cy.wrap(CognovisRestTimesheetService.putTimesheetTask({
                    taskId: task.task.id,
                    requestBody: {
                        task_name: task.task.name,
                        percent_completed: 0,
                        planned_units: task.planned_units,
                        billable_units: 0,
                        start_date: task.start_date,
                        end_date: task.end_date
                    }
                }))
                .then(res => {
                    const updatedTask = res as unknown as ITimesheetTask
                    expect(updatedTask.billable_units).eq(0)
                    cy.log("Updated task and reset billable units")
                    cy.wrap(CognovisRestTimesheetService.deleteTimesheetTask({
                        taskId: updatedTask.task.id
                    })).should('be.empty')
                    cy.log("Deleted task again")
                })
            })
        })
    })

    it("Should create update and delete a timesheet hour log entry for a project", function () {
        cy.get('@project').then(_project => {
            const project = _project as unknown as ITransProject
            cy.wrap(CognovisRestTimesheetService.postTimesheetEntry({
                projectId: project.project.id,
                requestBody: {
                    hours: 5,
                    note: 'This is my logged hours'
                }
            }))
            .then(res => {
                const entry = res as unknown as ITimesheetEntry
                expect(entry.project.id).eq(project.project.id)
                expect(entry.note).eq('This is my logged hours')
                expect(entry.hours).eq(5)
                cy.log("Created new log entry " + entry.hour_id)
                cy.wrap(CognovisRestTimesheetService.putTimesheetEntry({
                    hourId: entry.hour_id,
                    requestBody: {
                        hours: 4,
                        note: "Updated to 4 hours"
                    }
                }))
                .then(res => {
                    const updatedEntry = res as unknown as ITimesheetEntry
                    expect(updatedEntry.hours).eq(4)
                    expect(updatedEntry.note).eq('Updated to 4 hours')
                    cy.log("Updated task and reset billable units")
                    /* CognovisRestTimesheetService.putTimesheetEntry({
                        hourId: entry.hour_id,
                        requestBody: {
                            hours: -1,
                            note: "Updated to 4 hours"
                        }
                    })*/
                    cy.wrap(CognovisRestTimesheetService.deleteTimesheetEntry({
                        hourId:entry.hour_id
                    })).should('be.empty')
                    cy.log("Deleted task again")
                })
            })
        })
    })
})