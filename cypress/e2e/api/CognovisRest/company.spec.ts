import * as exp from "constants";
import { OpenAPI,CognovisRestCompanyService, ICompany, ICompanyContact, IInternalCompany, IntranetCompanyStatus, IntranetCompanyType, IntranetPaymentTerm, IntranetSalutation, CognovisRestService, IntranetBizObjectRole} from "../../../../sources/openapi";

before(() => {
    cy.fixture('cypress.env.json')
    .then((testEnv) => {
        // First we authorize to our app
        OpenAPI.TOKEN = testEnv.enviromentTestVariables.DEV.admin.api_key;
    });
});

describe("Testing offices and countries", () => {
    it("Should return list of countries", function () {
        cy.wrap(CognovisRestCompanyService.getCountries()).should('have.length.above',0)
    })

    it("Should have multiple offices", () => {
        cy.wrap(CognovisRestCompanyService.getCompanyOffices({
            companyId: 37605
        })).should('have.length.above',1)
    })
})

describe("Testing basic GET functions on companies", () => {
    it("should return a company", function () {
        cy.wrap(CognovisRestCompanyService.getCompanies({
            companyStatusIds: [IntranetCompanyStatus.ACTIVE],
            companyTypeIds: [IntranetCompanyType.CUSTOMER]
        })).then(res => {
            const companies = res as ICompany[]
            expect(companies.length).greaterThan(0);
            expect(companies[0].company_status.id).eq(46);
        })
    })

    it("should return a final company", function () {
        cy.wrap(CognovisRestCompanyService.getCompanies({
            companyTypeIds: [10247]
        }).then(res => {
            return res[Math.floor(Math.random() * (res.length -1))]
        })).as('final_company')

        cy.get('@final_company').then(_final_company => {
            const final_company = _final_company as unknown as ICompany
            cy.log("Found final company " + final_company.company.name);
            expect(final_company.company_type.id).eq(10247);

        })
    })

    it("should find a company and return the company contacts", function () {
        cy.wrap(CognovisRestCompanyService.getCompanies({
            companyStatusIds: [IntranetCompanyStatus.ACTIVE],
            companyTypeIds: [IntranetCompanyType.CUSTOMER]
        }).then(res => {
            return res[Math.floor(Math.random() * (res.length -1))]
        })).as('company')

        cy.get('@company').then( _company => {
            const company = _company as unknown as ICompany
            cy.log("Found company " + company.company.name)
            cy.wrap(CognovisRestCompanyService.getCompanyContacts({
                companyId: company.company.id
            }).then(res => {
                return res[Math.floor(Math.random() * (res.length -1))]
            })).as('companyContact')

            cy.get('@companyContact').then(_companyContact => {
                const companyContact = _companyContact as unknown as ICompanyContact
                cy.log("Found company contact " + companyContact.salutation + " " + companyContact.last_name);
                expect(companyContact.contact.id).not.null;
                expect(companyContact.company.id).eq(company.company.id);
                expect(companyContact.key_account).not.null;
                cy.log("with key account " + companyContact.key_account.name);
            })
        })
    })


    it("should return internal company", function () {
        cy.wrap(CognovisRestCompanyService.getInternalCompany()).should('not.be.empty')
        .as('internal_company')
        
        cy.get('@internal_company').then(_internal_company => {
            const internal_company = _internal_company as unknown as IInternalCompany
            cy.log("Found final company " + internal_company.company.company.name);
            expect(internal_company.favicon_url).not.be.empty;
        })
    })

    it("should return the key_account of a freelancer", function () {
        cy.wrap(CognovisRestCompanyService.getCompanies({
            companyStatusIds: [IntranetCompanyStatus.ACTIVE],
            companyTypeIds: [IntranetCompanyType.PROVIDER]
        }).then(res => {
            return res[Math.floor(Math.random() * (res.length -1))]
        })).as('company')

        // Switch to run the test as a freelancer
        cy.get('@company').then( _company => {
            const company = _company as unknown as ICompany
            cy.wrap(CognovisRestService.getApiKey({
                userId: company.primary_contact.id
            })).its('api_key').as('token')
            cy.get('@token').then(_token => {
                OpenAPI.TOKEN = _token as unknown as string
            })
        })

        cy.get('@company').then( _company => {
            const company = _company as unknown as ICompany
            cy.wrap(CognovisRestCompanyService.getCompanyContacts({
                contactIds: [company.primary_contact.id]
            })).its(0).as('company_contact')

            cy.get('@company_contact').then( _company_contact => {
                const company_contact = _company_contact as unknown as ICompanyContact    
                cy.wrap(CognovisRestCompanyService.getCompanyContacts({
                    contactIds: [company_contact.key_account.id]
                })).should('not.be.empty')
                .its(0).its('company').its('id').should('equal',8720)    
            })
        }).then(() => {
            cy.fixture('cypress.env.json')
            .then((testEnv) => {
                // First we authorize to our app
                OpenAPI.TOKEN = testEnv.enviromentTestVariables.DEV.admin.api_key;
            });
        })
    })
})

describe("Company creation and deletion", () => {
    const companyName = "Cypress Test Company " + Math.floor((9999 - 1) * Math.random());
    it.only("Should create a company and delete it again", function () {
        cy.wrap(CognovisRestCompanyService.postCompany({
            requestBody: {
                company_name: companyName,
                company_type_id: IntranetCompanyType.CUSTOMER,
                address_city: "Hamburg",
                address_country_code: "de",
                address_line1: "Schröderstrasse 7",
                address_postal_code: "22414",
                phone: "9012912",
                url: "https://test.de",
                payment_term_id: IntranetPaymentTerm.N14_DAYS,
                accounting_contact_id: 91246,
                primary_contact_id: 91246
            }
        }),{timeout:10000}).as('company')
        
        cy.get('@company').then(_company => {
            const company = _company as unknown as ICompany
            expect(company.company.name).eq(companyName);
            expect(company.company_status.id).eq(IntranetCompanyStatus.ACTIVE);
            expect(company.address_line1).eq("Schröderstrasse 7")
            expect(company.primary_contact.id).eq(91246)
            expect(company.accounting_contact.id).eq(91246)

            cy.wrap(CognovisRestCompanyService.postCompanyContact({
                companyId: company.company.id,
                requestBody: {
                    first_names: "Malte",
                    last_name: "Test",
                    email: Math.floor((9999 - 1) * Math.random()) + "test@malte.sussdorff.de",
                    salutation_id: IntranetSalutation.DEAR_MR_,
                    position: "Manager"
                },
                password: "testing"
            })).as('contact')

            cy.get('@contact').then(_contact => {
                const contact = _contact as unknown as ICompanyContact
                expect(contact.last_name).eq('Test')
                expect(contact.company.id).eq(company.company.id)
                cy.wrap(CognovisRestCompanyService.putCompanyContact({
                    contactId: contact.contact.id,
                    requestBody: {
                        first_names: "Malte 2",
                        last_name: "Test",
                        email: contact.email,
                        salutation_id: IntranetSalutation.DEAR_MR_,
                        position: contact.position
                    }
                })).then(res => {
                    const updatedContact = res as unknown as ICompanyContact
                    expect(updatedContact.first_names).eq("Malte 2")
                    cy.log("Successfully updated contact");
                }).then(() => {
                    cy.wrap(CognovisRestCompanyService.putCompany({
                        companyId: company.company.id,
                        requestBody: {
                            company_name: company.company.name,
                            primary_contact_id: contact.contact.id
                        }
                    })).then(res => {
                        const updatedCompany = res as unknown as ICompany
                        expect(updatedCompany.primary_contact.id).eq(contact.contact.id)
                    })
                }).then(() => {
                    cy.wrap(CognovisRestCompanyService.deleteCompanyContact({
                        contactId: contact.contact.id,
                        companyId: company.company.id
                    })).should('be.empty')
                    .then(() => {
                        cy.wrap(CognovisRestCompanyService.getCompanyContacts({
                            companyId: company.company.id,
                            roleIds: [IntranetBizObjectRole.FULL_MEMBER]
                        })).should('be.empty')
                        .then(() => {
                            cy.log("Successfully deleted contact from company")
                            cy.wrap(CognovisRestCompanyService.deleteCompanyContact({
                                contactId: contact.contact.id
                            })).should('be.empty')
                            .then(() => {
                                cy.log("Successfully nuked the contact")
                            })    
                        })
                    })
                })
            }).then(() => {
                cy.log('Updating company')
                cy.wrap(CognovisRestCompanyService.putCompany({
                    companyId: company.company.id,
                    requestBody: {
                        company_name: 'Malte Testing Updates',
                        address_city: 'Mumbai'
                    }
                })).as('updatedCompany')
            })

            cy.get('@updatedCompany').then(_updatedCompany => {
                const updatedCompany = _updatedCompany as unknown as ICompany
                expect(updatedCompany.company.name).eq('Malte Testing Updates');
                expect(updatedCompany.address_city).eq('Mumbai');
                expect(updatedCompany.payment_term.id).eq(IntranetPaymentTerm.N14_DAYS)
                expect(updatedCompany.url).eq('https://test.de');

                cy.log('Deleting company')
                cy.wrap(CognovisRestCompanyService.deleteCompany({
                    companyId: updatedCompany.company.id
                })).should('be.empty')
                .then(() => {
                    cy.log("Successfully deleted company")
                })
            })
        })
    })
})

