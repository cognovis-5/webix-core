import { OpenAPI,CognovisRestService, CognovisRestCompanyService, ICompany, WebixPortalTranslationService, ITransProject, ICategory, ITranslation, IMaterial, CognovisRestInvoiceService, ICostCenter, IGroup} from "../../../../sources/openapi";
import { IBizObjectMember } from "../../../../sources/openapi/models/IBizObjectMember";
import { IUser } from "../../../../sources/openapi/models/IUser";

before(() => {
    cy.fixture('cypress.env.json')
    .then((testEnv) => {
        // First we authorize to our app
        OpenAPI.TOKEN = testEnv.enviromentTestVariables.DEV.admin.api_key;
    });
});

describe("Testing basic functions on categories", () => {
    it.only("should return a category with a different user", function () {
        cy.setUser(276113,OpenAPI).wrap(
            CognovisRestService.getImCategory({
                categoryType: "Intranet UoM"
            })
            .then(res => {
                return res[Math.floor(Math.random() * res.length)]
            })
        )
        .should('not.be.empty')
        .as('category')
        
        // Above getImCategory is called with admin
        // Below getImCategory is called with userId 276113
        
        cy.get('@category').then(_category => {
            const category = _category as unknown as ICategory
            cy.log("Found category " + category.category);
            expect(category.category_id).to.be.oneOf([320, 321,322,324,330])
        })
        .then(() => {
            cy.wrap(
                CognovisRestService.getImCategory({
                    categoryType: "Intranet UoM"
                })
                .then(res => {
                    return res[Math.floor(Math.random() * res.length)]
                })
            )
            .should('not.be.empty')
            .as('category2')    
        })
    })

    it("should return a category", function () {
        cy.wrap(
            CognovisRestService.getImCategory({
                categoryType: "Intranet UoM"
            })
            .then(res => {
                return res[Math.floor(Math.random() * res.length)]
            })
        )
        .should('not.be.empty')
        .as('category')
        
        cy.get('@category').then(_category => {
            const category = _category as unknown as ICategory
            cy.log("Found category " + category.category);
            expect(category.category_id).to.be.oneOf([320, 321,322,324,330])
        })
    })
})

describe("Testing basic functions on materials", () => {

    it("should return a material", function () {
        cy.wrap(
            CognovisRestService.getMaterials({
                materialTypeIds: [9012]
            })
            .then(res => {
                return res[Math.floor(Math.random() * res.length)]
            })
        )
        .should('not.be.empty')
        .as('material')
        
        cy.get('@material').then(_material => {
            const material = _material as unknown as IMaterial
            cy.log("Found material " + material.material.name + " (" + material.material.id + ")");
            // check against both 9000 and 9012 as the type 9000 is  a child o 9012
            expect(material.material_type.id).to.be.oneOf([9012,9000])
        })
    })
})

describe("Testing basic functions on cost_center", () => {
    it("should return a cost center", function () {
        cy.wrap(
            CognovisRestInvoiceService.getCostCenters({
                costCenterTypeId: 3001
            })
            .then(res => {
                return res[Math.floor(Math.random() * res.length)]
            })
        )
        .should('not.be.empty')
        .as('cost_center')
        
        cy.get('@cost_center').then(_cost_center => {
            const cost_center = _cost_center as unknown as ICostCenter
            cy.log("Found cost center " + cost_center.cost_center.name + " (" + cost_center.cost_center.id + ")");
            // check against both 9000 and 9012 as the type 9000 is  a child o 9012
            expect(cost_center.cost_center_type.id).eq(3001)
        })
    })
})

describe("Testing basic functions on I18N", () => {
    it("should return a translation string", function () {
        cy.wrap(
            CognovisRestService.getI18N({
                packageKey: "webix-portal",
                locale: "de_DE"
            })
            .then(res => {
                return res[Math.floor(Math.random() * res.length)]
            })
        )
        .should('not.be.empty')
        .as('translation')
        
        cy.get('@translation').then(_translation => {
            const translation = _translation as unknown as ITranslation
            cy.log("Found translation " + translation.message_key + " into " + translation.message)
            // check against both 9000 and 9012 as the type 9000 is  a child o 9012
            expect(translation.locale).eq('de_DE')
        })
    })
})

describe("Testing basic functions on groups", () => {

    before(() => {
        cy.wrap(CognovisRestCompanyService.getCompanies({
            companyStatusIds: [46]
        })).its(0).as('company')
    
        cy.get('@company').then( _company => {
            const company = _company as unknown as ICompany
            cy.wrap(CognovisRestCompanyService.getCompanyContacts({
                companyId: company.company.id
            })).its(0).its('contact').its('id').as('companyContactId')
        })
    })
    it("should return a groups for a customer", function () {
        cy.wrap(
            CognovisRestService.getUserGroups({
                userId: this.companyContactId
            })
        )
        .should('not.be.empty')
        .as('userGroups')
        
        cy.get('@userGroups').then(_userGroups => {
            const userGroups = _userGroups as unknown as IGroup[]
            userGroups.forEach(group => {
                cy.log("Found  " + group.group_name)
            })
            const customerGroup =  userGroups.filter(el => el.group_id === 461)

            // check against both 9000 and 9012 as the type 9000 is  a child o 9012
            expect(customerGroup.length).eq(1)
        })
    })

    it("should return employees", function () {
        cy.wrap(CognovisRestService.getGroupMembers({
            groupId: 463
        }))
        .should('not.be.empty')
        .then(_members => {
            const members = _members as IUser[]
            members.forEach(member => {
                expect(member.user.name).not.empty
                cy.log("Found member " + member.user.name)
            })
        })
    })
})

describe("Handling of users", () => {
    before(() => {
        // Get a project with multiple target languages        
        cy.wrap(WebixPortalTranslationService.getTransProjects({
            projectTypeId: [87],
            projectStatusId: [76,71]
        })).as('projects')    
        
        cy.get('@projects').then(_projects => {
            const projects = _projects as unknown as Array<ITransProject>
            return projects[Math.floor(Math.random() * (projects.length -1))].project_lead.user.id
        }).as('projectLeadId')
    })

    it("should return user information for a project_lead in the system", function () {
        cy.get('@projectLeadId').then(userId => {
            cy.wrap(CognovisRestService.getUser({
                userIds: [userId as unknown as number]
            }))
            .then(_user => {
                const user = _user[0] as unknown as IUser
                cy.log("Found Project Lead " + user.user.name + " last seen " + user.seconds_since_last_request +  " seconds ago")
            })
        })
    })
})

describe("Handling of members", () => {
    before(() => {
        // Get a project with multiple target languages        
        cy.wrap(WebixPortalTranslationService.getTransProjects({
            projectTypeId: [87],
            projectStatusId: [78,79,81]
        })).as('projects')    
        
        cy.get('@projects').then(_projects => {
            const projects = _projects as unknown as Array<ITransProject>
            const multiTargetProject = projects.filter(el => el.target_language.length >1)
            return multiTargetProject[Math.floor(Math.random() * (multiTargetProject.length -1))]
        }).as('project')

        cy.wrap(CognovisRestService.getGroupMembers({
            groupId: 463
        })).as('employees')

        cy.get('@employees').then(_employees => {
            const employees = _employees as unknown as IUser[]
            return employees[Math.floor(Math.random() * (employees.length -1))]
        }).as('employee')
    })

    it("Should add a member to a project and remove the member afterwards", function () {
        cy.get('@project').then(_project => {
            const project = _project as unknown as ITransProject
            cy.log("FOund project " + project.project_nr)
            cy.wrap(CognovisRestService.getBizObjectMembers({
                objectId: project.project.id
            }))
            .then(_members => {
                const members = _members as unknown as IBizObjectMember[]
                members.forEach(member => {
                    cy.log("Found member " + member.member.user.name + " last seen " + member.member.seconds_since_last_request +  " seconds ago")
                    expect(member.object.id).eq(project.project.id)
                    expect(member.member.portrait_url).not.undefined
                })
            })

            cy.get('@employee').then(_employee => {
                const employee = _employee as unknown as IUser
                cy.log("Adding "+ employee.user.name + " to the project")
                cy.wrap(CognovisRestService.postBizObjectMember({
                    requestBody: {
                        member_id: employee.user.id,
                        object_id: project.project.id,
                        percentage: 100
                    }
                }))
                .then(_members => {
                    const members = _members as unknown as IBizObjectMember[]
                    const memberIds = [...new Set(members.map(item => item.member.user.id))]
                    expect(memberIds).to.include(employee.user.id)
                })
                .then(() => {
                    cy.log("Removing " + employee.user.name + " from the project " + project.project_nr)
                    cy.wrap(CognovisRestService.deleteBizObjectMember({
                        objectId: project.project.id,
                        memberId: employee.user.id
                    }))
                    .then(_members => {
                        const members = _members as unknown as IBizObjectMember[]
                        members.forEach(member => {
                            expect(member.member.user.id).not.eq(employee.user.id)
                        })
                    })    
                })
            })
            
        })
    })
})