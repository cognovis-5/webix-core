import * as exp from "constants";
import { OpenAPI,CognovisRestCompanyService, CognovisRestPricingService, IntranetMaterialType, IntranetCompanyType, WebixPortalTranslationService, ITransProject, IMaterial, IntranetProjectType, IntranetProjectStatus, IntranetTranslationLanguage, IntranetUom, IPrice, ICompany, IntranetPriceStatus} from "../../../../sources/openapi";
import {randomEnum} from "../../../support/helpers"

before(() => {
    cy.fixture('cypress.env.json')
    .then((testEnv) => {
        // First we authorize to our app
        OpenAPI.TOKEN = testEnv.enviromentTestVariables.DEV.admin.api_key;
    });
});

describe('Material checks', () => {

    it("Should return the list of materials for a project", function () {
        cy.wrap(WebixPortalTranslationService.getTransProjects({
            projectStatusId: [IntranetProjectStatus.OPEN],
            projectTypeId: [IntranetProjectType.TRANS_PROOF]
        }).then(res => {
            const projects = res.filter(el => el.subject_area != null)
            return projects[Math.floor(Math.random()* (projects.length))]
        })).as('project')    
        
        cy.get('@project').then(_project => {
            const project = _project as unknown as ITransProject
            console.log(JSON.stringify(project))
            const targetLanguageIds = [...new Set(project.target_language.map(item => item.id))]
            cy.wrap(CognovisRestPricingService.getMaterial({
                materialTypeId: IntranetMaterialType.TRANSLATION,
                sourceLanguageId: project.source_language.id,
                targetLanguageIds: targetLanguageIds,
                subjectAreaId: project.subject_area.id
            }).then(res => {
                if(res == []) {
                    return CognovisRestPricingService.getMaterial({
                        materialTypeId: IntranetMaterialType.TRANSLATION,
                        sourceLanguageId: project.source_language.id,
                        targetLanguageIds: targetLanguageIds
                    })
                } else {
                    return res
                }
            })).as('materials')
            cy.get('@materials').then(_materials => {
                const materials = _materials as unknown as IMaterial[]
                expect(materials[0].source_language.id).eq(project.source_language.id)
            })
        })
    })
})

describe ('Price check', () => {

    before(() => {
        // get a random test company
        cy.wrap(CognovisRestCompanyService.getCompanies({
            autocompleteQuery: 'test_',
            companyTypeIds: [IntranetCompanyType.CUSTOMER]
        }).then(res => {
            return res[Math.floor(Math.random() * (res.length -1))]
        })).as('testCompany')
    })

    it('Should create, update and delete a price with material_id', function () {
        cy.wrap(CognovisRestPricingService.getMaterial({
            materialTypeId: IntranetMaterialType.TRANSLATION,
            sourceLanguageId: IntranetTranslationLanguage.DEDE,
            targetLanguageIds: [IntranetTranslationLanguage.ELGR],
            materialUomId: IntranetUom.SWORD
        })).its(0).as('material')

        const company = this.testCompany as unknown as ICompany

        cy.get('@material').then(_material => {
            const material = _material as unknown as IMaterial
            cy.wrap(CognovisRestPricingService.postPrice({
                requestBody: {
                    company_id: company.company.id,
                    price: 0.37,
                    material_id: material.material.id
                }
            })).then(_price => {
                const price = _price as unknown as IPrice
                expect(price.price).eq(0.37)
                expect(price.company.id).eq(company.company.id)
                expect(price.material.material.id).eq(material.material.id)
                return price
            }).as('price')
        })

        cy.get('@price').then(_price => {
            const price = _price as unknown as IPrice
            cy.log('Updating price')
            cy.wrap(CognovisRestPricingService.putPrice({
                priceId: price.price_id,
                requestBody: {
                    price: 0.41,
                    company_id: price.company.id,
                    note: "Updated to 0.41"
                }
            })).as('upPrice')
            cy.get('@upPrice').then(_upPrice => {
                const upPrice = _upPrice as unknown as IPrice
                expect(upPrice.price).eq(0.41)
                expect(upPrice.note).eq('Updated to 0.41')
                expect(upPrice.price_status.id).eq(IntranetPriceStatus.ACTIVE)
                cy.wrap(CognovisRestPricingService.putPrice({
                    priceId:upPrice.price_id,
                    requestBody: {
                        price: upPrice.price,
                        company_id: upPrice.company.id,
                        price_status_id: IntranetPriceStatus.INACTIVE
                    }
                }).then(_inactivePrice => {
                    const inactivePrice = _inactivePrice as unknown as IPrice
                    expect(inactivePrice.price_status.id).eq(IntranetPriceStatus.INACTIVE)
                    expect(inactivePrice.price_id).not.eq(price.price_id)
                }))
                .then(() => {
                    cy.wrap(CognovisRestPricingService.deletePrice({
                        priceId:upPrice.price_id
                    }))
                })
            })
            .then(() => {
                cy.wrap(CognovisRestPricingService.deletePrice({
                    priceId:price.price_id
                }))
            })
        })
    })
})

describe ('Rate check', () => {

    before(() => {
        // get a random test company
        cy.wrap(CognovisRestCompanyService.getCompanies({
            autocompleteQuery: 'test_',
            companyTypeIds: [IntranetCompanyType.CUSTOMER]
        }).then(res => {
            return res[Math.floor(Math.random() * (res.length -1))]
        })).as('testCompany')
        cy.wrap(CognovisRestPricingService.getMaterial({
            materialTypeId: IntranetMaterialType.TRANSLATION,
            sourceLanguageId: IntranetTranslationLanguage.DEDE,
            materialUomId: IntranetUom.SWORD
        }).then(res => {
            return res[Math.floor(Math.random() * (res.length -1))]
        })).as('material')
    })

    it.only('Should add a price to a company and verify the price differs from the default one', function () {
        const material = this.material as unknown as IMaterial
        const company = this.testCompany as unknown as ICompany

        
            cy.wrap(CognovisRestPricingService.postPrice({
                requestBody: {
                    company_id: company.company.id,
                    price: 0.37,
                    material_id: material.material.id
                }
            })).then(_price => {
                const price = _price as unknown as IPrice
                expect(price.price).eq(0.37)
                expect(price.company.id).eq(company.company.id)
                expect(price.material.material.id).eq(material.material.id)
                return price
            }).as('price')
        })

        cy.get('@price').then(_price => {
            const price = _price as unknown as IPrice
            cy.log('Updating price')
            cy.wrap(CognovisRestPricingService.putPrice({
                priceId: price.price_id,
                requestBody: {
                    price: 0.41,
                    company_id: price.company.id,
                    note: "Updated to 0.41"
                }
            })).as('upPrice')
            cy.get('@upPrice').then(_upPrice => {
                const upPrice = _upPrice as unknown as IPrice
                expect(upPrice.price).eq(0.41)
                expect(upPrice.note).eq('Updated to 0.41')
                expect(upPrice.price_status.id).eq(IntranetPriceStatus.ACTIVE)
                cy.wrap(CognovisRestPricingService.putPrice({
                    priceId:upPrice.price_id,
                    requestBody: {
                        price: upPrice.price,
                        company_id: upPrice.company.id,
                        price_status_id: IntranetPriceStatus.INACTIVE
                    }
                }).then(_inactivePrice => {
                    const inactivePrice = _inactivePrice as unknown as IPrice
                    expect(inactivePrice.price_status.id).eq(IntranetPriceStatus.INACTIVE)
                    expect(inactivePrice.price_id).not.eq(price.price_id)
                }))
                .then(() => {
                    cy.wrap(CognovisRestPricingService.deletePrice({
                        priceId:upPrice.price_id
                    }))
                })
            })
            .then(() => {
                cy.wrap(CognovisRestPricingService.deletePrice({
                    priceId:price.price_id
                }))
            })
        })
    })
})