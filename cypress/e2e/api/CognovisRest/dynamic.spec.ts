import { OpenAPI,CognovisRestCompanyService,IntranetCompanyType, ICompany, CognovisRestDynamicService} from "../../../../sources/openapi";

before(() => {
    cy.fixture('cypress.env.json')
    .then((testEnv) => {
        // First we authorize to our app
        OpenAPI.TOKEN = testEnv.enviromentTestVariables.DEV.admin.api_key;
    });
});

describe("Testing generic objects", () => {
    it('Should return data for a company', function () {
        cy.wrap(CognovisRestCompanyService.getCompanies({
            companyTypeIds: [IntranetCompanyType.CUSTOMER]
        }).then(res => {
            const customers = res.filter(el => el.primary_contact !== null)
            return customers[Math.floor(Math.random() * (customers.length -1))]
        })).as('test_company')
    
        cy.get('@test_company').then(_company => {
            const company = _company as unknown as ICompany
            const companyId = company.company.id
            cy.wrap(CognovisRestDynamicService.getDynamicObject({
                objectId: companyId
            }).then(res => {
                expect(res.find(e => e.id === 'company_name').value).eq(company.company.name)
                const newLocal = res.find(e => e.id === 'company_type_id').value == company.company_type.id as unknown as string;
                expect(newLocal).eq(true)
            }))
        })
    })

    it.only('Should update data for a company', function () {
        cy.wrap(CognovisRestCompanyService.getCompanies({
            companyTypeIds: [IntranetCompanyType.CUSTOMER]
        }).then(res => {
            const customers = res.filter(el => el.primary_contact !== null)
            return customers[Math.floor(Math.random() * (customers.length -1))]
        })).as('test_company')
    
        cy.get('@test_company').then(_company => {
            const company = _company as unknown as ICompany
            const companyId = company.company.id
            const companyStatusId = company.company_status.id
            cy.wrap(CognovisRestDynamicService.putDynamicObject({
                requestBody: {
                    object_id: companyId,
                    id_values: [
                        {
                            id: 'company_status_id',
                            value: '4800'
                        },
                        {
                            id: 'company_type_id',
                            value: company.company_type.id as unknown as string
                        }
                    ]
                }
            }))
            .then(() => {
                cy.wrap(CognovisRestCompanyService.getCompanies({
                    companyId: company.company.id
                }).then(res =>{
                    expect(res[0].company_status.id).eq(4800)
                }))
                .then(() => {
                    cy.wrap(CognovisRestCompanyService.putCompany({
                        companyId: companyId,
                        requestBody: {
                            company_status_id: companyStatusId,
                            company_name: company.company.name
                        }
                    }).then(res => {
                        expect(res.company_status.id).eq(companyStatusId)
                    }))
                })
            })
        })
    })
})