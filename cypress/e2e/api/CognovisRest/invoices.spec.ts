import * as exp from "constants";
import { compact } from "cypress/types/lodash";
import { CognovisRestInvoiceService, OpenAPI, WebixPortalTranslationService, IInvoiceItem, ICompany, IInvoice, CognovisRestCompanyService, IntranetCompanyType, INamedId, IntranetCostStatus, IntranetCostType, CognovisRestTimesheetService, IntranetProjectType, IntranetTranslationLanguage, ITransProject, ITimesheetTask, ITimesheetEntry, IntranetProjectStatus, IntranetInvoicePaymentMethod, IntranetVatType, CognovisRestService, IPrivilege, IntranetCollmexRestService, ICollmexAccdoc, IntranetUom } from "../../../../sources/openapi";
import { randomizedName,precisionRound } from "../../../support/helpers"

before(() => {
    cy.fixture('cypress.env.json')
    .then((testEnv) => {
        // First we authorize to our app
        OpenAPI.TOKEN = testEnv.enviromentTestVariables.DEV.pm.api_key;
    });
});

describe("Should get a quote + po back", () => {

    beforeEach(() => {

        // Get a project with multiple target languages        
        cy.wrap(CognovisRestInvoiceService.getInvoice({
            costTypeIds: [3702]
        }).then(res => {
            const projectInvoices = res.filter(el => el.project !== null)
            return projectInvoices[Math.floor(Math.random()* (projectInvoices.length))].project
        })
        ).as('quoteProject')    

        cy.wrap(CognovisRestInvoiceService.getInvoice({
            costTypeIds: [3704]
        }).then(res => {

            const projectInvoices = res.filter(el => el.project !== null)
            return projectInvoices[Math.floor(Math.random()* (projectInvoices.length))].project
        })
        ).as('purchaseOrderProject')    

        cy.get('@quoteProject').then(_project => {
            const project = _project as unknown as INamedId
            cy.wrap(CognovisRestInvoiceService.getInvoice({
                projectId: project.id,
                costTypeIds: [3702]
            })
            .then(res => {
                return res[Math.floor(Math.random() * (res.length -1))]
            })).as('quote')
        })

        cy.get('@purchaseOrderProject').then(_project => {
            const project = _project as unknown as INamedId        
            cy.wrap(CognovisRestInvoiceService.getInvoice({
                projectId: project.id,
                costTypeIds: [3704]
            })
            .then(res => {
                expect(res.length).greaterThan(0);
                return res[Math.floor(Math.random() * (res.length -1))]
            })).as('purchase_order')
        })
    })

    it('should return a quote with address', function () {
        const quote = this.quote as IInvoice
        if(quote.invoice.id == undefined) {
            cy.log("No Quote found for " + this.project.project.name)
        } else {
            cy.log("Quote found " + quote.invoice.name)
            expect(quote.company_contact.name).not.be.empty
        }
    })
    it('should return a purchase_order with address', function () {
        cy.get('@purchase_order').then(_po => {
            const po = _po as unknown as IInvoice
            if(po.invoice.id == undefined) {
                cy.log("No purchase_order found for " + this.project.project.name)
            } else {
                cy.log("purchase_order found " + po.invoice.name)
                expect(po.address_country).not.be.empty
            }    
        })
    })
})

describe("Should get a quote with invoice_items back ", () => {

    it("Should return at least one rejected quote", function () {
        
        // Get a project with multiple target languages        
        cy.wrap(CognovisRestInvoiceService.getInvoice({
            costTypeIds: [3702],
            costStatusIds: [IntranetCostStatus.REJECTED]
        }).then(res => {
            const projectInvoices = res.filter(el => el.project !== null)
            return projectInvoices[Math.floor(Math.random()* (projectInvoices.length))]
        })
        ).as('quote')
        
        cy.get('@quote').then(_quote => {
            const quote = _quote as unknown as IInvoice
            cy.log("Found rejected quote " + quote.invoice.name)
            expect(quote.cost_status.id).eq(IntranetCostStatus.REJECTED)
        })
    })


    it("Should return at least one invoice item", function () {

        // Get a project with multiple target languages        
        cy.wrap(CognovisRestInvoiceService.getInvoice({
            costTypeIds: [3702]
        }).then(res => {
            const projectInvoices = res.filter(el => el.project !== null && el.amount >0)
            return projectInvoices[Math.floor(Math.random()* (projectInvoices.length))].project
        })
        ).as('quoteProject')
        
        cy.get('@quoteProject').then(_project => {
            const project = _project as unknown as INamedId
            cy.wrap(CognovisRestInvoiceService.getInvoice({
                projectId: project.id,
                costTypeIds: [3702]
            })
            .then(res => {
                return res[Math.floor(Math.random() * (res.length -1))]
            }))
            .then(_quote => {
                const quote = _quote as unknown as IInvoice
                cy.wrap(CognovisRestInvoiceService.getInvoiceItem({
                    invoiceId: quote.invoice.id
                })).should('not.be.empty')
                .then(_lineItems => {
                    const lineItems = _lineItems as unknown as IInvoiceItem[]
                    expect(lineItems.length).greaterThan(0)
                    cy.log('Found ' + lineItems.length + ' invoice items for ' + quote.invoice.name)
                    lineItems.forEach(lineItem => {
                        expect(lineItem.invoice.id).eq(quote.invoice.id)
                        cy.log(lineItem.item.name + ' ' + quote.currency_symbol + ' ' + lineItem.price_per_unit)
                    })
                })        
            })
        })
    })
})

describe("Handle quote cases",() => {
    beforeEach(() => {
        // get a random test company
        cy.wrap(CognovisRestCompanyService.getCompanies({
            autocompleteQuery: 'test_',
            companyTypeIds: [IntranetCompanyType.CUSTOMER]
        }).then(res => {
            return res[Math.floor(Math.random() * (res.length -1))]
        })).as('test_company')

        cy.get('@test_company').then(_company => {
            const company = _company as unknown as ICompany
            cy.wrap(CognovisRestInvoiceService.getInvoice({
                costTypeIds: [IntranetCostType.QUOTE],
                costStatusIds: [IntranetCostStatus.CREATED],
                companyId: company.company.id
            }).then(res => {
                const projectInvoices = res.filter(el => el.project !== null)
                return projectInvoices[Math.floor(Math.random()* (projectInvoices.length))]
            })).as('quote')
        })
    })

    it("Should create a translation quote with line_items", function () {
        cy.get('@quote').then(_quote => {
            const quote = _quote as unknown as IInvoice
            cy.wrap(CognovisRestInvoiceService.postInvoiceItem({
                invoiceId: quote.invoice.id,
                requestBody: {
                    item_name: 'New Line item',
                    item_units: 500,
                    material_nr: 'discount'
                }
            }))
            .then(_item => {
                const item = _item as unknown as IInvoiceItem
                expect(item.item.name).eq('New Line item')
                expect(item.item_units).eq(500)
                expect(item.item_material.name).eq('discount')
                expect(item.item_uom.id).eq(IntranetUom.UNIT)
                cy.wrap(CognovisRestInvoiceService.putInvoiceItem({
                    itemId: item.item.id,
                    requestBody: {
                        item_name: "Newly updated",
                        material_nr: 'default'
                    }
                }))
                .then(_update_item => {
                    const update_item = _update_item as unknown as IInvoiceItem
                    expect(update_item.item.name).eq('Newly updated')
                    expect(update_item.item_units).eq(500)
                    expect(update_item.item_material.name).eq('default')
                })
                .then(() => {
                    cy.wrap(CognovisRestInvoiceService.deleteInvoiceItem({
                        invoiceItemId: item.item.id
                    }))    
                })
            })
        })
    })
    it("Should create a quote, set it to accepted and delete it again", function () {
        cy.get('@quote').then(_quote => {
            const quote = _quote as unknown as IInvoice
            cy.wrap(WebixPortalTranslationService.putTransProject({
                projectId: quote.project.id,
                requestBody: {
                    project_status_id: IntranetProjectStatus.POTENTIAL
                }
            })).as('potentialProject');

            cy.get('@potentialProject').then(_project => {
                const project=_project as unknown as ITransProject
                expect(project.project_status.id).eq(IntranetProjectStatus.POTENTIAL)
            
                cy.wrap(CognovisRestInvoiceService.putInvoice({
                    invoiceId: quote.invoice.id,
                    requestBody: {
                        cost_status_id: IntranetCostStatus.ACCEPTED
                    }
                }), {timeout: 15000})
                .then(_invoice => {
                    const invoice = _invoice as unknown as IInvoice
                    expect(invoice.cost_status.id).eq(IntranetCostStatus.ACCEPTED)
                    cy.fixture('cypress.env.json')
                    .then((testEnv) => {
                        // We need to reset the quote to created to be able to delete it again as the accounting user.
                        OpenAPI.TOKEN = testEnv.enviromentTestVariables.DEV.accounting.api_key;
                        cy.wrap(CognovisRestInvoiceService.putInvoice({
                            invoiceId: quote.invoice.id,
                            requestBody: {
                                cost_status_id: IntranetCostStatus.CREATED
                            }
                        })).then(() => {
                            cy.wrap(CognovisRestInvoiceService.deleteInvoice({
                                invoices: [invoice.invoice.id]
                            }).then(_res => {
                                const res = _res as unknown as any
                                expect(res.success).eq(true)
                            }))    
                        })
                    });
                })
            })
            .then(()=> {
                cy.wrap(WebixPortalTranslationService.getTransProjects({
                    projectId: quote.project.id
                })).its(0).its('project_status.id').should('eq',IntranetProjectStatus.OPEN)
            })
        })
    })
})

describe("Creating invoices for timesheet tasks", () => {
    beforeEach(() => {
        // get a random test company
        cy.wrap(CognovisRestCompanyService.getCompanies({
            autocompleteQuery: 'cypress',
            companyTypeIds: [IntranetCompanyType.CUSTOMER]
        }).then(res => {
            const company = res[Math.floor(Math.random() * (res.length -1))] as ICompany 
            const projectLeadId = 648038
            return WebixPortalTranslationService.postTransProject(
                {
                    requestBody: {
                        company_id: company.company.id,
                        project_type_id: IntranetProjectType.TRANS_PROOF,
                        source_language_id: IntranetTranslationLanguage.DEDE,
                        target_language_ids: [IntranetTranslationLanguage.ENGB],
                        description: 'This is a new "timesheet" {task} [$project]',
                        project_lead_id: projectLeadId
                    }
                }
            )
            
        }), {timeout: 10000}).as('project')
    })
        
    it("Should add timesheet task and add them to the quote", function () {
        
        cy.get('@project').then(_project => {
            const project = _project as unknown as ITransProject
            // Create tasks (and its packages)
            cy.wrap(CognovisRestTimesheetService.postTimesheetTask({
                projectId:project.project.id, 
                requestBody: {
                    task_name: randomizedName('TestTask+'), 
                    billable_units: 0.45,

                }
            }))
            .then(() => {
                cy.wrap(CognovisRestInvoiceService.postInvoice({
                    requestBody: {
                        company_id: project.company.id,
                        company_contact_id: project.company_contact.id,
                        cost_type_id: IntranetCostType.QUOTE,
                        project_id: project.project.id
                    }
                }))
                .then(_invoice => {
                    const invoice = _invoice as unknown as IInvoice
                    cy.log('Created invoice ' + invoice.invoice.name)
                    cy.wrap(CognovisRestInvoiceService.putImportTimesheetTasks({
                        invoiceId: invoice.invoice.id,
                        requestBody: {
                            hour_log_ids: [90, 8912, 1029109],
                            only_unassigned: true
                        }
                    })).then(_invoiceItems => {
                        const invoiceItem = _invoiceItems[0] as unknown as IInvoiceItem
                        expect(invoiceItem.item_units).eq(0.45)
                    })
                })
            })
        })
    })    

    it.only("Should add timesheet task along with a time entry and add them to the invoice", function () {
        
        cy.get('@project').then(_project => {
            const project = _project as unknown as ITransProject
            // Create tasks (and its packages)
            cy.wrap(CognovisRestTimesheetService.postTimesheetTask({
                projectId:project.project.id, 
                requestBody: {
                    task_name: randomizedName('TestTask+'), 
                    billable_units: 0.45,
                }
            }))
            .then(_task => {
                const task = _task as unknown as ITimesheetTask
                const note = "This is a note"
                cy.wrap(CognovisRestTimesheetService.postTimesheetEntry({
                    projectId: task.task.id,
                    requestBody: {
                        hours: 0.7,
                        note: note
                    }
                }))
                .then(() => {
                    cy.wrap(CognovisRestTimesheetService.postTimesheetEntry({
                        projectId: task.task.id,
                        requestBody: {
                            hours: 0.9,
                            note: note
                        }
                    }))
                    .then(_entry => {        
                        const entry = _entry as unknown as ITimesheetEntry
                        expect(entry.hours).eq(0.9)
                        cy.wrap(CognovisRestTimesheetService.putTimesheetEntry({
                            hourId: entry.hour_id,
                            requestBody: {
                                hours: 1.4
                            }
                        })).then(_entry2 => {
                            const entry2 = _entry2 as unknown as ITimesheetEntry
                            expect(entry2.hours).eq(1.4)
                            cy.wrap(CognovisRestInvoiceService.postInvoice({
                                requestBody: {
                                    company_id: project.company.id,
                                    company_contact_id: project.company_contact.id,
                                    cost_type_id: IntranetCostType.CUSTOMER_INVOICE,
                                    project_id: project.project.id
                                }
                            }))
                            .then(_invoice => {
                                const invoice = _invoice as unknown as IInvoice
                                cy.log('Created invoice ' + invoice.invoice.name)
                                cy.wrap(CognovisRestInvoiceService.putImportTimesheetTasks({
                                    invoiceId: invoice.invoice.id,
                                    requestBody: {
                                        only_unassigned: true
                                    }
                                })).then(_invoiceItems => {
                                    const invoiceItem = _invoiceItems[0] as unknown as IInvoiceItem
                                    expect(invoiceItem.item_units).eq(2.1)
                                })
                            })        
                        })
                    })
                })
            })
        })
    })    

    afterEach(() => {
        cy.get('@project').then(_project => {
            const project = _project as unknown as ITransProject
    
            cy.wrap(WebixPortalTranslationService.deleteTransProject({
                projectId: project.project.id
            }))
            .then(() => {
                cy.wrap(WebixPortalTranslationService.getTransProjects({
                    projectNr: project.project_nr
                })).should('be.empty')    
            })
        })  
    })
}) 

describe("Handling multiple quotes and invoices in projects",() => {
    beforeEach(() => {
        // get a random test company
        cy.wrap(CognovisRestCompanyService.getCompanies({
            autocompleteQuery: 'test_',
            companyTypeIds: [IntranetCompanyType.CUSTOMER]
        }).then(res => {
            const customers = res.filter(el => el.primary_contact !== null)
            return customers[Math.floor(Math.random() * (res.length -1))] as ICompany
        })).as('company')

        cy.wrap(CognovisRestCompanyService.getCompanies({
            autocompleteQuery: 'test_',
            companyTypeIds: [IntranetCompanyType.PROVIDER]
        }).then(res => {
            const providers = res.filter(el => el.primary_contact !== null)
            return providers[Math.floor(Math.random() * (res.length -1))] as ICompany
        })).as('provider')

        cy.get('@company').then(_company => {
            const projectLeadId = 648038
            const company = _company as unknown as ICompany
            cy.wrap(WebixPortalTranslationService.postTransProject(
                {
                    requestBody: {
                        company_id: company.company.id,
                        project_type_id: IntranetProjectType.TRANS_PROOF_EDIT,
                        source_language_id: IntranetTranslationLanguage.DEDE,
                        target_language_ids: [IntranetTranslationLanguage.ENGB, IntranetTranslationLanguage.ESES],
                        description: 'This is a new description',
                        project_lead_id: projectLeadId
                    }
                }
            ), {timeout: 15000}).as('project')
        })
    })

    afterEach(() => {
        cy.get('@project').then(_project => {
            const project = _project as unknown as ITransProject
    
            cy.wrap(WebixPortalTranslationService.deleteTransProject({
                projectId: project.project.id
            }))
            .then(() => {
                cy.wrap(WebixPortalTranslationService.getTransProjects({
                    projectNr: project.project_nr
                })).should('be.empty')    
            })
        })  
    })
    
    it("Should two quotes and then build an invoice out of it", function () {
        const project = this.project as ITransProject
        const company = this.company as ICompany

        cy.log('Creating first quote')
        cy.wrap(CognovisRestInvoiceService.postInvoice({
            requestBody: {
                project_id: project.project.id,
                company_id: project.company.id,
                company_contact_id: project.company_contact.id,
                cost_type_id: IntranetCostType.QUOTE,
                payment_method_id: IntranetInvoicePaymentMethod.FOREIGN_TRANSFER
            }
        })).as('quote1')
        cy.get('@quote1').then(_quote => {
            const quote = _quote as unknown as IInvoice
            cy.wrap(CognovisRestInvoiceService.postInvoiceItem({
                invoiceId: quote.invoice.id,
                requestBody: {
                    item_name: 'New Line item',
                    item_units: 50.8,
                    price_per_unit: 0.824,
                    material_nr: 'default'
                }
            }))
        })
        .then(() => {
            cy.log('Creating second quote')
            cy.wrap(CognovisRestInvoiceService.postInvoice({
                requestBody: {
                    project_id: project.project.id,
                    company_id: project.company.id,
                    company_contact_id: project.company_contact.id,
                    cost_type_id: IntranetCostType.QUOTE,
                    payment_method_id: IntranetInvoicePaymentMethod.FOREIGN_TRANSFER
                }
            })).as('quote2')
            cy.get('@quote2').then(_quote => {
                const quote = _quote as unknown as IInvoice
                cy.wrap(CognovisRestInvoiceService.postInvoiceItem({
                    invoiceId: quote.invoice.id,
                    requestBody: {
                        item_name: 'New Line item',
                        item_units: 300,
                        price_per_unit: 0.5,
                        material_nr: 'default'
                    }
                }))
            })    
        })
        .then(() => {
            cy.wrap(CognovisRestInvoiceService.getInvoice({
                projectId: project.project.id,
                costTypeIds: [IntranetCostType.QUOTE]
            })).as('quotes')    
        })

        cy.wrap(WebixPortalTranslationService.putTransProject({
            projectId: project.project.id,
            requestBody: {
                project_status_id: IntranetProjectStatus.DELIVERED
            }
        })).as('deliveredProject')

        cy.get('@deliveredProject').then(_delivered_project => {
            const deliveredProject = _delivered_project as unknown as ITransProject
            expect(deliveredProject.end_date).not.null
        })

        cy.get('@quotes').then(_quotes => {
            const quotes = _quotes as unknown as IInvoice[]
            const sourceIds = [...new Set(quotes.map(item => item.invoice.id))]
            cy.log('Generating invoice of both quotes');

            cy.wrap(CognovisRestInvoiceService.postInvoiceCopy({
                sourceInvoiceIds: sourceIds,
                targetCostTypeId: IntranetCostType.CUSTOMER_INVOICE
            }),{timeout:10000}).as('invoice')

            cy.log('Checking quote and invoice')
            cy.get('@invoice').then(_invoice => {

                const invoice = _invoice as unknown as IInvoice
                expect(invoice.linked_invoice).not.empty
                expect(quotes[0].payment_term.id).eq(invoice.payment_term.id)
                expect(quotes[0].payment_method.id).eq(invoice.payment_method.id)
                expect(quotes[0].vat_type.id).eq(invoice.vat_type.id)
                expect(quotes[0].company_contact.id).eq(invoice.company_contact.id)
                expect(quotes[0].provider.id).eq(8720)
                expect(invoice.provider.id).eq(quotes[0].provider.id)
                expect(invoice.invoice_office?.id).eq(quotes[0].invoice_office?.id)
                expect(invoice.address_city).eq(company.address_city)
                expect(invoice.address_line1).eq(company.address_line1)
                expect(invoice.cost_status.id).eq(IntranetCostStatus.CREATED)
                // expect(quotes[0].cost_center.id).eq(invoice.cost_center.id)
            })

            cy.get('@invoice').then(_invoice => {
                const invoice = _invoice as unknown as IInvoice
                cy.log('Checking line items');

                cy.wrap(CognovisRestInvoiceService.getInvoiceItem({
                    invoiceId: invoice.invoice.id
                }),{timeout: 10000}).as('items')

                cy.get('@items').then(_items => {
                    const items = _items as unknown as IInvoiceItem[]
                    expect(items.length).eq(2)
                    items.forEach(item => {
                        cy.log(item.item.name)
                        expect(item.item_material.name).eq('default')
                    })
                })
            })

            cy.get('@invoice').then(_invoice => {
                const invoice = _invoice as unknown as IInvoice
                cy.log('Sending invoice');
                cy.wrap(CognovisRestInvoiceService.postSendFinancialDocuments({
                    projectId: invoice.project.id,
                    costTypeIds: [IntranetCostType.CUSTOMER_INVOICE],
                }),{timeout: 10000}).as('sendinvoices')

            })

            cy.get('@sendinvoices').then(_invoices => {
                const invoices = _invoices as unknown as IInvoice[]
                invoices.forEach(invoice => {
                    expect(invoice.cost_status.id).eq(IntranetCostStatus.OUTSTANDING)
                    cy.log("Query collmex")
                    cy.wrap(IntranetCollmexRestService.getCollmexAccdoc({
                        invoiceId: invoice.invoice.id
                    })).its(0).then(_accdoc => {
                        const accdoc = _accdoc as unknown as ICollmexAccdoc
                        expect(accdoc.invoice.id).eq(invoice.invoice.id)
                        expect(accdoc.booking_amount).eq(228.31)
                    })
                });
            })
        })

        cy.get('@invoice').then(_invoice => {
            const invoice = _invoice as unknown as IInvoice;
            cy.get('@deliveredProject').then(_delivered_project => {
                const deliveredProject = _delivered_project as unknown as ITransProject
                const endDate = new Date(deliveredProject.end_date)
                const deliveryDate = new Date(invoice.delivery_date)
                expect(endDate.toDateString()).eq(deliveryDate.toDateString())
            })
    
            cy.wrap(CognovisRestInvoiceService.getInvoice({
                projectId: project.project.id,
                costTypeIds: [IntranetCostType.QUOTE]
            }),{timeout: 6000}).as('quotesInvoiced')   
        })
    })

    it("Should two purchase orders and then build a bill out of it", function () {
        const project = this.project as ITransProject
        const company = this.provider as ICompany

        cy.log('Creating first purchase order')
        cy.wrap(CognovisRestInvoiceService.postInvoice({
            requestBody: {
                project_id: project.project.id,
                provider_id: company.company.id,
                company_id: 8720,
                company_contact_id: project.project_lead.user.id,
                cost_type_id: IntranetCostType.PURCHASE_ORDER,
                payment_method_id: IntranetInvoicePaymentMethod.FOREIGN_TRANSFER
            }
        })).as('po1')
        cy.get('@po1').then(_po => {
            const po = _po as unknown as IInvoice
            cy.wrap(CognovisRestInvoiceService.postInvoiceItem({
                invoiceId: po.invoice.id,
                requestBody: {
                    item_name: 'New Line item',
                    item_units: 500,
                    price_per_unit: 0.7,
                    material_nr: 'default'
                }
            }))
        })
        .then(() => {
            cy.log('Creating second po')
            cy.wrap(CognovisRestInvoiceService.postInvoice({
                requestBody: {
                    project_id: project.project.id,
                    provider_id: company.company.id,
                    company_id: 8720,
                    company_contact_id: project.project_lead.user.id,
                    cost_type_id: IntranetCostType.PURCHASE_ORDER,
                    payment_method_id: IntranetInvoicePaymentMethod.FOREIGN_TRANSFER
                }
            })).as('po2')
            cy.get('@po2').then(_po => {
                const po = _po as unknown as IInvoice
                cy.wrap(CognovisRestInvoiceService.postInvoiceItem({
                    invoiceId: po.invoice.id,
                    requestBody: {
                        item_name: 'New Line item',
                        item_units: 300,
                        price_per_unit: 0.25,
                        material_nr: 'default'
                    }
                }))
            })    
        })
        .then(() => {
            cy.wrap(CognovisRestInvoiceService.getInvoice({
                projectId: project.project.id,
                costTypeIds: [IntranetCostType.PURCHASE_ORDER]
            })).as('pos')    
        })


        cy.get('@pos').then(_pos => {
            const pos = _pos as unknown as IInvoice[]
            const sourceIds = [...new Set(pos.map(item => item.invoice.id))]
            cy.log('Generating invoice of both Purchase Orders');

            cy.wrap(CognovisRestInvoiceService.postInvoiceCopy({
                sourceInvoiceIds: sourceIds,
                targetCostTypeId: IntranetCostType.PROVIDER_BILL
            }),{timeout:10000}).as('bill')

            cy.log('Checking purchase order and bills')
            cy.get('@bill').then(_bill => {

                const bill = _bill as unknown as IInvoice
                expect(bill.linked_invoice).not.empty
                expect(pos[0].payment_term.id).eq(bill.payment_term.id)
                expect(pos[0].payment_method.id).eq(bill.payment_method.id)
                expect(pos[0].vat_type.id).eq(bill.vat_type.id)
                expect(pos[0].company_contact.id).eq(bill.company_contact.id)
                expect(pos[0].company.id).eq(bill.company.id)
                expect(bill.provider.id).eq(pos[0].provider.id)
                expect(bill.invoice_office?.id).eq(pos[0].invoice_office?.id)
                expect(bill.address_city).eq(company.address_city)
                expect(bill.address_line1).eq(company.address_line1)
                expect(bill.cost_status.id).eq(IntranetCostStatus.CREATED)
                // expect(pos[0].cost_center.id).eq(invoice.cost_center.id)
            })

            cy.get('@bill').then(_bill => {
                const bill = _bill as unknown as IInvoice
                cy.log('Checking line items');

                cy.wrap(CognovisRestInvoiceService.getInvoiceItem({
                    invoiceId: bill.invoice.id
                }),{timeout: 10000}).as('items')

                cy.get('@items').then(_items => {
                    const items = _items as unknown as IInvoiceItem[]
                    expect(items.length).eq(2)
                    items.forEach(item => {
                        cy.log(item.item.name)
                        expect(item.item_material.name).eq('default')
                    })
                })
            })

            cy.get('@bill').then(_bill => {
                const bill = _bill as unknown as IInvoice
                cy.log('Sending bill');
                cy.wrap(CognovisRestInvoiceService.postSendFinancialDocuments({
                    projectId: bill.project.id,
                    costTypeIds: [IntranetCostType.PROVIDER_BILL],
                }),{timeout: 10000}).as('sendBills')

            })

            cy.get('@sendBills').then(_bills => {
                const bills = _bills as unknown as IInvoice[]
                bills.forEach(bill => {
                    expect(bill.cost_status.id).eq(IntranetCostStatus.OUTSTANDING)
                    cy.log("Query collmex")
                    cy.wrap(IntranetCollmexRestService.getCollmexAccdoc({
                        invoiceId: bill.invoice.id
                    })).its(0).then(_accdoc => {
                        const accdoc = _accdoc as unknown as ICollmexAccdoc
                        expect(accdoc.invoice.id).eq(bill.invoice.id)
                        if(company.vat_type.id === IntranetVatType.PROVIDER_WITH_TAX) {
                            expect(accdoc.booking_amount).eq(-505.75)
                        } else {
                            expect(accdoc.booking_amount).eq(-425)
                        }    
                    })
                });
            })
        })

        cy.get('@bill').then(() => {
            cy.wrap(CognovisRestInvoiceService.getInvoice({
                projectId: project.project.id,
                costTypeIds: [IntranetCostType.QUOTE]
            }),{timeout: 6000}).as('posbilld')   
        })
    
        cy.get('@posbilld').then(_pos => {
            const pos = _pos as unknown as IInvoice[]
            pos.forEach(po => {
                expect(po.cost_status.id).eq(IntranetCostStatus.PAID)
            })
        })
    })
})

describe('Checking amounts', () => {
    beforeEach(() => {
        // get a random test company
        cy.wrap(CognovisRestCompanyService.getCompanies({
            autocompleteQuery: 'cypress',
            companyTypeIds: [IntranetCompanyType.CUSTOMER]
        }).then(res => {
            const customers = res.filter(el => el.primary_contact !== null && el.vat_type.id == IntranetVatType.CUSTOMER_TAX)
            return customers[Math.floor(Math.random() * (res.length -1))] as ICompany
        })).as('company')

        cy.get('@company').then(_company => {
            const projectLeadId = 648038
            const company = _company as unknown as ICompany
            cy.wrap(WebixPortalTranslationService.postTransProject(
                {
                    requestBody: {
                        company_id: company.company.id,
                        project_type_id: IntranetProjectType.TRANS_PROOF_EDIT,
                        source_language_id: IntranetTranslationLanguage.DEDE,
                        target_language_ids: [IntranetTranslationLanguage.ENGB, IntranetTranslationLanguage.ESES],
                        description: 'This is a new description',
                        project_lead_id: projectLeadId
                    }
                }
            ), {timeout: 15000}).as('project')
        })
    });

    afterEach(() => {
        cy.get('@project').then(_project => {
            const project = _project as unknown as ITransProject
    
            cy.wrap(WebixPortalTranslationService.deleteTransProject({
                projectId: project.project.id
            }))
            .then(() => {
                cy.wrap(WebixPortalTranslationService.getTransProjects({
                    projectNr: project.project_nr
                })).should('be.empty')    
            })
        })  
    });

    it('Should create a quote with one invoice item', function () {
        const project = this.project as ITransProject
        const company = this.company as ICompany

        cy.log('Creating first quote')
        cy.wrap(CognovisRestInvoiceService.postInvoice({
            requestBody: {
                project_id: project.project.id,
                company_id: project.company.id,
                company_contact_id: project.company_contact.id,
                cost_type_id: IntranetCostType.QUOTE,
                payment_method_id: IntranetInvoicePaymentMethod.FOREIGN_TRANSFER
            }
        })).as('quote')

        cy.get('@quote').then(_quote => {
            const quote = _quote as unknown as IInvoice
            expect(quote.amount).eq(0)
            expect(quote.vat_amount).eq(0)
            expect(quote.vat_type.id).eq(company.vat_type.id)
            cy.wrap(CognovisRestInvoiceService.postInvoiceItem({
                invoiceId: quote.invoice.id,
                requestBody: {
                    item_name: 'New Line item',
                    item_units: 50.8,
                    price_per_unit: 12.27, 
                    material_nr: 'default'
                }
            })).as('quoteItem')
        })


        cy.get('@quoteItem').then(_quoteItem => {
            const quoteItem = _quoteItem as unknown as IInvoiceItem
            expect(quoteItem.item_units).eq(50.8)
            expect(quoteItem.price_per_unit).eq(12.27)
            cy.wrap(CognovisRestInvoiceService.getInvoice({
                invoiceIds: [quoteItem.invoice.id]
            })).its(0).as('quoteWithItem')
        })

        cy.get('@quoteWithItem').then(_quote => {
            const quote = _quote as unknown as IInvoice
            expect(quote.amount).eq(precisionRound(50.8 * 12.27,2))
            expect(quote.vat_amount).eq(precisionRound(quote.amount * 0.19,2))

            cy.wrap(WebixPortalTranslationService.getTransProjects({
                projectId: quote.project.id
            })).its(0).as('quotedProject')

            cy.fixture('cypress.env.json')
            .then((testEnv) => {
                // First we authorize to our app
                OpenAPI.TOKEN = testEnv.enviromentTestVariables.DEV.admin.api_key;
                cy.log('Checking Permissions')
                cy.wrap(CognovisRestService.getObjectPermission({
                    objectId: quote.cost_center.id,
                    permissionUserId: quote.company_contact.id
                })).then(_privilege => {
                    const privilege = _privilege as unknown as IPrivilege;
                    expect(privilege.read).true;
                });
            });

            cy.get('@quotedProject').then(_project => {
                const quotedProject = _project as unknown as ITransProject
                cy.wrap(CognovisRestService.getObjectPermission({
                    objectId: quote.cost_center.id,
                    permissionUserId: quotedProject.company_contact.id
                })).then(_privilege => {
                    const privilege = _privilege as unknown as IPrivilege;
                    expect(privilege.read).true;
                });
            });
        })

        cy.get('@quotedProject').then(_project => {
            const quotedProject = _project as unknown as ITransProject
            expect(quotedProject.price).eq(precisionRound(50.8 * 12.27,2));
            expect(quotedProject.invoice_amount).eq(0);
        })
    })
})


describe("Handle Invoice mail sending",() => {
    beforeEach(() => {
        // get a random test company
        cy.wrap(CognovisRestInvoiceService.getInvoice({
            costTypeIds: [IntranetCostType.QUOTE],
            costStatusIds: [IntranetCostStatus.CREATED]
        }).then(res => {
            const invoices = res.filter(el => el.company_contact !== null)
            return invoices[Math.floor(Math.random() * (res.length -1))] as IInvoice
        })).as('invoice')

    });

    it('Get potential recipients', function () {
        cy.log('Check if the invoice company contact is a potential recipient')
        cy.get('@invoice').then(_invoice => {
            const invoice = _invoice as unknown as IInvoice
            cy.wrap(CognovisRestInvoiceService.getInvoiceMailRecipient({
                invoiceId: invoice.invoice.id
            }).then(res => {
                expect(res.length).gt(0)
                const company_contact = res.filter(el => el.person.id == invoice.company_contact.id)
                expect(company_contact.length).eq(1)
            }))
        })
    })

    it('Get potential senders', function () {
        cy.get('@invoice').then(_invoice => {
            const invoice = _invoice as unknown as IInvoice
            cy.wrap(CognovisRestInvoiceService.getInvoiceMailSender({
                invoiceId: invoice.invoice.id
            }).then(res => {
                expect(res.length).gt(0)
            }))
        })
    })

    it('Get invoice email', function () {
        cy.get('@invoice').then(_invoice => {
            const invoice = _invoice as unknown as IInvoice
            cy.wrap(CognovisRestInvoiceService.getInvoiceMail({
                invoiceId: invoice.invoice.id
            }).then(res => {
                expect(res.recipients[0].person.id).eq(invoice.company_contact.id)
            }))
        })
    })
})

describe("Simple Invoice creation",() => {
    beforeEach(() => {
        // get a random test company
        cy.wrap(CognovisRestCompanyService.getCompanies({
            autocompleteQuery: 'cypress',
            companyTypeIds: [IntranetCompanyType.CUSTOMER]
        }).then(res => {
            const customers = res.filter(el => el.primary_contact !== null)
            return customers[Math.floor(Math.random() * (res.length -1))] as ICompany
        })).as('company')

    })

    it.only('Create an invoice for a contact', function() {
        cy.get('@company').then(_company => {
            const company = _company as unknown as ICompany
            cy.wrap(CognovisRestInvoiceService.postInvoice({
                requestBody: {
                    company_contact_id: company.primary_contact.id,
                    cost_type_id: IntranetCostType.CUSTOMER_INVOICE
                }
            })).as('invoice')
            cy.get('@invoice').then(_invoice => {
                const invoice = _invoice as unknown as IInvoice
                expect(invoice.company.id).eq(company.company.id)
            })    
        })
    })

    it('Create an invoice in a project', function() {
        cy.get('@company').then(_company => {
            const projectLeadId = 648038
            const company = _company as unknown as ICompany
            cy.wrap(WebixPortalTranslationService.postTransProject(
                {
                    requestBody: {
                        company_id: company.company.id,
                        project_type_id: IntranetProjectType.TRANS_PROOF_EDIT,
                        source_language_id: IntranetTranslationLanguage.DEDE,
                        target_language_ids: [IntranetTranslationLanguage.ENGB, IntranetTranslationLanguage.ESES],
                        description: 'This is a new description',
                        project_lead_id: projectLeadId
                    }
                }
            ), {timeout: 15000}).as('project')
        })
    })

})
