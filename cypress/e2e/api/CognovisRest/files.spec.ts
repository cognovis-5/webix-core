/* eslint-disable security/detect-non-literal-fs-filename */
/// <reference types="cypress-downloadfile"/>

import { OpenAPI, CognovisRestFileService, ICrFile, CognovisRestCompanyService, IntranetCompanyType, ICompany} from "../../../../sources/openapi";
import { FileHelper } from "../../../../sources/modules/file-helpers/file-helpers"
import { getRandomName } from "../../../support/helpers"

before(() => {
    cy.fixture('cypress.env.json')
    .then((testEnv) => {
        // First we authorize to our app
        OpenAPI.TOKEN = testEnv.enviromentTestVariables.DEV.pm.api_key;
    });
});

describe("Testing basic functions on files", () => {

    beforeEach(() => {
        // get a random test company
        cy.wrap(CognovisRestCompanyService.getCompanies({
            autocompleteQuery: 'test_',
            companyTypeIds: [IntranetCompanyType.CUSTOMER]
        }).then(res => {
            return res[Math.floor(Math.random() * (res.length -1))]
        })).as('test_company')
    })



    it("should upload a file and download it again", function () {
        const fileName = "Dummy.pdf"
        const newFileName = getRandomName(12)

        cy.get('@test_company').then(_company => {
            console.log('Initiaiting token - ' + JSON.stringify(OpenAPI))
            const company = _company as unknown as ICompany
            cy.uploadCrFile(fileName, company.company.id, "This is my description", OpenAPI.TOKEN as string).then(_file => {
                const file = _file as unknown as ICrFile
                expect(file.name).eq(fileName)
            }).as('file')
        });
        console.log(`Token before switching user: ${OpenAPI.TOKEN }`);
        cy.setUser(276113)
        .then(() => {
            cy.get('@file').then(_file => {
            const file = _file as unknown as ICrFile
            cy.wrap(CognovisRestFileService.getCrFile({
                fileId: file.file_id
            })).its(0).its("name").should("eq",fileName)
            .then(() => {
                // Download the file again
                const downloadUrl = FileHelper.downloadCrFileUrl(file.file_id,OpenAPI.TOKEN as string)
                cy.downloadFile(downloadUrl,'cypress/fixtures/Download', file.name)
            
                // Verfiy the two files are equal or have at least the same file size
                cy.readFile('cypress/fixtures/'+fileName,'binary').then(_dummy => {
                    cy.readFile('cypress/fixtures/Download/'+file.name,'binary').should('eq',_dummy)
                })
            })
            .then(() => {
                cy.log("Renaming and providing new documentation") 
                cy.wrap(CognovisRestFileService.putCrFile({
                    fileId: file.file_id,
                    description: "This is a new description",
                    fileItemName: newFileName
                }))
                .then(res => {
                    const upFile = res as unknown as ICrFile
                    expect(upFile.description).eq("This is a new description")
                    expect(upFile.name).eq(newFileName)
                })
                .then(() => {
                    cy.log("Trying to update the documentation as a different user")

                    cy.fixture('cypress.env.json').then((testEnv) => {
                        const currentToken = OpenAPI.TOKEN
                        OpenAPI.TOKEN = testEnv.enviromentTestVariables.DEV.pm.api_key;
                        cy.wrap(CognovisRestFileService.putCrFile({
                            fileId: file.file_id,
                            description: "This is an old description",
                            fileItemName: newFileName
                        }).then(res => {
                            expect(res.status).eq(403)
                        }))
                        OpenAPI.TOKEN = currentToken
                    })
                })        
            });
        });
        })
        .then(() => {
            // cy.wrap(CognovisRestFileService.getCrFile({
            //     parentId: company.company.id,
            //     fileItemName: newFileName
            // }))
            // .then(res => {
            //     const newFile = res[0] as ICrFile
            //     cy.log("Unsetting live revision to make sure the file ain't avialable anymore" + newFile.file_id)
            //     cy.wrap(CognovisRestFileService.deleteCrFile({
            //         fileId: newFile.file_id,
            //     }).then(res => {
            //         expect(res).eq(null)
            //     }))
            // })
        })
    })
})