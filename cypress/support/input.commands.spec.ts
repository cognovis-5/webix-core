/* eslint-disable security/detect-object-injection */
/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable @typescript-eslint/no-namespace */



const typeValue = (componentId:string, value:string, overwrite:boolean) => {
    if(!overwrite) {
        cy.get(`[view_id=${componentId}]`).should("be.visible").find("input").invoke("val")
        .then(currentInputValue => {
            if(!currentInputValue) {
                cy.get(`[view_id=${componentId}]`).find("input").type(value);
            }
        });
    } else {
        cy.get(`[view_id=${componentId}]`).should("be.visible").find("input").type(value);
    }
}

const typeRandomName = (componentId:string, prefix:string, length:number, overwrite:boolean) => {
    function getRandomName(length) {
        let result           = '';
        const characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;
        for ( let i = 0; i < length; i++ ) {
           result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }
    const randomName = getRandomName(length);
    const randomValue = `${prefix}${randomName}`;
    if(!overwrite) {
        cy.get(`[view_id=${componentId}]`).should("be.visible").find("input").invoke("val")
        .then(currentInputValue => {
            if(!currentInputValue) {
                cy.get(`[view_id=${componentId}]`).find("input").type(randomValue);
            }
        });
    } else {
        cy.get(`[view_id=${componentId}]`).should("be.visible").find("input").type(randomValue);
    }
}

const typeRandomValue = (componentId:string, prefix:string, overwrite:boolean) => {
    const randomNumber = Math.floor((9999 - 1) * Math.random());
    const randomValue = `${prefix}_${randomNumber}`;
    if(!overwrite) {
        cy.get(`[view_id=${componentId}]`).should("be.visible").find("input").invoke("val")
        .then(currentInputValue => {
            if(!currentInputValue) {
                cy.get(`[view_id=${componentId}]`).find("input").type(randomValue);
            }
        });
    } else {
        cy.get(`[view_id=${componentId}]`).should("be.visible").find("input").type(randomValue);
    }
}


Cypress.Commands.add("typeValue", typeValue);
Cypress.Commands.add("typeRandomValue", typeRandomValue);
Cypress.Commands.add("typeRandomName", typeRandomName);
