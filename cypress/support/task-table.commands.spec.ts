/* eslint-disable security/detect-object-injection */
/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable @typescript-eslint/no-namespace */

// const newTaskDefaults = {
//     taskUnits:100, 
//     targetLanguages:"all",
//     taskUomId:324 // 324 is S-Word
// }

const addSingleTask = (taskName:string, taskTypeId?:number, taskUnits?:number, targetLanguages?:number | string, taskUomId?:number) => {
    // Clicking on "Add Single Task" button so we can start inputing data
    cy.get('[view_id="addSingleTask"').click();
    // Getting whole tasks-table
    cy.get('[view_id="tasks-table"]').then((table) => {
        // Typing task_name, which exists in column=1
        cy.get('[cog-id="task_name"]').type(taskName);
        // Target languages
        cy.wrap(table).get('[column="2"]').eq(2).then((datColumn) => {
            cy.wrap(datColumn).find('[cog_column_name="target_language"]').eq(0).click().wait(100);
            cy.get(`[webix_l_id="${targetLanguages}"]`).click();
        });
        // Task type
        cy.wrap(table).get('[column="3"]').eq(2).then((datColumn) => {
            cy.wrap(datColumn).find('[cog_column_name="task_type"]').eq(0).click().wait(100);
            cy.get(`[webix_l_id="${taskTypeId}"]`).click();
        });
        // Units
        cy.wrap(table).get('[column="4"]').eq(2).then((datColumn) => {
            cy.wrap(datColumn).find('[cog_column_name="task_units"]').eq(0).click().wait(100);
            cy.get('[cog-id="task_units"]').type(taskUnits.toString());
        });
        // Unit of measure
        cy.wrap(table).get('[column="5"]').eq(2).then((datColumn) => {
            cy.wrap(datColumn).find('[cog_column_name="task_uom"]').eq(0).click().wait(100);
            cy.get(`[webix_l_id="${taskUomId}"]`).click();
        });
        //Deadline
        cy.wrap(table).find('[cog_column_name="deadline"]').eq(0).find('[role="gridcell"]').eq(0).click().wait(100)
        .then(() => {
            cy.get('[aria-label="Calendar"]').eq(0).should("be.visible").find('[day="6"]').eq(6).click();
        });
        // Finaly click save icon
        cy.get('#edit-check').click();
    });
}



Cypress.Commands.add("addSingleTask", addSingleTask);



