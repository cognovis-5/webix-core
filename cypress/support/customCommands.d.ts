import { CognovisRestCompanyService, CognovisRestSystemService, IUserToken, IntranetProjectStatus, ITransProject, OpenAPI } from "../../sources/openapi";
declare namespace Cypress {
    interface Chainable<Subject> {
        getCompany(project : ITransProject): Chainable<any>
        cogRest(serviceCall: Function, userId: number, openapi:typeof OpenAPI): Chainable<any>
        setUser(userId : number): Chainable<any>
        removeTaskFromPackage(taskId: any, freelancePackageId: any): Chainable<any>
        createTestTransProject(projectStatusIdundefined : IntranetProjectStatus): Chainable<any>
        createTestTransProjectWithAcceptedAssignments(): Chainable<any>
        createTestTransProjectWithAssignments(): Chainable<any>
        loginToApp(username: string, password: string): Chainable<any>
        clickVisibleButton(buttonId:string, timeout?:number): Chainable<any>
        selectComboValueThatContains(undefined: any): Chainable<any>
        selectRandomValueFromCombo(undefined: any): Chainable<any>
        selectComboValueWithId(undefined: any): Chainable<any>
        selectComboValueWithIndex(undefined: any): Chainable<any>
        deselectAllValuesInCombo(undefined: any): Chainable<any>
        selectAllValuesInCombo(undefined: any): Chainable<any>
        selectDtRowByValue(tableName:string, columnName:string, value:string): Chainable<any>
        selectDtRowByEntityId(tableName:string, value:string): Chainable<any>
        selectDtRowByIndex(tableName:string, index:number): Chainable<any>
        scrollDtToBottom(tableName:string, deltaY:number): Chainable<any>
        uploadCrFile(fileName:string, contextId:number, description?:string, token?:string): Chainable<any>
        downloadFile(url: any, dir: any, fileName: any, userAgent: any): Chainable<any>
        typeValue(undefined: any): Chainable<any>
        typeRandomValue(undefined: any): Chainable<any>
        typeRandomName(undefined: any): Chainable<any>
        dragTo(prevSubject: any, subject: any, targetEl: any): Chainable<any>
        verifySubmenuMenuOptions(undefined: any): Chainable<any>
        selectRandomValuesFromMultiselect(undefined: any): Chainable<any>
        navigateTo(undefined: any): Chainable<any>
        navigateFromSidebar(undefined: any): Chainable<any>
        navigateToProjectByProjectNr(undefined: any): Chainable<any>
        navigateToProjectByProjectId(undefined: any): Chainable<any>
        visitProjectByProjectId(undefined: any): Chainable<any>
        visitAssignmentByAssignmentId(undefined: any): Chainable<any>
        addSingleTask(undefined: any): Chainable<any>
  }
}