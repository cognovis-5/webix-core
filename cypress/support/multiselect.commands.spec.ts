/* eslint-disable security/detect-object-injection */
/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable @typescript-eslint/no-namespace */

import { getRandomNumberBetween } from "./helpers";

const selectRandomValuesFromMultiselect = (componentId:string, numberOfPickedValues?:number) => {
    if(!numberOfPickedValues) {
        numberOfPickedValues = 1;
    }
    cy.get(`[view_id=${componentId}]`).should("be.visible").find(`.webix_el_box`).should("be.visible").click()
    cy.get(`[role="listbox"]:visible`).first()
    .find(`[role="option"]`).its("length")
    .then(totalOptionsInCombo => {
        // Now if we know total number of options in combo, we can generate random index
        for(let i = 0; i<numberOfPickedValues; i++) {
            const randomIndex = getRandomNumberBetween(0, (totalOptionsInCombo-1));
            cy.get(`[role="listbox"]:visible`).first().find(`[role="option"]`).eq(randomIndex).click();
        }
        cy.get(`body`).click({force:true})
    });
}



Cypress.Commands.add("selectRandomValuesFromMultiselect", selectRandomValuesFromMultiselect);