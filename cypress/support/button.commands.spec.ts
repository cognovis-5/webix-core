/* eslint-disable security/detect-object-injection */
/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable @typescript-eslint/no-namespace */


const clickVisibleButton = (buttonId:string, timeout?:number) => {
    if(!timeout) {
        timeout = 5000;
    }
    cy.get(`[view_id="${buttonId}"]`, {timeout:timeout})
    .then(button => {
        cy.wrap(button).should("be.visible", {timeout:timeout}).click({timeout:timeout});
    });
}  


Cypress.Commands.add("clickVisibleButton", clickVisibleButton);