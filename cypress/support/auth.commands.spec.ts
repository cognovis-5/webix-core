/* eslint-disable security/detect-object-injection */
/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable @typescript-eslint/no-namespace */

function loginToApp(username: string, password: string) {
    cy.visit("/");
    cy.get("[view_id='lUsernameInput']").type(username);
    cy.get("[view_id='lPasswordInput']").type(password);
    cy.get("[view_id='lLoginButton']").find("button").eq(0).click();
}  


Cypress.Commands.add("loginToApp", loginToApp);



