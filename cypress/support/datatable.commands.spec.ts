/* eslint-disable security/detect-object-injection */
/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable @typescript-eslint/no-namespace */

const selectDtRowByIndex = (tableName:string, index:number) => {
    cy.get(`[view_id=${tableName}]`)
    .then(table => {
        cy.wrap(table).find('[aria-rowindex="'+index+'"]').eq(0).click();
    });
}

const selectDtRowByValue = (tableName:string, columnName:string, value:string) => {
    cy.get(`[view_id=${tableName}]`)
    .then(table => {
        cy.wrap(table).find(`[cog_column_name='${columnName}']`).contains(value).click();
    });
}

const selectDtRowByEntityId = (tableName:string, value:string) => {
    cy.get(`[view_id=${tableName}]`)
    .then(table => {
        cy.wrap(table).find(`[cog_entity_id='${value}']`).eq(0).click();
    });
}


const scrollDtToBottom = (tableName:string, deltaY:number) => {
    cy.get(`[view_id=${tableName}]`)
    .then(table => {
        //const webix = win["webix"];
        //win["webix"].$$(tableName).scrollTo(0,1000);
        cy.wrap(table).find('[data-cell-name="cog-cell"]').eq(0).trigger("wheel", {deltaY:deltaY});
    });
}


Cypress.Commands.add("selectDtRowByValue", selectDtRowByValue);
Cypress.Commands.add("selectDtRowByEntityId", selectDtRowByEntityId);
Cypress.Commands.add("selectDtRowByIndex", selectDtRowByIndex);
Cypress.Commands.add("scrollDtToBottom", scrollDtToBottom);



