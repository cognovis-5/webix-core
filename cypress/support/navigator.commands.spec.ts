/* eslint-disable security/detect-object-injection */
/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable @typescript-eslint/no-namespace */


const navigateTo = (url:string, newUrl?:string) => {
    cy.get(`[webix_tm_id='${url}']`).should("be.visible")
    cy.get(`[webix_tm_id='${url}']`).click()
    if(newUrl) {
        cy.url().should("contain", newUrl)
    } 
}

const navigateFromSidebar = (url:string, makeSureComponent?:string) => {
    cy.get(`[webix_tm_id='${url}']`).should("be.visible")
    cy.get(`[view_id='${makeSureComponent}']`).should("be.visible")
    .then(() => {
        cy.get(`[webix_tm_id='${url}']`).click()
    });
}


const navigateToProjectByProjectNr = (projectsTableId:string, projectNr:string) => {
    // We navigate to projects list
    cy.navigateTo("project-overview.projects", "projects");
    // We select row by projectNr / projectName
    cy.scrollDtToBottom(`${projectsTableId}`, 2000);
    cy.selectDtRowByValue(`${projectsTableId}`, "project", `${projectNr}`); 
}


const navigateToProjectByProjectId = (projectsTableId:string, projectId:string | number) => {
    // We navigate to projects list
    cy.navigateTo("project-overview.projects", "projects");
    cy.selectDtRowByEntityId(`${projectsTableId}`, projectId as string); 
}


const visitProjectByProjectId = (projectId:number) => {
    cy.visit(`/main/assignments.overview?project_id=${projectId}`);
}

const visitAssignmentByAssignmentId = (assignmentId:string) => {
    cy.visit(`/main/assignments.assignment-details?assignment_id=${assignmentId}`)
}


Cypress.Commands.add("navigateTo", navigateTo);
Cypress.Commands.add("navigateFromSidebar", navigateFromSidebar);
Cypress.Commands.add("navigateToProjectByProjectNr", navigateToProjectByProjectNr);
Cypress.Commands.add("navigateToProjectByProjectId", navigateToProjectByProjectId);
Cypress.Commands.add("visitProjectByProjectId", visitProjectByProjectId);
Cypress.Commands.add("visitAssignmentByAssignmentId", visitAssignmentByAssignmentId);



