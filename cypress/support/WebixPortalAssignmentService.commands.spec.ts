import { IFreelancePackage, OpenAPI, WebixPortalAssignmentService, ITransProject, IntranetProjectStatus, IntranetFreelanceAssignmentStatus, IntranetTransTaskType, ICompany, CognovisRestCompanyService,IntranetUom, IntranetCompanyType,WebixPortalTranslationService,IntranetProjectType,IntranetTranslationLanguage, ITransTask, IInvoice, IFreelancer } from "../../sources/openapi";
import { randomizedName } from "./helpers"


Cypress.Commands.add("removeTaskFromPackage", (taskId, freelancePackageId) => {
    cy.fixture('cypress.env.json')
    .then((testEnv) => {
        // First we authorize to our app
        OpenAPI.TOKEN = testEnv.enviromentTestVariables.DEV.admin.api_key;
    });

    cy.wrap(WebixPortalAssignmentService.getPackages({
        freelancePackageIds: [freelancePackageId]
    }), { timeout: 10000} ).should('have.length.gt',0)
    .its(0).as('package');

    cy.get('@package').then(_package => {
        const trans_package = _package as unknown as IFreelancePackage;
        cy.log('Removing task ' + taskId + ' from ' + trans_package.freelance_package.name);
        const tasks = trans_package.tasks
        const taskIds = [...new Set(tasks.map(item => item.task.id))]
        taskIds.forEach((element,index)=>{
            if(element==taskId) taskIds.splice(index,1);
         });
        cy.wrap(WebixPortalAssignmentService.putPackages({
            freelancePackageId: trans_package.freelance_package.id,
            requestBody: {
                trans_task_ids: taskIds
            }
        })).as('return_package')
    })

    cy.get('@return_package').then(_package => {
        const trans_package = _package as unknown as IFreelancePackage;
        const tasks = trans_package.tasks
        const taskIds = tasks.map(item => item.task.id)
        expect(taskIds.indexOf(taskId)).lessThan(0)
        
    })
});


Cypress.Commands.add("createTestTransProject", (projectStatusId:IntranetProjectStatus) => {
    const DEFAULT_COMMAND_TIMEOUT = Cypress.config().defaultCommandTimeout;
    Cypress.config('defaultCommandTimeout', 15000);

    cy.fixture('cypress.env.json')
    .then((testEnv) => {
        // First we authorize to our app
        OpenAPI.TOKEN = testEnv.enviromentTestVariables.DEV.pm.api_key;
        const projectLeadId = testEnv.enviromentTestVariables.DEV.pm.user_id;
        const project = CognovisRestCompanyService.getCompanies({
            companyTypeIds: [IntranetCompanyType.CUSTOMER]
        })
        .then(res => {
            const customers = res.filter(el => el.primary_contact !== null)
            const company = customers[Math.floor(Math.random() * (customers.length -1))] as ICompany
            const companyProject = WebixPortalTranslationService.postTransProject(
                {
                    requestBody: {
                        company_id: company.company.id,
                        project_type_id: IntranetProjectType.TRANS_PROOF,
                        source_language_id: IntranetTranslationLanguage.DEDE,
                        project_status_id: projectStatusId,
                        target_language_ids: [IntranetTranslationLanguage.ENGB, IntranetTranslationLanguage.ESES]
                    }
                }
            )
            .then(_project => {
                const project = _project as unknown as ITransProject
                expect(project.target_language.length).eq(2)
                expect(project.project_lead.user.id).eq(projectLeadId)
                const randomNumber = Math.floor((9999 - 1) * Math.random());

                // Create tasks (and its packages)
                const project2 = WebixPortalTranslationService.postTransTasks({
                    projectId:project.project.id, 
                    requestBody: {
                        task_name: randomizedName('TestTask+'), 
                        task_uom_id: IntranetUom.SWORD,
                        task_units: randomNumber, 
                        task_type_id:87
                    }
                }).then(_tasks => {
                    const tasks = _tasks as unknown as ITransTask[]
                    const taskIds = [...new Set(tasks.map(item => item.task.id))]
                    
                    const transProject =  WebixPortalTranslationService.postTransQuote({
                        requestBody: {
                            trans_tasks: taskIds,
                            quote_per_language_p: true
                        }
                    }).then(_quotes => {
                        const quotes = _quotes as unknown as IInvoice[]
                        expect(quotes.length).eq(2)
                        const quoteProject = WebixPortalTranslationService.getTransProjects({
                            projectId: quotes[0].project.id
                        }).then(_quoteProjects => {
                            const quoteProjects = _quoteProjects as unknown as ITransProject[]
                            expect(quoteProjects.length).eq(1)
                            return quoteProjects[0]
                        }) 
                        return quoteProject
                    })
                    return transProject
                })
                return project2
            })
            return companyProject
        })
        return project
        Cypress.config('defaultCommandTimeout', DEFAULT_COMMAND_TIMEOUT);
    })
});


Cypress.Commands.add("createTestTransProjectWithAcceptedAssignments", () => {
    cy.createTestTransProject(IntranetProjectStatus.OPEN)
    .then(_project => {
        const project = _project as unknown as ITransProject;
        expect(project.target_language.length).eq(2)
        cy.wrap(WebixPortalAssignmentService.getPackages({
            projectId: project.project.id
        }))
        .then(_freelancePackages => {
            console.log("got packages");
            const freelancePackages = _freelancePackages as unknown as IFreelancePackage[]
            freelancePackages.forEach((_package, index) => {
                const packageId = _package.freelance_package.id
                cy.wrap(WebixPortalAssignmentService.getFreelancersForPackage({
                    freelancePackageId: packageId,
                    excludeFeeP: true
                }))
                .then(_freelancers => {
                    console.log("got freelancers");
                    const __freelancers = _freelancers as unknown as IFreelancer[]
                    const freelancers = __freelancers.filter(el => el.freelancer_status_description !== 'Employee')
                    cy.wrap(WebixPortalAssignmentService.postTranslationAssignments({
                        freelancePackageId: packageId,
                        freelancerIds: [freelancers[Math.floor(Math.random() * (freelancers["length"] -1))].freelancer.id],
                        requestBody: {}
                    }))
                    .then(assignment => {
                        console.log("will try to change status of : "+assignment[0].assignment.id);
                        cy.wrap(WebixPortalAssignmentService.putTranslationAssignments({
                            assignmentId: assignment[0].assignment.id,
                            requestBody: {
                                assignment_status_id: IntranetFreelanceAssignmentStatus.ACCEPTED
                            }
                        }))
                        .then(() => {
                            if((index + 1) === freelancePackages.length) {
                                const projectId = _package.project.id
                                cy.wrap(WebixPortalTranslationService.getTransProjects({
                                    projectId: projectId as unknown as number
                                }))
                                .then((projects) => {
                                    return projects[0];
                                });
                            }   
                        });
                    })
                })
            })
        })
    });
});

Cypress.Commands.add("createTestTransProjectWithAssignments", () => {
    cy.createTestTransProject(IntranetProjectStatus.POTENTIAL)
    .then(_project => {
        const project = _project as unknown as ITransProject;
        expect(project.target_language.length).eq(2)
        cy.wrap(WebixPortalAssignmentService.getPackages({
            projectId: project.project.id
        }))
        .then(_freelancePackages => {
            console.log("got packages");
            const freelancePackages = _freelancePackages as unknown as IFreelancePackage[]
            freelancePackages.forEach((_package, index) => {
                const packageId = _package.freelance_package.id
                cy.wrap(WebixPortalAssignmentService.getFreelancersForPackage({
                    freelancePackageId: packageId,
                    excludeFeeP: true
                }))
                .then(freelancers => {
                    console.log("got freelancers");
                    cy.wrap(WebixPortalAssignmentService.postTranslationAssignments({
                        freelancePackageId: packageId,
                        freelancerIds: [freelancers[Math.floor(Math.random() * (freelancers["length"] -1))].freelancer.id],
                        requestBody: {
                            rate: 0.03
                        }
                    }))
                    .then(() => {
                        if((index + 1) === freelancePackages.length) {
                            const projectId = _package.project.id
                            cy.wrap(WebixPortalTranslationService.getTransProjects({
                                projectId: projectId as unknown as number
                            }))
                            .then((projects) => {
                                return projects[0];
                            });
                        }   
                    });
                })
            })
        })
    });
});