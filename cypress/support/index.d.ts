/* eslint-disable @typescript-eslint/naming-convention */



// in cypress/support/index.d.ts
// load type definitions that come with Cypress module
/// <reference types="cypress" />

declare namespace Cypress {
    interface Chainable {
        loginToApp(username:string, password:string): Cypress.Chainable<void>;
        dragTo(targetEl): Cypress.Chainable<void>;
        navigateTo(url:string, newUrl?:string): Cypress.Chainable<void>;
        navigateFromSidebar(url:string, makeSureComponentId?:string): Cypress.Chainable<void>;
        selectDtRowByValue(tableName:string, columnName:string, value:string):Cypress.Chainable<void>;
        selectDtRowByIndex(tableName:string, index:number):Cypress.Chainable<void>;
        selectDtRowByEntityId(tableName:string, value:string):Cypress.Chainable<void>;
        scrollDtToBottom(tableName:string, deltaY:number):Cypress.Chainable<void>;
        visitProjectByProjectId(projectId:number):Cypress.Chainable<void>;
        navigateToProjectByProjectNr(projectsTableId:string, projectNr:string):Cypress.Chainable<void>;
        navigateToProjectByProjectId(projectsTableId:string, projectId:string | number):Cypress.Chainable<void>;
        visitAssignmentByAssignmentId(assignmentId:string):Cypress.Chainable<void>;
        selectComboValue(componentId:string, value:string | number):Cypress.Chainable<void>;
        selectComboValueWithId(componentId:string, valueId:number):Cypress.Chainable<void>;
        selectComboValueWithIndex(componentId:string, index:number):Cypress.Chainable<void>;
        selectRandomValueFromCombo(componentId:string):Cypress.Chainable<void>;
        selectRandomValuesFromMultiselect(componentId:string, numberOfPickedValues?:number):Cypress.Chainable<void>;
        selectComboValueThatContains(componentId:string, value:number | string):Cypress.Chainable<void>;
        deselectAllValuesInCombo(componentId:string):Cypress.Chainable<void>;
        selectAllValuesInCombo(componentId:string):Cypress.Chainable<void>;
        clickVisibleButton(buttonId:string, timeout?:number):Cypress.Chainable<void>;
        addSingleTask(taskName:string, taskTypeId?:number, taskUnits?:number, targetLanguages?:number | string, taskUomId?:number) :Cypress.Chainable<void>;
        verifySubmenuMenuOptions(menuButtonId:string, menuId:string, correctIconIds:string[]):Cypress.Chainable<void>;
        removeTaskFromPackage(taskId:number, freelancePackageId:number):Cypress.Chainable<void>;
        typeValue(componentId:string, value:string, overwrite:boolean):Cypress.Chainable<void>;
        typeRandomValue(componentId:string, prefix:string, overwrite:boolean):Cypress.Chainable<void>;
        typeRandomName(componentId:string, prefix:string, length:number, overwrite:boolean):Cypress.Chainable<void>;
        uploadCrFile(filename:string, contextId:number, description?:string, token?:string):Cypress.Chainable<void>;
        downloadFile(url:string, dir:string, fileName:string, userAgent:string):Cypress.Chainable<void>;
        createTestTransProject(projectStatusId:number):Cypress.Chainable<void>;
        createTestTransProject2():Cypress.Chainable<void>;
        createTestTransProjectWithAssignments():Cypress.Chainable<void>;
        createTestTransProjectWithAssignments2():Cypress.Chainable<void>;
        createTestTransProjectWithAcceptedAssignments():Cypress.Chainable<void>;
        getCompany(project):Cypress.Chainable<void>;
        cogRest(promise: Function, userId:number, token: typeof import("../../sources/openapi").OpenAPI):Cypress.Chainable<void>;
        setUser(userId:number): Chainable<void>;

    }
}

