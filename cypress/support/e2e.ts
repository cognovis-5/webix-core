
// Cypress plugins
import 'cypress-pipe';
import 'cypress-wait-until';
import 'cypress-file-upload';

// Commands imports
import "./auth.commands.spec";
import "./navigator.commands.spec";
import "./datatable.commands.spec";
import "./input.commands.spec";
import "./combobox.commands.spec";
import "./multiselect.commands.spec";
import "./button.commands.spec";
import "./task-table.commands.spec";
import "./menu.commands.spec";
import "./file.commands.spec";
import "./file.commands.spec";
import "./WebixPortalAssignmentService.commands.spec";
import "./CognovisCompanyService.commands.spec";

// Helpers
import "./helpers";

Cypress.on("uncaught:exception", () => {
    return false;
});


