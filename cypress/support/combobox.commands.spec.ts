/* eslint-disable security/detect-object-injection */
/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable @typescript-eslint/no-namespace */

import { getRandomNumberBetween } from "./helpers";

const selectComboValueThatContains = (componentId:string, value:string | number) => {
    cy.get(`[view_id=${componentId}]`).should("be.visible").find(`.webix_el_box`).should("be.visible").click()
    .get(`[role="dialog"]`).should("be.visible")
    .then(component => {
        cy.wrap(component).find(`[role="option"]`).filter(`:visible`)
        .each((option) => {
            cy.wrap(option).invoke("text")
            .then(text => {
                if(text === value) {
                    cy.wrap(option).click();
                }
            }); 
        });
    });
    cy.get(`[view_id=${componentId}]`).click();
    cy.get(`[role="dialog"]`).should("not.be.visible");
}

const selectRandomValueFromCombo = (componentId:string) => {
    cy.get(`[view_id=${componentId}]`).should("be.visible").find(`.webix_el_box`).should("be.visible").click()
    cy.get(`[role="listbox"]:visible`).first()
    .find(`[role="option"]`).its("length")
    .then(totalOptionsInCombo => {
        // Now if we know total number of options in combo, we can generate random index
        const randomIndex = getRandomNumberBetween(0, (totalOptionsInCombo-1));
        cy.get(`[role="listbox"]:visible`).first().find(`[role="option"]`).eq(randomIndex).click();
    });
}

const selectComboValueWithId = (componentId:string, value:number) => {
    cy.get(`[webix_l_id="${value}"]`).should("be.visible").click()
    .then(() => {
        cy.get(`[view_id=${componentId}]`).should("be.visible").find(`.webix_el_box`).should("be.visible")
        .then(() => {
            cy.get(`[view_id=${componentId}]`).should("be.visible").find(`.webix_el_box`).click()
            .then(() => {
                cy.get(`[webix_l_id="${value}"]`).click();      
            });
        });
    });
}

const selectComboValueWithIndex = (componentId:string, index:number) => {
    cy.get(`[view_id=${componentId}]`).should("be.visible").find(`.webix_el_box`).should("be.visible").click()
    .get(`[role="dialog"]`).should("be.visible")
    .then(component => {
        cy.wrap(component).find(`[role="option"]`).filter(`:visible`).eq(index).click(); 
    });
}

const deselectAllValuesInCombo = (componentId:string) => {
    cy.get(`[view_id=${componentId}]`).should("be.visible").find(`.webix_el_box`).should("be.visible").click()
    .get(`[role="dialog"]`).should("be.visible")
    .then(component => {
        cy.wrap(component).find(`[role="option"]`).filter(`:visible`).filter(`[aria-selected="true"]`)
        .each((option) => {
            cy.wrap(option)
            .then(isSelected => {
                if(isSelected) {
                    cy.wrap(option).click();
                }
            });
        });
    });
    cy.get(`[view_id=${componentId}]`).click();
    cy.get(`[role="dialog"]`).should("not.be.visible");
}

const selectAllValuesInCombo = (componentId:string) => {
    cy.get(`[view_id=${componentId}]`).should("be.visible").find(`.webix_el_box`).should("be.visible").click()
    .get(`[role="dialog"]`).should("be.visible")
    .then(component => {
        cy.wrap(component).find(`[role="option"]`).filter(`:visible`).not(`[aria-selected="true"]`)
        .each((option) => {
            cy.wrap(option)
            .then(isSelected => {
                if(isSelected) {
                    cy.wrap(option).click();
                }
            });
        });
    });
    cy.get(`[view_id=${componentId}]`).click();
    cy.get(`[role="dialog"]`).should("not.be.visible");
}



Cypress.Commands.add("selectComboValueThatContains", selectComboValueThatContains);
Cypress.Commands.add("selectRandomValueFromCombo", selectRandomValueFromCombo);
Cypress.Commands.add("selectComboValueWithId", selectComboValueWithId);
Cypress.Commands.add("selectComboValueWithIndex", selectComboValueWithIndex);
Cypress.Commands.add("deselectAllValuesInCombo", deselectAllValuesInCombo);
Cypress.Commands.add("selectAllValuesInCombo", selectAllValuesInCombo);