import { CognovisRestCompanyService, CognovisRestSystemService, IUserToken, ITransProject, OpenAPI } from "../../sources/openapi";

Cypress.Commands.add("getCompany", (project: ITransProject) => {
    cy.fixture('cypress.env.json')
    .then((testEnv) => {
        // First we authorize to our app
        OpenAPI.TOKEN = testEnv.enviromentTestVariables.DEV.pm.api_key;
        return CognovisRestCompanyService.getCompanies({
            companyId: project.company.id
        })
    })
})



Cypress.Commands.add("cogRest", (serviceCall: Function, userId: number, openapi:typeof OpenAPI) => {
    cy.fixture('cypress.env.json')
    .then((testEnv) => {
        // First we authorize to our app
        const currentToken = openapi.TOKEN;
        console.log('Current user ' + currentToken)
        console.log('Admin token ' + testEnv.enviromentTestVariables.DEV.admin.api_key)
        OpenAPI.TOKEN = testEnv.enviromentTestVariables.DEV.admin.api_key;
        const token = CognovisRestSystemService.getUserToken({
            userId: userId
        }).then(_contactUser => {
            const contactUser = _contactUser as unknown as IUserToken
            openapi.TOKEN = contactUser.bearer_token
            console.log('Bearer new ' + contactUser.user_id + ' ' + OpenAPI.TOKEN)
            return contactUser.bearer_token
        })
        // OpenAPI.TOKEN = token as unknown as string
        console.log('Returned token ' + OpenAPI.TOKEN)

        const res = serviceCall
        // OpenAPI.TOKEN = currentToken
        return res
    })
})

Cypress.Commands.add("setUser", (userId: number) => {
    cy.fixture('cypress.env.json')
    .then((testEnv) => {
        OpenAPI.TOKEN = testEnv.enviromentTestVariables.DEV.admin.api_key;
        CognovisRestSystemService.getUserToken({
            userId: userId
        })
        .then(_contactUser => {
            const contactUser = _contactUser as unknown as IUserToken
            OpenAPI.TOKEN = contactUser.bearer_token;
            console.log(`Token after switching user: ${OpenAPI.TOKEN }`);
        });
    });

})