/* eslint-disable security/detect-object-injection */
/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable @typescript-eslint/no-namespace */

const verifySubmenuMenuOptions = (menuButtonId:string, menuId:string, correctIconIds:string[]) => {
    // First let's open submenu
    cy.get(`[cog-submenu-button-id=${menuButtonId}]`).should("be.visible").click({force:true})
    .then(() => {
        cy.get(`[view_id=${menuId}]`).should("be.visible")
        .then(menu => {
            cy.wrap(menu).find(`[role="menuitem"]`)
            .each((option, index) => {
                cy.wrap(option).invoke("attr", "webix_l_id").should("eq",correctIconIds[index]);
            });
        });
    })
}

const verifySubmenuMenuOptions2 = (menuButtonId:string, menuId:string, correctIconIds:string[]) => {
    // First let's open submenu
    cy.get(`[cog-submenu-button-id=${menuButtonId}]`).should("be.visible").click({force:true})
    .then(() => {
        cy.get(`[view_id=${menuId}]`).should("be.visible")
        .then(menu => {
            cy.wrap(menu).find(`[role="menuitem"]`)
            .each((option, index) => {
                cy.wrap(option).invoke("attr", "webix_l_id").should("eq",correctIconIds[index]);
            });
        });
    })
}

Cypress.Commands.add("dragTo", { prevSubject: "element" }, (subject, targetEl) => {
    cy.wrap(subject).trigger("dragstart");
    cy.get(`[view_id=${targetEl}]`).trigger("drop");
  }
);

Cypress.Commands.add("verifySubmenuMenuOptions", verifySubmenuMenuOptions);