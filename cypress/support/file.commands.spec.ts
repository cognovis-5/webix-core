/* eslint-disable security/detect-possible-timing-attacks */

/* eslint-disable security/detect-object-injection */
/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable @typescript-eslint/no-namespace */

import { config } from "../../sources/config";
import { OpenAPI } from "../../sources/openapi";

const uploadCrFile = (fileName:string, contextId:number, description?:string, token?:string) => {
    cy.fixture('cypress.env.json')
    .then((testEnv) => {
        cy.fixture(fileName, "binary")
        .then(file => {
            const restUrl = config.restUrl;
            let bearerToken: string;
            if(token === undefined) {
                bearerToken =  OpenAPI.TOKEN = testEnv.enviromentTestVariables.DEV.admin.api_key;
            } else {
                bearerToken = token
            }
            const urlWithToken = restUrl + "upload_cr_file" +"?api_key="+bearerToken;
            const blob = Cypress.Blob.binaryStringToBlob(file)
            const formData = new FormData()
            formData.append("upload", blob, fileName)
            formData.append("context_id", contextId.toString());
            if(description) {
                formData.append("description", description);
            }
            return fetch(urlWithToken,{
                method:"POST",
                body:formData
            })
            .then(response => response.json())
            .then(data => {
               return cy.wrap(data);
            });
        })
    });
};


Cypress.Commands.add("uploadCrFile", uploadCrFile);


Cypress.Commands.add('downloadFile', function (url, dir, fileName, userAgent) {
    return cy.getCookies().then(function (cookies) {
      return cy.task('downloadFile', {
        url: url,
        directory: dir,
        cookies: cookies,
        fileName: fileName,
        userAgent: userAgent
      });
    });
  });