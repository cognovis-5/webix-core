var path = require("path");
var webpack = require("webpack");
const Dotenv = require("dotenv-webpack");
var devEnvironment = require("./env.json");
var portalUrl = new URL(devEnvironment.PORTAL_URL);

module.exports = function (env) {
    var pack = require("./package.json");
    var MiniCssExtractPlugin = require("mini-css-extract-plugin");
    var CopyWebpackPlugin = require("copy-webpack-plugin");
    const dotenvObj = new Dotenv();

    var production = !!(env && env.production === "true");
    var asmodule = !!(env && env.module === "true");
    var standalone = !!(env && env.standalone === "true");

    // baseURL based on gitignored .env file
    /* const baseUrl = production
        ? dotenvObj.definitions["process.env.PRODUCTION_URL"]
        : dotenvObj.definitions["process.env.DEV_URL"];*/
    
    var config = {
        mode: production ? "production" : "development",
        entry: {
            myapp: "./sources/myapp.ts",
        },
        output: {
            path: path.join(__dirname, "codebase"),
            publicPath: "/codebase/",
            filename: "[name].js",
            chunkFilename: "[name].bundle.js",
        },
        module: {
            exprContextCritical: false,
            rules: [
                {
                    test: /\.ts$/,
                    loader: "ts-loader",
                },
                {
                    test: /\.(jpg|png|svg|gif)$/,
                    type: 'asset/resource',
                },
                {
                    test: /\.(less|css)$/,
                    use: [MiniCssExtractPlugin.loader, "css-loader", "less-loader"],
                },
            ],
        },
        resolve: {
            extensions: [".ts", ".js"],
            modules: ["./sources", "node_modules"],
            mainFields: ["main"],
            alias: {
                "jet-views": path.resolve(__dirname, "sources/views"),
                "jet-locales": path.resolve(__dirname, "sources/locales"),
            },
        },
        plugins: [
            new MiniCssExtractPlugin({
                filename: "[name].css",
            }),
            new CopyWebpackPlugin({
                patterns: [{ from: "assets", to: "assets" }],
            }),
            new webpack.DefinePlugin({
                VERSION: `"${pack.version}"`,
                APPNAME: `"${pack.name}"`,
                PRODUCTION: production,
                BUILD_AS_MODULE: asmodule || standalone,
            }),
        ],
        devServer: {
            static: __dirname,
            port: portalUrl.port
        },
    };

    if (!production) {
        config.devtool = "inline-source-map";
    }

    if (asmodule) {
        if (!standalone) {
            config.externals = config.externals || {};
            config.externals = ["webix-jet"];
        }

        const out = config.output;
        const sub = standalone ? "full" : "module";

        out.library = pack.name.replace(/[^a-z0-9]/gi, "");
        out.libraryTarget = "umd";
        out.path = path.join(__dirname, "dist", sub);
        out.publicPath = "/dist/" + sub + "/";
    }

    switch (config.mode) {
    }

    return config;
};
