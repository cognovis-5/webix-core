FROM nginx:alpine
RUN mkdir /usr/share/nginx/html/fonts /usr/share/nginx/html/codebase /usr/share/nginx/html/assets /usr/share/nginx/html/skins
# COPY /node_modules/@xbs/webix-pro/webix.min.* /usr/share/nginx/html/
COPY /node_modules/@xbs/webix-pro/webix.* /usr/share/nginx/html/
COPY /node_modules/@xbs/webix-pro/fonts/* /usr/share/nginx/html/fonts/
COPY ./codebase/* /usr/share/nginx/html/codebase/
COPY ./assets/images/* /usr/share/nginx/html/assets/images/
COPY ./fonts/* /usr/share/nginx/html/fonts/
COPY ./skins/* /usr/share/nginx/html/skins/
COPY ./deploy/index.html /usr/share/nginx/html
COPY ./deploy/nginx.default.conf /etc/nginx/conf.d/default.conf

# Copy .env file and shell script to container
WORKDIR /usr/share/nginx/html
COPY ./env.sh .
COPY .env .
	
# Make our shell script executable
RUN chmod +x env.sh
	
# Start Nginx server
CMD ["/bin/sh", "-c", "/usr/share/nginx/html/env.sh && nginx -g \"daemon off;\""]
