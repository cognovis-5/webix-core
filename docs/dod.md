# Review

* Demonstrate only from staging environment


# Definition of Done

* Code adheres to coding standards (and sometimes enforced through linting)
* Automated Test case runs succesfully locally
* Code review for every merge to master (and check for coding-standards)
* Code is merged to master successfully
* After merge to master CI Pipeline runs successfully to staging