### How to setup

Configuration happens in **sources/env.ts** - Especially the location of the backend servers.
For your convenience there is a sources/env.ts.default which you can use that assumes a running backend server on http://localhost:8091

    export const environment = {
        DEV_URL: 'http://localhost:8091/',
        PRODUCTION_URL: 'http://localhost:8091/',
    };

Make sure to delete all browser storage for localhost:8080 in case you had the application already installed.

### Extensions for VS Code

The following extensions are going to be helpful

- TSLint and add the following to settings.json for VS Code to autosave fixes
  ````
  "editor.codeActionsOnSave": {
      "source.fixAll.tslint": true
  }, ```
  ````
- Gitlens
- Markdown All in One
- Wallaby.js

### Fire up the backend

- Go to your checkout of the backend server which contains your docker-compose.yml
- Check that the naviserver section is running on the same port as you configured in env.ts

If you have not loaded the database before, you will have to connect to the postgres docker container and manually load the database file (located in database/mydb.dmp)

    docker-compose exec postgres /usr/bin/psql -U openacs docker-entrypoint-initdb.d/mydb.dmp

Once this is done:

- run `docker-compose up -d` to start the backend server

### How to run the frontend

    npm install
    npm start

This will open the Webix Portal automatically at `http://localhost:8080`

For more details, check https://www.gitbook.com/book/webix/webix-jet/details

### Start up cypress for automated testing (and be patient)

    npx cypress open

This will open the browser and let you run cypress test scripts. While developing though you probably prefer to have the tests run in headless mode within your Visual Studio Code Terminal like this

    npx cypress run

### Run lint

Though VS Code is installed to automatically run TS Lint and fix upon save, you might want to occasionally run lint manually (e.g. before a `git push origin`)

    npm run lint

### Build production files

    npm run build

After that you can copy the "codebase" folder to the production server

### Access the Backends OpenAPI Specification and get documentation

First you need to download the OpenAPI specification from the backend (keep the URL from above in mind)

     curl http://localhost:8091/cognovis-rest/openapi -o openapi.yml

Then you can import this into your favorite API software.

### License
