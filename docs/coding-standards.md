# Description of our coding standard

## For naming all the files in the project folder e.g js,ts, css and docs files

1. Use kebab casing e.g example-kebab.ts

## For naming incode js and ts variables

incode variables are variable to be used in the code and not otherwise(backend)

1. Use camel casing e.g const camelCasing:string = "I am a camel case string variable";

## For variables to be sent to backend

1. Use snake casing e.g Interface Obj{user_name:string,user_age:number}

## For class and interface
1. Use PascalCasing for naming classes and interfaces e.g: SoftwareDevelopment(){}

## For component
1. Use kebab-casing for naming components e.g: user-profile.ts
2. use PascalCasing for naming component class or function e.g ToolBarComponent

## Css naming standard

1. Use BEM(Block Element Modifier) for class css naming

## What to use for type definition (type alias or interface)

1. We use interface

## When to create interface to be used in ts

1. When a type is an object of more than 4 key-value pairs
2. When a type is used in more than one scope or file
3. When a file starting becoming hard to read and scroll
