# Description of our development process

## Idea to code

1. Create the idea and tasks in JIRA
2. Build Webix UI based of Designs in UI Designer with Dummy Data => Together with Customer
3. Use Cypress Recorder to Record Test Scripts based on acceptance criteria against Dummy data
4. Turn Recorded test into actual test script during coding.
   
## From JIRA to Commit

1. Create a user story / product backlog item in JIRA
   1. Describe WHO wants WHAT **WHY**
   2. Add acceptance criteria
   
2. Add subtasks into JIRA to describe what needs to be done
   1. Endpoints (if any) should get a task. (along with the parameters to use)
   2. Frontend work necessary
   3. Automated test cases based on the acceptance criteria (see 1.2.)

3. Using the GIT Integration in JIRA go to the user story and click on "create branch".
   1. Find the correct package which contains the source. For Webix development search for webix. This has a high likelyhood of finding it
   2. Choose your base branch. Typically this should be **master**
   3. Click on "Create Branch" to actually create the branch

4. Go to your directory and run

    git fetch origin
    git checkout <yourbranch>

Now you are on a custom branch which allows you to do all the coding for the User Story / Product Backlog Item described in step 1.

**DO NOT CREATE BRANCHES FOR SUBTASKS!**

5. Code
6. Commit on a regular basis. Typically when you have completed a function, written a test case etc. So make regular, smaller commits within this branch.

Once done you can actually push all of your commits to the remote repository

7. ```git push```

Note that this will push your current branch to the remote repository. It also provides you with the URL to create the merge request. If you are sure you want to merge, feel free to create the merge request using the gitlab link.

Assign to somebody else but yourself, who can review the code.

That other person will then see the commits, can do a code review and if no changes are to be done, commit. If changes are necessary though, the reviewer needs to invite the code writer into  a ZOOM session to do a joint code review and make the necessary changes together. Then both of them can commit jointly in one go into master (no need for a merge request if you are pairing aleady).

# Visual Studio Code extensions

## Gitlab Workflow

This workflow allows us to handle the merge requests from Gitlab.

# API Workflow

1. During refinement, latest during sprint planning, we discuss what API is needed for the implementation. 
2. Based on the discussion, immediately write the ad_proc stuff to get the API into an OpenAPI Export
3. Based on that export, we auto generate typescript functions to use for accessing the API in our Webix code
4. Based on the OpenAPI Export we build auto-tests which will at least check that we can call the API with parameters and that something is returned back. (Up until the developer actually writes the code, nothing comes back and while it is not working, a 500 Error Code will appear)
5. Backend developer can let the automated test run until the backend code he is writing is adhereing to the test expectations of "running"
6. Additional assertions are added to reflect correct data (not data type/structure) is returned.

# Enabling views or component responsiveness
1. To ensure views responsiveness across devices, install **responsive viewer plugin** into your chrome browser(responsive viewer)[https://chrome.google.com/webstore/detail/responsive-viewer/inmopeiepgfljkpkidclfgbgbmfcennb?hl=en]
2. After the installation, click the plugin and load the application url into the plugin 
3. From the plugin, discover the views breaking point
4. Use the breaking point as the media query target e.g @media screen and (min-width:400px){}
