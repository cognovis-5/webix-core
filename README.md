Webix App for Translations
===========================

This is an App connecting to the backend of ]project-open[ for translation business.
### How to run

- configure .env with at least SERVER_URL and PORTAL_URL environment variables (alternatively set those in your shell)
- run ```npm install```
- run ```npm start```
- open ```PORTAL_URL```

### Other commands

#### Run lint

```
npm run lint
```

#### Build production files

```
npm run build
```

After that you can copy the "codebase" folder to the production server


### License

MIT